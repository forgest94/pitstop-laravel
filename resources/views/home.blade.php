@extends('layouts.template')
@section('content')
<div class="page__heading page__heading_light">
        <div class="container">
            <h1>Добро пожаловать, {{ Auth::user()->name }}</h1>
        </div>
    </div>
    <div class="tabs tabs_personal js-tabs-links-wrap">
    <div class="container tabs__inner">
        <a href="javascript:void(0);" data-type="personal" data-id="1" class="tabs__link tabs__link_active js-tab-link">Личные данные</a>
        <a href="javascript:void(0);" data-type="personal" data-id="2" class="tabs__link js-tab-link">Персоны</a>
        <a href="javascript:void(0);" data-type="personal" data-id="3" class="tabs__link js-tab-link">Автомобили</a>
        <a href="javascript:void(0);" data-type="personal" data-id="4" class="tabs__link js-tab-link">Услуги</a>

        <a href="{{ route('logout') }}" class="tabs__link tabs__link_right" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
        </form>
    </div>
</div>


    <div class="container page__columns-wrap">

        <div class="tabs__body js-tabs-wrap">
            <div class="container tabs__tab-inner js-tab" data-type="personal" data-id="1">
                @include('partials.home_personal')
            </div>
            <div class="container tabs__tab-inner js-tab" data-type="personal" data-id="2">
                @include('partials.home_persons')
            </div>
            <div class="container tabs__tab-inner js-tab" data-type="personal" data-id="3">
                @include('partials.home_cars')
            </div>
            <div class="container tabs__tab-inner js-tab" data-type="personal" data-id="4">
                @include('partials.home_services')
            </div>
        </div>

    </div>
<?/*
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>*/?>
@endsection
