<?
header('Content-Type: text/html; charset= utf-8');
function morph($n, $f1, $f2, $f5) {
	$n = abs(intval($n)) % 100;
	if ($n>10 && $n<20) return $f5;
	$n = $n % 10;
	if ($n>1 && $n<5) return $f2;
	if ($n==1) return $f1;
	return $f5;
}
function num2str($num) {
	$nul='ноль';
	$ten=array(
		array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
		array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
	);
	$a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
	$tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
	$hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
	$unit=array( // Units
		array('' ,'' ,'',	 1),
		array(''   ,''   ,''    ,0),
		array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
		array('миллион' ,'миллиона','миллионов' ,0),
		array('миллиард','милиарда','миллиардов',0),
	);
	//
	list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
	$out = array();
	if (intval($rub)>0) {
		foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
			if (!intval($v)) continue;
			$uk = sizeof($unit)-$uk-1; // unit key
			$gender = $unit[$uk][3];
			list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
			// mega-logic
			$out[] = $hundred[$i1]; # 1xx-9xx
			if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
			else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
			// units without rub & kop
			if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
		} //foreach
	}
	else $out[] = $nul;
	$out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
	//$out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
	return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}
if(isset($arFild["buyer"]["LAST_NAME"])){

    $buyer1 = $arFild["buyer"]["LAST_NAME"].' '.$arFild["buyer"]["FIRST_NAME"].' '.$arFild["buyer"]["MIDDLE_NAME"].', '.$arFild["buyer"]["DATE_BIRTH"];

    $buyer2 = $arFild["buyer"]["PLACE_BIRTH"].', '.$arFild["buyer"]["PASSPORT_PATH_ADDRESS"];

    $buyer3 = $arFild["buyer"]["PASSPORT_SERIAL"].' '.$arFild["buyer"]["PASSPORT_NUMBER"].', '.$arFild["buyer"]["PASSPORT_DATE"].', '.$arFild["buyer"]["PASSPORT_WHERE"];

}else if(isset($arFild["buyer"]["LEGAL_NAME"])){

    $buyer1 = $arFild["buyer"]["LEGAL_NAME"].', '.$arFild["buyer"]["LEGAL_INN"];

    $buyer2 = $arFild["buyer"]["LEGAL_ADDRESS"];

    $buyer3 = '';

}
if(isset($arFild["seller"]["S_LAST_NAME"])){

    $seller1 = $arFild["seller"]["S_LAST_NAME"].' '.$arFild["seller"]["S_FIRST_NAME"].' '.$arFild["seller"]["S_MIDDLE_NAME"].', '.$arFild["seller"]["S_DATE_BIRTH"];

    $seller2 = $arFild["seller"]["S_PLACE_BIRTH"].', '.$arFild["seller"]["S_PASSPORT_PATH_ADDRESS"];

    $seller3 = $arFild["seller"]["S_PASSPORT_SERIAL"].' '.$arFild["seller"]["S_PASSPORT_NUMBER"].', '.$arFild["seller"]["S_PASSPORT_DATE"].', '.$arFild["seller"]["S_PASSPORT_WHERE"];

}else if(isset($arFild["seller"]["S_LEGAL_NAME"])){

    $seller1 = $arFild["seller"]["S_LEGAL_NAME"].', '.$arFild["seller"]["S_LEGAL_INN"];

    $seller2 = $arFild["seller"]["S_LEGAL_ADDRESS"];

    $seller3 = '';

}
$price = number_format($arFild["auto"]["AUTO_PRICE"], 0, '', ' ');
$priceStr = num2str($arFild["auto"]["AUTO_PRICE"]);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pitstop</title>
</head>
<body style="font-family: 'DejaVu Sans', Arial, sans-serif; font-size:10px; margin: 0; line-height:1.2; letter-spacing: .2px">

<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <tr style="text-align: right;">
        <td>
            <!-- Шапка -->
            <table cellspacing="0" cellpadding="0" style="width: 371px; text-align: left; margin: 0 0 0 auto;">
                <tbody>
                <tr>
                   <td style="height:15px;width: 212px; border-bottom: 1px solid #000; padding-bottom: 0;">
                        <?=$arFild["auto"]["DOC_ADDRESS"];?>
                    </td>
                   <td style="height:15px;width: 32px"></td>
                   <td style="height:15px;width: 127px; border-bottom: 1px solid #000; padding-bottom: 0; ">
                        <?=$arFild["auto"]["DOC_DATE"];?>
                    </td>
                </tr>
                <tr>
                   <td style="height:15px;width: 212px;">
                        <span style="font-size:8px;">Место составления договора</span>
                    </td>
                   <td style="height:15px;width: 32px"></td>
                   <td style="height:15px;width: 127px;  ">
                        <span style="font-size: 8px">Дата составления</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<!-- Шапка END -->
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <!-- Заголовок -->
    <tr style="text-align: center;">
       <td style="padding-top: 35px;">
            <b style="font-size: 14px;">Договор купли-продажи транспортного средства</b>
        </td>
    </tr>
    <!-- Заголовок END -->
    <!-- Текст ФИО и пр. -->
    <tr>
       <td style="height:15px;width: 100%; padding-top: 8px; border-bottom: 1px solid #000">
            <?=$buyer1;?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;width: 100%; padding-top: 8px; border-bottom: 1px solid #000">
            <?=$buyer2;?>
        </td>
    </tr>
    <tr>
       <td style="height:15px; width: 100%;padding-top: 8px; border-bottom: 1px solid #000">
            <?=$buyer3;?>
        </td>
    </tr>
    <tr>
        <td>
            <span style="font-size: 8px">ФИО,дата и место рождения, адрес места жительства, паспортные данные:серия,номер,кем выдан и когда</span>
        </td>
    </tr>
    <!-- Текст ФИО и пр. END -->
    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 7px;">
            <span style="font-size: 10px;">именуемый(ая) в&nbsp;дальнейшем &laquo;Покупатель&raquo; с&nbsp;одной стороны,и</span>
        </td>
    </tr>
    <!-- Блок текста END-->
    <!-- Текст ФИО и пр. -->
    <tr>
       <td style="height:15px;padding-top: 10px; border-bottom: 1px solid #000">
            <?=$seller1;?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;padding-top: 8px; border-bottom: 1px solid #000">
            <?=$seller2;?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;padding-top: 8px; border-bottom: 1px solid #000">
            <?=$seller3;?>
        </td>
    </tr>
    <tr>
        <td>
            <span style="font-size: 8px">ФИО,дата и место рождения, адрес места жительства, паспортные данные:серия,номер,кем выдан и когда</span>
        </td>
    </tr>
    <!-- Текст ФИО и пр. END -->
    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 5px;">
            <span style="font-size: 10px; ">именуемый(ая) в&nbsp;дальнейшем &laquo;Продавец&raquo; с&nbsp;другой стороны, именуемые далее при совместном упоминании &laquo;Стороны&raquo;,<br> а&nbsp;по&nbsp;отдельности &laquo;Сторона&raquo;, заключили настоящий договор (далее&nbsp;&mdash; &laquo;Договор&raquo;) о&nbsp;нижеследующем:</span>
        </td>
    </tr>
    <!-- Блок текста END-->
    <!-- Заголовок пункта -->
    <tr style="text-align: center;">
       <td style="padding-top: 8px;">
            <b style="font-size: 11px">1. Предмет договора</b>
        </td>
    </tr>
    <!-- Заголовок пункта END-->
    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 8px;">
            <span style="font-size: 9px;">1.1 Продавец обязуется передать в&nbsp;собственность Покупателя, а&nbsp;Покупатель&nbsp;&mdash; принять и&nbsp;оплатить транспортное средство (далее&nbsp;– ТС).</span>
        </td>
    </tr>
    <!-- Блок текста END-->
    <tr>
       <td style="height:15px;padding-top: 10px; border-bottom: 1px solid #000">
            <?=$arFild["auto"]["AUTO_MAKE"];?> <?=$arFild["auto"]["FIRST_MODEL"];?>
        </td>
    </tr>
    <tr>
        <td>
            <span style="font-size: 7px">Марка и модель транспортного средства</span>
        </td>
    </tr>
    <!-- Текст ФИО и пр. END -->
    </tbody>
</table>

<!-- Три колонки -->
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <tr>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 8px;">
            <?=$arFild["auto"]["AUTO_VIN"];?>
        </td>
       <td style="height:15px;width: 20px"></td>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 8px;">
            <?=$arFild["auto"]["AUTO_TYPE"];?>
        </td>
       <td style="height:15px;width: 20px"></td>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 8px;">
            <?=$arFild["auto"]["AUTO_YEAR"];?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;width: 215px;">
            <span style="font-size:7px;">Идентификационный номер (VIN)</span>
        </td>
       <td style="height:15px;width: 20px"></td>
       <td style="height:15px;width: 215px;">
            <span style="font-size:7px;">Тип (легковой, грузовой и т. д.)</span>
        </td>
       <td style="height:15px;width: 20px"></td>
       <td style="height:15px;width: 215px;  ">
            <span style="font-size: 7px">Год изготовления</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- Три колонки END -->

<!-- Три колонки -->
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <tr>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 6px;">
            <!-- Пробег -->
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 106px; border-bottom: 1px solid #000; padding-top: 6px;">
            <?=$arFild["auto"]["AUTO_POWER"];?>
        </td>
       <td style="height:15px;width: 1px; border-bottom: 1px solid #000;">
            <div style="font-size: 13px; line-height: 13px; margin-bottom: -10px">/</div>
        </td>
       <td style="height:15px;width: 106px; border-bottom: 1px solid #000; padding-top: 6px; padding-left: 5px">
            <!-- Рабочий объем,  куб. см. -->
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 6px;">
            <?=$arFild["auto"]["AUTO_COLOR"];?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;width: 215px;">
            <span style="font-size:8px;">Пробег, км.</span>
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 106px;">
            <span style="font-size:7px;">Мощность двигателя л.с.</span>
        </td>
       <td style="height:15px;width: 1px;">
        </td>
       <td style="height:15px;width: 106px; padding-left: 5px">
            <span style="font-size:7px;">Рабочий объем,  куб. см.</span>
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 215px;  ">
            <span style="font-size: 7px">Цвет кузова</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- Три колонки END -->

<!-- Три колонки -->
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <tr>
       <td style="height:15px;width: 92px; border-bottom: 1px solid #000; padding-top: 8px;">
            
        </td>
       <td style="height:15px;width: 1px; border-bottom: 1px solid #000;">
            <div style="font-size: 13px; line-height: 13px; margin-bottom: -12px">/</div>
        </td>
       <td style="height:15px;width: 123px; border-bottom: 1px solid #000; padding-top: 8px; padding-left: 5px">
            
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 8px;">
            <?=$arFild["auto"]["AUTO_CHASSIS_NUMBER"];?>
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 215px; border-bottom: 1px solid #000; padding-top: 8px;">
            <?=$arFild["auto"]["AUTO_BODY_NUMBER"];?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;width: 92px;">
            <span style="font-size:7px;">Модель двигателя</span>
        </td>
       <td style="height:15px;width: 1px;">
        </td>
       <td style="height:15px;width: 123px; padding-left: 5px">
            <span style="font-size:7px;">Номер двигателя</span>
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 215px;">
            <span style="font-size:7px;">Номер шасси,рамы</span>
        </td>
       <td style="height:15px;width: 10px"></td>
       <td style="height:15px;width: 215px;">
            <span style="font-size: 7px">Номер кузова</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- Три колонки END -->

<!-- 4 колонки -->
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <tr>
       <td style="height:15px;width: 51px; border-bottom: 1px solid #000; padding-top: 7px;">
            <div style="width: 51px; overflow: hidden; text-overflow:ellipsis; white-space: nowrap;">
                <?=$arFild["auto"]["AUTO_STS_SERIAL"];?>
            </div>
        </td>
       <td style="height:15px;width: 1px; border-bottom: 1px solid #000;">
            <div style="font-size: 13px; line-height: 13px; margin-bottom: -12px">/</div>
        </td>
       <td style="height:15px;width: 65px; border-bottom: 1px solid #000; padding-top: 7px; padding-left: 5px">
            <?=$arFild["auto"]["AUTO_STS_NUMBER"];?>
        </td>

       <td style="height:15px;width: 20px"></td>

       <td style="height:15px;width: 351px; border-bottom: 1px solid #000; padding-top: 7px;">
            
        </td>
       <td style="height:15px;width: 15px"></td>
       <td style="height:15px;width: 61px; border-bottom: 1px solid #000; padding-top: 7px;">
            <?=$arFild["auto"]["AUTO_STS_DATE"];?>
        </td>
       <td style="height:15px;width: 15px"></td>
       <td style="height:15px;width: 96px; border-bottom: 1px solid #000; padding-top: 7px;">
            <?=$arFild["auto"]["AUTO_NUMBER"];?>
        </td>
    </tr>
    <tr>
       <td style="height:15px;width: 51px;">
            <span style="font-size:7px;"><?=$arFild["auto"]["AUTO_DOCUMENT"];?></span>
        </td>
       <td style="height:15px;width: 1px;">
        </td>
       <td style="height:15px;width: 65px; padding-left: 5px">
            <span style="font-size:7px;">серия/номер</span>
        </td>
       <td style="height:15px;width: 20px"></td>
       <td style="height:15px;width: 351px;">
            <span style="font-size:7px;">Кем выдан</span>
        </td>
       <td style="height:15px;width: 15px"></td>
       <td style="height:15px;width: 61px;  ">
            <span style="font-size: 7px">Дата выдачи</span>
        </td>
       <td style="height:15px;width: 15px"></td>
       <td style="height:15px;width: 96px;  ">
            <span style="font-size: 7px">Гос. номер</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- 4 колонки END -->
<?
    switch ($arFild["auto"]["AUTO_DOCUMENT"]) {
        case 'Паспорт ТС':
            $bigTxtDoc = 'Паспорт транспортного средства';
        break;
        case 'Свидетельство ТС':
            $bigTxtDoc = 'Свидетельство о регистрации автомототранспортного средства';
        break;
        case 'Регистрационный документ или технический паспорт ТС':
            $bigTxtDoc = 'Регистрационный документ или технический паспорт автомототранспортного средства';
        break;
    }
?>
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 10px;">
            <div style="font-size: 9px; line-height: 15px">
                1.2. Собственником ТС&nbsp;до&nbsp;его передачи Покупателю является Продавец (<?=$bigTxtDoc;?>)<br/> <span
                        style="border-bottom: 1px solid #000; width: 40px; display: inline-block;"><?=$arFild["auto"]["AUTO_STS_SERIAL"];?></span> &#8470;
                <span style="border-bottom: 1px solid #000; width: 65px; display: inline-block;"><?=$arFild["auto"]["AUTO_STS_NUMBER"];?></span> ,
                выдано <span style="border-bottom: 1px solid #000; width: 352px; display: inline-block;"></span>
                &laquo;<span style="border-bottom: 1px solid #000; width: 12px; display: inline-block;"><?=explode('.',$arFild["auto"]["AUTO_STS_DATE"])[0];?></span>&raquo;&nbsp;&nbsp;&nbsp;<span
                        style="border-bottom: 1px solid #000; width: 59px; display: inline-block; text-align:center;"><?=explode('.',$arFild["auto"]["AUTO_STS_DATE"])[1];?></span>&nbsp;&nbsp;&nbsp;<span
                        style="border-bottom: 1px solid #000; width: 39px; display: inline-block;"><?=explode('.',$arFild["auto"]["AUTO_STS_DATE"])[2];?></span>&nbsp;г.)
                <br>
                Право собственности на ТС переходит к Покупателю с момента подписания настоящего Договора.
            </div>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 4px;">
            <span style="font-size: 9px; ">1.3. Передача ТС осуществляется Продавцом в момент передачи Покупателем Продавцу денежных средств в счет оплаты стоимости ТС согласно п. 2. Договора.</span>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Заголовок пункта -->
    <tr style="text-align: center;">
       <td style="padding-top: 10px;">
            <b style="font-size: 11px">2. Стоимость автомобиля и порядок расчетов</b>
        </td>
    </tr>
    <!-- Заголовок пункта END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 13px;">
            <div style="font-size: 9px; line-height: 15px">
                Стоимость ТС составляет <span
                        style="border-bottom: 1px solid #000; width: 62px; display: inline-block;"> <?=$price;?></span> (<span
                        style="border-bottom: 1px solid #000; width: 438px; display: inline-block;"><?=$priceStr;?></span>) рублей
                <br>
                (НДС не облагается). Оплата стоимости ТС производится путем 100% предоплаты (наличным или
                безналичным расчетом).
            </div>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Заголовок пункта -->
    <tr style="text-align: center;">
       <td style="padding-top: 10px;">
            <b style="font-size: 11px">3. Гарантии и ответственность</b>
        </td>
    </tr>
    <!-- Заголовок пункта END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 9px;">
            <span style="font-size: 9px; ">Продавец гарантирует Покупателю что:</span>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 4px;">
            <span style="font-size: 9px; ">3.1. Продавец является собственником ТС.</span>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 4px;">
            <span style="font-size: 9px; ">3.2. ТС не является предметом обязательств Продавца перед третьими лицами, в том числе не является предметом залога, в отношении автомобиля не наложен запрет на совершение регистрационных действий, автомобиль не находится под арестом, не числится в базах данных МВД России как угнанное или похищенное транспортное средство и не имеет иных обременений.</span>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 4px;">
            <span style="font-size: 9px; ">3.3. В случае нарушения гарантий, указанных в п. 3.1. - 3.2. настоящего договора , Продавец обязуется назамедлительно возвратить Покупателю стоимость ТС в полном объеме со дня обнаружения соответствующего нарушения.</span>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Заголовок пункта -->
    <tr style="text-align: center;">
       <td style="padding-top: 10px;">
            <b style="font-size: 11px">4. Заключительные положения</b>
        </td>
    </tr>
    <!-- Заголовок пункта END-->

    <!-- Блок текста -->
    <tr>
       <td style="padding-top: 9px;">
            <span style="font-size: 9px; ">Настоящий Договор вступает в силу после его подписания Сторонами и действует до момента исполнения Сторонами своих обязательств по Договору. Договор составлен в трех экземплярах, имеющих равную юридическую силу.</span>
        </td>
    </tr>
    <!-- Блок текста END-->

    <!-- Заголовок пункта -->
    <tr style="text-align: center;">
       <td style="padding-top: 10px;">
            <b style="font-size: 11px">5. Подписи сторон</b>
        </td>
    </tr>
    <!-- Заголовок пункта END-->
    </tbody>
</table>

<!-- Подписи -->
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
    <tr>
       <td style="height:15px;width: 122px; border-bottom: 1px solid #000; padding-top: 13px;">
            
        </td>
       <td style="height:15px;width: 1px; border-bottom: 1px solid #000;">
            <div style="font-size: 13px; line-height: 13px; margin-bottom: -20px">/</div>
        </td>
       <td style="height:15px;width: 194px; border-bottom: 1px solid #000; padding-top: 13px; padding-left: 5px">
            
        </td>

       <td style="height:15px;width: 64px"></td>

       <td style="height:15px;width: 122px; border-bottom: 1px solid #000; padding-top: 13px;">
            
        </td>
       <td style="height:15px;width: 1px; border-bottom: 1px solid #000;">
            <div style="font-size: 13px; line-height: 13px; margin-bottom: -20px">/</div>
        </td>
       <td style="height:15px;width: 194px; border-bottom: 1px solid #000; padding-top: 13px; padding-left: 5px">
            
        </td>
    </tr>
    <tr>
       <td style="height:15px;width: 122px;">
            <span style="font-size:7px;">Продавец, подпись</span>
        </td>
       <td style="height:15px;width: 1px;">
        </td>
       <td style="height:15px;width: 194px; padding-left: 5px">
            <span style="font-size:7px;">Расшифровка</span>
        </td>

       <td style="height:15px;width: 64px"></td>

       <td style="height:15px;width: 122px;">
            <span style="font-size:7px;">Покупатель, подпись</span>
        </td>
       <td style="height:15px;width: 1px;">
        </td>
       <td style="height:15px;width: 194px; padding-left: 5px">
            <span style="font-size:7px;">Расшифровка</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- Подписи END -->

<!-- Подписи -->
<table cellspacing="0" cellpadding="0" width="640px">
    <tbody>
    <tr>
       <td style="height:10px;width: 317px; padding-top: 10px;">
                    <span style="font-size: 9px; line-height: 15px">
                        Денежные средства в сумме <span
                                style="border-bottom: 1px solid #000; width: 93px; display: inline-block;"><?=$price;?></span> руб. получил.
                    </span>
        </td>
       <td style="height:10px;width: 64px"></td>
       <td style="height:10px;width: 71px; padding-top: 10px;">
            <div style="font-size: 9px;">
                ТС получил
            </div>
        </td>
       <td style="height:10px;width: 71px; border-bottom: 1px solid #000; padding-top: 10px;">
            
        </td>
       <td style="height:10px;width: 1px; border-bottom: 1px solid #000;">
            <div style="font-size: 13px; line-height: 13px; margin-bottom: -17px">/</div>
        </td>
       <td style="height:10px;width: 123px; border-bottom: 1px solid #000; padding-top: 10px; padding-left: 5px">
            
        </td>
    </tr>
    <tr>
       <td style="width: 317px"></td>
       <td style="width: 64px"></td>
       <td style="width: 71px; padding-top: 7px;"></td>
       <td style="width: 71px;">
            <span style="font-size:7px;">Подпись</span>
        </td>
       <td style="width: 1px;">
        </td>
       <td style="width: 123px; padding-left: 5px">
            <span style="font-size:7px;">Расшифровка</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- Подписи END -->

<!-- Подписи -->
<table cellspacing="0" cellpadding="0" width="207px" style="position:absolute;">
    <thead>
        <tr>
            <td style="height:15px;width: 78px; border-bottom: 1px solid #000; padding-top: 2px;">
                
            </td>
            <td style="height:15px;width: 1px; border-bottom: 1px solid #000;">
                <div style="font-size: 13px; line-height: 13px; margin-bottom: -9px">/</div>
            </td>
            <td style="height:15px;width: 128px; border-bottom: 1px solid #000;  padding-top: 5px; padding-left: 5px">
                
            </td>
        </tr>
    </thead>
    <tbody>
    <tr>
       <td style="width: 78px;">
            <span style="font-size:7px;">Продавец, подпись</span>
        </td>
       <td style="width: 1px;">
        </td>
       <td style="width: 128px; padding-left: 5px">
            <span style="font-size:7px;">Расшифровка</span>
        </td>
    </tr>
    </tbody>
</table>
<!-- Подписи END -->


</body>
</html>