<?
header('Content-Type: text/html; charset= utf-8');
$insurace_order = $arFields->Allinsurance()->first();
?>
    <!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Заявление</title>
</head>
<body style="font-family: 'DejaVu Sans', Arial, sans-serif; font-size:10px; margin: 0; line-height:1.2; letter-spacing: .2px">
<h1>
    {{ $arFields->polis_number }} | {{ !empty($insurace_order->name)?$insurace_order->name:'' }}
</h1>
<p>Стоимость: {{ $arFields->price?$arFields->price:0 }}</p>
<p>Марка, модель: {{ explode('|', $arFields->AutoOrder[0]->mark)[1] }} {{ explode('|', $arFields->AutoOrder[0]->model)[1] }}</p>
<p>Год выпуска: {{ $arFields->AutoOrder[0]->year }}</p>
@if($arFields->AutoOrder[0]->vin)
   <p>Vin: {{ $arFields->AutoOrder[0]->vin }}</p>
@elseif($arFields->AutoOrder[0]->number_bodywork)
    <p>Номер кузова: {{ $arFields->AutoOrder[0]->number_bodywork }}</p>
@elseif($arFields->AutoOrder[0]->number_chassis)
    <p>Шасси: {{ $arFields->AutoOrder[0]->number_chassis }}</p>
@endif
<p>Мощность (л.с): {{ $arFields->AutoOrder[0]->power }}</p>
<p>Регион: {{ $arFields->RegionOfUse }}</p>
<p>Нас.пункт: {{ $arFields->CityOfUse }}</p>
<p>ФИО: {{ $arFields->LastName_Owner }} {{ $arFields->FirstName_Owner }} {{ $arFields->MiddleName_Owner }}</p>
<p>Дата рождения: {{ Date::parse($arFields->BirthDate_Owner)->format('d.m.Y') }}</p>
<p>Серия, номер ВУ: {{ $arFields->PeronsOrder[0]->docser_numb }}</p>
<p>Дата отсчета стажа: {{ Date::parse($arFields->ExperienceDate)->format('d.m.Y') }}</p>
</body>
</html>
