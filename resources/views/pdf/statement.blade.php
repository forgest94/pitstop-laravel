<? 
header('Content-Type: text/html; charset= utf-8'); 
$fio_dop = '';
$doc_dop = '';
$address_dop = '';
if(isset($arFild["seller"]["S_LAST_NAME"])){

$fio = $arFild["seller"]["S_LAST_NAME"].' '.$arFild["seller"]["S_FIRST_NAME"].' '.$arFild["seller"]["S_MIDDLE_NAME"];

$date = $arFild["seller"]["S_DATE_BIRTH"];

$doc = 'паспорт '.$arFild["seller"]["S_PASSPORT_SERIAL"].' '.$arFild["seller"]["S_PASSPORT_NUMBER"].', выдан '.$arFild["seller"]["S_PASSPORT_WHERE"];

$inn = $arFild["seller"]["S_INN"];

$address = $arFild["seller"]["S_PASSPORT_PATH_ADDRESS"];

$phone = $arFild["auto"]["DOC_USER_PHONE"];

}else if(isset($arFild["seller"]["S_LEGAL_NAME"])){

$fio = $arFild["seller"]["S_LEGAL_NAME"];

$date = $arFild["seller"]["S_LEGAL_DATE"];

$doc = '';

$inn = $arFild["seller"]["S_LEGAL_INN"];

$address = $arFild["seller"]["S_LEGAL_ADDRESS"];

$phone = $arFild["seller"]["S_LEGAL_TEL"];

$fio_dop = $arFild["seller"]["S_ADDITIONAL_LAST_NAME"].' '.$arFild["seller"]["S_ADDITIONAL_FIRST_NAME"].' '.$arFild["seller"]["S_ADDITIONAL_MIDDLE_NAME"];
$doc_dop = 'паспорт '.$arFild["seller"]["S_ADDITIONAL_PASSPORT_SERIAL"].' '.$arFild["seller"]["S_ADDITIONAL_PASSPORT_NUMBER"].', выдан '.$arFild["seller"]["S_ADDITIONAL_PASSPORT_WHERE"];
$address_dop = $arFild["seller"]["S_ADDITIONAL_PASSPORT_PATH_ADDRESS"];
}
?>
        <!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Заявление</title>
</head>
<style>
    .page-break {
        page-break-after: always;
    }
</style>
<body style="font-family: 'DejaVu Sans', Arial, sans-serif; font-size:10px; margin: 0; line-height:1.2; letter-spacing: .2px">
<table cellspacing="0" cellpadding="0" width="100%" style="border: 1px solid #000">
    <tbody>
    <!-- Заголовок -->
    <tr style="text-align: center;">
        <td>
            <b style="font-size: 15px; text-transform: uppercase;">Заявление *</b>
        </td>
    </tr>
    <!-- Заголовок END -->
    <!-- Строка с нижней подписью -->
    <tr>
        <td style="padding:0 14px 0 17px">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                    <td style="width: 100%; padding-left: 5px; border-bottom: 1px solid #000">
                        В Госавтоинспекцию
                    </td>
                </tr>
                <tr style="text-align: center;">
                    <td>
                        <span style="height:15px;font-size: 9px">(наименование регистрационного подразделения)</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!-- Строка с нижней подписью END -->
    <!-- Строка с нижней подписью -->
    <tr>
        <td style="padding:0 14px 0 17px">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                    <td style="width: 100%; padding-left: 5px; border-bottom: 1px solid #000">
                        <span style="height:15px;display: inline-block; width: 85%">Я, <?=$buyer1 = $arFild["buyer"]["LAST_NAME"].' '.$arFild["buyer"]["FIRST_NAME"].' '.$arFild["buyer"]["MIDDLE_NAME"];?></span><span
                                style="text-align: right">прошу:</span>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 9px; padding-left: 198px; line-height: 1;">
                        <span>(фамилия, имя, отчество (при наличии) заявителя)</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- Строка с нижней подписью END -->
    <tr>
        <td style="border-top: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%;">
                <tbody>
                <tr>
                    <td rowspan="5"
                        style="width: 188px; padding: 5px; border-right: 1px solid #000; text-align: right; vertical-align: top;">
                        Зарегистрировать<br>

                        <div style="text-align: center; padding-top: 16px;">
                            (необходимую строчку подчеркнуть)
                        </div>
                    </td>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">новое,приобретенное в Российской Федерации</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        ввезенное в Российскую Федерацию
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000;  padding: 2px 5px 10px 10px;">
                        приобретенное в качестве высвобождаемого военного имущества
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000;  padding: 2px 5px 10px 10px;">
                        изготовленное в Российской Федерации в индивидуальном
                        порядке из сборочного комплекта либо являющееся результатом
                        индивидуального технического творчества.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 2px 5px 10px 10px;">
                        временно ввезенное в Российскую Федерацию на срок более 6 месяцев
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-top: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%;">
                <tbody>
                <tr>
                    <td rowspan="6"
                        style="width: 188px; padding: 5px; border-right: 1px solid #000; text-align: right; vertical-align: top;">
                        Внести изменения<br>
                        в связи с<br>

                        <div style="text-align: center; padding-top: 16px;">
                            (необходимую строчку подчеркнуть)
                        </div>


                    </td>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        <!-- Пример выделеного элемента-->
                        <span style="height:15px;border-bottom: 1px solid #000;">изменением собственника (владельца)</span>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        изменением данных о собственнике (владельце)
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        заменой, либо получением регистрационных знаков взамен
                        утраченных или пришедших в негодность
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        получением свидетельства о регистрации ТС и (или) ПТС взамен
                        утраченных или пришедших в негодность
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        изменениями регистрационных данных, не связанных с изменением конструкции

                    </td>
                </tr>
                <tr>
                    <td style="padding: 2px 5px 10px 10px;">
                        изменением конструкции
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-top: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%;">
                <tbody>
                <tr>
                    <td rowspan="2"
                        style="width: 188px; padding: 5px; border-right: 1px solid #000; text-align: right; vertical-align: top;">
                        Снять с<br>
                        регистрационного учета в<br>
                        связи с:<br>
                        <div style="text-align: center; padding-top: 10px;">
                            (необходимую строчку подчеркнуть)
                        </div>


                    </td>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        вывозом его за пределы территории Российской Федерации и (или)
                        окончанием срока регистрации на ограниченный срок
                    </td>
                </tr>
                <tr>
                    <td style="padding: 2px 5px 10px 10px;">
                        дальнейшей утилизацией
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-top: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%;">
                <tbody>
                <tr>
                    <td rowspan="3"
                        style="width: 188px; padding: 5px; border-right: 1px solid #000; text-align: right; vertical-align: top;">
                        Прекратить регистрацию<br>
                        транспортного средства в<br>
                        связи с:<br>
                        <div style="text-align: center; padding-top: 10px;">
                            (необходимую строчку подчеркнуть)
                        </div>


                    </td>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        утратой (неизвестно место нахождения транспортного средства
                        или при невозможности пользоваться транспортным средством)
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000; padding: 2px 5px 10px 10px;">
                        хищением
                    </td>
                </tr>
                <tr>
                    <td style="padding: 2px 5px 0 10px;">
                        продажей (передачей) другому лицу
                    </td>
                </tr>

                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-top: 1px solid #000; border-bottom: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%;">
                <tbody>
                <tr>
                    <td rowspan="4"
                        style="width: 111px; border-right: 1px solid #000; text-align: left; vertical-align: top; padding: 5px;">
                        Транспортное<br>
                        средство<br>
                    </td>
                    <td style="width: 262px;border-bottom: 1px solid #000; padding: 2px 5px 5px 10px;">
                        Марка,модель
                    </td>
                    <td style="width: 245px; border-left: 1px solid #000; border-bottom: 1px solid #000; padding: 2px 5px 5px 10px;">
                        <?=$arFild["auto"]["AUTO_MAKE"];?> <?=$arFild["auto"]["FIRST_MODEL"];?>

                    </td>
                </tr>
                <tr>
                    <td style="width: 262px; border-bottom: 1px solid #000; padding: 2px 5px 5px 10px;">
                        Год выпуска
                    </td>
                    <td style="width: 245px; border-left: 1px solid #000; border-bottom: 1px solid #000; padding: 2px 5px 5px 10px;">
                        <?=$arFild["auto"]["AUTO_YEAR"];?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 262px; border-bottom: 1px solid #000; padding: 2px 5px 5px 10px;">
                        VIN идентификационный номер
                    </td>
                    <td style="width: 245px; border-left: 1px solid #000; border-bottom: 1px solid #000; padding: 2px 5px 5px 10px;">
                        <?=$arFild["auto"]["AUTO_VIN"];?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 262px; padding: 2px 5px 5px 10px;">
                        Регистрационный знак (при наличии)
                    </td>
                    <td style="width: 245px; border-left: 1px solid #000; padding: 2px 5px 5px 10px;">
                        <?=$arFild["auto"]["AUTO_NUMBER"];?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" width="100%" style="border: 1px solid #000; margin-top: 23px">
    <tbody>
    <!-- Заголовок -->
    <tr style="text-align: center;">
        <td style="padding-top: 3px;">
            <b style="font-size: 11px; text-transform: uppercase;">Сведения о собственнике транспортного средства</b>
        </td>
    </tr>
    <!-- Заголовок END -->
    <!-- Строка с нижней подписью -->
    <tr>
        <td style="padding:0 10px 0 7px">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                    <td style="width: 100%; padding-top: 15px; padding-left: 5px; border-bottom: 1px solid #000">
                        <?=$fio;?>
                    </td>
                </tr>
                <tr style="text-align: center;">
                    <td>
                        <span style="height:15px;font-size: 9px">(Наименование юридического лица или фамилия, имя, отчество (при наличии) физического лица)</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!-- Строка с нижней подписью END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            Дата регистрации юридического лица или дата рождения физического лица <span style="height:15px;width: 240px; display: inline-block; margin-left: 10px; border-bottom: 1px solid #000;"><?=$date;?></span>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            Документ, удостоверяющий личность ** <span style="height:15px;width: 444px; display: inline-block; margin-left: 10px; border-bottom: 1px solid #000;"><?=$doc;?></span>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            ИНН (для физических лиц при наличии) <span style="height:15px;width: 447px; display: inline-block; margin-left: 10px; border-bottom: 1px solid #000;"><?=$inn;?></span>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            <div style="display: inline-block; vertical-align: middle;">Адрес регистрации юридического лица <br>
                или адрес места жительства физического лица</div>
            <div style="display: inline-block; vertical-align: middle; width: 404px; text-align: center">
                <span style="height:15px;width: 404px; display: inline-block; text-align: left; margin-left: 10px; border-bottom: 1px solid #000;"></span>
                <span>(Индекс, субъект Российской Федерации,</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->

    <tr>
        <td style="padding: 8px 10px 0 7px">
            <div style="display: inline-block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: inline-block; text-align: left; border-bottom: 1px solid #000;"><?=$address;?></span>
                <span>район, населенный пункт, улица, дом, корпус, квартира)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            Тел. <span style="height:15px;width: 220px; display: inline-block; margin-left: 7px; margin-right: 5px;border-bottom: 1px solid #000;"><?=$phone;?></span>
            Адрес электронной почты (при наличии) <span style="height:15px;width: 189px; display: inline-block; margin-left: 5px; border-bottom: 1px solid #000;"></span>
        </td>
    </tr>
    <!-- Строка END -->
    </tbody>
</table>
<div class="page-break"></div>
<table cellspacing="0" cellpadding="0" width="100%" style="border: 1px solid #000;">
    <tbody>
    <!-- Заголовок -->
    <tr style="text-align: center;">
        <td style="padding-top: 3px;">
            <b style="font-size: 11px; text-transform: uppercase;">Представитель собственника ***</b>
        </td>
    </tr>
    <!-- Заголовок END -->
    <!-- Строка с нижней подписью -->
    <tr>
        <td style="padding:0 10px 0 7px">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <tr>
                    <td style="width: 100%; padding-top: 15px; padding-left: 5px; border-bottom: 1px solid #000">
                        <?=$fio_dop;?>
                    </td>
                </tr>
                <tr style="text-align: center;">
                    <td>
                        <span style="height:15px;font-size: 9px">(фамилия, имя, отчество(при наличии))</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!-- Строка с нижней подписью END -->

    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            <div style="display: inline-block; vertical-align: top;">Документ, удостоверяющий личность</div>
            <div style="display: inline-block; vertical-align: bottom; width: 464px; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; margin-left: 10px; border-bottom: 1px solid #000;"></span>
                <span>(серия, номер, когда, кем выдан)</span>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 10px 0 7px">
            <div style="display: inline-block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: inline-block; text-align: left; border-bottom: 1px solid #000;"><?=$doc_dop;?></span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 7px 0 7px">
            <div style="display: inline-block; vertical-align: top;">Адрес места жительства</div>
            <div style="display: inline-block; vertical-align: bottom; width: 533px; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; margin-left: 10px; border-bottom: 1px solid #000;"></span>
                <span>(субъект Российской Федерации, район</span>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 10px 0 7px">
            <div style="display: inline-block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: inline-block; text-align: left; border-bottom: 1px solid #000;"><?=$address_dop;?></span>
                <span>населенный пункт, улица, дом, корпус, квартира)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 10px 0 7px">
            <span style="height:15px;width: 100%; display: inline-block; text-align: left; border-bottom: 1px solid #000;">Телефон </span>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 10px 0 7px">
            <div style="display: block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; border-bottom: 1px solid #000;"></span>
                <span style="height:15px;width: 33%; display: inline-block;" >(дата)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(подпись)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(И.О. Фамилия заявителя)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" width="100%" style="border: 1px solid #000; margin-top: 15px;">
    <tbody>
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <tr style="vertical-align: top;">
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <b style="font-size: 11px; text-transform: uppercase;">Сведения о
                            <br>
                            транспортном
                            <br>
                            средстве ****</b>
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Из паспорта транспортного
                        средства и (или)
                        регистрационного документа
                        (заполняется заявителем или
                        из информационных учетов)
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                        По результатам осмотра
                        сотрудником
                        регистрационного
                        подразделения ГИБДД
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Марка, модель ТС
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_MAKE"];?>, <?=$arFild["auto"]["FIRST_MODEL"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Год выпуска
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_YEAR"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Тип/категория ТС
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_TYPE"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Цвет
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_COLOR"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Регистрационный знак
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_NUMBER"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Идентификационный номер VIN
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_VIN"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Кузов (кабина, прицеп) №
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_BODY_NUMBER"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Шасси (рама) №
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_CHASSIS_NUMBER"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Мощность двигателя л.с./кВт
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_POWER"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Экологический класс
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        <?=$arFild["auto"]["AUTO_ECO_CLASS"];?>
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Разрешенная максимальная масса
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                        Масса без нагрузки
                    </td>
                    <td style="width: 33%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                    <td style="width: 33%; border-bottom: 1px solid #000; padding: 1px 3px;">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!-- Строка -->
    <tr>
        <td style="padding: 0 30px 0 7px; text-align: right;">
            соответствует/не соответствует <span style="height:15px;font-size: 8px">(ненужное зачеркнуть)</span>
        </td>
    </tr>
    <!-- Строка END -->
    <!-- Строка -->
    <tr>
        <td style="padding: 8px 10px 5px 7px">
            <div style="display: block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; border-bottom: 1px solid #000;"></span>
                <span style="height:15px;width: 33%; display: inline-block;" >(дата, время осмотра)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(подпись)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(И.О. Фамилия сотрудника)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" width="100%" style="border: 1px solid #000; margin-top: 15px">
    <tbody>
    <tr>
        <td style="width: 80%; border-right: 1px solid #000; border-bottom: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <!-- Заголовок -->
                <tr style="text-align: center;">
                    <td style="padding-top: 3px;">
                        <b style="font-size: 11px; text-transform: uppercase;">Вносимые изменения в конструкцию ТС *****</b>
                    </td>
                </tr>
                <!-- Заголовок END -->
                <!-- Строка -->
                <tr>
                    <td style="padding: 8px 7px 0 7px">
                        <div style="display: inline-block; width: 100%; text-align: center">
                            <span style="height:15px;display: block; text-align: left; margin-left: 10px; border-bottom: 1px solid #000;"></span>
                            <span>подробно описываются изменения в конструкцию (например, тип и марка устанавливаемых или демонтируемых узлов и агрегатов, способ монтажа)</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 8px 10px 10px 7px">
                        <span style="height:15px;width: 100%; display: inline-block; border-bottom: 1px solid #000;"></span>
                    </td>
                </tr>
                <!-- Строка END -->
                </tbody>
            </table>
        </td>
        <td style="width: 20%; border-bottom: 1px solid #000;"></td>
    </tr>
    <!-- Строка -->
    <tr>
        <td colspan="2" style="padding: 10px 10px 0 7px">
            <div style="display: block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; border-bottom: 1px solid #000;"></span>
                <span style="height:15px;width: 33%; display: inline-block;" >(дата)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(подпись)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(И.О. Фамилия сотрудника)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" width="100%" style="border: 1px solid #000; margin-top: 15px">
    <tbody>
    <tr>
        <td style="width: 50%; border-right: 1px solid #000; border-bottom: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <!-- Заголовок -->
                <tr style="text-align: center;">
                    <td style="padding: 2px 5px 2px 5px;">
                        <b style="font-size: 11px; text-transform: uppercase;">Проверки по автоматизированным учетам</b>
                    </td>
                </tr>
                <!-- Заголовок END -->
                </tbody>
            </table>
        </td>
        <td style="width: 50%; border-bottom: 1px solid #000;"></td>
    </tr>
    <!-- Строка -->
    <tr>
        <td colspan="2" style="padding: 15px 10px 0 7px">
            <div style="display: block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; border-bottom: 1px solid #000;"></span>
                <span style="height:15px;width: 33%; display: inline-block;" >(дата, время проверки)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(подпись)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(И.О. Фамилия сотрудника)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    <tr>
        <td style="width: 50%; border-right: 1px solid #000; border-bottom: 1px solid #000; border-top: 1px solid #000;">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                <!-- Заголовок -->
                <tr style="text-align: center;">
                    <td style="padding: 2px 5px 2px 5px;">
                        <b style="font-size: 11px; text-transform: uppercase;">Решение по заявлению</b>
                    </td>
                </tr>
                <!-- Заголовок END -->
                </tbody>
            </table>
        </td>
        <td style="width: 50%; border-bottom: 1px solid #000; border-top: 1px solid #000;"></td>
    </tr>
    <!-- Строка -->
    <tr>
        <td colspan="2" style="padding: 15px 10px 0 7px">
            <div style="display: block; vertical-align: middle; width: 100%; text-align: center">
                <span style="height:15px;width: 100%; display: block; text-align: left; border-bottom: 1px solid #000;"></span>
                <span style="height:15px;width: 33%; display: inline-block;" >(дата, время принятия решения)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(подпись)</span>
                <span style="height:15px;width: 33%; display: inline-block;" >(И.О. Фамилия сотрудника)</span>
            </div>
        </td>
    </tr>
    <!-- Строка END -->
    </tbody>
</table>
</body>
</html>