@extends('layouts.template')
@section('content')
    @include('partials.advantages')
    <div class="services">
        <div class="services__background js-parallax-bg"></div>
    <div class="services__inner container">
        <h3 class="services__title">Все услуги для вашего автомобиля</h3>
        <div class="services__grid">
            <div class="services__cell">
                <div class="services__img">
                    <img src="{!! config('app.dir_static') !!}/img/services-1.svg" alt="">
                </div>
                <p class="services__text">ОСАГО онлайн</p>
            </div>
            <div class="services__cell">
                <div class="services__img">
                    <img src="{!! config('app.dir_static') !!}/img/services-3.svg" alt="">
                </div>
                <p class="services__text">Техосмотр</p>
            </div>
            <div class="services__cell">
                <div class="services__img">
                    <img src="{!! config('app.dir_static') !!}/img/services-2.svg" alt="">
                </div>
                <p class="services__text">Документы в ГИБДД онлайн</p>
            </div>
            <div class="services__cell">
                <div class="services__img">
                    <img src="{!! config('app.dir_static') !!}/img/services-4.svg" alt="">
                </div>
                <p class="services__text">Восстановление КБМ (скидки по ОСАГО)</p>
            </div>
        </div>
    </div>
</div>
    @include('partials.partners')
    @include('partials.reviews')
    @include('partials.seo_text')
@endsection
