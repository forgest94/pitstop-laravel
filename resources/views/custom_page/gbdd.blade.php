@extends('layouts.template')
@section('content')
<div class="c-welcome c-welcome_landing">
    <div class="c-welcome__inner container">
        <div class="c-welcome__text-wrap">
            <h1>{{ $page->page_title }}</h1>
            {!! $page->body !!}
        </div>
        <div class="c-welcome__call-block">
            <div class="c-welcome__call-inner">
                <div class="c-welcome__call-text">Вы можете подать заявку по телефону, заказав обратный звонок, либо
                    самостоятельно ввести данные в калькулятор
                </div>
                <a href="javascript:void(0);" class="c-welcome__call-link">Заказать звонок</a>
            </div>
            <div class="c-welcome__additional-img">
                <img src="{!! config('app.dir_static') !!}/img/calc-welcome-gibdd.svg">
            </div>
        </div>
    </div>
    <div class="c-welcome__buttons container">
        <a href="javascript:void(0);" class="c-welcome__btn btn">Сформировать документы</a>
    </div>
</div>

    <div class="answers">
    <div class="answers__inner container">
        <p class="answers__text">У нас есть ответы на все ваши вопросы!</p>
        <a href="/questions" class="answers__btn btn">Вопросы и ответы</a>
    </div>
</div>
@include('partials.advantages')
@include('partials.reviews')
@include('partials.seo_text')
@endsection