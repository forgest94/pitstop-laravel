@extends('layouts.template')
@section('content')
    <div class="c-welcome">
        @if(!empty($cityInfo))
            <h1 class="c-welcome__title">
                {!! $cityInfo->title !!}
            </h1>
        @else
            <h1 class="c-welcome__title">
                ОСАГО онлайн
            </h1>
        @endif
        <div class="c-welcome__inner container">
            <div class="c-welcome__slides">
                <div class="c-welcome__item">
                    <img class="c-welcome__item-img" src="{{ config('app.dir_static') }}/img/c-welcome-slide-1.svg">
                    <p class="c-welcome__item-text">Рассчитываете стоимость полиса</p>
                </div>
                <div class="c-welcome__item">
                    <img class="c-welcome__item-img" src="{{ config('app.dir_static') }}/img/c-welcome-slide-2.svg">
                    <p class="c-welcome__item-text">Сравниваете цены и выбираете компанию</p>
                </div>
                <div class="c-welcome__item">
                    <img class="c-welcome__item-img" src="{{ config('app.dir_static') }}/img/c-welcome-slide-3.svg">
                    <p class="c-welcome__item-text">Оплачиваете и получаете полис ОСАГО</p>
                </div>
            </div>
            <div class="c-welcome__call-block">
                <div class="c-welcome__call-inner">
                    <div class="c-welcome__call-text">Вы можете подать заявку по телефону, заказав обратный звонок, либо
                        самостоятельно ввести данные в калькулятор
                    </div>
                    <a href="javascript:void(0);" class="btn c-welcome__call-link js-modal-open"
                       data-modal-type="phone">Заказать звонок</a>
                </div>
            </div>
        </div>
    </div>

    <?/*<div class="cashback">
    <div class="cashback__desktop">
        <a class="cashback__inner container" href="/cashback" target="_blank">
            <img class="cashback__img" src="{{ config('app.dir_static') }}/img/cashback.png" alt="">
        </a>
    </div>
</div>*/?>
    @include('partials.calcTemplate')
    @include('partials.partners')
    <div class="answers">
        <div class="answers__inner container">
            <p class="answers__text">У нас есть ответы на все ваши вопросы!</p>
            <a href="/questions" class="answers__btn btn">Вопросы и ответы</a>
        </div>
    </div>
    @include('partials.advantages')
    @if(!empty($cityInfo->seo_text))
        @include('partials.seo_city')
    @elseif($page->seo_text_city)
        @include('partials.seo_city')
    @endif
    @include('partials.reviews')
    @include('partials.seo_text')
    <div class="modal js-modal" data-modal-type="phone" data-izimodal-width="600px">
        <div class="modal__inner">
            <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
            <form class="screen screen_active js-screen" id="revForm">
                @csrf
                <div class="modal__headline">Заказать звонок</div>
                <div class="row">
                    <div class="input input_wide">
                        <label for="nameReview" class="input__name">Ваше имя <span class="red">*</span></label>
                        <input placeholder="Ваше имя" name="name" type="text" id="namePage" class="req" tabindex="0"
                               value="@auth{{ Auth::user()->name }}@endif">
                    </div>
                </div>
                <div class="row">
                    <div class="input input_wide">
                        <label for="nameReview" class="input__name">Телефон <span class="red">*</span></label>
                        <input placeholder="Телефон" name="phone" type="text" id="phonePage" class="req" tabindex="0"
                               value="@auth{{ Auth::user()->phone }}@endif" data-mask="+7 (000) 000 00-00">
                    </div>
                </div>
                <div>
                    <input type="checkbox" class="checkbox-h" name="nda" id="checknda" checked>
                    <label for="checknda" class="form-block__nda">
                        Нажимая, я соглашаюсь на обработку персональных
                        данных
                    </label>
                </div>
                <input type="hidden" name="page" value="{{ $page->page_title }}">
                <div class="row row_center row_m-top">
                    <a href="javascript:void(0);" class="btn btn_big" tabindex="0" data-click="toPhone">Отправить</a>
                </div>
                <span class="mess_info"></span>
            </form>
        </div>
    </div>
    @empty(!$wiki)
        <div class="modal js-modal" data-modal-type="wiki" data-izimodal-width="1000px" data-izimodal-height="800">
            <div class="modal__inner modal__inner_no-paddings">
                <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                <div class="modal__wiki-wrap">
                    <div class="modal__wiki-links js-wiki js-modal-double">
                        <h3 class="modal__wiki-heading">Как заполнить поле</h3>
                        @foreach ($wiki as $key => $item)
                            @if($key == 0)
                                <a href="javascript:void(0);"
                                   class="modal__wiki-link modal__wiki-link_active js-link js-modal-screen"
                                   data-type-screen="wiki"
                                   data-id-screen="{{ $item->slug }}">{{ $item->name }}</a>
                            @else
                                <a href="javascript:void(0);" class="modal__wiki-link js-link js-modal-screen"
                                   data-type-screen="wiki"
                                   data-id-screen="{{ $item->slug }}">{{ $item->name }}</a>
                            @endif
                        @endforeach
                    </div>
                    <div class="modal__wiki-content js-modal-content ">
                        @foreach ($wiki as $key => $item)
                            @if($key == 0)
                                <div class="screen screen_active js-screen" data-type-screen="wiki"
                                     data-id-screen="{{ $item->slug }}">
                                    @else
                                        <div class="screen js-screen" data-type-screen="wiki"
                                             data-id-screen="{{ $item->slug }}">
                                            @endif
                                            <h3>{{ $item->name }}</h3>
                                            {!! $item->text !!}
                                        </div>
                                        @endforeach
                                </div>
                    </div>
                </div>
            </div>
            @endempty
            <div class="modal js-modal" data-modal-type="cross-strah" data-izimodal-width="460px">
                <div class="modal__inner">
                    <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                    <div class="modal__headline">Кросс предложение</div>
                    <form action class="js-cross-modal-form">

                    </form>
                    <button type="button" class="btn btn_big cross-option__btn js-end-cross-offer">Оформить</button>
                </div>
            </div>
            <div class="modal js-modal" data-modal-type="move-to-order" data-izimodal-width="750px">
                <div class="modal__inner">
                    <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                    <div class="modal__headline">Спасибо!</div>
                    <p class="js-cross-good-msg">Список одобренных кросс предложений:</p>
                    <div class="cross-result-list js-cross-list-good">
                    </div>
                    <div class="cross-option__price">
                        Итого к оплате:
                        <div class="cross-option__number js-result-price-order"></div>
                        руб
                    </div>
                    <div class="row">
                        <a href="" class="btn js-move-end-order">Перейти на оплату</a>
                        <a class="btn js-repeat-cross">Назад</a>
                    </div>
                </div>
            </div>

            <div class="modal js-modal" data-modal-type="result-table" data-izimodal-width="1000px">
                <div class="modal__inner">
                    <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                    <div class="result-table-wrapper">
                        <div class="result-table-wrapper__header">
                            <span>Расчет</span>
                        </div>
                        <table class="osago-table-result">
                            <tbody class="osago-table-result__body">
                            <tr>
                                <td>Параметры</td>
                                <td>Значение</td>
                            </tr>
                            <tr>
                                <td>Базовый коэфициент</td>
                                <td data-coefficient="tb"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент бонус-малус</td>
                                <td data-coefficient="kbm"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент возраста и стажа водителей</td>
                                <td data-coefficient="kvs"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент мощности</td>
                                <td data-coefficient="km"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент нарушений</td>
                                <td data-coefficient="kn"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент ограничения допуска</td>
                                <td data-coefficient="ko"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент периода использования</td>
                                <td data-coefficient="ks"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент прицепа</td>
                                <td data-coefficient="kpr"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент срока страхования</td>
                                <td data-coefficient="kp"></td>
                            </tr>
                            <tr>
                                <td>Коэфициент территории</td>
                                <td data-coefficient="kt"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection
