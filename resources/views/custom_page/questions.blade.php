@extends('layouts.template')
@section('content')
<div class="container page__heading">
<h1>{{ $page->page_title }}</h1>
<div class="tabs">
    <a href="/questions" data-tab="1" class="tabs__link <?if(!$category_slug):?>tabs__link_active<?endif;?> js-tab-link">Все</a>
    @empty(!$categories)
        <?$i=1;?>
        @foreach ($categories as $item)
            <?$i++;?>
            <a href="?cat={{ $item->slug }}" data-tab="<?=$i;?>" class="tabs__link <?if($category_slug == $item->slug):?>tabs__link_active<?endif;?> js-tab-link">{{ $item->name }}</a>
        @endforeach
    @endempty
</div>

    </div>
    <div class="container page__columns-wrap">
        <div class="page__column_big">
            @empty(!$questions)
                @foreach($questions as $key => $item)
                    <div class="answer js-answer <?if($key == 0):?>js-answer-opened<?endif;?>" data-category="1">
                            <a href="javascript:void(0);" class="answer__heading js-link">{{ $item->question }}</a>
                            <div class="answer__inner js-inner">
                                <div class="answer__text">
                                    <p>{{ $item->answer }}</p>
                                </div>
                                @if($item->src_cat)
                                    <a href="{{ $item->src_cat }}" class="answer__more">Перейти к услуге</a>
                                @endif
                            </div>
                    </div>
                @endforeach
            @endempty
        </div>
<div class="page__column_small">
    <div class="banner">
        <span class="banner__heading">{{ $page->excerpt }}</span>
        <p class="banner__text">{{ $page->text_form }}</p>
        <a href="javascript:void(0);" class="btn banner__btn js-modal-open" data-modal-type="ask">Задать вопрос</a>
        <div class="banner__girl">
            <img src="{!! config('app.dir_static') !!}/img/girl.png" alt="">
        </div>
    </div>
</div>
    </div>
<div class="modal js-modal" data-modal-type="ask" data-izimodal-width="705px">
    <div class="modal__inner">
        <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
        <form class="screen screen_active js-screen">
            @csrf
            <div class="modal__headline">Задать вопрос</div>
            <div class="row">
                <div class="input input_wide">
                    <label for="selectCat" class="input__name">Тема вопроса <span class="red">*</span></label>
                    <select name="selectCat" id="selectCat" class="js-select">
                        @empty(!$categories)
                            @foreach ($categories as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        @endempty
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="input input_part-2">
                    <label for="tel" class="input__name">Номер телефона</label>
                    <input name="tel" placeholder="Номер телефона" type="text" id="tel" tabindex="0" value="@auth{{ Auth::user()->phone }}@endif" data-mask="+7 (000) 000 00-00">
                </div>
                <div class="input input_part-2">
                    <label for="emailAsk" class="input__name">Email <span class="red">*</span></label>
                    <input name="email" placeholder="Email" type="text" id="emailAsk" tabindex="0" class="req" value="@auth{{ Auth::user()->email }}@endif">
                </div>
            </div>
            <div class="row">
                <div class="input input_wide input_textarea">
                    <label for="message" class="input__name">Вопрос <span class="red">*</span></label>
                    <textarea id="message" placeholder="Вопрос" tabindex="0" name="question" class="req"></textarea>
                </div>
            </div>
            <div>
                <input type="checkbox" class="checkbox-h" name="nda" id="checknda" checked>
                <label for="checknda" class="form-block__nda">
                    Нажимая, я соглашаюсь на обработку персональных
                    данных
                </label>
            </div>
            <div class="row row_center row_m-top">
                <a href="javascript:void(0);" class="btn btn_big" tabindex="0" data-click="addQuest">Отправить</a>
            </div>
            <span class="mess_info"></span>
        </form>
    </div>
</div>
@endsection