@extends('layouts.template')
@section('content')
<div class="container page__heading">
    <h1>{{ $page->page_title }}</h1>
<div class="sorting">
    <a href="{{ $url_sort['date'] }}" class="{{ $class_sort['date'] }}">Сортировать по дате</a>
    <a href="{{ $url_sort['rating'] }}" class="{{ $class_sort['rating'] }}">по рейтингу</a>
</div>

</div>
<div class="container page__columns-wrap">
    <div class="page__column_big">
        @empty(!$reviews)
            @foreach ($reviews as $item)
                <div class="comment">
                <div class="comment__image">
                    @empty($item->img)
                        <img src="{!! config('app.dir_static') !!}/img/user.jpg" alt="">
                    @else
                        <img src="{!! Voyager::image($item->img) !!}" alt="">
                    @endempty
                </div>
                <div class="comment__inner">
                    <div class="comment__header">
                        <div class="comment__stars" data-mark="{{ $item->rating }}">
                            <i class="comment__star"></i>
                            <i class="comment__star"></i>
                            <i class="comment__star"></i>
                            <i class="comment__star"></i>
                            <i class="comment__star"></i>
                        </div>
                        <div class="comment__info">
                            <span class="comment__name">{{ $item->name }}</span>
                            <span class="comment__city">г. {{ $item->city }}</span>
                            <span class="comment__date">{!! Date::parse($item->created_at)->format('j F Y') !!}</span>
                        </div>
                    </div>
                    <div class="comment__text">{{ $item->body }}</div>
                </div>
                </div>
            @endforeach  
        {{ $reviews->links('partials.nav_reviews') }}
        @endempty
    </div>
    <div class="page__column_small">
        <div class="banner">
<span class="banner__heading">{{ $page->excerpt }}</span>
<p class="banner__text">{{ $page->text_form }}</p>
<a href="javascript:void(0);" class="btn banner__btn js-modal-open" data-modal-type="review">Написать</a>
<div class="banner__girl">
    <img src="{!! config('app.dir_static') !!}/img/girl.png" alt="">
</div>
</div>
    </div>
</div>
<div class="modal js-modal" data-modal-type="review" data-izimodal-width="705px">
    <div class="modal__inner">
        <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
        <form class="screen screen_active js-screen" id="revForm">
            @csrf
            <h2>Оставить отзыв</h2>
            <div class="row row_space-beetween">
                <div class="input">
                    <div class="input__name">Рейтинг <span class="red">*</span></div>
                    <div class="star-group">
                    <span class="mess_star"></span>
                        <input type="radio" id="rating-5" name="rating" value="5"/><label for="rating-5"></label>
                        <input type="radio" id="rating-4" name="rating" value="4"/><label for="rating-4"></label>
                        <input type="radio" id="rating-3" name="rating" value="3"/><label for="rating-3"></label>
                        <input type="radio" id="rating-2" name="rating" value="2"/><label for="rating-2"></label>
                        <input type="radio" id="rating-1" name="rating" value="1"/><label for="rating-1"></label>
                    
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="input input_wide">
                        <label for="nameReview" class="input__name">Город <span class="red">*</span></label>
                        <input name="city" placeholder="Город" type="text" id="city" class="req" tabindex="0">
                    </div>
                </div>
            <div class="row">
                <div class="input input_wide">
                    <label for="nameReview" class="input__name">Ваше имя <span class="red">*</span></label>
                    <input name="name" placeholder="Ваше имя" type="text" id="nameReview" class="req" tabindex="0" value="@auth{{ Auth::user()->name }}@endif">
                </div>
            </div>
            <div class="row">
                <div class="input input_wide">
                    <label for="mailQuest" class="input__name">Email <span class="red">*</span></label>
                    <input name="mail" placeholder="Электронная почта" type="text" id="mailQuest" class="req" tabindex="0" value="@auth{{ Auth::user()->email }}@endif">
                </div>
            </div>
            <div class="row">
                <div class="input input_wide input_textarea">
                    <label for="reviewInput" class="input__name">Сообщение <span class="red">*</span></label>
                    <textarea id="reviewInput" placeholder="Сообщение" name="review" class="req" tabindex="0"></textarea>
                </div>
            </div>

            <div>
                <input type="checkbox" class="checkbox-h" name="nda" id="checknda" checked>
                <label for="checknda" class="form-block__nda">
                    Нажимая, я соглашаюсь на обработку персональных
                    данных
                </label>
            </div>
            <div class="row row_center row_m-top">
                <a href="javascript:void(0);" class="btn btn_big" tabindex="0" data-click="addRev">Отправить</a>
            </div>
            <span class="mess_info"></span>
        </form>
    </div>
</div>
@endsection