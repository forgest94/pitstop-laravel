@extends('layouts.template')
@section('content')
    <?
    $sliders = json_decode($page->sliders);
    ?>
    <div class="container page__heading">
        <h1>{{ $page->page_title }}</h1>
    </div>
    <div class="about">
        <div class="about__inner container">
            <div class="about__column">
                <div class="about__img about__img_big">
                    <img src="{!! Voyager::image($sliders[0]) !!}">
                </div>
                <div class="about__column about__column_sub-1">
                    @foreach ($sliders as $key => $item)
                        @if($key != 0)
                            <div class="about__img">
                                <img src="{!! Voyager::image($item) !!}">
                            </div>
                        @endif
                    @endforeach
                    {!! $page->body !!}
                </div>
            </div>
            <div class="about__column">
                <div class="about__img">
                    <img src="{!! Voyager::image($page->image) !!}">
                </div>
                <form class="about__column about__column_sub-2 about__form" action="" method="POST">
                    <h2>Обратная связь</h2>
                    <div class="input input_part-2">
                        <label for="name" class="input__name">Имя <span class="red">*</span></label>
                        <input type="text" placeholder="Имя" id="name" name="name" tabindex="0" class="req inp_ab"
                               value="@auth{{ Auth::user()->name }}@endif">
                    </div>
                    <div class="input input_part-2">
                        <label for="phone" class="input__name">Телефон <span class="red">*</span></label>
                        <input type="phone" placeholder="Телефон" id="phone" name="phone" tabindex="2"
                               class="req inp_ab" value="@auth{{ Auth::user()->phone }}@endif"
                               data-mask="+7 (000) 000 00-00">
                    </div>
                    <div class="input input_wide input_textarea">
                        <label for="comment" class="input__name">Текст</label>
                        <textarea id="comment" placeholder="Текст" name="comment" tabindex="3"></textarea>
                    </div>
                    <div>
                        <input type="checkbox" class="checkbox req" name="nda" id="checknda" checked/>
                        <label for="checknda" class="form-block__nda">
                            Нажимая, я соглашаюсь на обработку персональных
                            данных
                        </label>
                    </div>
                    {{ csrf_field() }}
                    <a href="javascript:void(0);" class="btn about__btn" data-click="form_about">Отправить</a>
                    <span class="mess_info"></span>
                </form>
            </div>

        </div>
    </div>
    <div class="map-about">
        <a class="dg-widget-link"
           href="http://2gis.ru/barnaul/profiles/563478235304372,70000001035080783,70000001031150812,70000001021487798,70000001033608358,70000001018399154,70000001007335520,70000001006480112/center/83.71444702148439,53.34809202306839/zoom/13?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
            на карте Барнаула</a>
        <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
        <script charset="utf-8">new DGWidgetLoader({
                "width": 900,
                "height": 600,
                "borderColor": "#a3a3a3",
                "pos": {"lat": 53.34809202306839, "lon": 83.71444702148439, "zoom": 13},
                "opt": {"city": "barnaul"},
                "org": [{"id": "563478235304372"}, {"id": "70000001035080783"}, {"id": "70000001031150812"}, {"id": "70000001021487798"}, {"id": "70000001033608358"}, {"id": "70000001018399154"}, {"id": "70000001007335520"}, {"id": "70000001006480112"}]
            });</script>
        <noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в
            настройках вашего браузера.
        </noscript>
    </div>

    @include('partials.city_list_about')
    @include('partials.advantages')
    @include('partials.partners')
@endsection
