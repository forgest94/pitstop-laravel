@extends('layouts.template')
@section('content')
<?
$sliders = json_decode($page->sliders);
?>
    <div class="c-welcome c-welcome_landing">
        <div class="c-welcome__inner container">
            <div class="c-welcome__text-wrap">
                <h1>{{ $page->page_title }}</h1>
                {!! $page->body !!}
            </div>
            <div class="c-welcome__call-block">
                <div class="c-welcome__call-inner">
                    <div class="c-welcome__call-text">
                        Вы можете подать заявку по телефону, заказав обратный звонок, либо самостоятельно ввести данные в калькулятор
                    </div>
                    <a href="javascript:void(0);" class="btn c-welcome__call-link js-modal-open" data-modal-type="phone">Заказать звонок</a>
                </div>
                <div class="c-welcome__additional-img">
                    <img src="{{ $icon }}">
                </div>
            </div>
        </div>
        @if($calc != 'doc' && $calc != 'kbm')
        <div class="container screen screen_active js-screen" data-type-screen="{{ $calc }}" data-id-screen="1">
            <div class="c-welcome__buttons container">
                <a href="javascript:void(0);" class="c-welcome__btn btn js-screen-link" data-type-screen="{{ $calc }}"
                   data-id-screen="2">{{ $text_button }}</a>
            </div>
        </div>
        @endif
        @include('partials.calc_'.$calc)
        @empty(!$sliders)
            <div class="c-welcome__diag-images container">
                @foreach ($sliders as $item)
                    <img src="{!! Voyager::image($item) !!}" alt="">
                @endforeach
            </div>
        @endempty
    </div>

    <div class="answers">
        <div class="answers__inner container">
            <p class="answers__text">У нас есть ответы на все ваши вопросы!</p>
            <a href="/questions" class="answers__btn btn">Вопросы и ответы</a>
        </div>
    </div>
    @include('partials.advantages')
    @include('partials.reviews')
<div class="modal js-modal" data-modal-type="phone" data-izimodal-width="600px">
    <div class="modal__inner">
        <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
        <form class="screen screen_active js-screen" id="revForm">
            @csrf
            <div class="modal__headline">Заказать звонок</div>
            <div class="row">
                <div class="input input_wide">
                    <label for="nameReview" class="input__name">Ваше имя <span class="red">*</span></label>
                    <input name="name" placeholder="Ваше имя" type="text" id="namePage" class="req" tabindex="0" value="@auth{{ Auth::user()->name }}@endif">
                </div>
            </div>
            <div class="row">
                    <div class="input input_wide">
                        <label for="nameReview" class="input__name">Телефон <span class="red">*</span></label>
                        <input name="phone" placeholder="Телефон" type="text" id="phonePage" class="req" tabindex="0" value="@auth{{ Auth::user()->phone }}@endif" data-mask="+7 (000) 000 00-00">
                    </div>
            </div>
            <div>
                <input type="checkbox" class="checkbox-h" name="nda" id="checknda" checked>
                <label for="checknda" class="form-block__nda">
                    Нажимая, я соглашаюсь на обработку персональных
                    данных
                </label>
            </div>
            <input type="hidden" name="page" value="{{ $page->page_title }}">
            <div class="row row_center row_m-top">
                <a href="javascript:void(0);" class="btn btn_big" tabindex="0" data-click="toPhone">Отправить</a>
            </div>
            <span class="mess_info"></span>
        </form>
    </div>
</div>
@endsection
