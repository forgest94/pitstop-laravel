@extends('layouts.template')
@section('content')
    <div class="personal-page js-personal-page">
        <div class="personal-page__container container">
            <div class="personal-page__user-name">Здравствуйте, {{ Auth::user()->name }}!</div>
            <div class="personal-page__user-group">Администратор</div>

            <div class="personal-page__tabs">
                <div class="personal-page__tabs-item active"><span>Осаго</span></div>
                <div class="personal-page__tabs-item"><span>Клещ</span></div>
                <div class="personal-page__tabs-item"><span>Кроссы</span></div>
            </div>

            <div class="personal-page__main">
                <ul class="personal-page__menu">
                    <li class="personal-page__menu-item">
                        <a href="javascript:void(0)" class="personal-page__menu-link personal-page__menu-link_stat">Статистика</a>
                    </li>
                    <li class="personal-page__menu-item personal-page__menu-item_agent">
                        <a href="javascript:void(0)"
                           class="personal-page__menu-link personal-page__menu-link_agent">Агенты</a>
                        <span class="personal-page__add-agent"></span>
                    </li>
                    <li class="personal-page__menu-item">
                        <a href="javascript:void(0)"
                           class="personal-page__menu-link personal-page__menu-link_pay">Выплаты</a>
                    </li>
                    <li class="personal-page__menu-item">
                        <a href="javascript:void(0)" class="personal-page__menu-link personal-page__menu-link_company">Компании</a>
                    </li>
                    <li class="personal-page__menu-item">
                        <a href="{{ route('logout') }}" class="personal-page__menu-link personal-page__menu-link_exit"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
                <div class="personal-page__info personal-page__info_stat">
                    <div class="personal-page__info-title">Статистика продаж и прибыли</div>
                    <div class="personal-page__date-range">
                        <div class="personal-page__option personal-page__option_date input">
                            <span class="personal-page__label">Сортировка по дате</span>
                            <label for="pp-date-sort" class="personal-page__date-range-label">
                                @empty($_REQUEST["datestart"])
                                    <input id="pp-date-sort" type="text" class="js-datepicker"
                                           data-mask="00.00.0000" data-range="true" data-multiple-dates-separator=" - "
                                           data-maxdate="{{ date('Y-m-d') }}" data-view="years" value="">
                                @else
                                    <input id="pp-date-sort" type="text" class="js-datepicker"
                                           data-mask="00.00.0000" data-range="true" data-multiple-dates-separator=" - "
                                           data-maxdate="{{ date('Y-m-d') }}" data-view="years"
                                           value="{{ $_REQUEST["datestart"].'-'.$_REQUEST["dateend"] }}">
                                @endempty
                            </label>
                        </div>
                        <div class="personal-page__date-range-clear js-clear-data" data-picker="pp-date-sort">Сбросить
                        </div>
                    </div>
                    <div class="row personal-page__row-top">
                        <div class="personal-page__info-option">
                            <div class="personal-page__info-option-title">Доход с клиентов</div>
                            <div
                                class="personal-page__info-option-descr rub">{{ number_format($sumPriceOrders, 2, '.', '') }}</div>
                        </div>
                        <div class="personal-page__info-option">
                            <div class="personal-page__info-option-title">Продано полисов</div>
                            <div class="personal-page__info-option-descr ">{{ $sumPayOrders }}</div>
                        </div>
                    </div>
                    <div class="row personal-page__row-top">
                        <div class="personal-page__info-option">
                            <div class="personal-page__info-option-title">Доход с агентов</div>
                            <div class="personal-page__info-option-descr rub">0</div>
                        </div>
                    </div>
                    <div class="row personal-page__row-top">
                        <div class="personal-page__info-title">Детализация дохода</div>
                    </div>
                    <div class="row personal-page__stat-filter">
                        <div class="personal-page__tabs personal-page__tabs_stat-filter">
                            <?/*<div class="personal-page__tabs-item" data-tab-header="1" data-tab-id="stat-filter">
                                <span>Агенты</span></div>*/?>
                            <div class="personal-page__tabs-item active" data-tab-header="2" data-tab-id="stat-filter">
                                <span>Клиенты</span>
                            </div>
                        </div>

                        <div class="personal-page__option personal-page__option_select-sm input_select">
                            <span class="personal-page__label">На странице</span>
                            <select class="js-select js-items-views">
                                <option value="10" {{ $count_elem==10?'selected':'' }}>10</option>
                                <option value="30" {{ $count_elem==30?'selected':'' }}>30</option>
                                <option value="50" {{ $count_elem==50?'selected':'' }}>50</option>
                            </select>
                        </div>
                    </div>
                    <div class="personal-page__tabs-content">
                        <?/*<div class="personal-page__tab-content active" data-tab-content="1" data-tab-id="stat-filter">
                            <div class="personal-page-table personal-page-table_five-column">
                                <div class="personal-page-table__header">
                                    <div class="tr">
                                        <div class="td">Имя агента</div>
                                        <div class="td">Город</div>
                                        <div class="td">Телефон, e-mail</div>
                                        <div class="td">Полисов продано</div>
                                        <div class="td">Доход</div>
                                    </div>
                                </div>
                                <div class="personal-page-table__body">
                                    <div class="tr">
                                        <div data-text="Имя агента" class="td">Альберт</div>
                                        <div data-text="Город" class="td">Барнаул</div>
                                        <div data-text="Телефон, e-mail" class="td">
                                            <div class="personal-page-table__td-wrapper">
                                                <span>+7 (999) 321 21-22</span>
                                                <span>albert@yandex.ru</span>
                                            </div>
                                        </div>
                                        <div data-text="Полисов продано" class="td">151</div>
                                        <div data-text="Доход" class="td">15 000 ₽</div>
                                    </div>
                                    <div class="tr">
                                        <div data-text="Имя агента" class="td">Владислав</div>
                                        <div data-text="Город" class="td">Санкт-Петербург</div>
                                        <div data-text="Телефон, e-mail" class="td">
                                            <div class="personal-page-table__td-wrapper">
                                                <span>+7 (999) 321 21-22</span>
                                                <span>albert@yandex.ru</span>
                                            </div>
                                        </div>
                                        <div data-text="Полисов продано" class="td">151</div>
                                        <div data-text="Доход" class="td">152 000 ₽</div>
                                    </div>
                                </div>
                            </div>
                        </div>*/?>
                        <div class="personal-page__tab-content active" data-tab-content="2" data-tab-id="stat-filter">
                            <div class="personal-page-table personal-page-table_eight-column">
                                <div class="personal-page-table__header">
                                    <div class="tr">
                                        <div class="td">Полис</div>
                                        <div class="td">Ф.И.О. клиента</div>
                                        <div class="td">Город</div>
                                        <div class="td">Телефон, e-mail</div>
                                        <div class="td">Агент</div>
                                        <div class="td">Компания</div>
                                        <div class="td">Стоимость</div>
                                        <div class="td"></div>
                                    </div>
                                </div>
                                <div class="personal-page-table__body">
                                    @if($orders->count())
                                        @foreach($orders as $order)
                                            @php ($insurace_order = $order->Allinsurance()->first())
                                            <div class="tr">
                                                <div data-text="Полис" class="td">{{ $order->polis_number }}</div>
                                                <div data-text="Ф.И.О. клиента"
                                                     class="td">{{ $order->LastName_Owner.' '.$order->FirstName_Owner.' '.$order->MiddleName_Owner }}</div>
                                                <div data-text="Город" class="td">{{ $order->CityOfUse }}</div>
                                                <div data-text="Телефон, e-mail" class="td">
                                                    <div class="personal-page-table__td-wrapper">
                                                        @empty($order->phone)
                                                            <span>{{ $order->UserOrder[0]->phone }}</span>
                                                        @else
                                                            <span>{{ $order->phone }}</span>
                                                        @endempty
                                                        @empty($order->mail)
                                                            <span>{{ $order->UserOrder[0]->email }}</span>
                                                        @else
                                                            <span>{{ $order->mail }}</span>
                                                        @endempty
                                                    </div>
                                                </div>
                                                <div data-text="Агент" class="td"></div>
                                                <div data-text="Компания"
                                                     class="td">{{ !empty($insurace_order->name)?$insurace_order->name:'' }}</div>
                                                <div data-text="Стоимость"
                                                     class="td">{{ $order->price?$order->price:0 }}</div>
                                                <div data-text="" class="td">
                                                    <a href="javascript:void(0)" class="js-modal-open"
                                                       data-modal-type="personal-page_stat_{{ $order->id }}">Подробно</a>
                                                </div>
                                            </div>
                                            <div class="modal modal_personal-page-stat js-modal"
                                                 data-modal-type="personal-page_stat_{{ $order->id }}"
                                                 data-izimodal-width="1170px">
                                                <div class="modal__inner">
                                                    <a data-izimodal-close="" href="javascript:void(0);"
                                                       class="modal__close"></a>
                                                    <div class="row modal__row">
                                                        <div class="modal__title">
                                                            {{ $order->polis_number }}
                                                            | {{ !empty($insurace_order->name)?$insurace_order->name:'' }}
                                                        </div>
                                                        <div class="modal__price">
                                                            <span class="modal__price-text">Стоимость</span>
                                                            <span
                                                                class="modal__price-number">{{ $order->price?$order->price:0 }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="modal__content-info">
                                                        <div class="modal__helper-wrapper">
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Марка, модель</span>
                                                                <span
                                                                    class="modal__option-descr">{{ explode('|', $order->AutoOrder[0]->mark)[1] }} {{ explode('|', $order->AutoOrder[0]->model)[1] }}</span>
                                                            </div>
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Год выпуска</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->AutoOrder[0]->year }}</span>
                                                            </div>
                                                            @if($order->AutoOrder[0]->vin)
                                                                <div class="modal__option">
                                                                    <span class="modal__option-title">Vin</span>
                                                                    <span
                                                                        class="modal__option-descr">{{ $order->AutoOrder[0]->vin }}</span>
                                                                </div>
                                                            @elseif($order->AutoOrder[0]->number_bodywork)
                                                                <div class="modal__option">
                                                                    <span
                                                                        class="modal__option-title">Номер кузова</span>
                                                                    <span
                                                                        class="modal__option-descr">{{ $order->AutoOrder[0]->number_bodywork }}</span>
                                                                </div>
                                                            @elseif($order->AutoOrder[0]->number_chassis)
                                                                <div class="modal__option">
                                                                    <span class="modal__option-title">Шасси</span>
                                                                    <span
                                                                        class="modal__option-descr">{{ $order->AutoOrder[0]->number_chassis }}</span>
                                                                </div>
                                                            @endif

                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Мощность (л.с)</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->AutoOrder[0]->power }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="modal__helper-wrapper">
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Регион</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->RegionOfUse }}</span>
                                                            </div>
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Нас.пункт</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->CityOfUse }}</span>
                                                            </div>
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Ф</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->LastName_Owner }}</span>
                                                            </div>
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">И</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->FirstName_Owner }}</span>
                                                            </div>
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">О</span>
                                                                <span
                                                                    class="modal__option-descr">{{ $order->MiddleName_Owner }}</span>
                                                            </div>
                                                            <div class="modal__option">
                                                                <span class="modal__option-title">Дата рождения</span>
                                                                <span
                                                                    class="modal__option-descr">{{ Date::parse($order->BirthDate_Owner)->format('d.m.Y') }}</span>
                                                            </div>
                                                            @if($order->PeronsOrder->count())
                                                                <div class="modal__option">
                                                                    <span
                                                                        class="modal__option-title">Серия, номер ВУ</span>
                                                                    <span
                                                                        class="modal__option-descr">{{ $order->PeronsOrder[0]->docser_numb }}</span>
                                                                </div>
                                                                <div class="modal__option">
                                                                <span
                                                                    class="modal__option-title">Дата отсчета стажа </span>
                                                                    <span
                                                                        class="modal__option-descr">{{ Date::parse($order->ExperienceDate)->format('d.m.Y') }}</span>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="modal__download">
                                                            <a href="/docs/order/{{ $order->id }}" class="btn"
                                                               target="_blank">Скачать</a>
                                                        </div>
                                                    </div>
                                                    <?/*<div class="modal__ticket">
                                                        <div class="modal__ticket-title">Квитанция</div>
                                                        <div class="modal__ticket-list">
                                                            <div class="modal__ticket-item">
                                                                <div class="modal__ticket-left">
                                                                    <div class="modal__ticket-name">
                                                                        Квитанция_об_оплате_21.02.2020.jpg
                                                                    </div>
                                                                    <div class="modal__ticket-remove">Удалить</div>
                                                                </div>
                                                                <div class="modal__ticket-right">
                                                                    <a href="javascript:void(0)"
                                                                       class="btn btn_light modal__btn">Открыть</a>
                                                                    <a href="javascript:void(0)"
                                                                       class="btn btn_light modal__btn">Скачать</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal__attach">
                                                        <a href="javascript:void(0)" class="btn">Прикрепить
                                                            квитанцию</a>
                                                    </div>*/?>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ $orders->links('partials.nav_panel') }}
                </div>
            </div>
            <div class="personal-page__download">
                <a href="/docs/all_orders" class="btn" target="_blank">Скачать (.xls)</a>
            </div>
        </div>
    </div>
@endsection
