<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $page->page_title }}</title>
    <script>
        var modernBrowser = (
            'fetch' in window &&
            'assign' in Object
        );

        if (!modernBrowser) {
            var scriptElement = document.createElement('script');
            scriptElement.async = false;
            scriptElement.src = '<?=config("app.dir_static");?>/polyfills.bundle.js';
            document.head.appendChild(scriptElement);
        }
    </script>
    @php
        // TODO:: вынести в config
        $assetPath ='/html_git/police-please';
    @endphp
    <link href="{{ $assetPath }}/js/vendors.css" rel="stylesheet">
    <link href="{{ $assetPath }}/css/style.css" rel="stylesheet">
</head>
<body class="page">
@if(!empty($slug) && $slug == 'calc')
    <div class="page__wrapper">
        <header class="header js-header">
            <div class="header__logo">
                <div class="logo">
                    <img src="{{ $assetPath }}/img/logo.svg" alt="Police please">
                </div>
                <div class="header__logo-title">Страховой маркетплейс</div>
            </div>
        </header>
        <div class="cashback">
            <div class="cashback__desktop">
                <div class="cashback__inner container">
                    <img class="cashback__img" src="{{ $assetPath }}/img/cashback.svg" alt="">
                </div>
            </div>
        </div>
        @include('partials.calcPolisePlease')
    </div>
@else
    <div class="page__wrapper">
        <header class="header header_main">
            <div class="header__logo">
                <div class="logo">
                    <img src="{{ $assetPath }}/img/logo.svg" alt="Police please">
                </div>
                <div class="header__logo-title">Страховой маркетплейс</div>
            </div>
            <div class="header-qr">
                <div class="qr-code">
                    <img src="{{ $assetPath }}/img/qr-code.png" alt="">
                </div>
            </div>
        </header>
    </div>
    @if(!empty($partners))
        <section class="partners js-partners">
            <div class="page__wrapper">
                <div class="partners__list js-partners-slider">
                    @foreach($partners as $partner)
                        <div class="partners__item">
                            <img src="{!! Voyager::image($partner->img) !!}" alt=""/>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <div class="page__wrapper">
        <div class="main-buttons">
            <a class="btn btn_bigger" href="/police-please/calc">Купить ОСАГО</a>
            <a class="btn btn_bigger no-elem" href="javascript:void(0);">Страхование от клеща</a>
        </div>
    </div>
    @if(!empty($sliders))
        <section class="footer-slider js-footer-slider">
            <div class="footer-slider__list js-footer-slider-bg">
                @foreach($sliders as $slider)
                    <div class="footer-slider__list-item" style="background-image: url('{!! Voyager::image($slider->image) !!}')">
                        <div class="footer-slider__list-title">
                            {{ $slider->text }}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="footer-slider__nav">
                <div class="footer-slider__nav-list js-footer-slider-nav">
                    @foreach($sliders as $slider)
                        <div class="footer-slider__nav-item" style="background-image: url('{{Voyager::image($slider->thumbnail('medium'))}}')"></div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endif
<div class="modal js-modal" data-modal-type="phone" data-izimodal-width="600px">
    <div class="modal__inner">
        <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
        <form class="screen screen_active js-screen" id="revForm">
            @csrf
            <div class="modal__headline">Заказать звонок</div>
            <div class="row">
                <div class="input input_wide">
                    <label for="nameReview" class="input__name">Ваше имя <span class="red">*</span></label>
                    <input placeholder="Ваше имя" name="name" type="text" id="namePage" class="req" tabindex="0"
                           value="@auth{{ Auth::user()->name }}@endif">
                </div>
            </div>
            <div class="row">
                <div class="input input_wide">
                    <label for="nameReview" class="input__name">Телефон <span class="red">*</span></label>
                    <input placeholder="Телефон" name="phone" type="text" id="phonePage" class="req" tabindex="0"
                           value="@auth{{ Auth::user()->phone }}@endif" data-mask="+7 (000) 000 00-00">
                </div>
            </div>
            <div>
                <input type="checkbox" class="checkbox-h" name="nda" id="checknda" checked>
                <label for="checknda" class="form-block__nda">
                    Нажимая, я соглашаюсь на обработку персональных
                    данных
                </label>
            </div>
            <input type="hidden" name="page" value="{{ $page->page_title }}">
            <div class="row row_center row_m-top">
                <a href="javascript:void(0);" class="btn btn_big" tabindex="0" data-click="toPhone">Отправить</a>
            </div>
            <span class="mess_info"></span>
        </form>
    </div>
</div>
@empty(!$wiki)
    <div class="modal js-modal" data-modal-type="wiki" data-izimodal-width="1000px" data-izimodal-height="800">
        <div class="modal__inner modal__inner_no-paddings">
            <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
            <div class="modal__wiki-wrap">
                <div class="modal__wiki-links js-wiki js-modal-double">
                    <h3 class="modal__wiki-heading">Как заполнить поле</h3>
                    @foreach ($wiki as $key => $item)
                        @if($key == 0)
                            <a href="javascript:void(0);"
                               class="modal__wiki-link modal__wiki-link_active js-link js-modal-screen"
                               data-type-screen="wiki"
                               data-id-screen="{{ $item->slug }}">{{ $item->name }}</a>
                        @else
                            <a href="javascript:void(0);" class="modal__wiki-link js-link js-modal-screen"
                               data-type-screen="wiki"
                               data-id-screen="{{ $item->slug }}">{{ $item->name }}</a>
                        @endif
                    @endforeach
                </div>
                <div class="modal__wiki-content js-modal-content ">
                    @foreach ($wiki as $key => $item)
                        @if($key == 0)
                            <div class="screen screen_active js-screen" data-type-screen="wiki"
                                 data-id-screen="{{ $item->slug }}">
                                @else
                                    <div class="screen js-screen" data-type-screen="wiki"
                                         data-id-screen="{{ $item->slug }}">
                                        @endif
                                        <h3>{{ $item->name }}</h3>
                                        {!! $item->text !!}
                                    </div>
                                    @endforeach
                            </div>
                </div>
            </div>
        </div>
    </div>
@endempty
<div class="modal js-modal" data-modal-type="result-table" data-izimodal-width="1000px">
    <div class="modal__inner">
        <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
        <div class="result-table-wrapper">
            <div class="result-table-wrapper__header">
                <span>Расчет</span>
            </div>
            <table class="osago-table-result">
                <tbody class="osago-table-result__body">
                <tr>
                    <td>Параметры</td>
                    <td>Значение</td>
                </tr>
                <tr>
                    <td>Базовый коэфициент</td>
                    <td data-coefficient="tb"></td>
                </tr>
                <tr>
                    <td>Коэфициент бонус-малус</td>
                    <td data-coefficient="kbm"></td>
                </tr>
                <tr>
                    <td>Коэфициент возраста и стажа водителей</td>
                    <td data-coefficient="kvs"></td>
                </tr>
                <tr>
                    <td>Коэфициент мощности</td>
                    <td data-coefficient="km"></td>
                </tr>
                <tr>
                    <td>Коэфициент нарушений</td>
                    <td data-coefficient="kn"></td>
                </tr>
                <tr>
                    <td>Коэфициент ограничения допуска</td>
                    <td data-coefficient="ko"></td>
                </tr>
                <tr>
                    <td>Коэфициент периода использования</td>
                    <td data-coefficient="ks"></td>
                </tr>
                <tr>
                    <td>Коэфициент прицепа</td>
                    <td data-coefficient="kpr"></td>
                </tr>
                <tr>
                    <td>Коэфициент срока страхования</td>
                    <td data-coefficient="kp"></td>
                </tr>
                <tr>
                    <td>Коэфициент территории</td>
                    <td data-coefficient="kt"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src="{!! $assetPath !!}/js/vendors.bundle.js"></script>
<script type="text/javascript" src="{!! $assetPath !!}/app.bundle.js"></script>
<script type="text/javascript" src="{!! $assetPath !!}/css/style.bundle.js"></script>
</body>
</html>
