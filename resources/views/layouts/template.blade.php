<?
$index = '';
$panel = '';
$title = '';
$description = '';
$keywords = '';
if (!empty($page) && is_object($page)) {
    switch ($page->slug) {
        case 'index':
            $index = 'Y';
            break;
        case 'panel':
            $panel = 'Y';
            break;
    }
    $title = $page->title;
    if (!empty($page->meta_description))
        $description = $page->meta_description;
    if (!empty($page->meta_keywords))
        $keywords = $page->meta_keywords;
} else {
    $page = '';
}
?>
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(!empty($cityInfo))
        <title>{{ $cityInfo->title }} | {{ $title }}</title>
        <meta name="description" content="{{ $description }}, {{ $cityInfo->name }}">
    @else
        <title>{{ $title }}</title>
        <meta name="description" content="{{ $description }}">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        var modernBrowser = (
            'fetch' in window &&
            'assign' in Object
        );

        if (!modernBrowser) {
            var scriptElement = document.createElement('script');
            scriptElement.async = false;
            scriptElement.src = '<?=config("app.dir_static");?>/polyfills.bundle.js';
            document.head.appendChild(scriptElement);
        }
    </script>
    <link href="{!! config('app.dir_static') !!}/js/vendors.css?v={!! config('app.version_script') !!}" rel="stylesheet">
    <link href="{!! config('app.dir_static') !!}/css/style.css?v={!! config('app.version_script') !!}" rel="stylesheet">
    <link href="/css/lets.css?v={!! config('app.version_script') !!}" rel="stylesheet">
    <?
    $urlParts = parse_url($_SERVER["REQUEST_URI"]);
    $protocol = 'http';
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
        $protocol = 'https';
    }
    echo '<link rel="canonical" href="' . $protocol . '://' . $_SERVER["HTTP_HOST"] . $urlParts["path"] . '">';
    ?>
    <script src="https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script src="//code-ya.jivosite.com/widget/4EHZKwS4nX" async></script>
</head>
<body class="page">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55614871, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/55614871" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<div class="page__wrapper" id="allPage">
    @if($index)
        <header class="header header_main js-header js-p-scene-wrap">
            <div class="header__background js-p-scene" data-speed="1"></div>
            @else
                <header class="header js-header">
                    @endif
                    <div class="header__wrap">
                        <div class="header__part header__part_welcome">
                            <div class="container header__inner header__inner_welcome">
                                <a href="javascript:void(0);" class="header__hamburger js-hamburger">
                                    <div class="header__hamburger-inner">
                                        <span class="line"></span>
                                        <span class="line"></span>
                                        <span class="line"></span>
                                    </div>
                                </a>
                                <a href="/" class="header__logo"></a>
                                <div class="header__slogan"><strong>СТРАХОВОЙ МАРКЕТПЛЕЙС</strong></div>
                                <div class="header__blocks-wrap header__blocks-wrap_mobile">
                                    <div
                                        class="header__blocks-wrap header__blocks-wrap_desktop header__blocks-wrap_phones">
                                        {!! setting('site.phone_top') !!}
                                    </div>
                                    @if (Route::has('login'))
                                        <div class="header__blocks-wrap header__blocks-wrap_desktop">
                                            <div class="header__block">
                                                @auth
                                                    @if($panel)
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                                           class="header__link header__link_personal">
                                                            <span class="header__link-text header__personal_subtitle">Выйти</span>
                                                        </a>
                                                    @else
                                                        <a href="{{ url('panel') }}"
                                                           class="header__link header__link_personal">
                                                            <span
                                                                class="header__link-text header__personal_title">{{ Auth::user()->name }}</span>
                                                        </a>
                                                    @endif

                                                @else
                                                    <a href="javascript:void(0);" data-modal-type="login"
                                                       class="header__link header__link_personal js-modal-open">
                                                        <span
                                                            class="header__link-text header__personal_subtitle">Войти</span>
                                                    </a>
                                                    <?/*@if (Route::has('register'))
                                <a href="{{ route('register') }}">Регистрация</a>{{ route('login') }}
                            @endif*/?>
                                                @endauth
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{ menu('top_menu','partials.topMenu', array("page"=>$page)) }}
                    </div>
                    @if($index)
                        <div class="container header__part header__part_main-title">
                            <h1>Купите ОСАГО онлайн</h1>
                            <p>Оформляйте полис ОСАГО без комиссий и переплат посредникам!</p>
                            <a href="/insurance" class="btn">Купить</a>
                        </div>
                        <div class="container header__part header__part_main-cards">
                            <?/*<a href="/gbdd" class="header__card no-elem">
                    <h3>Документы ГИБДД</h3>
                    <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего
                        сердца. Я совсем один и блаженствую в здешнем краю</p>
                    <span class="btn">Подробнее</span>
                </a>
                <a href="/kbm" class="header__card no-elem">
                    <h3>КБМ</h3>
                    <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего
                        сердца. Я совсем один и блаженствую в здешнем краю</p>
                    <span class="btn">Подробнее</span>
                </a>
                <a href="/inspection" class="header__card no-elem">
                    <h3>Техосмотр</h3>
                    <p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего
                        сердца. Я совсем один и блаженствую в здешнем краю</p>
                    <span class="btn">Подробнее</span>
                </a>*/?>
                        </div>
                    @endif

                </header>
                @yield('content')
                <a href="javascript:void(0);" class="topper js-to-top-link"></a>
                <footer class="footer">
                    <div class="footer__top">
                        <div class="footer__inner container">
                            <div class="footer__column footer__column_start">
                                <div class="footer__logo"></div>
                                <div class="footer__link-wrap">
                                    <a href="javascript:void(0);" class="footer__link footer__link_address">
                                        {!! setting('site.adres') !!}
                                    </a>
                                    <a href="mailto:{!! setting('site.mail') !!}" class="footer__link footer__link_email">
                                        {!! setting('site.mail') !!}
                                    </a>
                                    <a href="tel:{!! setting('site.phone') !!}" class="footer__link footer__link_phone">
                                        {!! setting('site.phone') !!}
                                    </a>
                                </div>
                            </div>
                            @widget('CitiesList')
                            <div class="footer__column footer__column_info">
                                {!! setting('site.soc') !!}
                                <p class="footer__seo-text">
                                    {!! setting('site.footer_text') !!}
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__inner footer__inner_bottom container">
                            <p class="footer__copyright footer__copyright_left">
                                <span class="footer__copyright-item">© {!! date('Y') !!} ООО "Полис-Плиз" ИНН 2222879932</span>
                                ОГРН 1192225034836
                            </p>
                            <div class="footer__pay">
                                <div class="footer__pay-item">
                                    <img src="/html_git/dist/img/p_1.png">
                                </div>
                                <div class="footer__pay-item">
                                    <img src="/html_git/dist/img/p_3.png">
                                </div>
                                <div class="footer__pay-item">
                                    <img src="/html_git/dist/img/p_2.png">
                                </div>
                                <div class="footer__pay-item">
                                    <img src="/html_git/dist/img/p_4.png">
                                </div>
                                <div class="footer__pay-item">
                                    <img src="/html_git/dist/img/p_5.png">
                                </div>
                            </div>
                            <p class="footer__copyright no-elem">Разработка сайта – <a href="https://letsrock.pro"
                                                                                       class="footer__letsrock"
                                                                                       target="_blank"></a></p>
                        </div>
                    </div>

                </footer>

</div>
@if(Route::has('login'))
    @auth
        <div class="modal js-modal" data-modal-type="removeDriver" data-izimodal-width="600px">
            <div class="modal__inner">
                <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                <div class="modal__headline">Вы действительно хотите удалить водителя?</div>
                <div class="row row_m-top row_center">
                    <a href="javascript:void(0);" data-type-screen="login" data-id-screen="2"
                       class="btn btn_light close_modal_f" data-deletePanel="auto" data-id="">Да</a>
                    <a href="javascript:void(0);" class="btn btn_big close_modal_f">Нет</a>
                </div>
            </div>
        </div>
        <div class="modal js-modal" data-modal-type="removePersons" data-izimodal-width="600px">
            <div class="modal__inner">
                <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                <div class="modal__headline">Вы действительно хотите удалить персону?</div>
                <div class="row row_m-top row_center">
                    <a href="javascript:void(0);" data-type-screen="login" data-id-screen="2"
                       class="btn btn_light close_modal_f" data-deletePanel="person" data-id="">Да</a>
                    <a href="javascript:void(0);" class="btn btn_big close_modal_f">Нет</a>
                </div>
            </div>
        </div>
    @else
        <div class="modal js-modal" data-modal-type="login" data-izimodal-width="480px">
            <div class="modal__inner">
                <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                <form class="screen screen_active js-screen js-register-screen" data-type-screen="login"
                      data-id-screen="1" method="POST"
                      action="{{ route('login') }}">
                    @csrf
                    <div class="modal__headline">Авторизация</div>
                    <div class="row">
                        <div class="input input_wide">
                            <label for="login" class="input__name">Email</label>
                            <div class="tooltip" data-tooltip="Введите Email указанный при регистрации"></div>
                            <input name="email" placeholder="Электронная почта" type="email" id="email" tabindex="0"
                                   required autocomplete="email" class="req">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input input_wide">
                            <label for="pass" class="input__name">Пароль</label>
                            <input name="password" placeholder="Пароль" type="password" id="password" tabindex="1"
                                   required autocomplete="current-password" class="req">
                        </div>
                    </div>
                    <div class="row row_space-beetween js-modal-register">
                        <a href="javascript:void(0);" class="link js-register-to" data-id-screen="4"
                           data-click="forgotPass">Забыли пароль?</a>
                    </div>
                    <div class="row row_space-beetween row_m-top js-modal-register">
                        <a href="javascript:void(0);" data-type-screen="login" data-id-screen="2"
                           class="btn js-register-to btn_light">Регистрация</a>
                        <a href="javascript:void(0);" class="btn btn_big" data-click="authForm">Вход</a>
                    </div>
                    <span class="mess_info"></span>
                </form>

                <form class="screen screen_active js-screen" data-type-screen="login" method="POST"
                      action="/registration/add">
                    @csrf
                    <div class="screen js-screen js-register-screen" data-id-screen="2" data-type-screen="register">
                        <div class="modal__headline">Регистрация</div>
                        <div class="row">
                            <div class="input input_wide">
                                <label for="nameReg" class="input__name">Ваше имя</label>
                                <input id="name" placeholder="Ваше имя" type="text" name="name" tabindex="0" required
                                       autocomplete="name" class="req">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input input_wide">
                                <label for="email" class="input__name">Email</label>
                                <div class="tooltip" data-tooltip="Email предназначен для входа в систему"></div>
                                <input name="email" placeholder="Электронная почта" type="email" id="emailNew"
                                       tabindex="0"
                                       required autocomplete="email" class="req">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input input_wide">
                                <label for="phoneNew" class="input__name">Телефон</label>
                                <div class="tooltip"
                                     data-tooltip="Телефон предназначен для подтверждения регистрации"></div>
                                <input name="phone" placeholder="Телефон" type="phone" id="phoneNew" tabindex="0"
                                       required autocomplete="phone" class="req" data-mask="+7 (000) 000 00-00">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input input_wide">
                                <label for="passReg" class="input__name">Пароль</label>
                                <div class="tooltip" data-tooltip="Введите пароль не меньше 8 символов"></div>
                                <input name="password" placeholder="Пароль" type="password" id="passwordNew"
                                       tabindex="1"
                                       required autocomplete="new-password" class="req">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input input_wide">
                                <label for="passRegRepeat" class="input__name">Повторите пароль</label>
                                <input name="password_confirmation" placeholder="Повторите пароль" type="password"
                                       id="password-confirm" tabindex="1" required autocomplete="new-password"
                                       class="req">
                            </div>
                        </div>
                        <div class="row row_space-beetween row_m-top js-modal-register">
                            <a href="javascript:void(0);" data-type-screen="login" data-id-screen="1"
                               class="btn btn_light js-register-to">Вход</a>
                            <a href="javascript:void(0);" data-type-screen="register" data-id-screen="3"
                               class="btn btn_big" data-click="firstReg">Регистрация</a>
                        </div>
                        <span class="mess_info"></span>
                    </div>

                    <div class="screen js-screen js-register-screen" data-type-screen="register" data-id-screen="3">
                        <div class="row">
                            <div class="input input_wide">
                                <label for="confirmCode1" class="input__name">Введите код который пришел вам по
                                    смс</label>
                                <input name="confirmCode" placeholder="Код подтверждения" type="number"
                                       id="confirmCode1" tabindex="1" required class="req">
                            </div>
                        </div>
                        <div class="row row_space-beetween row_m-top">
                            <a href="javascript:void(0);" class="btn btn_big" data-click="regForm">Подтвердить
                                регистрацию</a>
                        </div>
                    </div>
                </form>
                <form class="screen screen_active js-screen js-register-screen" data-type-screen="login" method="POST"
                      action="">
                    @csrf
                    <input type="hidden" name="hasAjax" value=""/>
                    <input type="hidden" name="phoneConfirm" value=""/>
                    <div class="screen js-screen js-register-screen" data-id-screen="4" data-type-screen="register">
                        <div class="modal__headline">Восстановление пароля</div>
                        <div class="password-confirm" data-step="1" data-step-active>
                            <div class="row">
                                <div class="input input_wide js-confirm-phone">
                                    <label for="phoneNewReset" class="input__name">Телефон</label>
                                    <div class="tooltip"
                                         data-tooltip="Для востаноновления пароля необходимо указать телефон"></div>
                                    <input name="phone" placeholder="Телефон" type="phone" id="phoneNewReset"
                                           tabindex="0"
                                           required autocomplete="phone" class="req" data-mask="+7 (000) 000 00-00">
                                </div>
                            </div>
                            <div class="row row_space-beetween row_m-top js-modal-register">
                                <a href="javascript:void(0);" class="btn btn_top btn_big js-reset-pass btn_full"
                                   data-click="reset-password-phone">Далее</a>
                                <a href="javascript:void(0);"
                                   class="btn_light js-go-login btn btn_big btn_full js-step-back">Назад</a>
                                <a href="javascript:void(0);" data-id-screen="1"
                                   class=" no-elem js-register-to btn btn_big btn_full js-first-step-back">Назад</a>
                            </div>
                            <span class="mess_info"></span>
                        </div>
                        <div class="password-confirm js-confirm-sms no-elem" data-step="2">
                            <div class="row">
                                <div class="input input_wide">
                                    <label for="resetConfirmCode" class="input__name">Введите код который пришел вам по
                                        смс</label>
                                    <input name="confirmCode" placeholder="Код подтверждения" type="number"
                                           id="resetConfirmCode" tabindex="1" required class="req">
                                </div>
                            </div>
                            <div class="row row_space-beetween row_m-top js-modal-register">
                                <a href="javascript:void(0);" class="btn btn_top btn_big js-reset-pass btn_full"
                                   data-click="reset-password-sms">Далее</a>
                                <a href="javascript:void(0);"
                                   class="btn_light js-go-login btn btn_big btn_full js-step-back">Назад</a>
                            </div>
                            <span class="mess_info"></span>
                        </div>
                        <div class="password-reset js-confirm-password no-elem" data-step="3">
                            <div class="row">
                                <div class="input input_wide">
                                    <label for="reset-password" class="input__name">Пароль</label>
                                    <div class="tooltip" data-tooltip="Введите новый пароль не меньше 8 символов"></div>
                                    <input name="password" placeholder="Пароль" type="password" id="reset-password"
                                           tabindex="1"
                                           required autocomplete="new-password" class="req">
                                </div>
                            </div>
                            <div class="row">
                                <div class="input input_wide">
                                    <label for="reset-passwordConfirm" class="input__name">Повторите новый
                                        пароль</label>
                                    <input name="password_confirmation" placeholder="Повторите пароль" type="password"
                                           id="reset-passwordConfirm" tabindex="1" required autocomplete="new-password"
                                           class="req">
                                </div>
                            </div>
                            <div class="row row_space-beetween row_m-top js-modal-register">
                                <a href="javascript:void(0);" class="btn btn_top btn_big js-reset-pass btn_full"
                                   data-click="reset-password-pass">Далее</a>
                                <a href="javascript:void(0);"
                                   class="btn_light js-go-login btn btn_big btn_full js-step-back">Назад</a>
                            </div>
                            <span class="mess_info"></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endauth
@endif
<script type="text/javascript"
        src="{!! config('app.dir_static') !!}/js/vendors.bundle.js?v={!! config('app.version_script') !!}"></script>
<script type="text/javascript" src="{!! config('app.dir_static') !!}/app.bundle.js?v={!! config('app.version_script') !!}"></script>
<script type="text/javascript"
        src="{!! config('app.dir_static') !!}/css/style.bundle.js?v={!! config('app.version_script') !!}"></script>
<script>
    /* getCity */
    window.onload = function () {
        function json(response) {
            return response.json()
        }

        ymaps.ready(function () {
            var geolocation = ymaps.geolocation;
            if (document.getElementById('city')) {
                document.getElementById('city').value = geolocation.city;
            }
            /*if(document.getElementById('CITY_SOB')){
                //document.getElementById('CITY_SOB').value = geolocation.city;
                    fetch('/ajax/kladr?type=city&query='+geolocation.city, {
                        method: 'get',
                        headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                        },
                    })
                    .then(json)
                    .then(function (data) {
                        if(data.length > 0){
                            document.getElementById('CITY_SOB_KLADR').value = data[0]["kladr"];
                            document.getElementById('CITY_SOB').value = geolocation.city;
                        }
                    });
            }
            if(document.getElementById('REGION_SOB')){
                //document.getElementById('REGION_SOB').value = geolocation.region;
                    fetch('/ajax/kladr?type=region&query='+geolocation.region, {
                        method: 'get',
                        headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                        },
                    })
                    .then(json)
                    .then(function (data) {
                        if(data.length > 0){
                            document.getElementById('REGION_SOB_KLADR').value = data[0]["kladr"];
                            document.getElementById('REGION_SOB').value = geolocation.region;
                        }
                    });
            }*/
            if (document.getElementById('PASSPORT_PATH_CITY_AUTO')) {
                document.getElementById('PASSPORT_PATH_CITY_AUTO').value = geolocation.city || "";
            }
            if (document.getElementById('CITY_B')) {
                document.getElementById('CITY_B').value = geolocation.city || "";
            }
        });
    }
</script>
</body>
<div class="assets" style="display: none;">
    <img src="{!! config('app.dir_static') !!}/img/p-slider-1.png">
    <img src="{!! config('app.dir_static') !!}/img/advantage-guarantee.png">
    <img src="{!! config('app.dir_static') !!}/img/Mikhail.jpg">
    <img src="{!! config('app.dir_static') !!}/img/seo.jpg">
</div>
</html>
