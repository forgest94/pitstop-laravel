@extends('layouts.template')
@section('content')
    <div class="personal-page js-personal-page">
        <div class="personal-page__container container">
            <div class="personal-page__user-name">Здравствуйте, {{ Auth::user()->name }}!</div>
            <div class="personal-page__user-group">Администратор</div>

            <div class="personal-page__tabs">
                <div class="personal-page__tabs-item active"><span>Осаго</span></div>
                <div class="personal-page__tabs-item"><span>Клещ</span></div>
                <div class="personal-page__tabs-item"><span>Кроссы</span></div>
            </div>

            <div class="personal-page__main">
                <ul class="personal-page__menu">
                    <li class="personal-page__menu-item">
                        <a href="{{ route('home', [], false) }}" class="personal-page__menu-link personal-page__menu-link_stat">Статистика</a>
                    </li>
                    <li class="personal-page__menu-item personal-page__menu-item_agent">
                        <a href="{{ route('panel.page.agents', [], false) }}"
                           class="personal-page__menu-link personal-page__menu-link_agent">Агенты</a>
                        <a class="personal-page__add-agent" href="{{ route('panel.agents.add', [], false) }}"></a>
                    </li>
                    <li class="personal-page__menu-item">
                        <a href="{{ route('panel.page.payouts', [], false) }}"
                           class="personal-page__menu-link personal-page__menu-link_pay">Выплаты</a>
                    </li>
                    <li class="personal-page__menu-item">
                        <a href="{{ route('panel.page.company', [], false) }}" class="personal-page__menu-link personal-page__menu-link_company">Компании</a>
                    </li>
                    <li class="personal-page__menu-item">
                        <a href="{{ route('logout') }}" class="personal-page__menu-link personal-page__menu-link_exit"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
                <div class="personal-page__info personal-page__info_stat">
                    @yield('content_admin')
                </div>
            </div>
        </div>
    </div>
@endsection
