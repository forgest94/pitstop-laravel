@extends('layouts.template')
@section('content')
        @if(!empty($page->page_title))
            <div class="container page__heading">
                    <h1>{{ $page->page_title }}</h1>
            </div>
        @endif
        @if(!empty($page->body))
            <div class="container">
                {!! $page->body !!}
            </div>
        @endif
@endsection