@empty(!$reviews)
        <div class="reviews">
            <div class="reviews__inner container">
                <h3 class="reviews__title section-title">Отзывы</h3>
                <div class="reviews__wrap">
                    @foreach ($reviews as $item)
                        <div class="review">
                            <div class="review__inner">
                                <p class="review__text">
                                    {{ $item->body }}
                                </p>
                                <div class="comment__stars" data-mark="{{ $item->rating }}">
                                    <i class="comment__star"></i>
                                    <i class="comment__star"></i>
                                    <i class="comment__star"></i>
                                    <i class="comment__star"></i>
                                    <i class="comment__star"></i>
                                </div>
                            </div>
                            <div class="review__under">
                                @empty($item->img)
                                    <div class="review__image" style="background-image: url('{!! config('app.dir_static') !!}/img/user.jpg')"></div>
                                @else
                                    <div class="review__image" style="background-image: url('{!! Voyager::image($item->img) !!}')"></div>
                                @endempty
                                <div class="review__text-wrap">
                                    <p class="review__name">{{ $item->name }}</p>
                                    <p class="review__city">г. {{ $item->city }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
@endempty
