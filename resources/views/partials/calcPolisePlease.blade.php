<div class="calc js-calc">
    <form class="calc__active-area container" data-slep="Y">
        <div id="block_auto_calc" class="js-slide-container-first">
            <div class="calc__inner container">
                <h1 class="calc__title">{{ $page->page_title }}</h1>
                <div class="calc__progressbar container">
                    <div class="calc__progressbar-points js-progressbar">
                        <?/*<a href="javascript:void(0);" class="calc__progressbar-point js-progressbar-point js-screen-link calc__progressbar-point_active"
                   data-type-screen="auto" data-id-screen="11" data-click="clearR"></a>*/?>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point auto_screen js-screen-lin calc__progressbar-point_active"
                           data-type-screen="auto" data-id-screen="1"></a>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point js-screen-link"
                           data-type-screen="auto" data-id-screen="3"></a>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point js-screen-link"
                           data-type-screen="auto" data-id-screen="4">
                        </a>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point js-screen-link"
                           data-type-screen="auto" data-id-screen="5" data-event="onCalc"></a>
                        <div class="calc__progressbar-loading js-loading"></div>
                    </div>
                    <div class="calc__progressbar-items">
                        <?/*<a href="javascript:void(0);" class="calc__progressbar-item calc__progressbar-item_active js-progressbar-item js-screen-link"
                   data-type-screen="auto" data-id-screen="11" data-click="clearR">
                    Поиск по гос номеру
                </a>*/?>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-item auto_screen js-progressbar-item js-screen-link calc__progressbar-item_active"
                           data-type-screen="auto" data-id-screen="1">
                            Данные об автомобиле
                        </a>
                        <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
                           data-type-screen="auto" data-id-screen="3">
                            Период и регион
                        </a>
                        <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
                           data-type-screen="auto" data-id-screen="4">
                            Данные о водителе
                        </a>
                        <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
                           data-type-screen="auto" data-id-screen="5" data-event="onCalc">
                            Результат
                        </a>
                    </div>
                </div>
            </div>
            <div class="screen screen_active js-screen" data-type-screen="auto" data-id-screen="11">
                <div class="calc__inner calc__auto-param">
                    <div class="calc__region">
                        <div class="row row_column-center">
                            @if(empty($slug))
                                <div class="alert alert_noicon alert_lg">
                                    <div class="alert__title">Вам потребуется информация из документов:</div>
                                    <ul class="alert__list">
                                        <li class="alert__item item-passport">
                                            <img src="/html_git/dist/img/pasport.jpg" alt="" class="img-document">
                                            <span class="text-document">Паспорт страхователя</span>
                                        </li>
                                        <li class="alert__item item-vu">
                                            <img src="/html_git/dist/img/VU.jpg" alt="" class="img-document">
                                            <span class="text-document">Водительское удостоверение (кто будет вписан в полис)</span>
                                        </li>
                                        <li class="alert__item item-pts">
                                            <img src="/html_git/dist/img/pts.jpg" alt="" class="img-document">
                                            <span class="text-document">ПТС или Свидетельство о регистрации ТС</span>
                                        </li>
                                        <li class="alert__item item-to">
                                            <img src="/html_git/dist/img/DC.jpg" alt="" class="img-document">
                                            <span class="text-document">Диагностическая карта ТО</span>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                            <span class="calc__text">Введите номер автомобиля, чтобы сервис заполнил данные о модификации</span>
                            <div class="car-number">
                        <span for="" class="car-number__label car-number__label_symbols">
                            <input data-input="0" data-textbig="Y" data-mask="R"
                                   class="car-number__input car-number__input_series-one" type="text" placeholder="A"
                                   maxlength="1" autocomplete="off">
                            <input data-input="1" data-textbig="Y" data-mask="000" inputmode="numeric"
                                   class="car-number__input car-number__input_number req" type="text" placeholder="777"
                                   maxlength="3" autocomplete="off">
                            <input data-input="2" data-textbig="Y" data-mask="RR"
                                   class="car-number__input car-number__input_series-two" type="text" placeholder="АА"
                                   maxlength="2" autocomplete="off">
                        </span>
                                <span class="car-number__label car-number__label_region" for="">
                            <input data-input="3" data-textbig="Y" data-mask="000" inputmode="numeric"
                                   class="car-number__input car-number__input_region" type="text" placeholder="22"
                                   maxlength="3">
                            <span class="car-number__region">
                                <span class="car-number__region-name car-number__region-name_rus">rus</span>
                            </span>
                        </span>
                            </div>
                            <div class="row_column calc__buttons-wrapper">
                                <a href="javascript:void(0);" class="btn btn_big btn_gos"
                                   data-click="searchGos">Найти</a>
                                <a href="javascript:void(0);"
                                   class="btn_column link-gos link-gos_underline btn_gos js-screen-link"
                                   data-type-screen="auto"
                                   data-id-screen="1" data-progressbar-id="1" data-prevpage="Y">Ввести данные
                                    вручную</a>
                            </div>
                        </div>
                        @if(!empty($slug) && $slug == 'calc')
                            <div class="row row_column-center">
                                <div class="calc__docs">
                                    <div class="calc__docs-title">
                                        Для оформления вам понадобятся документы:
                                    </div>
                                    <div class="calc__docs-list">
                                        <div class="calc__docs-item">
                                            <div class="calc__docs-item-img">
                                                <img src="{{ $assetPath }}/img/passport.png" alt="Паспорт">
                                            </div>
                                            <div class="calc__docs-item-title">
                                                Паспорт
                                            </div>
                                        </div>
                                        <div class="calc__docs-item">
                                            <div class="calc__docs-item-img">
                                                <img src="{{ $assetPath }}/img/registration-certificate.png"
                                                     alt="Свидетельство о регистрации">
                                            </div>
                                            <div class="calc__docs-item-title">
                                                Свидетельство о регистрации
                                            </div>
                                        </div>
                                        <div class="calc__docs-item">
                                            <div class="calc__docs-item-img">
                                                <img src="{{ $assetPath }}/img/driver-license.png"
                                                     alt="Водительское удостоверение">
                                            </div>
                                            <div class="calc__docs-item-title">
                                                Водительское удостоверение
                                            </div>
                                        </div>
                                        <div class="calc__docs-item">
                                            <div class="calc__docs-item-img">
                                                <img src="{{ $assetPath }}/img/inspection.png" alt="Техосмотр">
                                            </div>
                                            <div class="calc__docs-item-title">
                                                Техосмотр
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- <div class="row row_space-beetween">
                        <div class="input input_part-3" style="margin: 0 auto;">
                            <label for="gos_search" class="input__name">Гос. номер <span class="red">*</span></label>
                            <input type="text" class="req" name="gos_search" value="" placeholder="Пример: Х000ХХ00" data-mask="R000RR000"  data-textbig="Y">
                            <span class="info_auto"></span>
                            <br/>
                            <div>
                                <a href="javascript:void(0);" class="btn btn_big" data-click="searchGos">Найти</a>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="calc__category-list buttons-wrap">
                        <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link" data-type-screen="auto"
                           data-id-screen="1" data-progressbar-id="1" data-prevpage="Y">Пропустить</a>
                    </div> -->
                    <span class="mess_info"></span>
                </div>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="1">
                <div class="calc__inner calc__auto-param">
                    <div class="calc__category-list radi" data-errname="Выберите категорию">
                        @foreach($list_cat as $item)
                            <?
                            if ($item->id_category == 0) continue;
                            $azB = explode(' -', $item->name_category);
                            /*if($item->visible){
                                $disabled='';
                                $class_ds = '';
                                $data = '';
                                $class_data = '';
                            }else{
                                $disabled='disabled';
                                $class_ds = 'dis_admin';
                                $data = 'data-modal-type="phone"';
                                $class_data = 'js-modal-open';
                            }*/
                            ?>
                            @if($item->visible)
                                <label class="category" for="category_<?=$azB[0];?>">
                                    <input type="radio"
                                           name="category"
                                           id="category_<?=$azB[0];?>"
                                           value="{{ $item->id_category }}|{{ $item->name_category }}"
                                           class="category__radio-hidden"
                                           data-change="select_cat"
                                           data-type-screen="auto"
                                           data-id-screen="2"
                                           data-progressbar-id="2"
                                           data-avtocode="category">
                                    <div class="category__inner">
                                        <img src="{{ $item->img }}" alt="" class="category__img">
                                        <span class="category__name">Категория {{ $azB[0] }}</span>
                                    </div>
                                </label>
                            @else
                                <label class="category" for="category_<?=$azB[0];?>">
                                    <input type="radio"
                                           name="category"
                                           id="category_<?=$azB[0];?>"
                                           value="{{ $item->id_category }}|{{ $item->name_category }}"
                                           class="category__radio-hidden js-modal-open"
                                           data-modal-type="phone"
                                           data-avtocode="category">
                                    <div class="category__inner">
                                        <img src="{{ $item->img }}" alt="" class="category__img">
                                        <span class="category__name">Категория {{ $azB[0] }}</span>
                                    </div>
                                </label>
                            @endif
                        @endforeach
                    </div>
                    <?/*@if(empty($slug))*/?>
                    @empty(!$arCars)
                        <div class="row row_space-beetween">
                            <div class="input input_select input_part-2 auto_up_calc">
                                <p>Или выберете свой автомобиль: </p>
                                <select name="SLECT_AUTO" id="SLECT_AUTO" class="js-select" data-type-screen="auto"
                                        data-id-screen="3" data-progressbar-id="3">
                                    <option value="0">Выбрать</option>
                                    @foreach ($arCars as $itemCar)
                                        <option
                                                value="{{ $itemCar->id }}">{{ explode('|',$itemCar->mark)[1] }} {{ explode('|',$itemCar->model)[1] }} {{ $itemCar->year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endempty
                    <?/*@endif*/?>
                    <div class="row buttons-wrap">
                        <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link"
                           data-type-screen="auto"
                           data-id-screen="11" data-progressbar-id="11" data-click="clearR">Назад</a>
                    </div>
                    <!--<div class="calc__category-list buttons-wrap">
                        <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link" data-type-screen="auto"
                           data-id-screen="2" data-progressbar-id="2">Далее</a>
                    </div>-->
                    <span class="mess_info"></span>
                </div>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="2">
                <div class="calc__inner calc__auto-param">
                    <div class="row row_space-beetween">
                        <div class="input input_select input_part-3">
                            <label for="AUTO_MAKE" class="input__name">Марка автомобиля <span
                                        class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Марка автомобиля"></div>
                            <select
                                    name="AUTO_MAKE" id="AUTO_MAKE"
                                    class="js-select req_select"
                                    data-ajaxs="Y" data-type="elems"
                                    data-url="select_mark"
                                    data-idelem="FIRST_MODEL"
                                    data-typei="select"
                                    data-avtocode="mark"
                            >
                                <option value="0">Выбрать</option>
                                @foreach ($list_mark as $item)
                                    <option
                                            value="{{ $item->id_car_mark }}|{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input input_select input_part-3">
                            <label for="FIRST_MODEL" class="input__name">Модель <span class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Модель автомобиля"></div>
                            <select
                                    name="FIRST_MODEL"
                                    id="FIRST_MODEL"
                                    class="js-select req_select"
                                    data-avtocode="model"
                            >
                                <option value="0">Выбрать</option>
                            </select>
                        </div>
                        <div class="input input_select input_part-3">
                            <label for="YEAR" class="input__name">Год выпуска <span class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Год выпуска автомобиля"></div>
                            <select
                                    name="YEAR"
                                    id="YEAR"
                                    class="js-select req_select"
                                    data-ty="{{ date('Y') }}"
                                    data-avtocode="year"
                            >
                                <option value="0">Выбрать</option>
                            </select>
                        </div>
                        <?/*<div class="input input_select input_part-3 no-elem">
                            <label for="AUTO_TYPE" class="input__name">Тип транспортного средства <span
                                    class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Тип автомобиля"></div>
                            <select name="AUTO_TYPE" id="AUTO_TYPE" class="js-select req_select">
                                <option value="0">Выбрать</option>
                                <option
                                    value="{{ $item->id_type }}|{{ $item->name_type }}">{{ $item->name_type }}</option>
                                <?$it = 0;?>@foreach ($list_type as $key => $item)
                                    <?if ($item->id_type == 0) continue;$it++;?>
                                    @if($it == 1)
                                        <option
                                            value="{{ $item->id_type }}|{{ $item->name_type }}">{{ $item->name_type }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>*/?>
                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_select input_part-3">
                            <label for="POWER" class="input__name">Мощность двигателя (Л/c) <span
                                        class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Мощность автомобиля"></div>
                            <select
                                    name="AUTO_POWER"
                                    id="AUTO_POWER"
                                    class="js-select req_select"
                                    data-avtocode="power"
                            >
                                <option value="0">Выбрать</option>
                            </select>
                        </div>

                        <?/*
                        <div class="input input_power-wrap">
                            <div class="row row_space-beetween">
                                <div class="input input_power-companion">
                                    <label for="AUTO_POWER" class="input__name">Мощность двигателя (Л/c) <span class="red">*</span></label>
                                    <input type="number" placeholder="" name="AUTO_POWER" id="AUTO_POWER" tabindex="0" class="req">
                                </div>
                                <div class="input input_power input_no-heading no-elem">
                                    <label class="checkbox-r checkbox-r_power">
                                        <span class="checkbox-r__text-l">Л/c</span>
                                        <input type="checkbox" name="horns">
                                        <span class="checkbox-r__text">кВт</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        */?>
                    </div>

                </div>
                <div class="wrapper-agreed wrapper-agreed_noflex">
                    <input class="agreed-input req" type="checkbox" checked="" data-suberror="goal" value="on"
                           id="agreed-2" data-pattern="agree">
                    <label class="agreed" for="agreed-2">Цель использования – личная</label>
                    <span class="agreed-descr">В прокат, аренду не сдается, в качестве такси не используется</span>
                </div>
                <div class="wrapper-agreed wrapper-agreed_noflex">
                    <input class="agreed-input req" data-suberror="trailer" type="checkbox" checked="" value="on"
                           id="agreed-3" data-pattern="agree">
                    <label class="agreed" for="agreed-3">Без прицепа</label>
                </div>
                <div class="row buttons-wrap">
                    <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link" data-type-screen="auto"
                       data-id-screen="1" data-progressbar-id="1" data-click="clearR">Назад</a>
                    <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link" data-type-screen="auto"
                       data-id-screen="3" data-progressbar-id="3">Далее</a>
                </div>
                <span class="mess_info"></span>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="3">
                <div class="calc__inner calc__auto-param">
                    <div class="row row_space-beetween">
                        <div class="input input_part-3">
                            <label for="DATE_START" class="input__name">Начало действия нового полиса <span class="red">*</span></label>
                            <input type="text" placeholder="Начало действия нового полиса" name="DATE_START"
                                   id="DATE_START" tabindex="0" class="js-datepicker req" data-mask="00.00.0000"
                                   data-mindate="{{ gmdate('Y-m-d', $hours) }}" value="{{ gmdate('d.m.Y', $hours) }}"
                                   readonly>
                        </div>
                        <?/*<div class="input input_select input_part-2">
                            <?/*<select name="REGION_SOB" id="REGION_SOB" class="js-select-first req_select">
                        </select>*?>
                        <label for="REGION_SOB" class="input__name">Регион регистрации собственника по паспорту <span class="red">*</span></label>
                        <input type="text" placeholder="Регион регистрации собственника" name="REGION_SOB" id="REGION_SOB" data-address="REGION_SOB_KLADR" list="regions" tabindex="0" class="req" data-rus>
                        <ul class="datalist" id="regions">
                        </ul>
                    </div>*/?>
                        <div class="input input_part-2-3">
                            <label for="CITY_SOB" class="input__name">Населенный пункт регистрации собственника по
                                паспорту <span class="red">*</span></label>
                            <input type="text" placeholder="Населенный пункт регистрации собственника" name="CITY_SOB"
                                   id="CITY_SOB" data-next-elem="REGION_STRAH" data-keyup="autocomplite_elem"
                                   data-pattern="locality" data-auto-select data-address="CITY_SOB_KLADR" list="citys"
                                   tabindex="0" class="req" data-rus>
                            <ul class="datalist" id="citys">
                            </ul>
                        </div>
                    </div>
                    <?/*<div class="row row_space-beetween">
                </div>*/?>
                    <input type="hidden" name="CITY_SOB_KLADR" id="CITY_SOB_KLADR" value=""/>
                    <input type="hidden" name="REGION_SOB" id="REGION_SOB" value=""/>
                    <input type="hidden" name="CITY_SOB_DOP" id="CITY_SOB_DOP" value=""/>
                    <?/*<input type="hidden" name="REGION_SOB_KLADR" id="REGION_SOB_KLADR" value=""/>*/?>
                </div>
                <div class="row buttons-wrap">
                    <a href="javascript:void(0);" class="btn btn_light btn_prev auto_screen js-screen-link"
                       data-type-screen="auto"
                       data-id-screen="2" data-progressbar-id="2">Назад</a>
                    <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link" data-type-screen="auto"
                       data-id-screen="4" data-progressbar-id="4">Далее</a>
                </div>
                <span class="mess_info"></span>
            </div>
            <div class="screen js-screen js-drivers-wrap" data-type-screen="auto" data-id-screen="4">
                <div class="calc__inner calc__auto-param js-drivers">
                    <div class="row row_center">
                    <span class="alert">
                        Гарантируем сохранность и безопасность ваших данных в соответствии с ФЗ «О персональных данных»
                    </span>
                    </div>
                    <label class="checkbox-r checkbox-r_float">
                        <input type="checkbox" name="horns" class="js-hide-inputs">
                        <span class="checkbox-r__text">Без ограничений</span>
                    </label>
                    <div class="sob driv_sob no-elem">
                        <h3>Собственник</h3>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="LAST_NAME_SOB" class="input__name">Фамилия <span
                                            class="red">*</span></label>
                                <input type="text" placeholder="Фамилия" name="LAST_NAME_SOB" id="LAST_NAME_SOB"
                                       tabindex="0" data-keyup="autocomplite_elem" data-capital-letter
                                       data-next-elem="LAST_NAME_STRAH"
                                       class="req" data-mask="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN">
                            </div>
                            <div class="input input_part-3">
                                <label for="FIRST_NAME_SOB" class="input__name">Имя <span class="red">*</span></label>
                                <input type="text" placeholder="Имя" name="FIRST_NAME_SOB" id="FIRST_NAME_SOB"
                                       tabindex="0" data-keyup="autocomplite_elem" data-capital-letter
                                       data-next-elem="FIRST_NAME_STRAH"
                                       class="req" data-mask="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN">
                            </div>
                            <div class="input input_part-3">
                                <label for="MIDDLE_NAME_SOB" class="input__name">Отчество <span
                                            class="red">*</span></label>
                                <input type="text" placeholder="Отчество" id="MIDDLE_NAME_SOB" name="MIDDLE_NAME_SOB"
                                       tabindex="0" data-keyup="autocomplite_elem" data-capital-letter
                                       data-next-elem="MIDDLE_NAME_STRAH"
                                       class="req" data-mask="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN">
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="BRIT_SOB" class="input__name">Дата рождения <span
                                            class="red">*</span></label>
                                <input type="text" placeholder="Дата рождения" id="BRIT_SOB" name="BRIT_SOB"
                                       tabindex="0" data-keyup="autocomplite_elem" data-next-elem="BRIT_STRAH"
                                       class="js-datepicker req" data-mask="00.00.0000"
                                       data-maxdate="{{ gmdate('Y-m-d', $years18) }}" data-pattern="brith"
                                       data-view="years">
                            </div>
                            <div class="input input_part-2-3">
                                <label for="SERIA_SOB" class="input__name">Серия и номер паспорта <span
                                            class="red">*</span></label>
                                <input type="text" placeholder="Серия и номер паспорта" name="SERIA_SOB" id="SERIA_SOB"
                                       tabindex="0" class="req" data-mask="0000 000000" data-keyup="autocomplite_elem"
                                       data-next-elem="SERIA_STRAH">
                            </div>
                            <!--<div class="input input_part-3">
                                <label for="SERIA_SOB" class="input__name">Серия паспорта <span class="red">*</span></label>
                                <input type="text" placeholder="Серия паспорта" name="SERIA_SOB" id="SERIA_SOB" tabindex="0" class="req" data-mask="0000">
                            </div>
                            <div class="input input_part-3">
                                <label for="NUMBER_SOB" class="input__name">Номер паспорта <span class="red">*</span></label>
                                <input type="text" placeholder="Номер паспорта" name="NUMBER_SOB" id="NUMBER_SOB" tabindex="0" class="req" data-mask="000000">
                            </div>-->
                        </div>
                        <h3>Автомобиль</h3>
                        <div class="row">
                            <div class="input input_part-3">
                                <span class="input__name">Номер</span>
                                <div class="radio-group">
                                    <label class="radio">
                                        <input type="radio" name="number_type2" value="vin"
                                               class="category__radio-hidden" data-change="select_number" tabindex="0"
                                               data-avtocode="number_auto" checked>
                                        <div class="radio__inner">
                                            <span class="radio__name">VIN</span>
                                        </div>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="number_type2" value="bodywork" data-mask="AAAAAAAAAAAAAAAAA"
                                               class="category__radio-hidden" data-change="select_number" tabindex="0"
                                               data-avtocode="number_auto">
                                        <div class="radio__inner">
                                            <span class="radio__name">Кузов</span>
                                        </div>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="number_type2" value="chassis"
                                               class="category__radio-hidden" data-change="select_number" tabindex="0"
                                               data-avtocode="number_auto">
                                        <div class="radio__inner">
                                            <span class="radio__name">Шасси</span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="input input_part-3" id="inp_number2">
                                <div data-nuber-type="vin">
                                    <label for="VIN2" class="input__name">VIN <span class="red">*</span></label>
                                    <input type="text" placeholder="VIN" name="VIN2" id="VIN2" data-not-delete
                                           tabindex="0" data-keyup="autocomplite_elem" data-next-elem="VIN" class="req"
                                           data-pattern="vin" data-mask="AAAAAAAAAAAAAAAAA" data-textbig="Y">
                                </div>
                                <div data-nuber-type="bodywork" class="no-elem">
                                    <label for="bodywork2" class="input__name">Кузов <span class="red">*</span></label>
                                    <input type="text" placeholder="Кузов" name="bodywork2" data-not-delete
                                           data-mask="AAAAAAAAAAAAAAAAA"
                                           id="bodywork2" tabindex="0" data-keyup="autocomplite_elem"
                                           data-next-elem="bodywork" class="req" data-textbig="Y">
                                </div>
                                <div data-nuber-type="chassis" class="no-elem">
                                    <label for="bodywork2" class="input__name">Шасси <span class="red">*</span></label>
                                    <input type="text" placeholder="Шасси" name="chassis2" data-not-delete id="chassis2"
                                           data-mask="AAAAAAAAAAAAAAAAA"
                                           tabindex="0" data-keyup="autocomplite_elem" data-next-elem="chassis"
                                           class="req" data-textbig="Y">
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty(!$arPersons)
                        <div class="no-elem" data-pselect="person">
                            <span>Вы можете выбрать персону из своего списка: </span>
                            <select name="SELECT_PERSON_TWO" id="SELECT_PERSON_TWO" class="slp">
                                <option value="0">Выбрать</option>
                                @foreach ($arPersons as $itemPers)
                                    <option
                                            value="{{ $itemPers->id }}">{{ $itemPers->LastName }} {{ $itemPers->FirstName }} {{ $itemPers->MiddleName }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endempty
                    <div class="js-driver fater-driver" data-itm="1">
                        <h3>1 водитель </h3>
                        @empty(!$arPersons)
                            <div class="row row_space-beetween">
                                <div class="input input_select input_part-2" data-onselect="person">
                                    <span>Вы можете выбрать персону из своего списка: </span>
                                    <select name="SELECT_PERSON" id="SELECT_PERSON"
                                            class="js-select js-select-driver slp">
                                        <option value="0">Выбрать</option>
                                        @foreach ($arPersons as $itemPers)
                                            <option
                                                    value="{{ $itemPers->id }}">{{ $itemPers->LastName }} {{ $itemPers->FirstName }} {{ $itemPers->MiddleName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endempty
                        <div class="inp_driv">
                            <div class="row row_space-beetween">
                                <div class="input input_part-3">
                                    <label for="LAST_NAME_AUTO" class="input__name">Фамилия <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Фамилия" data-capital-letter name="LAST_NAME_AUTO"
                                           id="LAST_NAME_AUTO"
                                           tabindex="0" data-keyup="autocomplite_elem" data-next-elem="LAST_NAME_STRAH"
                                           class="req" data-mask="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN">
                                </div>
                                <div class="input input_part-3">
                                    <label for="FIRST_NAME_AUTO" class="input__name">Имя <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Имя" data-capital-letter name="FIRST_NAME_AUTO"
                                           id="FIRST_NAME_AUTO"
                                           tabindex="0" data-keyup="autocomplite_elem" data-next-elem="FIRST_NAME_STRAH"
                                           class="req" data-mask="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN">
                                </div>
                                <div class="input input_part-3">
                                    <label for="MIDDLE_NAME_AUTO" class="input__name">Отчество <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Отчество" id="MIDDLE_NAME_AUTO"
                                           name="MIDDLE_NAME_AUTO" tabindex="0" data-capital-letter
                                           data-keyup="autocomplite_elem"
                                           data-next-elem="MIDDLE_NAME_STRAH" class="req"
                                           data-mask="NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN">
                                </div>
                            </div>
                            <div class="row row_space-beetween">
                                <div class="input input_part-3">
                                    <label for="DATE_BIRTH_AUTO" class="input__name">Дата рождения <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Дата рождения" name="DATE_BIRTH_AUTO"
                                           id="DATE_BIRTH_AUTO" tabindex="0" data-keyup="autocomplite_elem"
                                           data-next-elem="BRIT_STRAH" class="js-datepicker req req_birthday"
                                           data-mask="00.00.0000" data-years18 autocomplete="off" maxlength="10"
                                           data-maxdate="{{ gmdate('Y-m-d', $years18) }}" data-view="years"
                                           data-pattern="brith">
                                </div>
                                <div class="input input_part-3">
                                    <label for="DRIVE_DOCUMENT_AUTO" class="input__name">Серия и номер ВУ <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Серия и номер ВУ" id="DRIVE_DOCUMENT_AUTO"
                                           name="DRIVE_DOCUMENT_AUTO" data-pattern="minlength" data-minlength="11"
                                           tabindex="0" class="req text-upper" data-mask="00LL 000000">
                                </div>
                                <div class="input input_part-3">
                                    <label for="DATE_DRIVE_DOCUMENT_AUTO" class="input__name">Дата отсчета стажа <span
                                                class="red">*</span></label>
                                    <div class="tooltip js-modal-open" data-modal-type="wiki"
                                         data-wiki="wiki-stazh"></div>
                                    <input type="text" placeholder="Дата отсчета стажа" name="DATE_DRIVE_DOCUMENT_AUTO"
                                           id="DATE_DRIVE_DOCUMENT_AUTO" tabindex="0"
                                           class="js-datepicker req req_experience" data-mask="00.00.0000"
                                           data-maxdate="{{ gmdate('Y-m-d') }}" data-pattern="stag" autocomplete="off"
                                           maxlength="10" data-view="years">
                                </div>
                                <!--<div class="input input_part-3">
                                    <label for="DRIVE_DOCUMENT_AUTO2" class="input__name">Номер ВУ <span class="red">*</span></label>
                                    <input type="text" placeholder="Номер ВУ" id="DRIVE_DOCUMENT_AUTO2" name="DRIVE_DOCUMENT_AUTO2" tabindex="0" class="req" data-mask="NNNNNN">
                                </div>-->
                            </div>
                        <!--<div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="DATE_DRIVE_DOCUMENT_AUTO" class="input__name">Дата отсчета стажа <span class="red">*</span></label>
                                <div class="tooltip" data-tooltip="Дата отсчета стажа"></div>
                                <input type="text" placeholder="Дата отсчета стажа" name="DATE_DRIVE_DOCUMENT_AUTO" id="DATE_DRIVE_DOCUMENT_AUTO" tabindex="0"
                                    class="js-datepicker req req_experience" data-mask="00.00.0000" data-maxdate="{{ gmdate('Y-m-d') }}"  data-pattern="stag" autocomplete="off" maxlength="10" data-view="years">
                            </div>
                        </div>-->
                            <div class="row row_space-beetween no-elem">
                                <div class="input input_part-3">
                                    <label for="PASSPORT_PATH_CITY_AUTO" class="input__name">Населенный пункт <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Населенный пункт" name="PASSPORT_PATH_CITY_AUTO"
                                           id="PASSPORT_PATH_CITY_AUTO" tabindex="0" class="req">
                                </div>
                                <div class="input input_part-3">
                                    <label for="PASSPORT_PATH_STREET_AUTO" class="input__name">Улица <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Улица" name="PASSPORT_PATH_STREET_AUTO"
                                           id="PASSPORT_PATH_STREET_AUTO" tabindex="0" class="req">
                                </div>
                                <div class="input input_part-3">
                                    <label for="PASSPORT_PATH_NUMBER_AUTO" class="input__name">Номер дома <span
                                                class="red">*</span></label>
                                    <input type="text" placeholder="Номер дома" id="PASSPORT_PATH_NUMBER_AUTO"
                                           name="PASSPORT_PATH_NUMBER_AUTO" tabindex="0" class="req">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row buttons-wrap">
                    <a href="javascript:void(0);" class="personal-table__btn btn btn_add btn_dark js-add-driver">Добавить
                        водителя</a>
                </div>
                <div class="wrapper-agreed">
                    <input class="agreed-input req" type="checkbox" checked value="on" id="agreed" data-pattern="agree">
                    <label class="agreed" for="agreed">Согласен с правилами предоставления информации</label>
                </div>
                <input type="hidden" name="driver_numb" value="1">
                <div class="row buttons-wrap">
                    <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link" data-type-screen="auto"
                       data-id-screen="3" data-progressbar-id="3">Назад</a>
                    <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link" data-type-screen="auto"
                       data-id-screen="5" data-progressbar-id="5" data-event="onCalc">Далее</a>
                </div>
                <span class="mess_info"></span>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="5">
                <div class="calc__inner calc__auto-param">
                    <div class="calc__result">
                        <div class="calc__result-information">
                            <div class="calc__result-information-top">
                                <div class="calc__result-section">
                                    <p class="calc__result-heading">
                                        Расчёт КБМ
                                    </p>
                                    <div data-info="cars">

                                    </div>
                                    <a class="calc__result-change js-screen-link" href="javascript:void(0)"
                                       data-type-screen="auto" data-id-screen="4"
                                       data-progressbar-id="4">Изменить параметры</a>
                                </div>
                                <div class="calc__result-section">
                                    <p class="calc__result-heading">
                                        Автомобиль
                                    </p>
                                    <p class="calc__result-content">
                                        <span data-info="mark"></span> <span data-info="model"></span>, <span
                                                data-info="year"></span> г. в., <span data-info="power"></span>
                                        <a class="calc__result-change js-screen-link"
                                           data-type-screen="auto"
                                           data-id-screen="2" data-progressbar-id="1" href="javascript:void(0)">Изменить
                                            параметры</a>
                                    </p>
                                </div>
                                <div class="calc__result-section">
                                    <p class="calc__result-heading">
                                        Условия страхования
                                    </p>

                                    <p class="calc__result-content">
                                        <span data-info="city"></span>,
                                        период страхования
                                        <span class="calc__date">с <span
                                                    data-info="date_s">{{ gmdate('d.m.Y', $hours) }}</span> по <span
                                                    class="date_n"
                                                    data-info="date_n">{{ gmdate('d.m.Y', $years2) }}</span></span>
                                        <a class="calc__result-change js-screen-link"
                                           data-id-screen="3"
                                           data-type-screen="auto" data-progressbar-id="3" href="javascript:void(0)">Изменить
                                            параметры</a>
                                    </p>
                                </div>
                            </div>
                            <div class="calc__result-information-bottom">
                                <div class="calc__result-section">
                                    <p class="calc__result-heading">
                                        Гарантия точной цены
                                    </p>
                                    <ul class="calc__result-list">
                                        <li class="calc__result-list-item">
                                            <span>Цены на сайте pitstop-group.ru = ценам страховых компаний</span>
                                        </li>
                                        <li class="calc__result-list-item">
                                            <span>Pitstop-group.ru — официальный партнёр страховых компаний</span>
                                        </li>
                                        <li class="calc__result-list-item">
                                            <span>Любой купленный полис у нас на сайте вы можете проверить на подлинность на сайте
                                                <a class="link-border"
                                                   href="https://dkbm-web.autoins.ru/dkbm-web-1.0/bsostate.htm"
                                                   target="_blank">РСА</a>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?/*<a href="javascript:void(0);" class="calc__edit-link">Показать таблицу расчета</a>*/?>
                            {{--                            <a href="javascript:void(0);" class="calc__edit-link js-screen-link" data-type-screen="auto"--}}
                            {{--                               data-id-screen="1" data-progressbar-id="1" data-click="clearR">Изменить параметры</a>--}}
                        </div>
                        <div class="calc__result-table">
                            <p class="calc__result-heading">
                                Результат
                            </p>
                            <div class="calc__table">
                                <div class="calc__table-header">
                                    <div class="calc__table-name">Компания</div>
                                    <div class="calc__table-rate">Рейтинг</div>
                                    <div class="calc__table-kbm">
                                        Ваш КБМ
                                        <div style="position: absolute;" class="tooltip js-modal-open"
                                             data-modal-type="wiki" data-wiki="wiki-kbm"></div>
                                    </div>
                                    <div class="calc__table-price">Стоимость</div>
                                </div>
                                <div class="calc__table-body" id="elems_calc">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="block_personal_calc" class="js-personal-calc-info no-elem">
            <div class="calc__inner container js-container-osago">
                <h1 class="calc__title">Оформление ОСАГО</h1>
                <div class="calc__progressbar container">
                    <div class="calc__progressbar-points js-progressbar">
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point js-screen-link calc__progressbar-point_active"
                           data-type-screen="auto" data-id-screen="6"></a>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point js-screen-link"
                           data-type-screen="auto" data-id-screen="7"></a>
                        <a href="javascript:void(0);"
                           class="calc__progressbar-point js-progressbar-point js-screen-link"
                           data-type-screen="auto" data-id-screen="13"></a>
                        <div class="calc__progressbar-loading js-loading"></div>
                    </div>
                    <div class="calc__progressbar-items">
                        <a href="javascript:void(0);"
                           class="calc__progressbar-item calc__progressbar-item_active js-progressbar-item js-screen-link"
                           data-type-screen="auto" data-id-screen="6">
                            Cтрахователь
                        </a>
                        <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
                           data-type-screen="auto" data-id-screen="7">
                            Автомобиль
                        </a>
                        <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
                           data-type-screen="auto" data-id-screen="13">
                            Оформление
                        </a>
                    </div>
                </div>
            </div>
            <div class="screen screen_active js-screen" data-type-screen="auto" data-id-screen="6">
                <div class="calc__inner calc__auto-param">
                    <div class="alert alert_100 alert_noicon js-icon-alert">
                        <div class="row row_space-beetween w100">
                            <img class="ofeer-img"
                                 src="http://pitstop.dev.letsrock.pro/storage/allinsurances/September2019/AYUwVFL3fXhvdMIjWxFR.png">
                            <div class="alert-info">
                                <div class="price-text">Стоимость</div>
                                <div class="price-number">10.500 ₽</div>
                            </div>
                        </div>
                    </div>
                    <div class="calc__title-inner">
                        <span>Данные страхователя
                            <span class="tooltip js-modal-open" data-modal-type="wiki" data-wiki="wiki-kbm"></span>
                        </span>

                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_part-3">
                            <label for="LAST_NAME_STRAH" class="input__name">Фамилия <span class="red">*</span></label>
                            <input type="text" placeholder="Фамилия" data-capital-letter name="LAST_NAME_STRAH"
                                   id="LAST_NAME_STRAH"
                                   tabindex="0" class="req" data-rus>
                        </div>
                        <div class="input input_part-3">
                            <label for="FIRST_NAME_STRAH" class="input__name">Имя <span class="red">*</span></label>
                            <input type="text" placeholder="Имя" data-capital-letter name="FIRST_NAME_STRAH"
                                   id="FIRST_NAME_STRAH"
                                   tabindex="0" class="req" data-rus>
                        </div>
                        <div class="input input_part-3">
                            <label for="MIDDLE_NAME_STRAH" class="input__name">Отчество <span
                                        class="red">*</span></label>
                            <input type="text" placeholder="Отчество" data-capital-letter id="MIDDLE_NAME_STRAH"
                                   name="MIDDLE_NAME_STRAH"
                                   tabindex="0" class="req" data-rus>
                        </div>
                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_part-3">
                            <label for="BRIT_STRAH" class="input__name">Дата рождения <span class="red">*</span></label>
                            <input type="text" placeholder="Дата рождения" id="BRIT_STRAH" name="BRIT_STRAH"
                                   tabindex="0" class="js-datepicker req" data-mask="00.00.0000"
                                   data-maxdate="{{ gmdate('Y-m-d', $years18) }}" data-pattern="brith"
                                   data-view="years">
                        </div>
                        <div class="input input_part-3">
                            <label for="SERIA_STRAH" class="input__name">Серия и номер паспорта <span
                                        class="red">*</span></label>
                            <input type="text" placeholder="Серия и номер паспорта" name="SERIA_STRAH"
                                   data-pattern="minlength" data-minlength="11" id="SERIA_STRAH" tabindex="0"
                                   class="req" data-mask="0000 000000">
                        </div>
                        <div class="input input_part-3">
                            <label for="DATE_DOC_STRAH" class="input__name">Дата выдачи <span
                                        class="red">*</span></label>
                            <input type="text" placeholder="Дата выдачи" id="DATE_DOC_STRAH" name="DATE_DOC_STRAH"
                                   tabindex="0" class="js-datepicker req" data-maxdate="{{ gmdate('Y-m-d')}}"
                                   data-pattern="extradition" data-mask="00.00.0000">
                        </div>

                        <!--<div class="input input_part-3">
                            <label for="SERIA_STRAH" class="input__name">Серия паспорта <span class="red">*</span></label>
                            <input type="text" placeholder="Серия паспорта" name="SERIA_STRAH" id="SERIA_STRAH" tabindex="0" class="req" data-mask="0000">
                        </div>
                        <div class="input input_part-3">
                            <label for="NUMBER_STRAH" class="input__name">Номер паспорта <span class="red">*</span></label>
                            <input type="text" placeholder="Номер паспорта" name="NUMBER_STRAH" id="NUMBER_STRAH" tabindex="0" class="req" data-mask="000000">
                        </div>-->
                    </div>
                    <!--<div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="KOD_DOC_STRAH" class="input__name">Код подразделения <span class="red">*</span></label>
                                <input type="text" placeholder="Код подразделения" name="KOD_DOC_STRAH" id="KOD_DOC_STRAH" tabindex="0" class="req" data-mask="000-000">
                            </div>
                            <div class="input input_part-3">
                                <label for="FOR_DOC_STRAH" class="input__name">Кем выдан <span class="red">*</span></label>
                                <input type="text" placeholder="Кем выдан" name="FOR_DOC_STRAH" id="FOR_DOC_STRAH" tabindex="0" class="req">
                            </div>
                    </div>-->
                    <div class="row row_space-beetween">
                        <div class="input input_part-2-3">
                            <label for="REGION_STRAH" class="input__name">Адрес регистрации <span
                                        class="red">*</span></label>
                            <input type="text" placeholder="Адрес регистрации" name="REGION_STRAH" id="REGION_STRAH"
                                   data-address="CITY_STRAH_KLADR" data-auto-select data-pattern="address"
                                   list="address" tabindex="0" class="req">
                            <ul class="datalist" id="address">
                            </ul>
                        </div>
                        <!--<div class="input input_part-3">
                            <label for="REGION_STRAH" class="input__name">Регион <span class="red">*</span></label>
                            <input type="text" placeholder="Регион" name="REGION_STRAH" id="REGION_STRAH" tabindex="0" class="req" data-rus>
                        </div>
                        <div class="input input_part-3">
                            <label for="CITY_STRAH" class="input__name">Населенный пункт <span class="red">*</span></label>
                            <input type="text" placeholder="Населенный пункт" name="CITY_STRAH" id="CITY_STRAH" tabindex="0" class="req" data-rus>
                        </div>
                        <div class="input input_part-3">
                            <label for="STREET_STRAH" class="input__name">Улица <span class="red">*</span></label>
                            <input type="text" placeholder="Улица" name="STREET_STRAH" id="STREET_STRAH" tabindex="0" class="req">
                        </div>-->
                        <div class="input input_part-3">
                            <label for="APARTAMENT_STRAH" class="input__name">Квартира</label>
                            <input type="text" placeholder="Квартира" name="APARTAMENT_STRAH" id="APARTAMENT_STRAH"
                                   tabindex="0">
                        </div>
                        <input type="hidden" value="" id="CITY_STRAH_KLADR" name="CITY_STRAH_KLADR"/>
                        <input type="hidden" value="" id="REGION_STRAH_DOP" name="REGION_STRAH_DOP"/>
                        <input type="hidden" value="" id="CITY_STRAH" name="CITY_STRAH"/>
                        <input type="hidden" value="" id="STREET_STRAH" name="STREET_STRAH"/>
                        <input type="hidden" value="" id="HOME_STRAH" name="HOME_STRAH"/>
                        <input type="hidden" value="" id="HOME_STRAH_KLADR" name="HOME_STRAH_KLADR"/>
                    </div>
                    <div class="row row_space-beetween">
                        <!--<div class="input input_part-3">
                            <label for="HOME_STRAH" class="input__name">Дом <span class="red">*</span></label>
                            <input type="text" placeholder="Дом" name="HOME_STRAH" id="HOME_STRAH" tabindex="0" class="req">
                        </div>
                        <div class="input input_part-3">
                            <label for="BUILDING_STRAH" class="input__name">Корпус</label>
                            <input type="text" placeholder="Корпус" name="BUILDING_STRAH" id="BUILDING_STRAH" tabindex="0">
                        </div>
                        <div class="input input_part-2-3">
                                <label for="ADDRESS_STRAH" class="input__name">Адрес регистрации <span class="red">*</span></label>
                                <div class="tooltip" data-tooltip="При вводе, вам будут показыватся подсказки. Пример адреса: Город, ул Улица, д Номер_дома"></div>
                                <input type="text" placeholder="Адрес регистрации" id="ADDRESS_STRAH" name="ADDRESS_STRAH" data-address="ADDRESS_STRAH_KLADR" list="address" tabindex="0" class="req">
                                <datalist id="address">
                                </datalist>
                                <input type="hidden" name="ADDRESS_STRAH_KLADR" id="ADDRESS_STRAH_KLADR" value=""/>
                        </div>
                            <div class="input input_part-3">
                                <label for="APARTAMENT_STRAH" class="input__name">Квартира</label>
                                <input type="text" placeholder="Квартира" name="APARTAMENT_STRAH" id="APARTAMENT_STRAH" tabindex="0">
                            </div>-->
                    </div>

                </div>
                <div class="row buttons-wrap">
                    <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link" data-type-screen="auto"
                       data-id-screen="5" data-progressbar-id="5" data-displayc="block_auto_calc"
                       data-ndisplayc="block_personal_calc">Назад</a>
                    <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link" data-type-screen="auto"
                       data-id-screen="7" data-progressbar-id="7">Далее</a>
                </div>
                <span class="mess_info"></span>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="7">
                <div class="calc__inner calc__auto-param">
                    <div class="calc__inner calc__auto-param">
                        <div class="alert alert_100 alert_noicon js-icon-alert">
                            <div class="row row_space-beetween w100">
                                <img class="ofeer-img"
                                     src="http://pitstop.dev.letsrock.pro/storage/allinsurances/September2019/AYUwVFL3fXhvdMIjWxFR.png">
                                <div class="alert-info">
                                    <div class="price-text">Стоимость</div>
                                    <div class="price-number">10.500 ₽</div>
                                </div>
                            </div>
                        </div>
                        <div class="calc__title-inner">
                            Автомобиль
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <span class="input__name">Документ</span>
                                <div class="radio-group">
                                    <label class="radio">
                                        <input type="radio" name="doc_ts" value="sts" class="category__radio-hidden"
                                               tabindex="0">
                                        <div class="radio__inner">
                                            <span class="radio__name">СТС</span>
                                        </div>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="doc_ts" value="pts" class="category__radio-hidden"
                                               tabindex="0" checked>
                                        <div class="radio__inner">
                                            <span class="radio__name">ПТС</span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="input input_part-3">
                                <label for="SERIA_NUMBER_TS" class="input__name">Серия и номер документа<span
                                            class="red">*</span></label>
                                <div class="tooltip" data-tooltip="Серия и номер документа"></div>
                                <input type="text" placeholder="Серия и номер документа" name="SERIA_NUMBER_TS"
                                       id="SERIA_NUMBER_TS" data-mask="GGGG GGGGGG" tabindex="0" class="req"
                                       data-avtocode="stspts">
                            </div>
                            <div class="input input_part-3">
                                <label for="DATE_TS" class="input__name">Дата выдачи <span class="red">*</span></label>
                                <div class="tooltip" data-tooltip="Дата выдачи документа"></div>
                                <input type="text" placeholder="Дата выдачи" name="DATE_TS" id="DATE_TS" tabindex="0"
                                       class="js-datepicker req" data-mask="00.00.0000">
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <span class="input__name">Номер</span>
                                <div class="radio-group">
                                    <label class="radio">
                                        <input type="radio" name="number_type" value="vin"
                                               class="category__radio-hidden"
                                               data-change="select_number" tabindex="0" data-avtocode="number_auto"
                                               checked>
                                        <div class="radio__inner">
                                            <span class="radio__name">VIN</span>
                                        </div>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="number_type" value="bodywork"
                                               class="category__radio-hidden" data-change="select_number" tabindex="0"
                                               data-avtocode="number_auto">
                                        <div class="radio__inner">
                                            <span class="radio__name">Кузов</span>
                                        </div>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="number_type" value="chassis"
                                               class="category__radio-hidden" data-change="select_number" tabindex="0"
                                               data-avtocode="number_auto">
                                        <div class="radio__inner">
                                            <span class="radio__name">Шасси</span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="input input_part-3" id="inp_number">
                                <div data-nuber-type="vin">
                                    <label for="VIN" class="input__name">VIN <span class="red">*</span></label>
                                    <input type="text" placeholder="VIN" name="VIN" id="VIN" tabindex="0" class="req"
                                           data-pattern="vin" data-mask="AAAAAAAAAAAAAAAAA" data-textbig="Y">
                                </div>
                                <div data-nuber-type="bodywork" class="no-elem">
                                    <label for="bodywork" class="input__name">Кузов <span class="red">*</span></label>
                                    <input type="text" placeholder="Кузов" data-mask="AAAAAAAAAAAAAAAAA" name="bodywork" id="bodywork" tabindex="0"
                                           class="req" data-textbig="Y">
                                </div>
                                <div data-nuber-type="chassis" class="no-elem">
                                    <label for="chassis" class="input__name">Шасси <span class="red">*</span></label>
                                    <input type="text" placeholder="Шасси" data-mask="AAAAAAAAAAAAAAAAA" name="chassis" id="chassis" tabindex="0"
                                           class="req" data-textbig="Y">
                                </div>
                            </div>
                            <div class="input input_part-3 js-reset-req-parent">
                                <label for="REG_NUMBER" class=" input__name">Регистрационный номер <span
                                            class="red js-reset-req-label">*</span></label>
                                <div class="tooltip" data-tooltip="Регистрационный номер автомобиля"></div>
                                <!-- G000GG00ASSS -->
                                <input type="text" placeholder="Пример: А000АА00" name="REG_NUMBER" id="REG_NUMBER"
                                       tabindex="0" data-mask="R000RR000" data-textbig="Y" data-avtocode="reg_number"
                                       class="req js-check-checked">
                                <div class="wrapper-agreed mt-1">
                                    <input class="agreed-input req js-reset-req-input" name="check_regnumber_off"
                                           type="checkbox" value="on" id="agreed2">
                                    <label class="agreed" for="agreed2">Пока нет номера</label>
                                </div>
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-2-3">
                                <label for="CART_DIAG" class="input__name">Диагностическая карта</label>
                                <div class="tooltip" data-tooltip="Диагностическая карта"></div>
                                <input type="text" placeholder="Диагностическая карта" name="CART_DIAG" id="CART_DIAG"
                                       tabindex="0" data-mask="AAAAAAAAAAAAAAAAAAAAA" data-textbig="Y">
                            </div>
                            <div class="input input_part-3">
                                <label for="DATE_CART_DIAG" class="input__name">Срок действия карты</label>
                                <input type="text" placeholder="Срок действия карты" name="DATE_CART_DIAG"
                                       id="DATE_CART_DIAG" tabindex="0" class="js-datepicker" data-mask="00.00.0000">
                            </div>
                        </div>

                    </div>
                    <div class="row buttons-wrap">
                        <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link"
                           data-type-screen="auto"
                           data-id-screen="6" data-progressbar-id="6">Назад</a>
                        <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link"
                           data-type-screen="auto"
                           data-id-screen="13" data-progressbar-id="13">Далее</a>
                    </div>
                    <span class="mess_info"></span>
                </div>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="8">
                <div class="calc__title-inner calc__title-inner_lg">
                    Пожалуйста, проверьте данные
                </div>
                <div class="calc__result-table-info">
                    <div class="alert alert_100 alert_noshadow alert_noicon js-icon-alert">
                        <div class="row row_space-beetween w100">
                            <img class="ofeer-img"
                                 src="http://pitstop.dev.letsrock.pro/storage/allinsurances/September2019/AYUwVFL3fXhvdMIjWxFR.png">
                            <div class="alert-info">
                                <div class="price-text">Стоимость</div>
                                <div class="price-number">10.500 ₽</div>
                            </div>
                        </div>
                    </div>
                    <div class="calc__fake-line"></div>
                    <div class="calc__result-table-info-left">
                        <div class="calc__result-section calc__result-section_noborder">
                            <p class="calc__result-heading">
                                Период страхования
                            </p>
                            <p class="calc__result-content">
                    <span class="calc__date">с
                        <span data-info="date_s">06.03.2020</span> по
                        <span data-info="date_n">04.03.2021</span>
                    </span>
                                <a class="calc__result-change js-screen-link" data-id-screen="3"
                                   data-click="setPrevSlideContainer"
                                   data-type-screen="auto" data-progressbar-id="3" href="javascript:void(0)">Изменить
                                    параметры</a>
                            </p>
                        </div>
                        <div class="calc__result-section calc__result-section_noborder">
                            <p class="calc__result-heading">
                                Страхователь
                            </p>
                            <div>
                                <div class="input input_wide">
                                    <label>
                                        <span class="calc__result-line js-confirm-info-name">Ершов Виталий Васильевич</span>
                                        <span class="calc__result-line js-confirm-info-birthday">Дата рождения: 14.10.1998</span>
                                        <span class="calc__result-line js-confirm-info-passport">Паспорт: 0115 226632, выдан 12.08.2015</span>
                                        <span class="calc__result-line js-confirm-info-name-address">Адрес регистрации: г. Барнаул, ул Нахимова, д 8</span>
                                    </label>
                                    <a class="calc__result-change js-screen-link" data-id-screen="6"
                                       data-type-screen="auto" data-progressbar-id="6" href="javascript:void(0)">Изменить
                                        параметры</a>
                                </div>
                            </div>
                        </div>
                        <div class="calc__result-section calc__result-section_noborder">
                            <p class="calc__result-heading">
                                Допущенные к управлению
                            </p>
                            <div>
                                <div data-info="cars"></div>
                                <a class="calc__result-change js-screen-link" data-click="setPrevSlideContainer"
                                   data-type-screen="auto"
                                   data-id-screen="4" data-progressbar-id="3" href="javascript:void(0)">Изменить
                                    параметры</a>
                            </div>
                        </div>
                        <div class="calc__result-section calc__result-section_noborder">
                            <p class="calc__result-heading">
                                Автомобиль
                            </p>
                            <p class="calc__result-content">
                                <span data-info="mark">Kia</span> <span data-info="model">Rio</span>, <span
                                        data-info="year">2012</span> г. в., <span data-info="power">124  л. с.</span>
                                <a class="calc__result-change js-screen-link" data-click="setPrevSlideContainer"
                                   data-type-screen="auto"
                                   data-id-screen="2" data-progressbar-id="1" href="javascript:void(0)">Изменить
                                    параметры</a>
                            </p>
                        </div>
                        <div class="calc__result-section calc__result-section_noborder">
                            <p class="calc__result-heading">
                                Данные по автомобилю
                            </p>
                            <span class="calc__result-line">Регистрационный номер: <span
                                        class="js-confirm-info-regnumber"></span> </span>
                            <span class="calc__result-line">Серия и номер документа: <span
                                        class="js-confirm-info-numerdoc"></span>.</span>
                            <span class="calc__result-line">Вин: <span class="js-confirm-info-vin"></span>.</span>
                            <span class="calc__result-line">Номер диагностической карты: <span
                                        class="js-confirm-info-numberdiag"></span></span>
                            <span class="calc__result-line">До: <span class="js-confirm-info-datediag"></span>.</span>

                            <a class="calc__result-change js-screen-link"
                               data-type-screen="auto"
                               data-id-screen="7" data-progressbar-id="1" href="javascript:void(0)">Изменить
                                параметры</a>
                        </div>
                        <div class="calc__result-section calc__result-section_noborder">
                            <p class="calc__result-heading">
                                Контактные данные
                            </p>
                            <p class="calc__result-content">
                                <span class="calc__result-line js-confirm-info-mail"></span>
                                <span class="calc__result-line js-confirm-info-phone"></span>
                                <a class="calc__result-change js-screen-link"
                                   data-type-screen="auto"
                                   data-id-screen="13" data-progressbar-id="1" href="javascript:void(0)">Изменить
                                    параметры</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row row_mw  buttons-wrap">
                    <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link"
                       data-type-screen="auto"
                       data-id-screen="7" data-progressbar-id="7">Назад</a>
                    <a href="javascript:void(0);" class="btn btn_big btn_next js-screen-link"
                       data-type-screen="auto"
                       data-calslug=""
                       data-id-screen="9" data-progressbar-id="8">Далее</a>
                </div>
                <span class="mess_info"></span>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="9">
                <div class="waiting-screen">
                    <svg class="waiting-screen__mobile" width="365" height="373" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M171 72c-69.036 0-125 55.964-125 125h125V72z" fill="#FFD336"/>
                        <path d="M178 82c-69.036 0-125 55.964-125 125h125V82z" fill="#fff" fill-opacity=".85"/>
                        <path d="M310 197c0-69.036-55.964-125-125-125v125h125z" fill="#FFD336"/>
                        <path d="M303 207c0-69.036-55.964-125-125-125v125h125z" fill="#fff" fill-opacity=".85"/>
                        <mask id="a" maskUnits="userSpaceOnUse" x="0" y="0" width="365" height="373">
                            <path fill="#fff" d="M0 0h364.726v373H0z"/>
                        </mask>
                        <g filter="url(#filter0_d)" mask="url(#a)">
                            <path d="M191.889 38.776a162.134 162.134 0 0157.546 16.11l2.747-4.194-4.091-2.69a2.99 2.99 0 01-.851-4.123l4.983-7.614a2.973 2.973 0 014.112-.854l23.07 15.181a2.992 2.992 0 01.852 4.123l-4.983 7.612a2.971 2.971 0 01-4.112.856l-4.087-2.69-2.071 3.165a164.36 164.36 0 0127.97 22.494c29.587 29.666 47.889 70.649 47.889 115.918 0 45.266-18.302 86.251-47.889 115.914C263.387 347.65 222.511 366 177.364 366c-45.149 0-86.024-18.35-115.612-48.016-29.587-29.663-47.889-70.648-47.889-115.914 0-45.269 18.3-86.252 47.89-115.918a164.252 164.252 0 0126.566-21.59l-2.661-4.069-4.088 2.69a2.972 2.972 0 01-4.113-.856l-4.983-7.61a2.994 2.994 0 01.854-4.125l23.069-15.181a2.975 2.975 0 014.114.854l4.983 7.614a2.991 2.991 0 01-.854 4.123l-4.088 2.69 3.236 4.944a162.152 162.152 0 0159.047-16.86v-7.211h-7.975c-2.664 0-4.842-2.187-4.842-4.858v-14.85c0-2.673 2.178-4.857 4.842-4.857h45.006c2.664 0 4.843 2.184 4.843 4.858v14.849c0 2.67-2.179 4.858-4.843 4.858h-7.977v7.211zm83.578 64.93c-25.107-25.17-59.791-40.742-98.103-40.742-38.313 0-73 15.572-98.104 40.742-25.104 25.17-40.635 59.951-40.635 98.364 0 38.412 15.53 73.188 40.635 98.361 25.106 25.17 59.791 40.74 98.104 40.74 38.312 0 72.998-15.57 98.103-40.74 25.104-25.173 40.632-59.949 40.632-98.361 0-38.413-15.528-73.191-40.632-98.364z"
                                  fill="#fff"/>
                        </g>
                        <path d="M310 197c0-69.036-55.964-125-125-125v125h125z" fill="#FFD336"/>
                        <path d="M303 207c0-69.036-55.964-125-125-125v125h125z" fill="#fff" fill-opacity=".85"/>
                        <path d="M185 329c69.036 0 125-55.964 125-125H185v125z" fill="#FFD336"/>
                        <path d="M175 322c69.036 0 125-55.964 125-125H175v125z" fill="#fff" fill-opacity=".85"/>
                        <path d="M46 207c0 69.036 55.964 125 125 125V207H46z" fill="#FFD336"/>
                        <path d="M53 197c0 69.036 55.964 125 125 125V197H53z" fill="#fff" fill-opacity=".85"/>
                        <defs>
                            <filter id="filter0_d" x="1.863" y="-2" width="351" height="383"
                                    filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                                <feOffset dy="3"/>
                                <feGaussianBlur stdDeviation="6"/>
                                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
                                <feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/>
                                <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                            </filter>
                        </defs>
                    </svg>
                    <svg class="waiting-screen__desctop" width="1170" height="515" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M568 150c-69.036 0-125 55.964-125 125h125V150z" class="it-tl-1" fill="#FFD336"/>
                        <path d="M575 160c-69.036 0-125 55.964-125 125h125V160z" fill="#fff" class="it-tl-2"
                              fill-opacity=".85"/>
                        <path d="M195 151c0-8.284 6.716-15 15-15M948 130.5c0-12.979-10.521-23.5-23.5-23.5"
                              stroke="#C1C1C1"
                              stroke-width="3"/>
                        <g filter="url(#filter0_d)">
                            <g clip-path="url(#clip0)">
                                <path d="M744.99 247.395a8.752 8.752 0 005.769 8.228v1.618l-.979 54.106v1.831a8.749 8.749 0 00-4.79 7.804 8.747 8.747 0 004.79 7.803v1.198s-.092 8.565.979 10.134c0 0 2.638-1.086 5.17-.483 2.533.603 7.597-.12 10.36-5.911 3.835-8.037 5.678-28.349 5.678-28.349s7.959-12.459 9.406-18.534c1.447-6.075 4.424-28.392-7.374-51.554 0 0-1.146-6.011-1.064-9.165a.867.867 0 00-1.004-.875c-1.99.316-6.128.812-7.772-.194-1.146-.702-1.63-1.234-1.83-1.583a.959.959 0 00-.833-.488h-6.049l-1.241 2.071s-2.256-.151-2.618.518c-.361.668-1.176 9.354-.829 12.37v1.228a8.75 8.75 0 00-5.769 8.227z"
                                      fill="#FFE072"/>
                                <path d="M779.852 287.623c1.798-6.003 1.389-16.156 1.389-16.156l-8.49-.675-.758 31.042s6.04-8.136 7.859-14.211zM772.785 269.403l8.574.754c0-12.258-5.398-28.461-7.978-29.414l-.596 28.66zM762.387 335.216v-6.787l-3.136 1.429-.238 7.621s3.374-.676 3.374-2.263z"
                                      fill="#FEEEB8"/>
                            </g>
                        </g>
                        <mask id="a" fill="#fff">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M959.97 130.899c.02-.464.03-.93.03-1.399 0-17.949-14.551-32.5-32.5-32.5-13.117 0-24.419 7.771-29.554 18.96a46.728 46.728 0 00-9.446-.96c-25.681 0-46.5 20.819-46.5 46.5s20.819 46.5 46.5 46.5c12.875 0 24.529-5.233 32.949-13.688C928.694 202.689 939.469 208 951.5 208c21.815 0 39.5-17.461 39.5-39 0-18.668-13.285-34.273-31.03-38.101z"/>
                        </mask>
                        <path d="M959.97 130.899l-2.997-.127-.107 2.526 2.472.533.632-2.932zm-62.024-14.939l-.606 2.938 2.337.483.996-2.17-2.727-1.251zm23.503 78.352l2.269-1.962-2.114-2.445-2.281 2.29 2.126 2.117zM957 129.5c0 .426-.009.85-.027 1.272l5.995.254c.021-.506.032-1.015.032-1.526h-6zM927.5 100c16.292 0 29.5 13.208 29.5 29.5h6c0-19.606-15.894-35.5-35.5-35.5v6zm-26.827 17.211C905.336 107.048 915.598 100 927.5 100v-6c-14.333 0-26.675 8.493-32.281 20.709l5.454 2.502zM888.5 118c3.032 0 5.988.31 8.84.898l1.212-5.876A49.723 49.723 0 00888.5 112v6zM845 161.5c0-24.024 19.476-43.5 43.5-43.5v-6c-27.338 0-49.5 22.162-49.5 49.5h6zm43.5 43.5c-24.024 0-43.5-19.476-43.5-43.5h-6c0 27.338 22.162 49.5 49.5 49.5v-6zm30.823-12.805C911.443 200.108 900.546 205 888.5 205v6c13.705 0 26.114-5.574 35.074-14.571l-4.251-4.234zM951.5 205c-11.131 0-21.088-4.91-27.782-12.65l-4.538 3.924C926.975 205.288 938.568 211 951.5 211v-6zm36.5-36c0 19.846-16.305 36-36.5 36v6c23.436 0 42.5-18.768 42.5-42h-6zm-28.662-35.169C975.755 137.373 988 151.799 988 169h6c0-20.136-14.325-36.919-33.397-41.034l-1.265 5.865z"
                              fill="#C1C1C1" mask="url(#a)"/>
                        <mask id="b" fill="#fff">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M186.253 151.425a21.973 21.973 0 01-.162-2.669c0-12.015 9.74-21.756 21.756-21.756 10.994 0 20.083 8.154 21.549 18.744 17.133.069 31.001 13.979 31.001 31.128 0 17.191-13.937 31.128-31.128 31.128-9.031 0-17.164-3.846-22.85-9.989a26.544 26.544 0 01-15.977 5.303c-14.603 0-26.442-11.689-26.442-26.107 0-13.012 9.64-23.8 22.253-25.782z"/>
                        </mask>
                        <path d="M186.253 151.425l.466 2.964 2.864-.45-.352-2.878-2.978.364zm43.143-5.681l-2.971.412.357 2.578 2.602.01.012-3zm-22.977 52.267l2.202-2.038-1.838-1.985-2.164 1.623 1.8 2.4zm-17.188-46.95a19.022 19.022 0 01-.14-2.305h-6c0 1.026.062 2.038.184 3.033l5.956-.728zm-.14-2.305c0-10.359 8.397-18.756 18.756-18.756v-6c-13.672 0-24.756 11.084-24.756 24.756h6zM207.847 130c9.475 0 17.314 7.029 18.578 16.156l5.943-.824C230.699 133.279 220.359 124 207.847 124v6zm55.55 46.872c0-18.802-15.204-34.052-33.988-34.128l-.025 6c15.482.062 28.013 12.632 28.013 28.128h6zM229.269 211c18.848 0 34.128-15.28 34.128-34.128h-6c0 15.535-12.594 28.128-28.128 28.128v6zm-25.052-10.951c6.231 6.731 15.15 10.951 25.052 10.951v-6c-8.161 0-15.506-3.471-20.648-9.027l-4.404 4.076zm-13.775 6.265a29.544 29.544 0 0017.777-5.903l-3.6-4.8a23.545 23.545 0 01-14.177 4.703v6zM161 177.207c0 16.111 13.218 29.107 29.442 29.107v-6c-12.983 0-23.442-10.381-23.442-23.107h-6zm24.787-28.746C171.766 150.665 161 162.667 161 177.207h6c0-11.484 8.514-21.057 19.719-22.818l-.932-5.928z"
                              fill="#C1C1C1" mask="url(#b)"/>
                        <mask id="c" fill="#fff">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M430.474 36.74a33.033 33.033 0 01-.244-4.014C430.23 14.652 444.882 0 462.956 0c16.537 0 30.208 12.265 32.415 28.195C521.141 28.299 542 49.222 542 75.017c0 25.86-20.963 46.823-46.823 46.823-13.584 0-25.817-5.785-34.37-15.025-6.676 5.006-15.003 7.977-24.033 7.977-21.966 0-39.774-17.583-39.774-39.271 0-19.572 14.501-35.8 33.474-38.781z"/>
                        </mask>
                        <path d="M430.474 36.74l.466 2.963 2.864-.45-.352-2.877-2.978.364zm64.897-8.545l-2.972.411.357 2.578 2.603.01.012-3zm-34.564 78.62l2.202-2.038-1.838-1.985-2.164 1.623 1.8 2.4zm-27.355-70.44a30.084 30.084 0 01-.222-3.65h-6c0 1.482.091 2.943.266 4.379l5.956-.728zm-.222-3.65C433.23 16.31 446.539 3 462.956 3v-6c-19.731 0-35.726 15.995-35.726 35.726h6zM462.956 3c15.018 0 27.439 11.14 29.443 25.606l5.943-.823C495.933 10.39 481.011-3 462.956-3v6zM545 75.017c0-27.448-22.195-49.711-49.617-49.822l-.024 6C519.478 31.292 539 50.875 539 75.017h6zm-49.823 49.823c27.517 0 49.823-22.306 49.823-49.823h-6c0 24.203-19.62 43.823-43.823 43.823v6zm-36.572-15.987c9.097 9.828 22.118 15.987 36.572 15.987v-6c-12.714 0-24.159-5.41-32.168-14.063l-4.404 4.076zm-21.831 8.939c9.698 0 18.653-3.193 25.833-8.577l-3.6-4.8c-6.172 4.628-13.871 7.377-22.233 7.377v6zM394 75.521c0 23.381 19.187 42.271 42.774 42.271v-6c-20.346 0-36.774-16.275-36.774-36.271h-6zm36.009-41.745C409.627 36.98 394 54.421 394 75.521h6c0-18.044 13.375-33.058 30.94-35.818l-.931-5.927z"
                              fill="#C1C1C1" mask="url(#c)"/>
                        <path stroke="#C1C1C1" stroke-width="3" d="M0 513.5h1170"/>
                        <path stroke="#C1C1C1" stroke-width="2" stroke-dasharray="20 20" d="M0 466h1170"/>
                        <path stroke="#C1C1C1" stroke-width="3" d="M0 414.5h1170"/>
                        <mask id="d" maskUnits="userSpaceOnUse" x="397" y="78" width="365" height="373">
                            <path fill="#fff" d="M397 78h364.726v373H397z"/>
                        </mask>
                        <g filter="url(#filter1_d)" mask="url(#d)">
                            <path d="M588.889 116.776a162.128 162.128 0 0157.546 16.111l2.747-4.195-4.091-2.69a2.99 2.99 0 01-.851-4.123l4.983-7.614a2.973 2.973 0 014.112-.854l23.07 15.181a2.991 2.991 0 01.852 4.123l-4.983 7.612a2.972 2.972 0 01-4.112.856l-4.087-2.69-2.071 3.165a164.342 164.342 0 0127.97 22.494c29.587 29.666 47.889 70.649 47.889 115.918 0 45.266-18.302 86.251-47.889 115.914C660.387 425.65 619.511 444 574.364 444c-45.149 0-86.024-18.35-115.612-48.016-29.587-29.663-47.889-70.648-47.889-115.914 0-45.269 18.3-86.252 47.889-115.918a164.322 164.322 0 0126.567-21.591l-2.661-4.068-4.089 2.69a2.972 2.972 0 01-4.112-.856l-4.983-7.61a2.994 2.994 0 01.854-4.125l23.068-15.181a2.976 2.976 0 014.115.854l4.983 7.614a2.991 2.991 0 01-.854 4.123l-4.088 2.69 3.236 4.944a162.138 162.138 0 0159.047-16.86v-7.211h-7.975c-2.664 0-4.842-2.187-4.842-4.858v-14.85c0-2.673 2.178-4.857 4.842-4.857h45.006c2.664 0 4.843 2.184 4.843 4.858v14.849c0 2.671-2.179 4.858-4.843 4.858h-7.977v7.211zm83.578 64.93c-25.107-25.17-59.791-40.742-98.103-40.742-38.313 0-73 15.572-98.104 40.742-25.104 25.17-40.635 59.951-40.635 98.364 0 38.412 15.531 73.188 40.635 98.361 25.106 25.17 59.791 40.74 98.104 40.74 38.312 0 72.998-15.57 98.103-40.74 25.104-25.173 40.632-59.949 40.632-98.361 0-38.413-15.528-73.191-40.632-98.364z"
                                  fill="#fff"/>
                        </g>
                        <path stroke="#C1C1C1" stroke-width="3"
                              d="M875.5 266.5h90v148h-90zM965.5 327.5h45v87h-45zM337.5 322.5h52v92h-52zM196.5 296.5h51v118h-51zM182.5 355.5h14v59h-14z"/>
                        <path stroke="#C1C1C1"
                              d="M887.5 285.5h18v17h-18zM259.5 285.5h18v17h-18zM887.5 313.5h18v17h-18zM259.5 313.5h18v17h-18zM213.5 313.5h18v17h-18zM887.5 341.5h18v17h-18zM259.5 341.5h18v17h-18zM213.5 341.5h18v17h-18zM887.5 369.5h18v17h-18zM259.5 369.5h18v17h-18zM213.5 369.5h18v17h-18zM911.5 285.5h18v17h-18zM283.5 285.5h18v17h-18zM911.5 313.5h18v17h-18zM283.5 313.5h18v17h-18zM911.5 341.5h18v17h-18zM283.5 341.5h18v17h-18zM911.5 369.5h18v17h-18zM283.5 369.5h18v17h-18zM935.5 285.5h18v17h-18zM307.5 285.5h18v17h-18zM935.5 313.5h18v17h-18zM307.5 313.5h18v17h-18zM935.5 341.5h18v17h-18zM979.5 341.5h18v17h-18zM307.5 341.5h18v17h-18zM354.5 341.5h18v17h-18zM935.5 369.5h18v17h-18zM979.5 369.5h18v17h-18zM307.5 369.5h18v17h-18zM354.5 369.5h18v17h-18z"/>
                        <path d="M707 275c0-69.036-55.964-125-125-125v125h125z" class="it-tr-1" fill="#FFD336"/>
                        <path d="M409.698 83.954c-4.252-14.376 4.299-29.578 19.098-33.954" stroke="#C1C1C1"
                              stroke-width="3"/>
                        <path d="M700 285c0-69.036-55.964-125-125-125v125h125z" class="it-tr-2" fill="#fff"
                              fill-opacity=".85"/>
                        <path d="M582 407c69.036 0 125-55.964 125-125H582v125z" class="it-br-1" fill="#FFD336"/>
                        <path d="M572 400c69.036 0 125-55.964 125-125H572v125z" class="it-br-2" fill="#fff"
                              fill-opacity=".85"/>
                        <path d="M443 285c0 69.036 55.964 125 125 125V285H443z" class="it-bl-1" fill="#FFD336"/>
                        <path d="M450 275c0 69.036 55.964 125 125 125V275H450z" class="it-bl-2" fill="#fff"
                              fill-opacity=".85"/>
                        <path stroke="#C6C7C7" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"
                              d="M365.5 34.5h20"/>
                        <path stroke="#C1C1C1" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"
                              d="M128.5 136.5h20M805.5 136.5h20M337.5 63.5h33M100.5 165.5h33M777.5 165.5h33M353.5 93.5h25M116.5 195.5h25M793.5 195.5h25"/>
                        <path stroke="#C1C1C1" stroke-width="3"
                              d="M247.5 266.5h90v148h-90zM976.354 327l5.146-6.568 5.146 6.568h-10.292z"/>
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M983.299 298.025l1.023-.836 13.5 16.5-1.15.941.855 1.062c-5.162 4.157-12.716 3.342-16.873-1.819-4.157-5.162-3.342-12.716 1.82-16.873l.825 1.025zm-.144 2.476c-2.762 3.227-2.925 8.062-.164 11.49 2.791 3.466 7.624 4.318 11.383 2.222l-11.219-13.712z"
                              fill="#C1C1C1"/>
                        <path stroke="#C1C1C1" stroke-width="3" stroke-linecap="round"
                              d="M994.852 303.026l3.696-3.079"/>
                        <path stroke="#C1C1C1" stroke-width="3"
                              d="M314.5 266v-28M206.5 283.5h21v13h-21zM910.5 253.5h21v13h-21z"/>
                        <defs>
                            <filter id="filter0_d" x="725" y="203" width="77.789" height="157.099"
                                    filterUnits="userSpaceOnUse"
                                    color-interpolation-filters="sRGB">
                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                                <feOffset/>
                                <feGaussianBlur stdDeviation="10"/>
                                <feColorMatrix values="0 0 0 0 0.976471 0 0 0 0 0.729412 0 0 0 0 0.282353 0 0 0 0.2 0"/>
                                <feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/>
                                <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                            </filter>
                            <filter id="filter1_d" x="398.863" y="76" width="351" height="383"
                                    filterUnits="userSpaceOnUse"
                                    color-interpolation-filters="sRGB">
                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                                <feOffset dy="3"/>
                                <feGaussianBlur stdDeviation="6"/>
                                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
                                <feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/>
                                <feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                            </filter>
                            <clipPath id="clip0">
                                <path transform="rotate(90 782.789 223)" fill="#fff"
                                      d="M782.789 223h117.099v37.789H782.789z"/>
                            </clipPath>
                        </defs>
                    </svg>

                    <div class="waiting-screen__numbers">
                        <span class="waiting-screen__minutes js-minutes">1</span> минут
                        <div class="waiting-screen__seconds js-seconds">53</div>
                        секунд
                    </div>
                    <div class="waiting-screen__description">
                        Проверяем данные...
                    </div>
                    <a href="javascript:void(0);" class="btn js-screen-res-person no-elem js-screen-link"
                       data-type-screen="auto"
                       data-id-screen="12" data-progressbar-id="8">Перейти к оплате</a>
                </div>
            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="12">
                <div class="calc__result-success js-confirm-success-screen">
                    <div class="row row_center">
                        <svg width="70" height="71" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="1.25" y="9.25" width="51.5" height="59.5" rx="3.75" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <rect x="9.25" y="4.25" width="12.5" height="4.5" rx="1.75" fill="#FFD336" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <rect x="32.25" y="4.25" width="12.5" height="4.5" rx="1.75" fill="#FFD336" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <path stroke="#32353A" stroke-width="2.5" d="M14 42.75h9M14 26.75h9M14 48.75h6M14 32.75h6"/>
                            <path d="M28.5 44.5l5 3.5 6-6M28 28.5l5 3.5 6-6" stroke="#32353A" stroke-width="2.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                            <path fill="#FFD336" d="M7 58h40v5H7z"/>
                            <path stroke="#32353A" stroke-width="2.5" d="M6 57.75h42"/>
                            <rect x="6.25" y="14.25" width="41.5" height="49.5" rx="3.75" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <path d="M21.25 7a5.75 5.75 0 0111.5 0v8A2.75 2.75 0 0130 17.75h-6A2.75 2.75 0 0121.25 15V7z"
                                  fill="#EDF1F2" stroke="#32353A" stroke-width="2.5"/>
                            <path fill="#FFD336" d="M58 16h8v4h-8z"/>
                            <path d="M66 12.286a4 4 0 00-8 0M58 20.036h9" stroke="#32353A" stroke-width="2.5"/>
                            <path d="M66 61.786l-4 7-4-7v-46.5h8v46.5z" stroke="#32353A" stroke-width="2.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>

                    <div class="calc__title-inner calc__title-inner_lg">
                        Проверка данных прошла успешно
                    </div>
                    <div class="alert alert_100 alert_noicon js-icon-alert">
                        <div class="row row_space-beetween w100">
                            <img class="ofeer-img"
                                 src="http://pitstop.dev.letsrock.pro/storage/allinsurances/September2019/AYUwVFL3fXhvdMIjWxFR.png">
                            <div class="alert-info">
                                <div class="price-text">Стоимость</div>
                                <div class="price-number">10.500 ₽</div>
                            </div>
                        </div>

                    </div>

                    <p class="calc__text calc__text_md">Оплата оформленного полиса производится на официальном сайте
                        страховой компании.
                        Нажмите на кнопку «Перейти к оплате», заполните данные вашей платёжной карты, оплатите и
                        получите электронный полис ОСАГО на ваш e-mail</p>
                    <div class="row row_center">
                        <a href="javascript:void(0);" class="btn js-location-order"
                        >Перейти к оплате</a>
                    </div>
                </div>

                <div class="calc__result-nosuccess js-confirm-nosuccess-screen">
                    <div class="row row_center">
                        <svg width="70" height="71" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="1.25" y="9.25" width="51.5" height="59.5" rx="3.75" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <rect x="9.25" y="4.25" width="12.5" height="4.5" rx="1.75" fill="#FFD336" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <rect x="32.25" y="4.25" width="12.5" height="4.5" rx="1.75" fill="#FFD336" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <path stroke="#32353A" stroke-width="2.5" d="M14 42.75h9M14 26.75h9M14 48.75h6M14 32.75h6"/>
                            <path d="M28.5 44.5l5 3.5 6-6M28 28.5l5 3.5 6-6" stroke="#32353A" stroke-width="2.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                            <path fill="#FFD336" d="M7 58h40v5H7z"/>
                            <path stroke="#32353A" stroke-width="2.5" d="M6 57.75h42"/>
                            <rect x="6.25" y="14.25" width="41.5" height="49.5" rx="3.75" stroke="#32353A"
                                  stroke-width="2.5"/>
                            <path d="M21.25 7a5.75 5.75 0 0111.5 0v8A2.75 2.75 0 0130 17.75h-6A2.75 2.75 0 0121.25 15V7z"
                                  fill="#EDF1F2" stroke="#32353A" stroke-width="2.5"/>
                            <path fill="#FFD336" d="M58 16h8v4h-8z"/>
                            <path d="M66 12.286a4 4 0 00-8 0M58 20.036h9" stroke="#32353A" stroke-width="2.5"/>
                            <path d="M66 61.786l-4 7-4-7v-46.5h8v46.5z" stroke="#32353A" stroke-width="2.5"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>

                    <div class="calc__title-inner calc__title-inner_lg">
                        Сервис оформления ОСАГО у выбранной вами страховой компании недоступен, выберите другую
                        компанию:
                    </div>
                    <div class="mess_info"></div>
                    <div class="alert-list js-select-alert-list">

                    </div>

                    <p class="calc__text calc__text_md">Оплата оформленного полиса производится на официальном сайте
                        страховой компании АльфаСтрахование.
                        Нажмите на кнопку «Перейти к оплате», заполните данные вашей платёжной карты, оплатите и
                        получите электронный полис ОСАГО на ваш e-mail</p>
                    <div class="row row_center">
                        <a href="javascript:void(0);" class="btn js-re-offer"
                           data-type-screen="auto"
                           data-calslug=""
                        >Перейти к оплате</a>
                    </div>
                    <div class="row row_center">
                        <a href="javascript:void(0);" class="btn no-elem js-re-offer-prev js-screen-link"
                           data-type-screen="auto"
                           data-id-screen="9" data-progressbar-id="8">Перейти к оплате</a>
                    </div>
                </div>

            </div>
            <div class="screen js-screen" data-type-screen="auto" data-id-screen="13">
                <div class="calc__inner calc__auto-param">
                    <div class="alert alert_100 alert_noicon js-icon-alert">
                        <div class="row row_space-beetween w100">
                            <img class="ofeer-img"
                                 src="http://pitstop.dev.letsrock.pro/storage/allinsurances/September2019/AYUwVFL3fXhvdMIjWxFR.png">
                            <div class="alert-info">
                                <div class="price-text">Стоимость</div>
                                <div class="price-number">10.500 ₽</div>
                            </div>
                        </div>
                    </div>
                    <div class="calc__title-inner">
                        Контактные данные
                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_part-3">
                            <label for="MAIL_PERSON" class="input__name">Email<span class="red">*</span></label>
                            <input type="text" placeholder="Электронная почта" name="MAIL_PERSON" id="MAIL_PERSON"
                                   tabindex="0" class="req" value="@auth{{ Auth::user()->email }}@endif"
                                   onfocus="this.removeAttribute('readonly')" autocomplete="off" data-pattern="mail">
                        </div>
                        <div class="input input_part-3">
                            <label for="PHONE_PERSON" class="input__name">Телефон для связи<span
                                        class="red">*</span></label>
                            <input type="text" placeholder="Телефон для связи" name="PHONE_PERSON" id="PHONE_PERSON"
                                   tabindex="0" class="js-btn-look req" value="@auth{{ Auth::user()->phone }}@endif"
                                   data-mask="+7 (000) 000 00-00">
                        </div>
                        <div class="input input_part-3">
                            <div class="no-elem js-end-confirm-sms">
                                <label for="confirmCode" class="input__name">Код подтверждения<span
                                            class="red">*</span></label>
                                <input type="text" placeholder="Код подтверждения" name="confirmCode" id="confirmCode"
                                       tabindex="0" class="req" data-mask="000000">
                            </div>
                        </div>
                        <?/*<div class="input input_part-3 input_check-phone">
                            <a href="javascript:void(0);"
                               class="btn btn_big btn_check-phone"
                               data-click="confirmPhone">Подтвердить номер телефона</a>
                        </div>*/?>
                    </div>
                    <?/*<div class="row row_space-beetween no-elem" id="confirm-phone-row">
                        <div class="input input_part-2-3">
                            <label for="MAIL_PERSON" class="input__name">
                                Код подтверждения<span class="red">*</span>
                            </label>
                            <input
                                type="text"
                                placeholder="Код подтверждения"
                                name="CONFIRMATION_CODE"
                                id="CONFIRMATION_CODE"
                                class="req"
                                value=""
                                autocomplete="off"
                            />
                        </div>
                        <div class="input input_part-3 input_check-phone">
                            <a
                                href="javascript:void(0);"
                                class="btn btn_big btn_check-phone"
                                data-click="confirmSmsCode"
                            >Подтвердить код из SMS</a>
                        </div>
                    </div>*/?>
                    <div class="row row_space-beetween block_pass no-elem">
                        <div class="input input_part-2">
                            <span class="mess_verifi"></span><br/><br/>
                            <label for="PASSWORD_PERSON" class="input__name">Пароль<span class="red">*</span></label>
                            <input type="password" placeholder="Пароль от личного кабинета" name="PASSWORD_PERSON"
                                   id="PASSWORD_PERSON" value="" tabindex="0" class="req" readonly
                                   onfocus="this.removeAttribute('readonly')" autocomplete="off">
                        </div>
                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_part-2">
                            <input type="checkbox" class="checkbox-h" name="check_calc" id="check_calc" checked>
                            <label for="check_calc" class="form-block__nda">
                                <a href="/policy" target="_blank">Нажимая, я соглашаюсь на обработку персональных
                                    данных</a>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row buttons-wrap">
                    <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link" data-type-screen="auto"
                       data-id-screen="7" data-progressbar-id="7">Назад</a>
                    <a href="javascript:void(0);" class="btn btn_big js-send-offer" data-click="confirmSmsCodeOffer">Отправить
                        код</a>
                    @if(!empty($slug) && $slug == 'calc')
                        <a
                            href="javascript:void(0);"
                            class="btn btn_big js-screen-link js-end-offer no-elem"
                            data-calslug=""
                            data-type-screen="auto"
                            data-id-screen="14"
                            data-progressbar-id="8"
                            data-vid="police-please"

                        >Оформить</a>
                    @else
                        <a href="javascript:void(0);" class="btn btn_big js-screen-link js-end-offer no-elem"
                           data-type-screen="auto"
                           data-click="takeInfo"
                           data-id-screen="8" data-progressbar-id="8"
                           data-vid="site">Оформить</a>
                    @endif
                </div>
                <span class="mess_info"></span>
            </div>
            @if(!empty($slug) && $slug == 'calc')
                <div class="screen js-screen" data-type-screen="auto" data-id-screen="14">
                    <div class="calc__inner">
                        <div class="row row_column-center calc__order-step">
                            <div class="calc__order-step-number">1</div>
                            <div class="calc__order-step-row">
                                <div class="calc__order-step-text">
                                    Отсканируйте QR-код с помощью мобильного телефона или нажмите кнопку для оплаты с
                                    терминала
                                </div>
                                <div class="qr-code">
                                    <img class="js-qr-code" src="" alt="qr">
                                </div>
                            </div>
                            <div class="row_column calc__buttons-wrapper">
                                <a class="btn btn_big btn_gos js-pay-button"
                                   href="">Оплата</a>
                            </div>
                        </div>
                        <div class="row row_column-center calc__order-step">
                            <div class="calc__order-step-number">2</div>
                            <div class="calc__order-step-text">
                                После оплаты нажмите эту кнопку
                            </div>
                            <div class="row_column calc__buttons-wrapper">
                                <a class="btn btn_big btn_gos"
                                   href="javascript:void(0);" data-click="unDocPolice" data-slugdoc="" data-policeid=""
                                   data-dopid="">Печать</a>
                            </div>
                        </div>
                        <div class="row calc__order-step-nav">
                            <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link"
                               data-type-screen="auto"
                               data-id-screen="13" data-progressbar-id="8">Назад</a>
                        </div>
                        <span class="mess_info"></span>
                    </div>
                </div>
            @endif
        </div>
        @csrf
    </form>
</div>
