<div class="container screen js-screen" data-type-screen="diag" data-id-screen="2">
        <form class="screen screen_active js-screen" data-type-screen="step" data-id-screen="1">
            @csrf
            <div class="calc__inner calc__auto-param">
                <h3>Личные данные</h3>
                <div class="row row_space-beetween">
                    <div class="input input_part-2">
                        <label for="FIRST_NAME" class="input__name">ФИО <span class="red">*</span></label>
                        <input type="text" placeholder="ФИО" name="FIRST_NAME" id="FIRST_NAME" tabindex="0" class="req" value="@auth{{ Auth::user()->name }}@endif">
                    </div>
                    <div class="input input_part-2">
                        <label for="TEL" class="input__name">Телефон <span class="red">*</span></label>
                        <input type="text" placeholder="Телефон" id="TEL" name="TEL" tabindex="0" data-mask="+7 (000) 000 00-00" class="req" value="@auth{{ Auth::user()->phone }}@endif">
                    </div>
                </div>
                <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label for="DATE_BIRTH" class="input__name">Желаемая дата <span class="red">*</span></label>
                        <input type="text" placeholder="Желаемая дата" name="DATE_BIRTH" id="DATE_BIRTH" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                        <span class="input__subtext">
                            <span class="red">*</span> Вы выбираете дату, мы перезваниваем вам и выбираем наиболее удобное для вас время</span>
                    </div>
                    <div class="input input_part-3">
                        <label for="DATE_BIRTH_TIME" class="input__name">Время <span class="red">*</span></label>
                        <select name="DATE_BIRTH_TIME" id="DATE_BIRTH_TIME" class="js-select req_select">
                            <option value="0">Выбрать</option>
                            @foreach ($list_time as $item)
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input input_part-3">
                        <span class="input__name">Категория <span class="red">*</span></span>
                        <div class="radio-group radi" data-errname="Выберите категорию">
                            <label class="radio">
                                <input type="radio" name="category" class="category__radio-hidden" tabindex="0" value="A">
                                <div class="radio__inner">
                                    <span class="radio__name">A</span>
                                </div>
                            </label>
                            <label class="radio">
                                <input type="radio" name="category" class="category__radio-hidden" tabindex="0" value="B">
                                <div class="radio__inner">
                                    <span class="radio__name">B</span>
                                </div>
                            </label>
                            <label class="radio">
                                <input type="radio" name="category" class="category__radio-hidden" tabindex="0" value="C">
                                <div class="radio__inner">
                                    <span class="radio__name">C</span>
                                </div>
                            </label>
                            <label class="radio">
                                <input type="radio" name="category" class="category__radio-hidden" tabindex="0" value="D">
                                <div class="radio__inner">
                                    <span class="radio__name">D</span>
                                </div>
                            </label>
                            <label class="radio">
                                <input type="radio" name="category" class="category__radio-hidden" tabindex="0" value="E">
                                <div class="radio__inner">
                                    <span class="radio__name">E</span>
                                </div>
                            </label>
                        </div>
                    </div>
                    <?/*<div class="input input_part-3 input_power input_no-heading">
                        <label class="checkbox-r checkbox-r_power">
                            <span class="checkbox-r__text-l">до 1,5 тонн</span>
                            <input type="checkbox" name="horns">
                            <span class="checkbox-r__text">от 1,5 тонн</span>
                        </label>
                    </div>*/?>
                </div>
                <div class="row row_m-top row_center">
                    <div class="input">
                        <input type="checkbox" class="checkbox-h" name="nda" id="checknda2" checked>
                        <label for="checknda2" class="form-block__nda">
                            Нажимая, я соглашаюсь на обработку персональных
                            данных
                        </label>
                    </div>
                </div>
                <div class="row row_m-top row_center">
                    <a href="javascript:void(0);" class="c-welcome__btn btn js-screen-link" data-click="calc_diag">{{ $text_button }}</a>
                </div>
            </div>
            <span class="mess_info"></span>
        </form>
</div>