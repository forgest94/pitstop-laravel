<?
$years18 = strtotime('- 18 years');
?>
    <div class="screen screen_active js-screen" data-type-screen="person" data-id-screen="1">
        <span class="mess_good"></span>
        <div class="personal-table js-personal-person form_list">
                @if($person->count())
                    @foreach ($person as $key => $item)
                        <div class="personal-table__row">
                            <div class="personal-table__column personal-table__column_name" data-name="{{ $item->id }}">{{ $item->LastName }} {{ $item->FirstName }} {{ $item->MiddleName }}</div>
                            <div class="personal-table__column personal-table__column_look"><a href="javascript:void(0);" class="personal-table__link personal-table__link_look js-screen-link" data-type-screen="person" data-id-screen="edit-{{ $key }}">Посмотреть</a></div>
                            <div class="personal-table__column personal-table__column_remove"><a href="javascript:void(0);" class="personal-table__link personal-table__link_remove" data-id="{{ $item->id }}" data-open="removePersons">Удалить</a></div>
                        </div>
                    @endforeach
                @else
                    <div class="personal-table__row empty_list_elem">
                        <div class="personal-table__column personal-table__column_name">Ваш список пуст</div>
                    </div>
                @endif
                
            
            <a href="javascript:void(0);" class="personal-table__btn btn btn_add btn_dark js-screen-link" data-type-screen="person" data-id-screen="add-1">Добавить персону</a>
        </div>
    </div>
    @if($person->count())
        @foreach ($person as $key => $item)
            <form data-slep="Y">
                <div class="screen js-screen" data-type-screen="person" data-id-screen="edit-{{ $key }}">
                <h3>Редактирование персоны.</h3>
                    <div class="row row_space-beetween">
                        <div class="input input_part-3"> 
                            <label for="LAST_NAME_AUTO_edit-{{ $key }}" class="input__name">Фамилия <span class="red">*</span></label>
                            <input type="text" placeholder="Фамилия" name="LAST_NAME_AUTO_edit" id="LAST_NAME_AUTO_edit-{{ $key }}" tabindex="0" class="req" value="{{ $item->LastName }}">
                        </div>
                        <div class="input input_part-3">
                            <label for="FIRST_NAME_AUTO_edit-{{ $key }}" class="input__name">Имя <span class="red">*</span></label>
                            <input type="text" placeholder="Имя" name="FIRST_NAME_AUTO_edit" id="FIRST_NAME_AUTO_edit-{{ $key }}" tabindex="0" class="req" value="{{ $item->FirstName }}">
                        </div>
                        <div class="input input_part-3">
                            <label for="MIDDLE_NAME_AUTO_edit-{{ $key }}" class="input__name">Отчество <span class="red">*</span></label>
                            <input type="text" placeholder="Отчество" id="MIDDLE_NAME_AUTO_edit-{{ $key }}" name="MIDDLE_NAME_AUTO_edit" tabindex="0" class="req" value="{{ $item->MiddleName }}">
                        </div>
                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_part-3">
                            <label for="DATE_BIRTH_AUTO_edit-{{ $key }}" class="input__name">Дата рождения <span class="red">*</span></label>
                            <input type="text" placeholder="Дата рождения" name="DATE_BIRTH_AUTO_edit" id="DATE_BIRTH_AUTO_edit-{{ $key }}" tabindex="0" class="js-datepicker req"
                                data-mask="00.00.0000" autocomplete="off" maxlength="10" data-maxdate="{{ gmdate('Y-m-d', $years18) }}" readonly value="{{ Date::parse($item->BirthDate)->format('d.m.Y') }}" data-view="years">
                        </div>
                        <div class="input input_part-3">
                            <label for="DRIVE_DOCUMENT_AUTO_edit-{{ $key }}" class="input__name">Серия и нормер ВУ <span class="red">*</span></label>
                            <input type="text" placeholder="Серия и нормер ВУ" id="DRIVE_DOCUMENT_AUTO_edit-{{ $key }}" name="DRIVE_DOCUMENT_AUTO_edit" tabindex="0" class="req" data-mask="0000 000000" value="{{ $item->docser_numb }}">
                        </div>
                        <div class="input input_part-3">
                            <label for="DATE_DRIVE_DOCUMENT_AUTO_edit-{{ $key }}" class="input__name">Дата отсчета стажа <span class="red">*</span></label>
                            <input type="text" placeholder="Дата отсчета стажа" name="DATE_DRIVE_DOCUMENT_AUTO_edit" id="DATE_DRIVE_DOCUMENT_AUTO_edit-{{ $key }}" tabindex="0"
                                class="js-datepicker req" data-mask="00.00.0000" autocomplete="off" maxlength="10" value="{{ Date::parse($item->ExperienceDate)->format('d.m.Y') }}">
                        </div>
                    </div>
                    <div class="row buttons-wrap">
                        <a href="javascript:void(0);" class="btn btn_light js-screen-link" data-type-screen="person" data-id-screen="1">Отмена</a>
                        <a href="javascript:void(0);" class="btn btn_big js-screen-link" data-type-screen="person" data-id-screen="1" data-editPanel="person" data-id="{{ $item->id }}">Сохранить</a>
                    </div>
                    <span class="mess_info"></span>
                </div>
                @csrf
            </form>
        @endforeach
    @endif
        <form data-slep="Y">
        <div class="screen js-screen" data-type-screen="person" data-id-screen="add-1">
            <h3>Добавление персоны.</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="LAST_NAME_AUTO" class="input__name">Фамилия <span class="red">*</span></label>
                    <input type="text" placeholder="Фамилия" name="LAST_NAME_AUTO" id="LAST_NAME_AUTO" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="FIRST_NAME_AUTO" class="input__name">Имя <span class="red">*</span></label>
                    <input type="text" placeholder="Имя" name="FIRST_NAME_AUTO" id="FIRST_NAME_AUTO" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="MIDDLE_NAME_AUTO" class="input__name">Отчество <span class="red">*</span></label>
                    <input type="text" placeholder="Отчество" id="MIDDLE_NAME_AUTO" name="MIDDLE_NAME_AUTO" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="DATE_BIRTH_AUTO" class="input__name">Дата рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Дата рождения" name="DATE_BIRTH_AUTO" id="DATE_BIRTH_AUTO" tabindex="0" class="js-datepicker req"
                           data-mask="00.00.0000" autocomplete="off" maxlength="10" data-maxdate="{{ gmdate('Y-m-d', $years18) }}" readonly data-view="years">
                </div>
                <div class="input input_part-3">
                    <label for="DRIVE_DOCUMENT_AUTO" class="input__name">Серия и нормер ВУ <span class="red">*</span></label>
                    <input type="text" placeholder="Серия и нормер ВУ" id="DRIVE_DOCUMENT_AUTO" name="DRIVE_DOCUMENT_AUTO" tabindex="0" class="req" data-mask="0000 000000">
                </div>
                <div class="input input_part-3">
                    <label for="DATE_DRIVE_DOCUMENT_AUTO" class="input__name">Дата отсчета стажа <span class="red">*</span></label>
                    <input type="text" placeholder="Дата отсчета стажа" name="DATE_DRIVE_DOCUMENT_AUTO" id="DATE_DRIVE_DOCUMENT_AUTO" tabindex="0"
                           class="js-datepicker req" data-mask="00.00.0000" autocomplete="off" maxlength="10">
                </div>
            </div>
            <div class="row buttons-wrap">
                <a href="javascript:void(0);" class="btn btn_light js-screen-link" data-type-screen="person" data-id-screen="1">Отмена</a>
                <a href="javascript:void(0);" class="btn btn_big js-screen-link" data-type-screen="person" data-id-screen="1" data-addPanel="person">Сохранить</a>
            </div>
        </div>
        @csrf
        </form>