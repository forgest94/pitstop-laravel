<?
use App\Advantage;
$items_adv = Advantage::getList();
?>
<div class="advantages">
    <div class="advantages__inner container">
        <div class="advantages__timer wow slideInLeft"></div>
        <h3 class="advantages__title">Наши преимущества</h3>
        <div class="advantages__cards">
            @foreach ($items_adv as $key => $item)
                <?$key++;?>
                <div class="h-card wow fadeInUp" data-wow-delay="0.<?=$key;?>>s">
                    <div class="h-card__image" style="background-image: url('{!! Voyager::image($item->img) !!}')"></div>
                    <div class="h-card__inner">
                        <p class="h-card__title">
                            {{ $item->name }}
                        </p>
                        <p class="h-card__text">
                            {{ $item->text }}
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
