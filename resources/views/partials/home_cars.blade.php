    <div class="screen screen_active js-screen" data-type-screen="auto" data-id-screen="1">
        <span class="mess_good"></span>
        <div class="personal-table js-personal-auto form_list">
            @if($cars->count())
                @foreach ($cars as $key => $item)
                    <div class="personal-table__row">
                            <div class="personal-table__column personal-table__column_name" data-name="{{ $item->id }}">{{ explode('|',$item->mark)[1] }} {{ explode('|',$item->model)[1] }} {{ $item->year }}</div>
                            <div class="personal-table__column personal-table__column_look">
                                <a href="javascript:void(0);" class="personal-table__link personal-table__link_look js-screen-link" data-type-screen="auto" data-id-screen="edit-{{ $key }}">Посмотреть</a>
                            </div>
                            <div class="personal-table__column personal-table__column_remove">
                                <a href="javascript:void(0);" class="personal-table__link personal-table__link_remove" data-id="{{ $item->id }}"  data-open="removeDriver">Удалить</a>
                            </div>
                    </div>
                @endforeach
            @else
                <div class="personal-table__row empty_list_elem">
                    <div class="personal-table__column personal-table__column_name">Ваш список пуст</div>
                </div>
            @endif
        <a href="javascript:void(0);" class="personal-table__btn btn btn_add btn_dark js-screen-link" data-type-screen="auto" data-id-screen="add-1">Добавить
            автомобиль</a>
        </div>
    </div>
@if($cars->count())
    @foreach ($cars as $keyCar => $itemCar)
        <form action="" data-slep="Y">
        <div class="screen js-screen" data-type-screen="auto" data-id-screen="edit-{{ $keyCar }}">
        <h3>Редактирование автомобиля. Шаг 1: Сведения о ТС </h3>
        <div class="row row_space-beetween">
                <div class="input input_part-3">
                        <span class="input__name">Категория</span>
                        <div class="radio-group">
                            @foreach ($list_cat as $item)
                            <?if($item->id_category == 0 || !$item->visible)continue;$buk=explode(' -',$item->name_category);?>
                            <?$checked="";if($item->id_category.'|'.$item->name_category == $itemCar->category){$checked="checked";}?>
                                <label class="radio">
                                    <input type="radio" name="category_edit" value="{{ $item->id_category }}|{{ $item->name_category }}" class="category__radio-hidden" tabindex="0" {{ $checked }}>
                                    <div class="radio__inner">
                                        <span class="radio__name">{{ $buk[0] }}</span>
                                    </div>
                                </label>
                            @endforeach
                        </div>
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_MAKE_edit-{{ $keyCar }}" class="input__name">Марка автомобиля <span class="red">*</span></label>
                    <div class="tooltip" data-tooltip="Марка автомобиля"></div>
                    <select name="AUTO_MAKE_edit" id="AUTO_MAKE_edit-{{ $keyCar }}" class="js-select req_select" data-ajaxs="Y" data-type="elems" data-url="select_mark" data-idelem="FIRST_MODEL_edit-{{ $keyCar }}" data-typei="select">
                        <option value="0">Выбрать</option>
                        @foreach ($list_mark as $item)
                            <?$select="";if($item->id_mark.'|'.$item->value == $itemCar->mark){$select="selected";}?>
                            <option value="{{ $item->id_mark }}|{{ $item->value }}" {{ $select }}>{{ $item->value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input input_part-3">
                    <label for="FIRST_MODEL_edit-{{ $keyCar }}" class="input__name">Модель <span class="red">*</span></label>
                    <div class="tooltip" data-tooltip="Модель автомобиля"></div>
                    <select name="FIRST_MODEL_edit" id="FIRST_MODEL_edit-{{ $keyCar }}" class="js-select req_select">
                        <?$exModel = explode('|', $itemCar->model);?>
                        <option value="{{ $itemCar->mark }}">{{ $exModel[1] }}</option>
                    </select>
                </div>
            </div>
            <div class="row row_space-beetween">
                    <?
                        $checkNs = '';
                        $checkNs2 = '';
                        $checkNs3 = '';
                        $input = '';
                        if($itemCar->vin){
                            $checkNs = 'checked';
                            $input = '
                            <label for="VIN_edit-'.$keyCar.'" class="input__name">VIN <span class="red">*</span></label>
                            <input type="text" placeholder="VIN" name="VIN_edit" id="VIN_edit-'.$keyCar.'" tabindex="0" class="req" data-pattern="vin" data-mask="AAAAAAAAAAAAAAAAA" data-textbig="Y" value="'.$itemCar->vin.'">
                            ';
                        }elseif($itemCar->number_bodywork){
                            $checkNs2 = 'checked';
                            $input = '
                            <label for="bodywork_edit-'.$keyCar.'" class="input__name">Кузов <span class="red">*</span></label>\
                            <input type="text" placeholder="Кузов" name="bodywork_edit" id="bodywork_edit-'.$keyCar.'" tabindex="0" class="req" data-textbig="Y" value="'.$itemCar->number_bodywork.'"></input>
                            ';
                        }elseif($itemCar->number_chassis){
                            $checkNs3 = 'checked';
                            $input = '
                            <label for="chassis_edit-'.$keyCar.'" class="input__name">Шасси <span class="red">*</span></label>\
                            <input type="text" placeholder="Шасси" name="chassis_edit" id="chassis_edit-'.$keyCar.'" tabindex="0" class="req" data-textbig="Y" value="'.$itemCar->number_chassis.'"></input>
                            ';
                        }
                    ?>
                    <div class="input input_part-3">
                            <span class="input__name">Номер</span>
                            <div class="radio-group">
                                <label class="radio">
                                    <input type="radio" name="number_type_edit" value="vin" class="category__radio-hidden" data-change="select_number" data-edit="Y" tabindex="0" {{ $checkNs }}>
                                    <div class="radio__inner">
                                        <span class="radio__name">VIN</span>
                                    </div>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="number_type_edit" value="bodywork" class="category__radio-hidden" data-change="select_number" data-edit="Y" tabindex="0" {{ $checkNs2 }}>
                                    <div class="radio__inner">
                                        <span class="radio__name">Кузов</span>
                                    </div>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="number_type_edit" value="chassis" class="category__radio-hidden" data-change="select_number" data-edit="Y" tabindex="0" {{ $checkNs3 }}>
                                    <div class="radio__inner">
                                        <span class="radio__name">Шасси</span>
                                    </div>
                                </label>
                            </div>
                    </div>
                    <div class="input input_part-3" id="inp_number">
                            {!! $input !!}
                    </div>
                    <div class="input input_part-3">
                            <label for="YEAR_edit-{{ $keyCar }}" class="input__name">Год выпуска <span class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Год выпуска автомобиля"></div>
                            <select name="YEAR_edit" id="YEAR_edit-{{ $keyCar }}" class="js-select req_select">
                                <option value="0">Выбрать</option>
                                @foreach($years as $key => $year)
                                    <?$select="";if($year == $itemCar->year){$select="selected";}?>
                                    <option value="{{ $year }}" {{ $select }}>{{ $year }}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="input input_part-3">
                            <label for="AUTO_POWER_edit-{{ $keyCar }}" class="input__name">Мощность двигателя (Л/c) <span class="red">*</span></label>
                            <input type="number" placeholder="Мощность двигателя" name="AUTO_POWER_edit" id="AUTO_POWER_edit-{{ $keyCar }}" tabindex="0" class="req" data-mask="000" value="{{ $itemCar->power }}">
                    </div>
                    <div class="input input_part-3">
                            <label for="REG_NUMBER_edit-{{ $keyCar }}" class="input__name">Регистрационный номер <span class="red">*</span></label>
                            <div class="tooltip" data-tooltip="Регистрационный номер автомобиля"></div>
                            <input type="text" placeholder="Регистрационный номер" name="REG_NUMBER_edit" id="REG_NUMBER_edit-{{ $keyCar }}" tabindex="0" class="req" value="{{ $itemCar->gos_reg }}">
                    </div>
            </div>

        <div class="row buttons-wrap">
        <a href="javascript:void(0);" class="btn btn_light js-screen-link" data-type-screen="auto" data-id-screen="1">Отмена</a>
        <a href="javascript:void(0);" class="btn btn_light btn_next js-next js-screen-link" data-type-screen="auto" data-id-screen="edit2-{{ $keyCar }}"  data-ts="auto" data-is="3" v-on:click="next_cars_save">Следующий шаг</a>
        </div>
        </div>
        <div class="screen js-screen" data-type-screen="auto" data-id-screen="edit2-{{ $keyCar }}">
        <h3>Редактирование автомобиля. Шаг 2: Документ ТС</h3>
        <div class="row row_space-beetween">
                <?
                $checkTs = '';
                $checkTs2 = '';
                if($itemCar->seria_sts){
                    $checkTs = 'checked';
                    $seria = $itemCar->seria_sts;
                    $number= $itemCar->number_sts;
                    $date = Date::parse($itemCar->date_sts)->format('d.m.Y');
                }elseif($itemCar->seria_pts){
                    $checkTs2 = 'checked';
                    $seria = $itemCar->seria_pts;
                    $number= $itemCar->number_pts;
                    $date = Date::parse($itemCar->date_pts)->format('d.m.Y');
                }
                ?>
                <div class="input input_part-3">
                        <span class="input__name">Документ</span>
                        <div class="radio-group">
                            <label class="radio">
                                <input type="radio" name="doc_ts_edit" value="sts" class="category__radio-hidden" data-change="select_number" tabindex="0" {{ $checkTs }}>
                                <div class="radio__inner">
                                    <span class="radio__name">СТС</span>
                                </div>
                            </label>
                            <label class="radio">
                                <input type="radio" name="doc_ts_edit" value="pts" class="category__radio-hidden" data-change="select_number" tabindex="0" {{ $checkTs2 }}>
                                <div class="radio__inner">
                                    <span class="radio__name">ПТС</span>
                                </div>
                            </label>
                        </div>
                </div>
                <div class="input input_part-3">
                        <label for="SERIA_NUMBER_TS_edit-{{ $keyCar }}" class="input__name">Серия и номер документа<span class="red">*</span></label>
                        <div class="tooltip" data-tooltip="Серия и номер документа"></div>
                        <input type="text" placeholder="Серия и номер документа" name="SERIA_NUMBER_TS_edit" id="SERIA_NUMBER_TS_edit-{{ $keyCar }}" data-mask="AAAA AAAAAA" tabindex="0" class="req" value="{{ $seria }} {{ $number }}">
                </div>
                <div class="input input_part-3">
                        <label for="DATE_TS_edit-{{ $keyCar }}" class="input__name">Дата выдачи <span class="red">*</span></label>
                        <div class="tooltip" data-tooltip="Дата выдачи паспорта автомобиля"></div>
                        <input type="text" placeholder="Дата выдачи" name="DATE_TS_edit" id="DATE_TS_edit-{{ $keyCar }}" tabindex="0" class="js-datepicker req" data-mask="00.00.0000" value="{{ $date }}" readonly>
                </div>
        </div>

        <div class="row buttons-wrap">
            <a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link" data-type-screen="auto" data-id-screen="edit-{{ $keyCar }}">Назад</a>
            <a href="javascript:void(0);" class="btn btn_big js-screen-link" data-type-screen="auto" data-id-screen="1" data-editPanel="auto" data-id="{{ $itemCar->id }}">Сохранить</a>
        </div>
        </div>
        @csrf
        </form>
    @endforeach
@endif
<form action="" data-slep="Y">
<div class="screen js-screen" data-type-screen="auto" data-id-screen="add-1">
<h3>Добавление автомобиля. Шаг 1: Сведения о ТС </h3>
<div class="row row_space-beetween">
        <div class="input input_part-3">
                <span class="input__name">Категория</span>
                <div class="radio-group">
                    @foreach ($list_cat as $item)
                    <?if($item->id_category == 0 || !$item->visible)continue;$buk=explode(' -',$item->name_category);?>
                    <?$checked="";if($buk[0] == 'B'){$checked="checked";}?>
                        <label class="radio">
                            <input type="radio" name="category_id" value="{{ $item->id_category }}|{{ $item->name_category }}" class="category__radio-hidden" tabindex="0" {{ $checked }}>
                            <div class="radio__inner">
                                <span class="radio__name">{{ $buk[0] }}</span>
                            </div>
                        </label>
                    @endforeach
                </div>
        </div>
        <div class="input input_part-3">
            <label for="AUTO_MAKE" class="input__name">Марка автомобиля <span class="red">*</span></label>
            <div class="tooltip" data-tooltip="Марка автомобиля"></div>
            <select name="AUTO_MAKE" id="AUTO_MAKE" class="js-select req_select" data-ajaxs="Y" data-type="elems" data-url="select_mark" data-idelem="FIRST_MODEL" data-typei="select">
                <option value="0">Выбрать</option>
                @foreach ($list_mark as $item)
                    <option value="{{ $item->id_mark }}|{{ $item->value }}">{{ $item->value }}</option>
                @endforeach
            </select>
        </div>
        <div class="input input_part-3">
            <label for="FIRST_MODEL" class="input__name">Модель <span class="red">*</span></label>
            <div class="tooltip" data-tooltip="Модель автомобиля"></div>
            <select name="FIRST_MODEL" id="FIRST_MODEL" class="js-select req_select">
                <option value="0">Выбрать</option>
            </select>
        </div>
    </div>
    <div class="row row_space-beetween">
            <div class="input input_part-3">
                    <span class="input__name">Номер</span>
                    <div class="radio-group">
                        <label class="radio">
                            <input type="radio" name="number_type" value="vin" class="category__radio-hidden" data-change="select_number" tabindex="0" checked>
                            <div class="radio__inner">
                                <span class="radio__name">VIN</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="number_type" value="bodywork" class="category__radio-hidden" data-change="select_number" tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Кузов</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="number_type" value="chassis" class="category__radio-hidden" data-change="select_number" tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Шасси</span>
                            </div>
                        </label>
                    </div>
            </div>
            <div class="input input_part-3" id="inp_number">
                    <label for="VIN" class="input__name">VIN <span class="red">*</span></label>
                    <input type="text" placeholder="VIN" name="VIN" id="VIN" tabindex="0" class="req" data-pattern="vin" data-mask="AAAAAAAAAAAAAAAAA" data-textbig="Y">
            </div>
            <div class="input input_part-3">
                    <label for="YEAR" class="input__name">Год выпуска <span class="red">*</span></label>
                    <div class="tooltip" data-tooltip="Год выпуска автомобиля"></div>
                    <select name="YEAR" id="YEAR" class="js-select req_select">
                        <option value="0">Выбрать</option>
                        @foreach($years as $key => $year)
                            <option value="{{ $year }}">{{ $year }}</option>
                        @endforeach
                    </select>
            </div>
            <div class="input input_part-3">
                    <label for="AUTO_POWER" class="input__name">Мощность двигателя (Л/c) <span class="red">*</span></label>
                    <input type="number" placeholder="Мощность двигателя" name="AUTO_POWER" id="AUTO_POWER" tabindex="0" class="req" data-mask="000">
            </div>
            <div class="input input_part-3">
                    <label for="REG_NUMBER" class="input__name">Регистрационный номер <span class="red">*</span></label>
                    <div class="tooltip" data-tooltip="Регистрационный номер автомобиля"></div>
                    <input type="text" placeholder="Регистрационный номер" name="REG_NUMBER" id="REG_NUMBER" tabindex="0" class="req">
            </div>
    </div>

<div class="row buttons-wrap">
<a href="javascript:void(0);" class="btn btn_light js-screen-link" data-type-screen="auto" data-id-screen="1">Отмена</a>
<a href="javascript:void(0);" class="btn btn_light btn_next js-next js-screen-link" data-type-screen="auto" data-id-screen="add-2"  data-ts="auto" data-is="3" v-on:click="next_cars_save">Следующий шаг</a>
</div>
</div>
<div class="screen js-screen" data-type-screen="auto" data-id-screen="add-2">
<h3>Добавление автомобиля. Шаг 2: Документ ТС</h3>
<div class="row row_space-beetween">
        <div class="input input_part-3">
                <span class="input__name">Документ</span>
                <div class="radio-group">
                    <label class="radio">
                        <input type="radio" name="doc_ts" value="sts" class="category__radio-hidden" data-change="select_number" tabindex="0">
                        <div class="radio__inner">
                            <span class="radio__name">СТС</span>
                        </div>
                    </label>
                    <label class="radio">
                        <input type="radio" name="doc_ts" value="pts" class="category__radio-hidden" data-change="select_number" tabindex="0" checked>
                        <div class="radio__inner">
                            <span class="radio__name">ПТС</span>
                        </div>
                    </label>
                </div>
        </div>
        <div class="input input_part-3">
                <label for="SERIA_NUMBER_TS" class="input__name">Серия и номер документа<span class="red">*</span></label>
                <div class="tooltip" data-tooltip="Серия и номер документа"></div>
                <input type="text" placeholder="Серия и номер документа" name="SERIA_NUMBER_TS" id="SERIA_NUMBER_TS" data-mask="AAAA AAAAAA" tabindex="0" class="req">
        </div>
        <div class="input input_part-3">
                <label for="DATE_TS" class="input__name">Дата выдачи <span class="red">*</span></label>
                <div class="tooltip" data-tooltip="Дата выдачи паспорта автомобиля"></div>
                <input type="text" placeholder="Дата выдачи" name="DATE_TS" id="DATE_TS" tabindex="0" class="js-datepicker req" data-mask="00.00.0000" readonly>
        </div>
</div>

<div class="row buttons-wrap">
<a href="javascript:void(0);" class="btn btn_light btn_prev js-screen-link" data-type-screen="auto" data-id-screen="add-1">Назад</a>
<a href="javascript:void(0);" class="btn btn_big js-screen-link" data-type-screen="auto" data-id-screen="1" data-addPanel="auto">Сохранить</a>
</div>
</div>
@csrf
</form>