<div class="price_land no-before">
    <div class="services__background js-parallax-bg"></div>
    <div class="services__inner container">
        <h2>Стоимость восстановления скидки КБМ - 499 рублей</h2>
    </div>
</div>
<div class="container" data-type-screen="kbm" data-id-screen="2">
        <form class="screen screen_active js-screen" data-type-screen="step" data-id-screen="1">
            @csrf
            <div class="calc__inner calc__auto-param">
                <h3>Введите ваши данные</h3>
                <div class="row row_space-beetween">
                    <div class="input input_part-2">
                        <label for="FIRST_NAME" class="input__name">Имя <span class="red">*</span></label>
                        <input type="text" placeholder="Имя" name="FIRST_NAME" id="FIRST_NAME" tabindex="0" class="req" value="@auth{{ Auth::user()->name }}@endif">
                    </div>
                    <div class="input input_part-2">
                            <label for="TEL" class="input__name">Телефон <span class="red">*</span></label>
                            <input type="text" placeholder="Телефон" id="TEL" name="TEL" tabindex="0" data-mask="+7 (000) 000 00-00" class="req" value="@auth{{ Auth::user()->phone }}@endif">
                    </div>
                </div>
                <?/*
                <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label for="LAST_NAME" class="input__name">Фамилия <span class="red">*</span></label>
                        <input type="text" name="LAST_NAME" id="LAST_NAME" tabindex="0" class="req">
                    </div>
                    <div class="input input_part-3">
                        <label for="MIDDLE_NAME" class="input__name">Отчество <span class="red">*</span></label>
                        <input type="text" id="MIDDLE_NAME" name="MIDDLE_NAME" tabindex="0" class="req">
                    </div>
                    <div class="input input_part-3">
                        <label for="TEL" class="input__name">Телефон <span class="red">*</span></label>
                        <input type="text" id="TEL" name="TEL" tabindex="0" data-mask="+7 (000) 000 00-00" class="req">
                    </div>
                    <div class="input input_part-2-3">
                        <label for="PLACE_BIRTH" class="input__name">Электронная почта <span class="red">*</span></label>
                        <input type="text" name="PLACE_BIRTH" id="PLACE_BIRTH" tabindex="0" class="req">
                    </div>
                </div>*/?>
                <div class="row row_m-top row_center">
                    <div class="input">
                        <input type="checkbox" class="checkbox-h" name="nda" id="checknda2" checked>
                        <label for="checknda2" class="form-block__nda">
                            Нажимая, я соглашаюсь на обработку персональных
                            данных
                        </label>
                    </div>
                </div>
                <div class="row row_m-top row_center">
                    <a href="javascript:void(0);" class="c-welcome__btn btn" data-click="calc_kbm">{{ $text_button }}</a>
                </div>
            </div>
            <span class="mess_info"></span>
        </form>
    </div>
