@if ($paginator->hasPages())
    <ul class="personal-page__pagination">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <!--<li class="pagination__item pagination__item_prev disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <a aria-hidden="true"></a>
            </li>-->
        @else
            <li class="personal-page__pagination-item">
                <a href="{{ $paginator->previousPageUrl() }}" class="personal-page__pagination-link personal-page__pagination-link_prev"></a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="personal-page__pagination-item">
                    <span>{{ $element }}</span>
                </li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="personal-page__pagination-item">
                            <a href="javascript:void(0);" class="personal-page__pagination-link active">{{ $page }}</a>
                        </li>
                    @else
                        <li class="personal-page__pagination-item">
                            <a href="{{ $url }}" class="personal-page__pagination-link">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="personal-page__pagination-item">
                <a href="{{ $paginator->nextPageUrl() }}" class="personal-page__pagination-link personal-page__pagination-link_next"></a>
            </li>
        @else
        <!--<li class="pagination__item disabled pagination__item_next" aria-disabled="true" aria-label="@lang('pagination.next')">
                <a aria-hidden="true"></a>
            </li>-->
        @endif
    </ul>
@endif
