@if ($paginator->hasPages())
    <div class="paginator">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <!--<li class="pagination__item pagination__item_prev disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <a aria-hidden="true"></a>
            </li>-->
        @else
        <a href="{{ $paginator->previousPageUrl() }}" class="paginator__item paginator__item_arrow paginator__item_prev"></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pagination__item disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="javascript:void(0);" class="paginator__item paginator__item_active">{{ $page }}</a>
                    @else
                        <a href="{{ $url }}" class="paginator__item">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="paginator__item paginator__item_arrow paginator__item_next"></a>
        @else
            <!--<li class="pagination__item disabled pagination__item_next" aria-disabled="true" aria-label="@lang('pagination.next')">
                <a aria-hidden="true"></a>
            </li>-->
        @endif
        </div>
@endif