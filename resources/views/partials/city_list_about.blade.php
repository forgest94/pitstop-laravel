@empty(!$list_city)
    <div class="maps__inner container">
        <h3 class="maps__title">Мы в городах</h3>
        <div class="city-block">
            @foreach($list_city as $key => $citys)
                <div class="city-box">
                    <span class="city-box__head">{{ $key }}</span>
                    <ul>
                        @foreach($citys as $city)
                            <li><a href="/{{ $city->pages[0]->slug }}/{{ $city->slug }}">{{ $city->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
@endempty
