@empty(!$partners)
        <div class="partners js-partners">
            <div class="partners__inner container">
                <h3 class="partners__title section-title">Наши партнеры</h3>
                <div class="partners__slider-wrap">
                    <div class="partners__slider js-partner-slider">
                        @foreach ($partners as $item)
                            <div class="partners__slide-wrap">
                                <div class="partners__slide" style="background-image: url('{!! Voyager::image($item->img) !!}');"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
@endif