<form class="container" data-type-screen="doc" data-id-screen="2" data-slep="Y">
    <div class="calc__progressbar container">
        <div class="calc__progressbar-points js-progressbar">
            <a href="javascript:void(0);"
               class="calc__progressbar-point calc__progressbar-point_active js-progressbar-point js-screen-link"
               data-type-screen="step" data-id-screen="1"></a>
            <a href="javascript:void(0);" class="calc__progressbar-point js-progressbar-point js-screen-link"
               data-type-screen="step" data-id-screen="2"></a>
            <a href="javascript:void(0);" class="calc__progressbar-point js-progressbar-point js-screen-link"
               data-type-screen="step" data-id-screen="3"></a>
            <a href="javascript:void(0);" class="calc__progressbar-point js-progressbar-point js-screen-link"
               data-type-screen="step" data-id-screen="4"></a>
            <a href="javascript:void(0);" class="calc__progressbar-point js-progressbar-point js-screen-link"
               data-type-screen="step" data-id-screen="5"></a>
               <div class="calc__progressbar-loading js-loading"></div>
        </div>
        <div class="calc__progressbar-items">
            <a href="javascript:void(0);"
               class="calc__progressbar-item calc__progressbar-item_active js-progressbar-item js-screen-link"
               data-type-screen="step" data-id-screen="1">
               Продавец
            </a>
            <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
               data-type-screen="step" data-id-screen="2">
               Покупатель
            </a>
            <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
               data-type-screen="step" data-id-screen="3">
                Сведения о ТС
            </a>
            <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
               data-type-screen="step" data-id-screen="4">
                Документ ТС
            </a>
            <a href="javascript:void(0);" class="calc__progressbar-item js-progressbar-item js-screen-link"
               data-type-screen="step" data-id-screen="5">
                Данные ДКП
            </a>
        </div>
    </div>
    <div class="screen js-screen screen_active" data-type-screen="step" data-id-screen="1">
        <div class="calc__inner calc__auto-param">
            <div class="js-docs-wrap">
            <div class="row row_center">
                <div class="input input_no-heading">
                    <label class="gbdd checkbox-r checkbox-r_power checkbox-r_big js-checkbox-setBold">
                        <span class="checkbox-r__text-l checkbox-r__text_bold js-checkbox-text">Физическое лицо</span>
                        <input type="checkbox" class="js-checkbox js-legal-checkbox" name="S_horns">
                        <span class="checkbox-r__text js-checkbox-text">Юридическое лицо</span>
                    </label>
                </div>
            </div>
            <div class="js-individual">
            <h3>Введите ваши данные</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="S_LAST_NAME" class="input__name">Фамилия <span class="red">*</span></label>
                    <input type="text" placeholder="Фамилия" name="S_LAST_NAME" id="S_LAST_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="S_FIRST_NAME" class="input__name">Имя <span class="red">*</span></label>
                    <input type="text" placeholder="Имя" name="S_FIRST_NAME" id="S_FIRST_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="S_MIDDLE_NAME" class="input__name">Отчество <span class="red">*</span></label>
                    <input type="text" placeholder="Отчество" id="S_MIDDLE_NAME" name="S_MIDDLE_NAME" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="S_DATE_BIRTH" class="input__name">Дата рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Дата рождения" name="S_DATE_BIRTH" id="S_DATE_BIRTH" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
                <div class="input input_part-2-3">
                    <label for="S_PLACE_BIRTH" class="input__name">Место рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Место рождения" name="S_PLACE_BIRTH" id="S_PLACE_BIRTH" tabindex="0" class="req">
                </div>
            </div>
            <h3>Паспортные данные</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="S_PASSPORT_SERIAL" class="input__name">Серия <span class="red">*</span></label>
                    <input type="text" placeholder="Серия" name="S_PASSPORT_SERIAL" id="S_PASSPORT_SERIAL" tabindex="0" class="req" data-mask="0000">
                </div>
                <div class="input input_part-3">
                    <label for="S_PASSPORT_NUMBER" class="input__name">Номер <span class="red">*</span></label>
                    <input type="text" placeholder="Номер" name="S_PASSPORT_NUMBER" id="S_PASSPORT_NUMBER" tabindex="0" class="req" data-mask="000000">
                </div>
                <div class="input input_part-3">
                    <label for="S_PASSPORT_DATE" class="input__name">Дата выдачи <span class="red">*</span></label>
                    <input type="text" placeholder="Дата выдачи" id="S_PASSPORT_DATE" name="S_PASSPORT_DATE" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_wide">
                    <label for="S_PASSPORT_WHERE" class="input__name">Кем и когда выдан <span class="red">*</span></label>
                    <input type="text" placeholder="Кем и когда выдан" name="S_PASSPORT_WHERE" id="S_PASSPORT_WHERE" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                    <div class="input input_wide">
                            <label for="S_PASSPORT_PATH_ADDRESS" class="input__name">Место жительства <span class="red">*</span></label>
                            <input type="text" placeholder="Формат: Город, улица, дом, квартира" name="S_PASSPORT_PATH_ADDRESS" id="S_PASSPORT_PATH_ADDRESS" tabindex="0" class="req">
                    </div>
                <!--<div class="input input_part-3">
                    <label for="S_PASSPORT_PATH_CITY" class="input__name">Населенный пункт <span class="red">*</span></label>
                    <input type="text" placeholder="Населенный пункт" name="S_PASSPORT_PATH_CITY" id="S_PASSPORT_PATH_CITY" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="S_PASSPORT_PATH_STREET" class="input__name">Улица <span class="red">*</span></label>
                    <input type="text" placeholder="Улица" name="S_PASSPORT_PATH_STREET" id="S_PASSPORT_PATH_STREET" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="S_PASSPORT_PATH_NUMBER" class="input__name">Номер дома <span class="red">*</span></label>
                    <input type="text" placeholder="Номер дома" id="S_PASSPORT_PATH_NUMBER" name="S_PASSPORT_PATH_NUMBER" tabindex="0" class="req">
                </div>-->
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="S_NATIONALITY" class="input__name">Гражданство <span class="red">*</span></label>
                    <input type="text" placeholder="Гражданство" name="S_NATIONALITY" id="S_NATIONALITY" tabindex="0" class="req" value="РФ">
                </div>
                <div class="input input_part-3">
                    <span class="input__name">Пол <span class="red">*</span></span>
                    <div class="radio-group radi" data-errname="Выберите пол">
                        <label class="radio">
                            <input type="radio" name="S_SEX" value="М" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">М</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="S_SEX" value="Ж" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Ж</span>
                            </div>
                        </label>
                    </div>
                </div>
                <!--<div class="input input_part-3">
                    <label for="S_INN" class="input__name">ИНН <span class="red">*</span></label>
                    <input type="text" placeholder="ИНН" id="S_INN" name="S_INN" tabindex="0" class="req">
                </div>-->
            </div>
                    
                </div>
                <div class="js-legal hidden">
                        <h2>Информация о юридическом лице</h2>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="S_LEGAL_NAME" class="input__name">Наименование <span class="red">*</span></label>
                                <input type="text" placeholder="Наименование" name="S_LEGAL_NAME" id="S_LEGAL_NAME" tabindex="0" class="req">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_LEGAL_DATE" class="input__name">Дата регистрации <span class="red">*</span></label>
                                <input type="text" placeholder="Дата регистрации" name="S_LEGAL_DATE" id="S_LEGAL_DATE" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_LEGAL_INN" class="input__name">ИНН <span class="red">*</span></label>
                                <input type="text" placeholder="ИНН" id="S_LEGAL_INN" name="S_LEGAL_INN" tabindex="0" class="req">
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="S_LEGAL_ADDRESS" class="input__name">Адрес регистрации <span class="red">*</span></label>
                                <input type="text" placeholder="Адрес регистрации" name="S_LEGAL_ADDRESS" id="S_LEGAL_ADDRESS" tabindex="0" class="req">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_LEGAL_TEL" class="input__name">Телефон <span class="red">*</span></label>
                                <input type="text" placeholder="Телефон" name="S_LEGAL_TEL" id="S_LEGAL_TEL" tabindex="0" class="req" data-mask="+7 (000) 000 00-00">
                            </div>
                            <div class="input input_part-3"></div>
                        </div>
                </div>
                <div class="js-hidden hidden js-legal">
                        <h2 class="head_bl">Данные представителя</h2>
                        <h3>Введите ваши данные</h3>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_LAST_NAME" class="input__name">Фамилия</label>
                                <input type="text" placeholder="Фамилия" name="S_ADDITIONAL_LAST_NAME" id="S_ADDITIONAL_LAST_NAME"
                                       tabindex="0" class="req">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_FIRST_NAME" class="input__name">Имя</label>
                                <input type="text" placeholder="Имя" name="S_ADDITIONAL_FIRST_NAME" id="S_ADDITIONAL_FIRST_NAME"
                                       tabindex="0" class="req">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_MIDDLE_NAME" class="input__name">Отчество</label>
                                <input type="text" placeholder="Отчество" id="S_ADDITIONAL_MIDDLE_NAME" name="S_ADDITIONAL_MIDDLE_NAME"
                                       tabindex="0" class="req">
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_DATE_BIRTH" class="input__name">Дата рождения</label>
                                <input type="text" placeholder="Дата рождения" name="S_ADDITIONAL_DATE_BIRTH" id="S_ADDITIONAL_DATE_BIRTH"
                                       tabindex="0"
                                       class="js-datepicker req"
                                       data-mask="00.00.0000">
                            </div>
                            <div class="input input_part-2-3">
                                <label for="S_ADDITIONAL_PLACE_BIRTH" class="input__name">Место рождения</label>
                                <input type="text" placeholder="Место рождения" name="S_ADDITIONAL_PLACE_BIRTH" id="S_ADDITIONAL_PLACE_BIRTH"
                                       tabindex="0" class="req">
                            </div>
                        </div>
                        <h3>Паспортные данные</h3>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_PASSPORT_SERIAL" class="input__name">Серия</label>
                                <input type="text" placeholder="Серия" name="S_ADDITIONAL_PASSPORT_SERIAL" id="S_ADDITIONAL_PASSPORT_SERIAL"
                                       tabindex="0" class="req" data-mask="0000">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_PASSPORT_NUMBER" class="input__name">Номер</label>
                                <input type="text" placeholder="Номер" name="S_ADDITIONAL_PASSPORT_NUMBER" id="S_ADDITIONAL_PASSPORT_NUMBER"
                                       tabindex="0" class="req" data-mask="000000">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_PASSPORT_DATE" class="input__name">Дата выдачи</label>
                                <input type="text" placeholder="Дата выдачи" id="S_ADDITIONAL_PASSPORT_DATE" name="S_ADDITIONAL_PASSPORT_DATE"
                                       tabindex="0" class="js-datepicker req"
                                       data-mask="00.00.0000">
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_wide">
                                <label for="S_ADDITIONAL_PASSPORT_WHERE" class="input__name">Кем и когда выдан</label>
                                <input type="text" placeholder="Кем и когда выдан" name="S_ADDITIONAL_PASSPORT_WHERE" id="S_ADDITIONAL_PASSPORT_WHERE"
                                       tabindex="0" class="req">
                            </div>
                        </div>
                        <div class="row row_space-beetween">
                                <div class="input input_wide">
                                        <label for="S_ADDITIONAL_PASSPORT_PATH_ADDRESS" class="input__name">Место жительства <span class="red">*</span></label>
                                        <input type="text" placeholder="Формат: Город, улица, дом, квартира" name="S_ADDITIONAL_PASSPORT_PATH_ADDRESS" id="S_ADDITIONAL_PASSPORT_PATH_ADDRESS" tabindex="0" class="req">
                                </div>
                            <!--<div class="input input_part-3">
                                <label for="S_ADDITIONAL_PASSPORT_PATH_CITY" class="input__name">Населенный
                                    пункт</label>
                                <input type="text" placeholder="Населенный" name="S_ADDITIONAL_PASSPORT_PATH_CITY"
                                       id="S_ADDITIONAL_PASSPORT_PATH_CITY" tabindex="0" class="req">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_PASSPORT_PATH_STREET" class="input__name">Улица</label>
                                <input type="text" placeholder="Улица" name="S_ADDITIONAL_PASSPORT_PATH_STREET"
                                       id="S_ADDITIONAL_PASSPORT_PATH_STREET"
                                       tabindex="0" class="req">
                            </div>
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_PASSPORT_PATH_NUMBER" class="input__name">Номер дома</label>
                                <input type="text" placeholder="Номер дома" id="S_ADDITIONAL_PASSPORT_PATH_NUMBER"
                                       name="S_ADDITIONAL_PASSPORT_PATH_NUMBER"
                                       tabindex="0" class="req">
                            </div>-->
                        </div>
                        <div class="row row_space-beetween">
                            <div class="input input_part-3">
                                <label for="S_ADDITIONAL_NATIONALITY" class="input__name">Гражданство</label>
                                <input type="text" placeholder="Гражданство" name="S_ADDITIONAL_NATIONALITY" id="S_ADDITIONAL_NATIONALITY"
                                       tabindex="0" class="req" value="РФ">
                            </div>
                            <div class="input input_part-3">
                                <span class="input__name">Пол</span>
                                <div class="radio-group radi" data-errname="Выберите пол">
                                    <label class="radio">
                                        <input type="radio" name="S_ADDITIONAL_SEX" value="М"
                                               class="category__radio-hidden"
                                               tabindex="0">
                                        <div class="radio__inner">
                                            <span class="radio__name">М</span>
                                        </div>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="S_ADDITIONAL_SEX" value="Ж"
                                               class="category__radio-hidden"
                                               tabindex="0">
                                        <div class="radio__inner">
                                            <span class="radio__name">Ж</span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <!--<div class="input input_part-3">
                                <label for="S_ADDITIONAL_INN" class="input__name">ИНН</label>
                                <input type="text" placeholder="ИНН" id="S_ADDITIONAL_INN" name="S_ADDITIONAL_INN" tabindex="0" class="req">
                            </div>-->
                        </div>
                    </div>
                    <div class="row row_m-top">
                        <div class="input">
                            <input type="checkbox" class="checkbox-h" name="nda" id="checknda" checked>
                            <label for="checknda" class="form-block__nda">
                                Нажимая, я соглашаюсь на обработку персональных данных
                            </label>
                        </div>
                    </div>
                    <div class="js-visible">
                        <div class="row buttons-wrap">
                            <a href="javascript:void(0);" class="personal-table__btn btn btn_add btn_dark js-add-user">
                                Добавить представителя *</a>
                        </div>
                        <div class="row">
                            <p>Данные необходимы в том случае, если заявитель не является собственником.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row buttons-wrap">
                <a href="javascript:void(0);" class="btn btn_light btn_next js-screen-link" data-type-screen="step"
                   data-id-screen="3" data-progressbar-id="3">Следующий шаг</a>
            </div>
            <span class="mess_info"></span>
        </div>
    <div class="screen js-screen" data-type-screen="step" data-id-screen="2">
        <div class="js-docs-wrap">
        <div class="calc__inner calc__auto-param">
            <div class="row row_center">
                <div class="input input_no-heading">
                    <label class="gbdd checkbox-r checkbox-r_power checkbox-r_big js-checkbox-setBold">
                        <span class="checkbox-r__text-l checkbox-r__text_bold js-checkbox-text">Физическое лицо</span>
                        <input type="checkbox" class="js-checkbox js-legal-checkbox" name="horns">
                        <span class="checkbox-r__text js-checkbox-text">Юридическое лицо</span>
                    </label>
                </div>
            </div>
            <div class="js-individual">
            <h3>Введите ваши данные</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="LAST_NAME" class="input__name">Фамилия <span class="red">*</span></label>
                    <input type="text" placeholder="Фамилия" name="LAST_NAME" id="LAST_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="FIRST_NAME" class="input__name">Имя <span class="red">*</span></label>
                    <input type="text" placeholder="Имя" name="FIRST_NAME" id="FIRST_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="MIDDLE_NAME" class="input__name">Отчество <span class="red">*</span></label>
                    <input type="text" placeholder="Отчество" id="MIDDLE_NAME" name="MIDDLE_NAME" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="DATE_BIRTH" class="input__name">Дата рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Дата рождения" name="DATE_BIRTH" id="DATE_BIRTH" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
                <div class="input input_part-2-3">
                    <label for="PLACE_BIRTH" class="input__name">Место рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Место рождения" name="PLACE_BIRTH" id="PLACE_BIRTH" tabindex="0" class="req">
                </div>
            </div>
            <h3>Паспортные данные</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="PASSPORT_SERIAL" class="input__name">Серия <span class="red">*</span></label>
                    <input type="text" placeholder="Серия" name="PASSPORT_SERIAL" id="PASSPORT_SERIAL" tabindex="0" class="req" data-mask="0000">
                </div>
                <div class="input input_part-3">
                    <label for="PASSPORT_NUMBER" class="input__name">Номер <span class="red">*</span></label>
                    <input type="text" placeholder="Номер" name="PASSPORT_NUMBER" id="PASSPORT_NUMBER" tabindex="0" class="req" data-mask="000000">
                </div>
                <div class="input input_part-3">
                    <label for="PASSPORT_DATE" class="input__name">Дата выдачи <span class="red">*</span></label>
                    <input type="text" placeholder="Дата выдачи" id="PASSPORT_DATE" name="PASSPORT_DATE" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_wide">
                    <label for="PASSPORT_WHERE" class="input__name">Кем и когда выдан <span class="red">*</span></label>
                    <input type="text" placeholder="Кем и когда выдан" name="PASSPORT_WHERE" id="PASSPORT_WHERE" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                        <div class="input input_wide">
                            <label for="PASSPORT_PATH_ADDRESS" class="input__name">Место жительства <span class="red">*</span></label>
                            <input type="text" placeholder="Формат: Город, улица, дом, квартира" name="PASSPORT_PATH_ADDRESS" id="PASSPORT_PATH_ADDRESS" tabindex="0" class="req">
                        </div>
                <!--<div class="input input_part-2-3">
                    <label for="PASSPORT_PATH_CITY" class="input__name">Населенный пункт <span class="red">*</span></label>
                    <input type="text" placeholder="Населенный пункт" name="PASSPORT_PATH_CITY" id="PASSPORT_PATH_CITY" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="PASSPORT_PATH_STREET" class="input__name">Улица <span class="red">*</span></label>
                    <input type="text" placeholder="Улица" name="PASSPORT_PATH_STREET" id="PASSPORT_PATH_STREET" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="PASSPORT_PATH_NUMBER" class="input__name">Номер дома <span class="red">*</span></label>
                    <input type="text" placeholder="Номер дома" id="PASSPORT_PATH_NUMBER" name="PASSPORT_PATH_NUMBER" tabindex="0" class="req">
                </div>-->
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="NATIONALITY" class="input__name">Гражданство <span class="red">*</span></label>
                    <input type="text" placeholder="Гражданство" name="NATIONALITY" id="NATIONALITY" tabindex="0" class="req" value="РФ">
                </div>
                <div class="input input_part-3">
                    <span class="input__name">Пол <span class="red">*</span></span>
                    <div class="radio-group radi" data-errname="Выберите пол">
                        <label class="radio">
                            <input type="radio" name="SEX" value="М" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">М</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="SEX" value="Ж" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Ж</span>
                            </div>
                        </label>
                    </div>
                </div>
                <!--<div class="input input_part-3">
                    <label for="INN" class="input__name">ИНН <span class="red">*</span></label>
                    <input type="text" placeholder="ИНН" id="INN" name="INN" tabindex="0" class="req">
                </div>-->
            </div>
        </div>
        <div class="js-legal hidden">
            <h2>Информация о юридическом лице</h2>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="LEGAL_NAME" class="input__name">Наименование <span class="red">*</span></label>
                    <input type="text" placeholder="Наименование" name="LEGAL_NAME" id="LEGAL_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="LEGAL_DATE" class="input__name">Дата регистрации <span class="red">*</span></label>
                    <input type="text" placeholder="Дата регистрации" name="LEGAL_DATE" id="LEGAL_DATE" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
                <div class="input input_part-3">
                    <label for="LEGAL_INN" class="input__name">ИНН <span class="red">*</span></label>
                    <input type="text" placeholder="ИНН" id="LEGAL_INN" name="LEGAL_INN" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="LEGAL_ADDRESS" class="input__name">Адрес регистрации <span class="red">*</span></label>
                    <input type="text" placeholder="Адрес регистрации" name="LEGAL_ADDRESS" id="LEGAL_ADDRESS" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="LEGAL_TEL" class="input__name">Телефон <span class="red">*</span></label>
                    <input type="text" placeholder="Телефон" name="LEGAL_TEL" id="LEGAL_TEL" tabindex="0" class="req" data-mask="+7 (000) 000 00-00">
                </div>
                <div class="input input_part-3"></div>
            </div>
        </div>
        <div class="js-hidden hidden js-legal">
            <h2 class="head_bl">Данные представителя</h2>
            <h3>Введите ваши данные</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="ADDITIONAL_LAST_NAME" class="input__name">Фамилия <span class="red">*</span></label>
                    <input type="text" placeholder="Фамилия" name="ADDITIONAL_LAST_NAME" id="ADDITIONAL_LAST_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="ADDITIONAL_FIRST_NAME" class="input__name">Имя <span class="red">*</span></label>
                    <input type="text" placeholder="Имя" name="ADDITIONAL_FIRST_NAME" id="ADDITIONAL_FIRST_NAME" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="ADDITIONAL_MIDDLE_NAME" class="input__name">Отчество <span class="red">*</span></label>
                    <input type="text" placeholder="Отчество" id="ADDITIONAL_MIDDLE_NAME" name="ADDITIONAL_MIDDLE_NAME" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="ADDITIONAL_DATE_BIRTH" class="input__name">Дата рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Дата рождения" name="ADDITIONAL_DATE_BIRTH" id="ADDITIONAL_DATE_BIRTH"
                           tabindex="0"
                           class="js-datepicker req"
                           data-mask="00.00.0000">
                </div>
                <div class="input input_part-2-3">
                    <label for="ADDITIONAL_PLACE_BIRTH" class="input__name">Место рождения <span class="red">*</span></label>
                    <input type="text" placeholder="Место рождения" name="ADDITIONAL_PLACE_BIRTH" id="ADDITIONAL_PLACE_BIRTH"
                           tabindex="0" class="req">
                </div>
            </div>
            <h3>Паспортные данные</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="ADDITIONAL_PASSPORT_SERIAL" class="input__name">Серия <span class="red">*</span></label>
                    <input type="text" placeholder="Серия" name="ADDITIONAL_PASSPORT_SERIAL" id="ADDITIONAL_PASSPORT_SERIAL"
                           tabindex="0" class="req" data-mask="0000">
                </div>
                <div class="input input_part-3">
                    <label for="ADDITIONAL_PASSPORT_NUMBER" class="input__name">Номер <span class="red">*</span></label>
                    <input type="text" placeholder="Номер" name="ADDITIONAL_PASSPORT_NUMBER" id="ADDITIONAL_PASSPORT_NUMBER"
                           tabindex="0" class="req" data-mask="000000">
                </div>
                <div class="input input_part-3">
                    <label for="ADDITIONAL_PASSPORT_DATE" class="input__name">Дата выдачи <span class="red">*</span></label>
                    <input type="text" placeholder="Дата выдачи" id="ADDITIONAL_PASSPORT_DATE" name="ADDITIONAL_PASSPORT_DATE"
                           tabindex="0" class="js-datepicker req"
                           data-mask="00.00.0000">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_wide">
                    <label for="ADDITIONAL_PASSPORT_WHERE" class="input__name">Кем и когда выдан <span class="red">*</span></label>
                    <input type="text" placeholder="Кем и когда выдан" name="ADDITIONAL_PASSPORT_WHERE" id="ADDITIONAL_PASSPORT_WHERE"
                           tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                    <div class="input input_wide">
                            <label for="ADDITIONAL_PASSPORT_PATH_ADDRESS" class="input__name">Место жительства <span class="red">*</span></label>
                            <input type="text" placeholder="Формат: Город, улица, дом, квартира" name="ADDITIONAL_PASSPORT_PATH_ADDRESS" id="ADDITIONAL_PASSPORT_PATH_ADDRESS" tabindex="0" class="req">
                    </div>
                <!--<div class="input input_part-3">
                    <label for="ADDITIONAL_PASSPORT_PATH_CITY" class="input__name">Населенный
                        пункт <span class="red">*</span></label>
                    <input type="text" placeholder="Населенный пункт" name="ADDITIONAL_PASSPORT_PATH_CITY"
                           id="ADDITIONAL_PASSPORT_PATH_CITY" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="ADDITIONAL_PASSPORT_PATH_STREET" class="input__name">Улица <span class="red">*</span></label>
                    <input type="text" placeholder="Улица" name="ADDITIONAL_PASSPORT_PATH_STREET"
                           id="ADDITIONAL_PASSPORT_PATH_STREET"
                           tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="ADDITIONAL_PASSPORT_PATH_NUMBER" class="input__name">Номер дома <span class="red">*</span></label>
                    <input type="text" placeholder="Номер дома" id="ADDITIONAL_PASSPORT_PATH_NUMBER"
                           name="ADDITIONAL_PASSPORT_PATH_NUMBER"
                           tabindex="0" class="req">
                </div>-->
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="ADDITIONAL_NATIONALITY" class="input__name">Гражданство <span class="red">*</span></label>
                    <input type="text" placeholder="Гражданство" name="ADDITIONAL_NATIONALITY" id="ADDITIONAL_NATIONALITY"
                           tabindex="0" class="req" value="РФ">
                </div>
                <div class="input input_part-3">
                    <span class="input__name">Пол <span class="red">*</span></span>
                    <div class="radio-group radi" data-errname="Выберите пол">
                        <label class="radio">
                            <input type="radio" name="ADDITIONAL_SEX" value="М"
                                   class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">М</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="ADDITIONAL_SEX" value="Ж"
                                   class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Ж</span>
                            </div>
                        </label>
                    </div>
                </div>
                <!--<div class="input input_part-3">
                    <label for="ADDITIONAL_INN" class="input__name">ИНН <span class="red">*</span></label>
                    <input type="text" placeholder="ИНН" id="ADDITIONAL_INN" name="ADDITIONAL_INN" tabindex="0" class="req">
                </div>-->
            </div>
        </div>
        <div class="js-visible">
            <div class="row buttons-wrap">
                <a href="javascript:void(0);" class="personal-table__btn btn btn_add btn_dark js-add-user">Добавить
                    представителя
                    собственника *</a>
            </div>
            <div class="row">
                <p>Данные необходимы в том случае, если заявитель не является собственником.</p>
            </div>
        </div>
    </div>
</div>
        <div class="row buttons-wrap">
            <a href="javascript:void(0);" class="btn btn_light btn_next js-screen-link" data-type-screen="step"
               data-id-screen="2" data-progressbar-id="2">Следующий шаг</a>
               
        </div>
        <span class="mess_info"></span>
</div>
    <div class="screen js-screen" data-type-screen="step" data-id-screen="3">
        <div class="calc__inner calc__auto-param">
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="AUTO_MAKE" class="input__name">Марка автомобиля <span class="red">*</span></label>
                    <select name="AUTO_MAKE" id="AUTO_MAKE" class="js-select req_select selc_doc" data-ajaxs="Y" data-type="elems" data-url="select_mark" data-idelem="FIRST_MODEL" data-typei="select">
                        <option value="0">Выбрать</option>
                        @foreach ($list_mark as $item)
                            <option value="{{ $item->id_mark }}|{{ $item->value }}">{{ $item->value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input input_part-3">
                    <label for="FIRST_MODEL" class="input__name">Модель <span class="red">*</span></label>
                    <select name="FIRST_MODEL" id="FIRST_MODEL" class="js-select req_select selc_doc">
                        <option value="0">Выбрать</option>
                    </select>
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_TYPE" class="input__name">Тип транспортного средства <span class="red">*</span></label>
                    <select name="AUTO_TYPE" id="AUTO_TYPE" class="js-select req_select">
                        <option value="0">Выбрать</option>
                        @foreach ($list_type as $item)
                        <?if($item->id_type == 0)continue;?>
                            <option value="{{ $item->name_type }}">{{ $item->name_type }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="AUTO_NUMBER" class="input__name">Гос. рег. знак <span class="red">*</span></label>
                    <input type="text" placeholder="Гос. рег. знак" name="AUTO_NUMBER" id="AUTO_NUMBER" tabindex="0" class="req">
                    <input type="checkbox" class="checkbox-h" name="chech_req" id="reg_znak">
                    <label for="reg_znak" class="form-block__nda">
                        Регистрационный знак отсутствует
                    </label>
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_VIN" class="input__name">VIN номер</label>
                    <input type="text" placeholder="VIN номер" name="AUTO_VIN" id="AUTO_VIN" tabindex="0" data-mask="AAAAAAAAAAAAAAAAA">
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_CREATOR_COUNTRY" class="input__name">Орг. изготовитель <span class="red">*</span></label>
                    <input type="text" placeholder="Орг. изготовитель" name="AUTO_CREATOR_COUNTRY" id="AUTO_CREATOR_COUNTRY" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <span class="input__name">Категория <span class="red">*</span></span>
                    <div class="radio-group radi" data-errname="Выберите категорию">
                        <label class="radio">
                            <input type="radio" name="AUTO_CATEGORY" value="A" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">A</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_CATEGORY" value="B" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">B</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_CATEGORY" value="C" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">C</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_CATEGORY" value="D" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">D</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_CATEGORY" value="E" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">E</span>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_YEAR" class="input__name">Год выпуска <span class="red">*</span></label>
                    <select name="AUTO_YEAR" id="AUTO_YEAR" class="js-select req_select">
                        <option value="0">Выбрать</option>
                        @foreach($years as $key => $year)
                            <option value="{{ $year }}">{{ $year }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_CHASSIS_NUMBER" class="input__name">Номер шасси (рамы)</label>
                    <input type="text" placeholder="Номер шасси (рамы)" id="AUTO_CHASSIS_NUMBER" name="AUTO_CHASSIS_NUMBER" tabindex="0">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="AUTO_BODY_NUMBER" class="input__name">Номер кузова</label>
                    <input type="text" placeholder="Номер кузова" id="AUTO_BODY_NUMBER" name="AUTO_BODY_NUMBER" tabindex="0">
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_COLOR" class="input__name">Цвет <span class="red">*</span></label>
                    <input type="text" placeholder="Цвет" id="AUTO_COLOR" name="AUTO_COLOR" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_POWER" class="input__name">Мощность двиг. (л.с.) <span class="red">*</span></label>
                    <input type="number" placeholder="Мощность двиг. (л.с.)" id="AUTO_POWER" name="AUTO_POWER" tabindex="0" class="req">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-2-3">
                    <span class="input__name">Экологический класс <span class="red">*</span></span>
                    <div class="radio-group radi" data-errname="Выберите экологический класс">
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="0" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро0</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="1" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро1</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="2" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро2</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="3" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро3</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="4" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро4</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="5" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро5</span>
                            </div>
                        </label>
                        <label class="radio">
                            <input type="radio" name="AUTO_ECO_CLASS" value="6" class="category__radio-hidden"
                                   tabindex="0">
                            <div class="radio__inner">
                                <span class="radio__name">Евро6</span>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="input input_part-2">
                        <label for="AUTO_PRICE" class="input__name">Стоимость ТС <span class="red">*</span></label>
                        <div class="tooltip" data-tooltip="Стоимость ТС"></div>
                        <input type="text" placeholder="Стоимость ТС" id="AUTO_PRICE" name="AUTO_PRICE" tabindex="0" class="req">
                </div>
            </div>
        </div>
        @auth
        <div class="row row_m-top no-elem">
            <div class="input">
                <input type="checkbox" class="checkbox-h" name="AUTO_SAVE_TO_PERSONAL" id="AUTO_SAVE_TO_PERSONAL">
                <label for="AUTO_SAVE_TO_PERSONAL" class="form-block__nda">
                    Сохранить данные автомобиля в личный кабинет
                </label>
            </div>
        </div>
        @endauth
        <div class="row buttons-wrap">
            <a href="javascript:void(0);" class="btn btn_light btn_зкум js-screen-link" data-type-screen="step"
               data-id-screen="2" data-progressbar-id="2">Назад</a>
            <a href="javascript:void(0);" class="btn btn_light btn_next js-screen-link" data-type-screen="step"
               data-id-screen="4" data-progressbar-id="4">Следующий шаг</a>
        </div>
        <span class="mess_info"></span>
    </div>
    <div class="screen js-screen" data-type-screen="step" data-id-screen="4">
        <div class="calc__inner calc__auto-param">
            <div class="row">
                <div class="input input_wide">
                    <label for="AUTO_DOCUMENT" class="input__name">Тип документа <span class="red">*</span></label>
                    <select name="AUTO_DOCUMENT" id="AUTO_DOCUMENT" class="js-select req_select">
                        <option value="Паспорт ТС">Паспорт транспортного средства</option>
                        <option value="Свидетельство ТС">Свидетельство о регистрации автомототранспортного средства (прицепа)</option>
                        <option value="Регистрационный документ или технический паспорт ТС">Регистрационный документ или технический паспорт автомототранспортного средства и (или) прицепа</option>
                    </select>
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label for="AUTO_STS_SERIAL" class="input__name">Серия <span class="red">*</span></label>
                    <input type="text" placeholder="Серия" name="AUTO_STS_SERIAL" id="AUTO_STS_SERIAL" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_STS_NUMBER" class="input__name">Номер <span class="red">*</span></label>
                    <input type="text" placeholder="Номер" name="AUTO_STS_NUMBER" id="AUTO_STS_NUMBER" tabindex="0" class="req">
                </div>
                <div class="input input_part-3">
                    <label for="AUTO_STS_DATE" class="input__name">Дата выдачи <span class="red">*</span></label>
                    <input type="text" placeholder="Дата выдачи" id="AUTO_STS_DATE" name="AUTO_STS_DATE" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
            </div>
        </div>
        <div class="row buttons-wrap">
            <a href="javascript:void(0);" class="btn btn_light btn_зкум js-screen-link" data-type-screen="step"
               data-id-screen="3" data-progressbar-id="3">Назад</a>
            <a href="javascript:void(0);" class="btn btn_light btn_next js-screen-link" data-type-screen="step"
               data-id-screen="5" data-progressbar-id="5">Следующий шаг</a>
        </div>
        <span class="mess_info"></span>
    </div>
    <div class="screen js-screen" data-type-screen="step" data-id-screen="5">
        <div class="calc__inner calc__auto-param">
            <div class="row row_space-beetween">
                <div class="input input_part-2">
                    <label for="DOC_ADDRESS" class="input__name">Место составления <span class="red">*</span></label>
                    <input type="text" placeholder="Место составления" name="DOC_ADDRESS" id="DOC_ADDRESS" tabindex="0" class="req">
                </div>
                <div class="input input_part-2">
                    <label for="DOC_DATE" class="input__name">Дата составления <span class="red">*</span></label>
                    <input type="text" placeholder="Дата составления" id="DOC_DATE" name="DOC_DATE" tabindex="0" class="js-datepicker req" data-mask="00.00.0000">
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-2">
                    <label for="DOC_USER_MAIL" class="input__name">Почта <span class="red">*</span></label>
                    <input type="text" placeholder="Почта" id="DOC_USER_MAIL" name="DOC_USER_MAIL" tabindex="0" class="req">
                </div>
                <div class="input input_part-2">
                    <label for="DOC_USER_PHONE" class="input__name">Телефон <span class="red">*</span></label>
                    <input type="text" placeholder="Телефон" id="DOC_USER_PHONE" name="DOC_USER_PHONE" tabindex="0" class="req" data-mask="+7 (000) 000 00-00">
                </div>
            </div>
        </div>
        <div class="row row_center buttons-wrap">
            <a href="javascript:void(0);" class="btn" data-click="calc_doc">Сформировать</a>
        </div>
        <span class="mess_info"></span>
    </div>
@csrf
</form>