@extends('layouts.home_admin')
@section('content_admin')
    <div class="row personal-page__row_payouts">
        <div class="personal-page__info-title">Выплаты агентам</div>
        <a href="{{ route('panel.payouts.add', [], false) }}" class="btn">Добавить выплату</a>
    </div>

    <div class="personal-page__payouts-date-filter">
        @if(!empty($agents))
            <div class="personal-page__option personal-page__option_date input personal-page__payouts-option-agent">
                <span class="personal-page__label">Агент</span>
                <select class="js-select personal-page__select js-payouts-agents" name="select_agent">
                    <option value="0">Выбрать агента</option>
                    @foreach($agents as $agent)
                        <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="personal-page__date-range personal-page__date-range_payouts">
            <div class="personal-page__option personal-page__option_date input">
                <span class="personal-page__label">Даты</span>
                <label for="pp-date-sort2" class="personal-page__date-range-label">
                    <input id="pp-date-sort2" type="text" class="js-datepicker js-put-get-data"
                           data-mask="00.00.0000" data-range="true" data-multiple-dates-separator=" - "
                           data-view="years">
                </label>
            </div>
            @if(!empty($_REQUEST["datestart"]))
                <div class="personal-page__date-range-clear js-clear-data" data-picker="pp-date-sort2">
                    Сбросить
                </div>
            @endif
        </div>
    </div>
    <div class="personal-page__payouts-content js-payouts-content">
        <div class="personal-page-table personal-page-table_seven-column">
            <div class="personal-page-table__header">
                <div class="tr">
                    <div class="td">Номер выплаты</div>
                    <div class="td">Агент</div>
                    <div class="td">Диапазон дат</div>
                    <div class="td">Суммы продаж</div>
                    <div class="td">Доход</div>
                    <div class="td">Статус оплаты</div>
                    <div class="td">Подробнее</div>
                </div>
            </div>
            <div class="personal-page-table__body">
                @if(!empty($payouts[0]))
                    @foreach($payouts as $payout)
                        <div class="tr">
                            <div data-text="Номер выплаты" class="td">{{ $payout->id }}</div>
                            <div data-text="Агент" class="td">
                                <div class="personal-page__payouts-table-agent">
                                    <span>{{ $payout->AgentPayout[0]->id }}</span>
                                    <span>{{ $payout->AgentPayout[0]->name }}</span>
                                </div>
                            </div>
                            <div data-text="Диапазон дат"
                                 class="td">{{ Date::parse($payout->date_start)->format('d.m.Y') }}
                                - {{ Date::parse($payout->date_end)->format('d.m.Y') }}</div>
                            <div data-text="Суммы продаж" class="td">{{ $payout->OrdersPayouts->sum('price') }} Р</div>
                            <div data-text="Доход" class="td">{{ $payout->OrdersPayouts->sum('price_commission') }} Р
                            </div>
                            <div data-text="Статус оплаты" class="td" data-method="POST"
                                 data-url="{{ route('panel.payouts.editStatus', [], false) }}">
                                <input type="hidden" name="payouts_id" value="{{ $payout->id }}"/>
                                <select class="js-select js-payouts-status" name="status">
                                    <option value="0" @if($payout->status == 0) selected @endif>Ожидает оплаты</option>
                                    <option value="1" @if($payout->status == 1) selected @endif>Выплачено</option>
                                </select>
                            </div>
                            <div data-text="Подробнее" class="td">
                                <a href="javascript:void(0)" class="js-toggle-payouts"
                                   data-toggle="payouts-one-{{ $payout->id }}"
                                >Подробно</a>
                            </div>
                        </div>
                        <div class="personal-page-table__body-hidden personal-page-table__body-hidden_payouts"
                             data-toggled="payouts-one-{{ $payout->id }}">
                            <div class="personal-page__payouts-bank">
                                <div class="personal-page__table-inner-option">
                                    <div class="personal-page__table-inner-label">Номер карты</div>
                                    <div
                                        class="personal-page__table-inner-text">{{ $payout->AgentPayout[0]->number_card }}</div>
                                </div>
                                <div class="personal-page__table-inner-option">
                                    <div class="personal-page__table-inner-label">Банки</div>
                                    <div
                                        class="personal-page__table-inner-text">{{ $payout->AgentPayout[0]->bank }}</div>
                                </div>
                            </div>
                            <div class="personal-page__table-inner-strah">
                                @foreach($payout->OrdersPayouts as $order)
                                    <div class="personal-page__payouts-inner">
                                        <div class="personal-page__payout-col personal-page__payout-col_strah">
                                            <div class="personal-page__table-inner-label">Страховая компания</div>
                                            <div
                                                class="personal-page__table-inner-text">{{ $order->Allinsurance[0]->name }}</div>
                                        </div>
                                        <div class="row personal-page__inner-row">
                                            <div class="personal-page__payout-col personal-page__payout-col_sum">
                                                <div class="personal-page__table-inner-label">Суммы продаж</div>
                                                <div
                                                    class="personal-page__table-inner-text">{{ number_format($order->price, 2, '.', '') }}
                                                    Р
                                                </div>
                                            </div>
                                            <div class="personal-page__payout-col">
                                                <div class="personal-page__table-inner-label">Доход</div>
                                                <div
                                                    class="personal-page__table-inner-text">{{ number_format($order->price_commission, 2, '.', '') }}
                                                    Р
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>Список пуст</p>
                @endif
            </div>
        </div>
    </div>
    {{ $payouts->links('partials.nav_panel') }}
@endsection
