@extends('layouts.home_admin')
@section('content_admin')
    <div class="row personal-page__row_payouts">
        <div class="personal-page__info-title">Добавление агента</div>
    </div>
    <div class="personal-page__add-agent-subtitle">Общая информация</div>
    <div data-form="agentsAdd" data-method="POST"
         data-action="{{ route('panel.agents.add', [], false) }}">
        <div class="personal-page__agent-add-data row">
            <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                <span class="personal-page__label">E-mail</span>
                <input type="mail" value="" placeholder="agent@mail.ru" name="email">
            </div>
            <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                <span class="personal-page__label">Пароль</span>
                <input type="password" value="" placeholder="********" name="password">
            </div>
            <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                <span class="personal-page__label">Подтверждение пароля</span>
                <input type="password" value="" placeholder="********" name="confirm_password">
            </div>
        </div>
        <div class="personal-page__agent-add-data row">
            <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                <span class="personal-page__label">ФИО</span>
                <input type="text" value="" name="name" placeholder="Агент">
            </div>
            <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                <span class="personal-page__label">Телефон</span>
                <input type="text" value="" placeholder="+7 999 999 99 99" name="phone">
            </div>
            <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                <span class="personal-page__label">Город</span>
                <input type="text" value="" placeholder="Барнаул" name="city">
            </div>
        </div>

        <div class="row row_space-beetween personal-page__add-agent-info">
            <div class="personal-page__agent-add-col">
                <div class="personal-page__agent-add-col-info">Платёжная информация</div>
                <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                    <span class="personal-page__label">Номер карты</span>
                    <input type="text" value="" placeholder="0000 0000 0000 0000" name="number_card">
                </div>
                <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                    <span class="personal-page__label">Банк</span>
                    <input type="text" value="" placeholder="Сбербанк" name="bank">
                </div>
                <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                    <span class="personal-page__label">Ф.И.О. получателя</span>
                    <input type="text" value="" name="name_recipient">
                </div>
            </div>
            <div class="personal-page__agent-add-col">
                <div class="personal-page__agent-add-col-info">Информация о кураторе</div>
                <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                    <span class="personal-page__label">Куратор</span>
                    <input type="text" value="{{ Auth::user()->name }}" name="name_curator">
                </div>
                <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                    <span class="personal-page__label">Телефон</span>
                    <input type="text" data-mask="+7 000 000 00 00" placeholder="+7 999 999 99 99" value="{{ Auth::user()->phone }}" name="phone_curator">
                </div>
                <div class="personal-page__option personal-page__md-input personal-page__agent-mr input">
                    <span class="personal-page__label">E-mail</span>
                    <input type="text" value="{{ Auth::user()->email }}" name="mail_curator">
                </div>
            </div>
        </div>

        <div class="personal-page__agent-add-col personal-page__agent-add-col_w100">
            <div class="personal-page__agent-add-col-info">Страховые компании</div>
            <table class="personal-page__inner-table personal-page__add-agent-table">
                <thead>
                <tr>
                    <th class="personal-page__table-inner-label">Название компании</th>
                    <th class="personal-page__table-inner-label">Регионы и комиссия</th>
                    <th class="personal-page__agent-add-table-delete"></th>
                </tr>
                </thead>
                <tbody class="js-agent-add-region-list">
                <?/*<tr class="js-add-company-row">
                    <td data-title="Название компании" class="personal-page__table-inner-text">Альфа
                        Стахование
                    </td>
                    <td data-title="Регионы и комиссия" class="personal-page__table-inner-text">
                        <a href="javascript:void(0)" class="js-modal-open" data-modal-type="regions-commission">0
                            регионов</a>
                    </td>
                    <td class="personal-page__agent-add-table-delete">
                        <div class="personal-page__data-clear js-delete-table">Удалить компанию</div>
                    </td>
                    <input type="hidden" name="company_id[]"/>
                    <input type="hidden" name="check_all_idStrah[]"/>
                    <input type="hidden" name="commission_all_idStrah[]"/>
                    <input type="hidden" name="type_commission_all_idStrah[]"/>
                    <input type="hidden" name="regions_idStrah[]"/>
                    <input type="hidden" name="commission_regions_idStrah[0]"/>
                    <input type="hidden" name="type_commission_regions_idStrah[0]"/>
                </tr>*/?>
                </tbody>
            </table>
            <div class="personal-page__inner-helper">
                <a href="javascript:void(0)"
                   class="btn btn_light personal-page__agent-add-company personal-page__agent-add-btn js-modal-open"
                   data-modal-type="add-company">Добавить компанию</a>
            </div>
        </div>
        <a href="javascript:void(0)" class="btn personal-page__add-agent-confirm js-submiter-form"
           data-form-name="agentsAdd">Добавить агента</a>
        <div class="personal-page__content js-payouts-content">
        </div>
    </div>
    <div class="modal modal_personal-page-company js-modal" data-modal-type="add-company"
         data-izimodal-width="1000px">
        <div class="modal__inner">
            <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
            <div class="modal__title">Добавить компанию и регионы</div>
            @if(!empty($insurance))
                <div class="modal__row personal-page__add-company">
                    <select name="company_id" class="js-select js-company-id js-parse-id">
                        @foreach($insurance as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="error-modal"></div>
            <div class="modal__row modal__row_header">
                <div class="modal__left-option">
                    <div class="modal__text">Регион</div>
                </div>
                <div class="modal__right-option">
                    <div class="modal__text">Комиссия за ед.</div>
                </div>
            </div>
            <div class="modal__row modal__row_bb">
                <div class="modal__left-option">
                    <input id="pp333" type="checkbox" onchange="changeAllCommission(this)" name="check_all"
                           class="personal-page__checkbox-input">
                    <label for="pp333" class="personal-page__checkbox-label modal__label">Для всех
                        регионов</label>
                </div>
                <div class="modal__right-option">
                    <div class="modal__commission">
                        <div class="modal__commission-input input">
                            <input type="text" name="commission_all">
                        </div>
                        <div class="modal__radio-commission">
                            <div class="modal__option">
                                <input type="radio" name="type_commission_all" checked class="personal-page__radio-input"
                                       id="r11">
                                <label for="r11" class="personal-page__radio-label">%</label>
                            </div>
                            <div class="modal__option">
                                <input type="radio" name="type_commission_all" class="personal-page__radio-input"
                                       id="r22">
                                <label for="r22" class="personal-page__radio-label">Р</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($regions))
                <div class="modal__region-list js-region-commission-inner">
                    <div class="modal__row js-data-clone-region js-region-option-agent">
                        <div class="modal__left-option">
                            <div class="input modal__company-input">
                                <select name="regions" class="js-select personal-page__select">
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}">{{ $region->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal__right-option">
                            <div class="modal__commission js-commission-region">
                                <div class="modal__commission-input  input">
                                    <input type="text" class="js-parse-id js-parse-input" name="commission_regions[0]">
                                </div>
                                <div class="modal__radio-commission">
                                    <div class="modal__option">
                                        <input type="radio" name="type_commission_regions[0]"
                                               class="js-parse-id js-parse-radio personal-page__radio-input" checked id="r44">
                                        <label for="r44" class="personal-page__radio-label">%</label>
                                    </div>
                                    <div class="modal__option">
                                        <input type="radio" name="type_commission_regions[0]"
                                               class="js-parse-id js-parse-radio personal-page__radio-input"  id="r33">
                                        <label for="r33" class="personal-page__radio-label">Р</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-agent">Удалить</div>
                    </div>
                </div>
                <a href="javascript:void(0)" class="modal__link js-add-new-region" data-add="name">+ Добавить регион</a>
            @endif
            <a href="javascript:void(0)" class="modal__save js-add-company btn">Сохранить</a>
        </div>
    </div>
@endsection
