@extends('layouts.home_admin')
@section('content_admin')
    <div class="row personal-page__row_payouts">
        <div class="personal-page__info-title">Компании</div>
    </div>

    <div class="personal-page__company-content">
        <div class="personal-page-table personal-page__company-table personal-page-table_three-column">
            <div class="personal-page-table__header">
                <div class="tr">
                    <div class="td">Страховая компания</div>
                    <div class="td">Регионы и комиссия</div>
                    <div class="td">Активность</div>
                </div>
            </div>
            <div class="personal-page-table__body" data-form="company-list" data-method="POST"
                 data-action="{{ route('panel.company.editActive', [], false) }}">
                @foreach($insurance as $key_fater => $item)
                    @php($itemCommission = '')
                    @if(!empty($commissionUser[$item->id]["commission_fater"]->id))
                        @php($itemCommission = $commissionUser[$item->id]["commission_fater"])
                        <div class="tr">
                            <input name="id_elem[{{ $key_fater }}]" value="{{ $item->id }}" type="hidden">
                            <div data-text="Страховая компания" class="td">{{ $item->name }}</div>
                            <div data-text="Сумма продаж" class="td">
                                <a href="javascript:void(0)" class="js-modal-open"
                                   data-modal-type="regions-commission-{{ $item->id }}">{{ count($commissionUser[$item->id]["regions"]) }}
                                    регионов</a>
                            </div>
                            <div data-text="Активность" class="td">
                                @if($item->active == 0)
                                    <input id="pp{{ $item->id }}" type="checkbox" class="personal-page__checkbox-input"
                                           name="active[{{ $key_fater }}]">
                                @else
                                    <input id="pp{{ $item->id }}" type="checkbox" class="personal-page__checkbox-input"
                                           name="active[{{ $key_fater }}]" checked>
                                @endif
                                <label for="pp{{ $item->id }}" class="personal-page__checkbox-label"></label>
                            </div>
                        </div>
                        <div class="modal modal_personal-page-company js-modal"
                             data-modal-type="regions-commission-{{ $item->id }}"
                             data-izimodal-width="1000px">
                            <div class="modal__inner" data-form="region-commision-{{ $item->id }}" data-method="POST"
                                 data-action="{{ route('panel.company.commission', [], false) }}">
                                <input name="id_company" value="{{ $item->id }}" type="hidden">
                                <input name="id_user" value="{{ Auth::user()->id }}" type="hidden">
                                <input name="id_commission" value="{{ $itemCommission->id }}" type="hidden">
                                <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                                <div class="modal__title">{{ $item->name }}: регионы и комиссия</div>
                                <div class="error-modal"></div>
                                <div class="modal__row modal__row_header">
                                    <div class="modal__left-option">
                                        <div class="modal__text">Регион</div>
                                    </div>
                                    <div class="modal__right-option">
                                        <div class="modal__text">Комиссия за ед.</div>
                                    </div>
                                </div>
                                <div class="modal__row modal__row_bb">
                                    <div class="modal__left-option">
                                        @if($itemCommission->check_commission == 1)
                                            <input id="ppr{{ $item->id }}" type="checkbox"
                                                   onchange="changeAllCommission(this)"
                                                   name="check_all_regions[{{ $item->id }}]"
                                                   class="personal-page__checkbox-input" checked>
                                        @else
                                            <input id="ppr{{ $item->id }}" type="checkbox"
                                                   onchange="changeAllCommission(this)"
                                                   name="check_all_regions[{{ $item->id }}]"
                                                   class="personal-page__checkbox-input">
                                        @endif
                                        <label for="ppr{{ $item->id }}"
                                               class="personal-page__checkbox-label modal__label">Для
                                            всех регионов</label>
                                    </div>
                                    <div class="modal__right-option">
                                        <div class="modal__commission">
                                            <div class="modal__commission-input input">
                                                <input type="text" name="commission_all_regions[{{ $item->id }}]"
                                                       value="{{ $itemCommission->commission }}">
                                            </div>
                                            <div class="modal__radio-commission">
                                                @if($itemCommission->type_commission == 1)
                                                    <div class="modal__option">
                                                        <input type="radio"
                                                               name="type_commission_all_regions[{{ $item->id }}]"
                                                               class="personal-page__radio-input" id="r{{ $item->id }}">
                                                        <label for="r{{ $item->id }}"
                                                               class="personal-page__radio-label">%</label>
                                                    </div>
                                                    <div class="modal__option">
                                                        <input type="radio"
                                                               name="type_commission_all_regions[{{ $item->id }}]"
                                                               class="personal-page__radio-input"
                                                               id="r2{{ $item->id }}"
                                                               checked>
                                                        <label for="r2{{ $item->id }}"
                                                               class="personal-page__radio-label">Р</label>
                                                    </div>
                                                @else
                                                    <div class="modal__option">
                                                        <input type="radio"
                                                               name="type_commission_all_regions[{{ $item->id }}]"
                                                               class="personal-page__radio-input" id="r{{ $item->id }}"
                                                               checked>
                                                        <label for="r{{ $item->id }}"
                                                               class="personal-page__radio-label">%</label>
                                                    </div>
                                                    <div class="modal__option">
                                                        <input type="radio"
                                                               name="type_commission_all_regions[{{ $item->id }}]"
                                                               class="personal-page__radio-input" id="r2{{ $item->id }}"
                                                               >
                                                        <label for="r2{{ $item->id }}"
                                                               class="personal-page__radio-label">Р</label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(!empty($regions))
                                    @if(!empty($commissionUser[$item->id]["regions"]))
                                        <div class="modal__region-list js-region-commission-inner">
                                            @foreach($commissionUser[$item->id]["regions"] as $key => $regionInfo)
                                                <div class="modal__row js-region-row-company  @if($key==0){{ 'js-data-clone-region' }}@endif">
                                                    <div class="modal__left-option">
                                                        <div class="input modal__company-input">
                                                            <select class="js-select personal-page__select"
                                                                    name="regions_list[{{ $item->id }}][{{ $key }}]">
                                                                @foreach($regions as $region)
                                                                    @if($regionInfo["region_id"] == $region->id)
                                                                        <option value="{{ $region->id }}"
                                                                                selected>{{ $region->name }}</option>
                                                                    @else
                                                                        <option
                                                                            value="{{ $region->id }}">{{ $region->name }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="modal__right-option">
                                                        <div
                                                            class="modal__commission js-commission-region @if($item->check_commission == 1){{ 'disabled-commission' }}@endif">
                                                            <div class="modal__commission-input input">
                                                                <input type="text"
                                                                       name="commission_regions[{{ $item->id }}][{{ $key }}]"
                                                                       value="{{ $regionInfo["commission"] }}">
                                                            </div>
                                                            <div class="modal__radio-commission">
                                                                @if($regionInfo["type"] == 0)
                                                                    <div class="modal__option">
                                                                        <input type="radio"
                                                                               name="type_commission_region[{{ $item->id }}][{{ $key }}]"
                                                                               class="personal-page__radio-input"
                                                                               id="r3{{ $item->id }}"
                                                                               checked>
                                                                        <label for="r3{{ $item->id }}"
                                                                               class="personal-page__radio-label">%</label>
                                                                    </div>
                                                                    <div class="modal__option">
                                                                        <input type="radio"
                                                                               name="type_commission_region[{{ $item->id }}][{{ $key }}]"
                                                                               class="personal-page__radio-input"
                                                                               id="r4{{ $item->id }}">
                                                                        <label for="r4{{ $item->id }}"
                                                                               class="personal-page__radio-label">Р</label>
                                                                    </div>
                                                                @else
                                                                    <div class="modal__option">
                                                                        <input type="radio"
                                                                               name="type_commission_region[{{ $item->id }}][{{ $key }}]"
                                                                               class="personal-page__radio-input"
                                                                               id="r3{{ $item->id }}"
                                                                        >
                                                                        <label for="r3{{ $item->id }}"
                                                                               class="personal-page__radio-label">%</label>
                                                                    </div>
                                                                    <div class="modal__option">
                                                                        <input type="radio"
                                                                               name="type_commission_region[{{ $item->id }}][{{ $key }}]"
                                                                               class="personal-page__radio-input"
                                                                               id="r4{{ $item->id }}" checked>
                                                                        <label for="r4{{ $item->id }}"
                                                                               class="personal-page__radio-label">Р</label>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-company">
                                                        Удалить
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="modal__region-list js-region-commission-inner">
                                            <div class="modal__row js-region-row-company js-data-clone-region">
                                                <div class="modal__left-option">
                                                    <div class="input modal__company-input">
                                                        <select class="js-select personal-page__select"
                                                                name="regions_list[{{ $item->id }}][0]">
                                                            @foreach($regions as $region)
                                                                <option
                                                                    value="{{ $region->id }}">{{ $region->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal__right-option">
                                                    <div
                                                        class="modal__commission js-commission-region @if($item->check_commission == 1){{ 'disabled-commission' }}@endif">
                                                        <div class="modal__commission-input input">
                                                            <input type="text"
                                                                   name="commission_regions[{{ $item->id }}][0]">
                                                        </div>
                                                        <div class="modal__radio-commission">
                                                            <div class="modal__option">
                                                                <input type="radio"
                                                                       name="type_commission_region[{{ $item->id }}][0]"
                                                                       class="personal-page__radio-input"
                                                                       id="r3{{ $item->id }}"
                                                                       checked>
                                                                <label for="r3{{ $item->id }}"
                                                                       class="personal-page__radio-label">%</label>
                                                            </div>
                                                            <div class="modal__option">
                                                                <input type="radio"
                                                                       name="type_commission_region[{{ $item->id }}][0]"
                                                                       class="personal-page__radio-input"
                                                                       id="r4{{ $item->id }}">
                                                                <label for="r4{{ $item->id }}"
                                                                       class="personal-page__radio-label">Р</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-company">
                                                    Удалить
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <a href="javascript:void(0)" class="modal__link js-add-new-region" data-company="true">+ Добавить
                                        регион</a>
                                @endif
                                <a href="javascript:void(0)" class="modal__save btn js-company-check-regions">Сохранить</a>
                                <a href="javascript:void(0)" class="modal__save btn js-submiter-form no-elem"
                                   data-form-name="region-commision-{{ $item->id }}">Сохранить</a>
                            </div>
                        </div>
                    @else
                        <div class="tr">
                            <input name="id_elem[{{ $key_fater }}]" value="{{ $item->id }}" type="hidden">
                            <div data-text="Страховая компания" class="td">{{ $item->name }}</div>
                            <div data-text="Сумма продаж" class="td">
                                <a href="javascript:void(0)" class="js-modal-open"
                                   data-modal-type="regions-commission-{{ $item->id }}">{{ $item->RegionsInsurance->count() }}
                                    регионов</a>
                            </div>
                            <div data-text="Активность" class="td">
                                @if($item->active == 0)
                                    <input id="pp{{ $item->id }}" type="checkbox" class="personal-page__checkbox-input"
                                           name="active[{{ $key_fater }}]">
                                @else
                                    <input id="pp{{ $item->id }}" type="checkbox" class="personal-page__checkbox-input"
                                           name="active[{{ $key_fater }}]" checked>
                                @endif
                                <label for="pp{{ $item->id }}" class="personal-page__checkbox-label"></label>
                            </div>
                        </div>
                        <div class="modal modal_personal-page-company js-modal"
                             data-modal-type="regions-commission-{{ $item->id }}"
                             data-izimodal-width="1000px">
                            <div class="modal__inner" data-form="region-commision-{{ $item->id }}" data-method="POST"
                                 data-action="{{ route('panel.company.commission', [], false) }}">
                                <input name="id_company" value="{{ $item->id }}" type="hidden">
                                <input name="id_user" value="{{ Auth::user()->id }}" type="hidden">
                                <input name="id_commission" value="" type="hidden">
                                <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
                                <div class="modal__title">{{ $item->name }}: регионы и комиссия</div>
                                <div class="error-modal"></div>
                                <div class="modal__row modal__row_header">
                                    <div class="modal__left-option">
                                        <div class="modal__text">Регион</div>
                                    </div>
                                    <div class="modal__right-option">
                                        <div class="modal__text">Комиссия за ед.</div>
                                    </div>
                                </div>
                                <div class="modal__row modal__row_bb">
                                    <div class="modal__left-option">
                                        <input id="ppr{{ $item->id }}" type="checkbox"
                                               onchange="changeAllCommission(this)"
                                               name="check_all_regions[{{ $item->id }}]"
                                               class="personal-page__checkbox-input">
                                        <label for="ppr{{ $item->id }}"
                                               class="personal-page__checkbox-label modal__label">Для
                                            всех регионов</label>
                                    </div>
                                    <div class="modal__right-option">
                                        <div class="modal__commission">
                                            <div class="modal__commission-input input">
                                                <input type="text" name="commission_all_regions[{{ $item->id }}]"
                                                       value="">
                                            </div>
                                            <div class="modal__radio-commission">
                                                <div class="modal__option">
                                                    <input type="radio"
                                                           name="type_commission_all_regions[{{ $item->id }}]"
                                                           class="personal-page__radio-input" id="r{{ $item->id }}"
                                                           checked>
                                                    <label for="r{{ $item->id }}"
                                                           class="personal-page__radio-label">%</label>
                                                </div>
                                                <div class="modal__option">
                                                    <input type="radio"
                                                           name="type_commission_all_regions[{{ $item->id }}]"
                                                           class="personal-page__radio-input" id="r2{{ $item->id }}">
                                                    <label for="r2{{ $item->id }}"
                                                           class="personal-page__radio-label">Р</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($regions))
                                    <div class="modal__region-list js-region-commission-inner">
                                        <div class="modal__row js-region-row-company js-data-clone-region">
                                            <div class="modal__left-option">
                                                <div class="input modal__company-input">
                                                    <select class="js-select personal-page__select"
                                                            name="regions_list[{{ $item->id }}][0]">
                                                        @foreach($regions as $region)
                                                            <option
                                                                value="{{ $region->id }}">{{ $region->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal__right-option">
                                                <div
                                                    class="modal__commission js-commission-region @if($item->check_commission == 1){{ 'disabled-commission' }}@endif">
                                                    <div class="modal__commission-input input">
                                                        <input type="text"
                                                               name="commission_regions[{{ $item->id }}][0]">
                                                    </div>
                                                    <div class="modal__radio-commission">
                                                        <div class="modal__option">
                                                            <input type="radio"
                                                                   name="type_commission_region[{{ $item->id }}][0]"
                                                                   class="personal-page__radio-input"
                                                                   id="r3{{ $item->id }}"
                                                                   checked>
                                                            <label for="r3{{ $item->id }}"
                                                                   class="personal-page__radio-label">%</label>
                                                        </div>
                                                        <div class="modal__option">
                                                            <input type="radio"
                                                                   name="type_commission_region[{{ $item->id }}][0]"
                                                                   class="personal-page__radio-input"
                                                                   id="r4{{ $item->id }}">
                                                            <label for="r4{{ $item->id }}"
                                                                   class="personal-page__radio-label">Р</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-company">
                                                Удалить
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" class="modal__link js-add-new-region" data-company="true">+ Добавить
                                        регион</a>
                                @endif
                                <a href="javascript:void(0)" class="modal__save btn js-company-check-regions">Сохранить</a>
                                <a href="javascript:void(0)" class="modal__save btn js-submiter-form no-elem"
                                   data-form-name="region-commision-{{ $item->id }}">Сохранить</a>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    {{ $insurance->links('partials.nav_panel') }}
    <div class="personal-page__company-save">
        <a href="javascript:void(0)" class="btn js-submiter-form" data-form-name="company-list">Сохранить</a>
    </div>
@endsection
