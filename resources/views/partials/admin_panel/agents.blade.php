@extends('layouts.home_admin')
@section('content_admin')
    <div class="row personal-page__row_payouts">
        <div class="personal-page__info-title">Агенты</div>
        <a href="{{ route('panel.agents.add', [], false) }}" class="btn personal-page__add-agent-btn">Добавить
            агента</a>
    </div>
    <form class="personal-page__agent-data row">
        <div class="personal-page__option personal-page__agent-login input">
            <span class="personal-page__label">ФИО, телефон или e-mail</span>
            <input type="text" class="js-reset-value"
                   value="@if(!empty($_REQUEST["filter"])){{ $_REQUEST["filter"] }}@endif"
                   name="filter" placeholder="agent@mail.ru">
        </div>
        @if(!empty($_REQUEST["filter"]))
            <button class="personal-page__data-clear js-agent-confirm" data-confirm="not-confirm" type="submit">
                Сбросить
            </button>
        @else
            <button class="personal-page__data-clear js-agent-confirm" data-confirm="confirm" type="submit">Применить
            </button>
        @endif
        <div
            class="personal-page__option personal-page__option_select-sm input_select input personal-page__agent-viewers">
            <span class="personal-page__label">На странице</span>
            <select class="js-select js-items-views">
                <option value="10" {{ $count_elem==10?'selected':'' }}>10</option>
                <option value="30" {{ $count_elem==30?'selected':'' }}>30</option>
                <option value="50" {{ $count_elem==50?'selected':'' }}>50</option>
            </select>
        </div>
    </form>
    <!--startblock-->
    <div class="personal-page__content personal-page__payouts-content js-payouts-content">
        <div class="personal-page-table personal-page-table_agent">
            <div class="personal-page-table__header">
                <div class="tr">
                    <div class="td">ФИО</div>
                    <div class="td">Город</div>
                    <div class="td">Телефон, e-mail</div>
                    <div class="td">Полисов продано</div>
                    <div class="td">Статус оплаты</div>
                    <div class="td"></div>
                </div>
            </div>
            <div class="personal-page-table__body">
                @if(!empty($agents))
                    @foreach($agents as $agentKey=>$agent)
                        <div class="tr">
                            <div data-text="Имя" class="td">{{ $agent->name }}</div>
                            <div data-text="Город" class="td">{{ $agent->city }}</div>
                            <div data-text="Телефон, e-mail" class="td">
                                <div>
                                    <span>{{ $agent->phone }}</span>
                                    <span>{{ $agent->email }}</span>
                                </div>
                            </div>
                            <div data-text="Полисов продано" class="td">{{ $agent->OrderAgents->count() }}</div>
                            <div data-text="Статус оплаты" class="td">{{ $agent->OrderAgents->sum('price_commission') }}
                                ₽
                            </div>
                            <div data-text="Подробная информация" class="td">
                                <a href="javascript:void(0)" class="js-toggle-payouts"
                                   data-toggle="agent-{{ $agent->id }}">Подробно</a>
                            </div>
                        </div>
                        <div class="personal-page-table__body-hidden personal-page-table__body-hidden_payouts"
                             data-toggled="agent-{{ $agent->id }}" data-form="agentsEdit-{{ $agent->id }}"
                             data-method="POST"
                             data-action="{{ route('panel.agents.edit', [], false) }}">
                            <input type="hidden" name="agent_id" value="{{ $agent->id }}">
                            <div class="personal-page__table-inner-strah">
                                <div class="personal-page__agent-inner">
                                    <div class="personal-page__agent-inner-info">
                                        <table class="personal-page__inner-table">
                                            <thead>
                                            <tr>
                                                <th class="personal-page__table-inner-label">Куратор</th>
                                                <th class="personal-page__table-inner-label">Телефон</th>
                                                <th class="personal-page__table-inner-label">Номер карты</th>
                                                <th class="personal-page__table-inner-label">Банк</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td data-title="Куратор" class="personal-page__table-inner-text">
                                                    {{ $agent->name_curator }}
                                                </td>
                                                <td data-title="Телефон" class="personal-page__table-inner-text">
                                                    {{ $agent->phone_curator }}
                                                </td>
                                                <td data-title="Номер карты" data-title="Компания"
                                                    class="personal-page__table-inner-text">{{ $agent->number_card }}
                                                </td>
                                                <td data-title="Банк" class="personal-page__table-inner-text">
                                                    {{ $agent->bank }}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <table class="personal-page__inner-table">
                                        <thead>
                                        <tr>
                                            <th class="personal-page__table-inner-label">Компания</th>
                                            <th class="personal-page__table-inner-label personal-page__agent-inner-region">
                                                Регионы и комиссия
                                            </th>
                                            <th class="personal-page__table-inner-label personal-page__agent-inner-police">
                                                Полисов продано
                                            </th>
                                            <th class="personal-page__table-inner-label personal-page__agent-inner-income">
                                                Доход
                                            </th>
                                            <th class="personal-page__table-inner-label"></th>
                                        </tr>
                                        </thead>
                                        <tbody class="js-agent-add-region-list">
                                        @if(!empty($agent->CommissionsCompany[0]))
                                            @foreach($agent->CommissionsCompany as $key=>$commission)
                                                <tr data-clone-id="{{($agentKey+1)*300+($key+1)}}"
                                                    data-commission="{{ $commission->id }}"
                                                    data-id-company="{{ $commission->CompanyCommission[0]->id }}"
                                                    class="js-add-company-row">
                                                    <td data-title="Компания" class="personal-page__table-inner-text">
                                                        {{ $commission->CompanyCommission[0]->name }}
                                                    </td>
                                                    <td data-title="Регионы и комиссия"
                                                        class="personal-page__table-inner-text personal-page__agent-inner-region">
                                                        <a href="javascript:void(0)" class="js-modal-open"
                                                           data-idblock="agent-{{ $agent->id }}"
                                                           data-select-company="true"
                                                           data-modal-type="regions-commission-agent-{{$agent->id}}-{{($agentKey+1)*300+($key+1)}}">{{ count($commission->RegionsCommission) }}
                                                            регионов</a>
                                                    </td>
                                                    <td data-title="Полисов продано"
                                                        class="personal-page__table-inner-text personal-page__agent-inner-police">
                                                        {{ $agent->OrdersAgentsInsurance($commission->CompanyCommission[0]->id)->count() }}
                                                    </td>
                                                    <td data-title="Доход"
                                                        class="personal-page__table-inner-text personal-page__agent-inner-income">
                                                        {{ number_format($agent->OrdersAgentsInsurance($commission->CompanyCommission[0]->id)->sum('price_commission'), 2, '.', '') }}
                                                        ₽
                                                    </td>
                                                    <td class="personal-page__table-inner-text">
                                                        <div class="personal-page__data-clear js-delete-table">Удалить
                                                        </div>
                                                    </td>
                                                    <div class="modal modal_personal-page-company js-modal"
                                                         data-commission="{{ $commission->id }}"
                                                         data-modal-type="regions-commission-agent-{{$agent->id}}-{{($agentKey+1)*300+($key+1)}}"
                                                         data-izimodal-width="1000px">
                                                        <div class="modal__inner">
                                                            <a data-izimodal-close="" href="javascript:void(0);"
                                                               class="modal__close"></a>
                                                            <div class="modal__title"> <span class="js-agent-company-name"></span> Добавить компанию и регионы</div>
                                                            @if(!empty($insurance))
                                                                <div
                                                                    class="modal__row personal-page__add-company disabled-commission">
                                                                    <select name="company_id"
                                                                            class="js-select js-company-id js-parse-id">
                                                                        @foreach($insurance as $item)
                                                                            @if($commission->CompanyCommission[0]->id == $item->id)
                                                                                <option
                                                                                    value="{{ $item->id }}"
                                                                                    selected>{{ $item->name }}</option>
                                                                            @else
                                                                                <option
                                                                                    value="{{ $item->id }}">{{ $item->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            @endif
                                                            <div class="error-modal"></div>
                                                            <div class="modal__row modal__row_header">
                                                                <div class="modal__left-option">
                                                                    <div class="modal__text">Регион</div>
                                                                </div>
                                                                <div class="modal__right-option">
                                                                    <div class="modal__text">Комиссия за ед.</div>
                                                                </div>
                                                            </div>
                                                            <div class="modal__row modal__row_bb">
                                                                <div class="modal__left-option">
                                                                    @if($commission->check_commission == 1)
                                                                        <input id="pp333-{{$key}}-{{$agent->id}}"
                                                                               type="checkbox"
                                                                               onchange="changeAllCommission(this)"
                                                                               name="check_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                               class="personal-page__checkbox-input"
                                                                               checked>
                                                                    @else
                                                                        <input id="pp333-{{$key}}-{{$agent->id}}"
                                                                               type="checkbox"
                                                                               onchange="changeAllCommission(this)"
                                                                               name="check_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                               class="personal-page__checkbox-input">
                                                                    @endif

                                                                    <label for="pp333-{{$key}}-{{$agent->id}}"
                                                                           class="personal-page__checkbox-label modal__label">
                                                                        Для всех регионов
                                                                    </label>
                                                                </div>
                                                                <div class="modal__right-option">
                                                                    <div class="modal__commission">
                                                                        <div class="modal__commission-input input">
                                                                            <input type="text" name="commission_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                                   value="{{ $commission->commission }}">
                                                                        </div>
                                                                        @if($commission->type_commission == 1)
                                                                            <div class="modal__radio-commission">
                                                                                <div class="modal__option">
                                                                                    <input type="radio"
                                                                                           name="type_commission_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                                           class="personal-page__radio-input"
                                                                                           id="r11-{{$key}}-{{$agent->id}}">
                                                                                    <label
                                                                                        for="r11-{{$key}}-{{$agent->id}}"
                                                                                        class="personal-page__radio-label">%</label>
                                                                                </div>
                                                                                <div class="modal__option">
                                                                                    <input type="radio"
                                                                                           name="type_commission_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                                           class="personal-page__radio-input"
                                                                                           id="r22-{{$key}}-{{$agent->id}}"
                                                                                           checked>
                                                                                    <label
                                                                                        for="r22-{{$key}}-{{$agent->id}}"
                                                                                        class="personal-page__radio-label">Р</label>
                                                                                </div>
                                                                            </div>
                                                                        @else
                                                                            <div class="modal__radio-commission">
                                                                                <div class="modal__option">
                                                                                    <input type="radio"
                                                                                           name="type_commission_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                                           checked
                                                                                           class="personal-page__radio-input"
                                                                                           id="r11-{{$key}}-{{$agent->id}}">
                                                                                    <label
                                                                                        for="r11-{{$key}}-{{$agent->id}}"
                                                                                        class="personal-page__radio-label">%</label>
                                                                                </div>
                                                                                <div class="modal__option">
                                                                                    <input type="radio"
                                                                                           name="type_commission_all[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}]"
                                                                                           class="personal-page__radio-input"
                                                                                           id="r22-{{$key}}-{{$agent->id}}">
                                                                                    <label
                                                                                        for="r22-{{$key}}-{{$agent->id}}"
                                                                                        class="personal-page__radio-label">Р</label>
                                                                                </div>
                                                                            </div>
                                                                        @endif

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @if(!empty($regions))
                                                                <div
                                                                    class="modal__region-list js-region-commission-inner">
                                                                    @if(!empty($commission->RegionsCommission[0]))
                                                                        @foreach($commission->RegionsCommission as $keyRegionCommission => $regionCommission)
                                                                            <div
                                                                                class="modal__row @if($keyRegionCommission == 0) js-data-clone-region @endif js-region-option-agent">
                                                                                <div class="modal__left-option">
                                                                                    <div
                                                                                        class="input modal__company-input">
                                                                                        <select name="regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][{{ $keyRegionCommission }}]"
                                                                                                class="js-select personal-page__select">
                                                                                            @foreach($regions as $region)
                                                                                                @if($regionCommission->id == $region->id)
                                                                                                    <option
                                                                                                        value="{{ $region->id }}"
                                                                                                        selected>{{ $region->name }}</option>
                                                                                                @else
                                                                                                    <option
                                                                                                        value="{{ $region->id }}">{{ $region->name }}</option>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal__right-option">
                                                                                    <div
                                                                                        class="modal__commission js-commission-region">
                                                                                        <div
                                                                                            class="modal__commission-input  input">
                                                                                            <input type="text"
                                                                                                   class="js-parse-id js-parse-input"
                                                                                                   name="commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][{{ $keyRegionCommission }}]"
                                                                                                   value="{{ $regionCommission->pivot->commission_region }}">
                                                                                        </div>
                                                                                        @if($regionCommission->pivot->type_commission_region == 1)
                                                                                            <div
                                                                                                class="modal__radio-commission">
                                                                                                <div
                                                                                                    class="modal__option">
                                                                                                    <input type="radio"
                                                                                                           name="type_commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][{{ $keyRegionCommission }}]"
                                                                                                           class="js-parse-id js-parse-radio personal-page__radio-input"
                                                                                                           id="rq1-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}">
                                                                                                    <label
                                                                                                        for="rq1-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}"
                                                                                                        class="personal-page__radio-label">%</label>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="modal__option">
                                                                                                    <input type="radio"
                                                                                                           name="type_commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][{{ $keyRegionCommission }}]"
                                                                                                           checked
                                                                                                           class="js-parse-id js-parse-radio personal-page__radio-input"
                                                                                                           id="rq2-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}">
                                                                                                    <label
                                                                                                        for="rq2-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}"
                                                                                                        class="personal-page__radio-label">Р</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        @else
                                                                                            <div
                                                                                                class="modal__radio-commission">
                                                                                                <div
                                                                                                    class="modal__option">
                                                                                                    <input type="radio"
                                                                                                           name="type_commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][{{ $keyRegionCommission }}]"
                                                                                                           checked
                                                                                                           class="js-parse-id js-parse-radio personal-page__radio-input"
                                                                                                           id="rq1-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}">
                                                                                                    <label
                                                                                                        for="rq1-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}"
                                                                                                        class="personal-page__radio-label">%</label>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="modal__option">
                                                                                                    <input type="radio"
                                                                                                           name="type_commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][{{ $keyRegionCommission }}]"
                                                                                                           class="js-parse-id js-parse-radio personal-page__radio-input"
                                                                                                           id="rq2-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}">
                                                                                                    <label
                                                                                                        for="rq2-{{$key}}-{{$agent->id}}-{{$keyRegionCommission}}"
                                                                                                        class="personal-page__radio-label">Р</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-agent">
                                                                                    Удалить
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    @else
                                                                        <div
                                                                            class="modal__row js-data-clone-region js-region-option-agent">
                                                                            <div class="modal__left-option">
                                                                                <div class="input modal__company-input">
                                                                                    <select name="regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][0]"
                                                                                            class="js-select personal-page__select">
                                                                                        @foreach($regions as $region)
                                                                                            <option
                                                                                                value="{{ $region->id }}">{{ $region->name }}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal__right-option">
                                                                                <div
                                                                                    class="modal__commission js-commission-region">
                                                                                    <div
                                                                                        class="modal__commission-input  input">
                                                                                        <input type="text"
                                                                                               class="js-parse-id js-parse-input"
                                                                                               name="commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][0]">
                                                                                    </div>
                                                                                    <div
                                                                                        class="modal__radio-commission">
                                                                                        <div class="modal__option">
                                                                                            <input type="radio"
                                                                                                   name="type_commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][0]"
                                                                                                   class="js-parse-id js-parse-radio personal-page__radio-input"
                                                                                                   id="rq1-{{$key}}-{{$agent->id}}">
                                                                                            <label
                                                                                                for="rq1-{{$key}}-{{$agent->id}}"
                                                                                                class="personal-page__radio-label">%</label>
                                                                                        </div>
                                                                                        <div class="modal__option">
                                                                                            <input type="radio"
                                                                                                   name="type_commission_regions[{{ $agent->id }}][{{ $commission->CompanyCommission[0]->id }}][0]"
                                                                                                   checked
                                                                                                   class="js-parse-id js-parse-radio personal-page__radio-input"
                                                                                                   id="rq2-{{$key}}-{{$agent->id}}">
                                                                                            <label
                                                                                                for="rq2-{{$key}}-{{$agent->id}}"
                                                                                                class="personal-page__radio-label">Р</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-agent">
                                                                                Удалить
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <a href="javascript:void(0)"
                                                                   class="modal__link js-add-new-region"
                                                                   data-add="name">+ Добавить регион</a>
                                                            @endif
                                                            <a href="javascript:void(0)"
                                                               class="modal__save js-add-company btn " data-page="agent"
                                                               data-save="{{($agentKey+1)*300+($key+1)}}">Сохранить</a>
                                                        </div>
                                                    </div>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="personal-page__inner-helper">
                                        <a href="javascript:void(0)"
                                           class="btn btn_light personal-page__agent-add-company js-modal-open"
                                           data-modal-type="add-company" data-idblock="agent-{{ $agent->id }}">Добавить
                                            компанию</a>
                                    </div>
                                    <div class="personal-page__inner-helper">
                                        <a href="javascript:void(0)"
                                           class="btn personal-page__agent-save js-submiter-form"
                                           data-form-name="agentsEdit-{{ $agent->id }}">Сохранить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>Список пуст</p>
                @endif
            </div>
        </div>
    </div>
    {{ $agents->links('partials.nav_panel') }}
    <!--endblock-->
    <div class="modal modal_personal-page-company js-modal" data-modal-type="add-company"
         data-izimodal-width="1000px">
        <div class="modal__inner">
            <a data-izimodal-close="" href="javascript:void(0);" class="modal__close"></a>
            <div class="modal__title"><span class="js-agent-company-name"></span> Добавить компанию и регионы</div>
            @if(!empty($insurance))
                <div class="modal__row personal-page__add-company">
                    <select name="company_id" class="js-select js-company-id js-parse-id">
                        @foreach($insurance as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="error-modal"></div>
            <div class="modal__row modal__row_header">
                <div class="modal__left-option">
                    <div class="modal__text">Регион</div>
                </div>
                <div class="modal__right-option">
                    <div class="modal__text">Комиссия за ед.</div>
                </div>
            </div>
            <div class="modal__row modal__row_bb">
                <div class="modal__left-option">
                    <input id="pp333" type="checkbox" onchange="changeAllCommission(this)" name="check_all"
                           class="personal-page__checkbox-input">
                    <label for="pp333" class="personal-page__checkbox-label modal__label">Для всех
                        регионов</label>
                </div>
                <div class="modal__right-option">
                    <div class="modal__commission">
                        <div class="modal__commission-input input">
                            <input type="text" name="commission_all">
                        </div>
                        <div class="modal__radio-commission">
                            <div class="modal__option">
                                <input type="radio" name="type_commission_all" checked
                                       class="personal-page__radio-input"
                                       id="r11">
                                <label for="r11" class="personal-page__radio-label">%</label>
                            </div>
                            <div class="modal__option">
                                <input type="radio" name="type_commission_all" class="personal-page__radio-input"
                                       id="r22">
                                <label for="r22" class="personal-page__radio-label">Р</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($regions))
                <div class="modal__region-list js-region-commission-inner">
                    <div class="modal__row js-data-clone-region js-region-option-agent">
                        <div class="modal__left-option">
                            <div class="input modal__company-input">
                                <select name="regions" class="js-select personal-page__select">
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}">{{ $region->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal__right-option">
                            <div class="modal__commission js-commission-region">
                                <div class="modal__commission-input  input">
                                    <input type="text" class="js-parse-id js-parse-input" name="commission_regions[0]">
                                </div>
                                <div class="modal__radio-commission">
                                    <div class="modal__option">
                                        <input type="radio" name="type_commission_regions[0]"
                                               class="js-parse-id js-parse-radio personal-page__radio-input" checked
                                               id="r44">
                                        <label for="r44" class="personal-page__radio-label">%</label>
                                    </div>
                                    <div class="modal__option">
                                        <input type="radio" name="type_commission_regions[0]"
                                               class="js-parse-id js-parse-radio personal-page__radio-input" id="r33">
                                        <label for="r33" class="personal-page__radio-label">Р</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="personal-page__data-clear personal-page__delete-region-agent js-delete-region-agent">
                            Удалить
                        </div>
                    </div>
                </div>
                <a href="javascript:void(0)" class="modal__link js-add-new-region" data-add="name">+ Добавить регион</a>
            @endif
            <a href="javascript:void(0)" class="modal__save js-add-company btn " data-page="agent">Сохранить</a>
        </div>
    </div>
@endsection
