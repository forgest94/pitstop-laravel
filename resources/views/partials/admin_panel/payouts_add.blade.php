@extends('layouts.home_admin')
@section('content_admin')
    <div class="row personal-page__row_payouts">
        <div class="personal-page__info-title">Добавить выплаты</div>
    </div>
    <div data-form="payoutsAdd" data-method="POST" data-action="{{ route('panel.payouts.addForm', [], false) }}" data-search="{{ route('panel.payouts.searchAgent', [], false) }}">
        <div class="personal-page__payouts-date-filter personal-page__payouts-date-filter_add">
            @if(!empty($agents))
                <div class="personal-page__option personal-page__option_date input personal-page__payouts-option-agent personal-page__payouts-option-agent_add">
                    <span class="personal-page__label">Агент</span>
                    <select name="payouts_agent" class="js-select js-payouts-add-select-agent personal-page__select">
                        <option value="0">Выбрать агента</option>
                        @foreach($agents as $agent)
                            <option value="{{ $agent->id }}">{{ $agent->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="personal-page__payouts-pay-col">
                <div class="personal-page__payouts-pay-col-text">Номер карты</div>
                <div class="personal-page__payouts-pay-col-text js-payout-card-number">0000-0000-0000-0000</div>
            </div>
            <div class="personal-page__payouts-pay-col">
                <div class="personal-page__payouts-pay-col-text">Банк</div>
                <div class="personal-page__payouts-pay-col-text js-payout-bank-name"></div>
            </div>
        </div>
        <div class="personal-page__date-range personal-page__date-range_payouts personal-page__date-range_payouts_add">
            <div class="personal-page__option personal-page__option_date input">
                <span class="personal-page__label">Даты</span>
                <label class="personal-page__date-range-label">
                    <input type="text" class="js-datepicker ja-payouts-add-datepicker disabled-commission"
                           data-mask="00.00.0000" data-range="true" data-multiple-dates-separator=" - "
                           name="payouts_date"
                           data-view="years">
                </label>
            </div>
        </div>

        <div class="personal-page__payouts-content js-payouts-content">
            <div class="personal-page-table personal-page-table__payout-add personal-page-table_three-column">
                <div class="personal-page-table__header">
                    <div class="tr">
                        <div class="td">Страховая компания</div>
                        <div class="td">Сумма продаж</div>
                        <div class="td">Доход</div>
                    </div>
                </div>
                <div class="personal-page-table__body js-payouts-add-agent-info">

                </div>
            </div>

            <div class="personal-page__payouts-add-row">
                <div class="personal-page__payouts-add-result">
                    <div class="personal-page__payouts-add-result-title">Итого к выплате:</div>
                    <div class="personal-page__payouts-add-result-text js-payouts-add-result">0 ₽</div>
                </div>
                <div class="personal-page__payouts-pay-col">
                    <div class="personal-page__payouts-pay-col-text">Статус оплаты</div>
                    <div class="personal-page__payouts-pay-col-text">
                        <select name="payouts_status" class="js-select js-payouts-add-status ">
                            <option value="0">Ожидает оплаты</option>
                            <option value="1">Выплачено</option>
                        </select>
                    </div>
                </div>
            </div>

            <a href="javascript:void(0)" class="btn personal-page__payout-add js-submiter-form disabled-commission"
               data-form-name="payoutsAdd">Добавить выплату</a>
        </div>
    </div>
@endsection
