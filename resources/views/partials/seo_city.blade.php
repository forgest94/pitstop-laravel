<div class="seo">
    <div class="seo__inner container">
        <div class="seo-block">
            {!! !empty($cityInfo->seo_text)?$cityInfo->seo_text:$page->seo_text_city !!}
        </div>
        <div class="seo-shadow"></div>
    </div>
</div>
