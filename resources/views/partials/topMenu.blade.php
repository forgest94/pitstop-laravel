      <?
      $page = '';
      if(!empty($options->page->slug)){
          $page = $options->page->slug;
      }
      ?>
      <div class="header__part header__part_navigation js-navigation">
            <div class="container header__inner header__inner_navigation">   
                <ul class="header__navigation-wrap">
                    @foreach($items as $menu_item)
                        <li class="header__navigation-item">
                            @if('/'.$page == $menu_item->url)
                                <a href="{{ $menu_item->url }}" class="header__navigation-link header__navigation-link_active" target="{{ $menu_item->target }}">{{ $menu_item->title }}</a>
                            @else
                                <a href="{{ $menu_item->url }}" class="header__navigation-link" target="{{ $menu_item->target }}">{{ $menu_item->title }}</a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>