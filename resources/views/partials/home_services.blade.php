<div class="screen screen_active js-screen" data-type-screen="order" data-id-screen="1">
        <div class="personal-table js-personal-person">
                @if($order->count())
                    @foreach ($order as $key => $item)
                        <div class="personal-table__row">
                            <div class="personal-table__column personal-table__column_name">Услуга ОСАГО №{{ $item->id }} от {{ Date::parse($item->date_start)->format('d.m.Y') }}</div>
                            <div class="personal-table__column personal-table__column_look"><a href="javascript:void(0);" class="personal-table__link personal-table__link_look js-screen-link" data-type-screen="order" data-id-screen="info_{{ $key }}">Посмотреть</a></div>
                            <div class="personal-table__column personal-table__column_remove">@if($item->pay) Оплачена @else Не оплачена @endif</div>
                        </div>
                    @endforeach
                @else
                    <div class="personal-table__row">
                        <div class="personal-table__column personal-table__column_name">Ваш список пуст</div>
                    </div>
                @endif
        </div>
</div>
@if($order->count())
    @foreach ($order as $keyOrd => $itemOrder)
    <?
    $auto = $itemOrder->AutoOrder()->first();
    $mark = explode('|',$auto->mark);
    $model = explode('|',$auto->model);
    $category = explode('|',$auto->category);
    $persons = $itemOrder->PeronsOrder;
    /*echo '<pre>';
        print_r($itemOrder);
    echo '</pre>';*/
    ?>
        <div class="screen js-screen" data-type-screen="order" data-id-screen="info_{{ $keyOrd }}">
            <h2>Услуга ОСАГО №{{ $itemOrder->id }} от {{ Date::parse($itemOrder->date_start)->format('d.m.Y') }}</h2>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label class="input__name">Дата начала страхования:</label>
                    <p>{{ Date::parse($itemOrder->date_start)->format('d.m.Y') }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Дата окончания страхования:</label>
                    <p>{{ gmdate('d.m.Y', strtotime($itemOrder->date_start.'+ 1 years - 1 day')) }}</p>
                </div>
            </div>
            <h3>Автомобиль</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label class="input__name">Марка:</label>
                    <p>{{ $mark[1] }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Модель:</label>
                    <p>{{ $model[1] }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Год:</label>
                    <p>{{ $auto->year }}</p>
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label class="input__name">Категория:</label>
                    <p>{{ $category[1] }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Регистрационный номер:</label>
                    <p>{{ $auto->gos_reg }}</p>
                </div>
                @if($auto->vin)
                    <div class="input input_part-3">
                        <label class="input__name">VIN</label>
                        <p>{{ $auto->vin }}</p>
                    </div>
                @elseif($auto->number_chassis)
                    <div class="input input_part-3">
                        <label class="input__name">Номер шасси</label>
                        <p>{{ $auto->number_chassis }}</p>
                    </div>
                @elseif($auto->number_bodywork)
                    <div class="input input_part-3">
                        <label class="input__name">Номер кузова</label>
                        <p>{{ $auto->number_bodywork }}</p>
                    </div>
                @endif
            </div>
            @if($auto->seria_pts && $auto->number_pts)
                <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label class="input__name">Серия ПТС:</label>
                        <p>{{ $auto->seria_pts }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Номер ПТС:</label>
                        <p>{{ $auto->number_pts }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Дата документа:</label>
                        <p>{{ Date::parse($auto->date_pts)->format('d.m.Y') }}</p>
                    </div>
                </div>
            @elseif($auto->seria_sts && $auto->number_sts)
                <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label class="input__name">Серия СТС:</label>
                        <p>{{ $auto->seria_sts }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Номер СТС:</label>
                        <p>{{ $auto->number_sts }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Дата документа:</label>
                        <p>{{ Date::parse($auto->date_sts)->format('d.m.Y') }}</p>
                    </div>
                </div>
            @endif
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label class="input__name">Мощность двигателя:</label>
                    <p>{{ $auto->power }}</p>
                </div>
            </div>
            @if($persons->count() > 0)
                <h3>Водители</h3>
                @foreach($persons as $itemPerson)
                    <div class="row row_space-beetween">
                        <div class="input input_part-3"> 
                            <label class="input__name">Фамилия</label>
                            <p>{{ $itemPerson->LastName }}</p>
                        </div>
                        <div class="input input_part-3">
                            <label class="input__name">Имя</label>
                            <p>{{ $itemPerson->FirstName }}</p>
                        </div>
                        <div class="input input_part-3">
                            <label class="input__name">Отчество</label>
                            <p>{{ $itemPerson->MiddleName }}</p>
                        </div>
                    </div>
                    <div class="row row_space-beetween">
                        <div class="input input_part-3">
                            <label class="input__name">Дата рождения</label>
                            <p>{{ Date::parse($itemPerson->BirthDate)->format('d.m.Y') }}</p>
                        </div>
                        <div class="input input_part-3">
                            <label class="input__name">Серия и нормер ВУ</label>
                            <p>{{ $itemPerson->docser_numb }}</p>
                        </div>
                        <div class="input input_part-3">
                            <label class="input__name">Дата отсчета стажа</label>
                            <p>{{ Date::parse($itemPerson->ExperienceDate)->format('d.m.Y') }}</p>
                        </div>
                    </div>
                @endforeach
            @else
                <h3>Собственник</h3>
                <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label class="input__name">Фамилия</label>
                        <p>{{ $itemOrder->LastName_Owner }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Имя</label>
                        <p>{{ $itemOrder->FirstName_Owner }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Отчество</label>
                        <p>{{ $itemOrder->MiddleName_Owner }}</p>
                    </div>
                </div>
                <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label class="input__name">Дата рождения</label>
                        <p>{{ Date::parse($itemOrder->BirthDate_Owner)->format('d.m.Y') }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Серия паспорта</label>
                        <p>{{ $itemOrder->Seria_pass }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Номер паспорта</label>
                        <p>{{ $itemOrder->Number_pass }}</p>
                    </div>
                </div>
            @endif
            <h3>Страхователь</h3>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label class="input__name">Фамилия</label>
                    <p>{{ $itemOrder->LastName_Insurer }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Имя</label>
                    <p>{{ $itemOrder->FirstName_Insurer }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Отчество</label>
                    <p>{{ $itemOrder->MiddleName_Insurer }}</p>
                </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-3">
                    <label class="input__name">Дата рождения</label>
                    <p>{{ $itemOrder->BirthDate_Insurer }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Серия паспорта</label>
                    <p>{{ $itemOrder->Seria_pas_Insurer }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Номер паспорта</label>
                    <p>{{ $itemOrder->Number_pas_Insurer }}</p>
                </div>
            </div>
            <div class="row row_space-beetween">
                    <div class="input input_part-3">
                        <label class="input__name">Дата выдачи</label>
                        <p>{{ $itemOrder->Date_pass_Insurer }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Код подразделения</label>
                        <p>{{ $itemOrder->Kod_doc_pass_Insurer }}</p>
                    </div>
                    <div class="input input_part-3">
                        <label class="input__name">Кем выдан</label>
                        <p>{{ $itemOrder->For_doc_pass_Insurer }}</p>
                    </div>
            </div>
            <div class="row row_space-beetween">
                <div class="input input_part-2-3">
                    <label class="input__name">Адрес регистрации</label>
                    <p>{{ $itemOrder->Address_doc_pass_Insurer }}</p>
                </div>
                <div class="input input_part-3">
                    <label class="input__name">Квартира</label>
                    <p>{{ $itemOrder->Apartment_doc_pass_Insurer }}</p>
                </div>
            </div>
            <div class="row buttons-wrap">
                <a href="javascript:void(0);" class="btn btn_light js-screen-link" data-type-screen="order" data-id-screen="1">Назад</a>
            </div>
        </div>
    @endforeach
@endif