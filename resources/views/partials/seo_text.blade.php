@if($page->seo_title && $page->seo_text)
    <div class="seo-block">
        <div class="seo-block__inner container">
            <h3 class="seo-block__title">{{ $page->seo_title }}</h3>
            <noindex>
                <p class="seo-block__text">
                    {{ $page->seo_text }}
                </p>
            </noindex>
            <div class="seo-block__image wow slideInRight"
                 style="background-image: url('{!! config('app.dir_static') !!}/img/seo.jpg')"></div>
        </div>
    </div>
@endif
