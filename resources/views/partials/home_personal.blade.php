
<div class="columns-wrap">
    <form class="column column_personal" action="" method="POST">
        <div class="input input_wide">
            <label for="phone" class="input__name">Номер телефона</label>
            <div class="tooltip"
                data-tooltip="Введите номер контактного телефона, чтобы мы могли связаться с вами для уточнения заявки"></div>
            <input class="req" type="phone" id="phone" name="phone" value="{{ Auth::user()->phone }}" tabindex="0" data-mask="+7 (000) 000 00-00">
        </div>
        <div class="input input_wide">
            <label for="mail" class="input__name">Email</label>
            <div class="tooltip"
                data-tooltip="Email предназначен для входа в систему"></div>
            <input class="req" type="mail" id="mail" name="mail" tabindex="0" value="{{ Auth::user()->email }}">
        </div>
        <h3>Сменить пароль</h3>
        <div class="input input_wide">
            <label for="password" class="input__name">Старый пароль</label>
            <input type="password" id="current-password" name="current-password" tabindex="0">
        </div>
        <div class="input input_wide">
            <label for="password_confirm" class="input__name">Новый пароль</label>
            <input type="password" id="password" name="password" tabindex="0">
        </div>
        <div class="input input_wide">
            <label for="password_confirm" class="input__name">Подтвердить пароль</label>
            <input type="password" id="password_confirmation" name="password_confirmation" tabindex="0">
        </div>
        {{ csrf_field() }}
        <a href="javascript:void(0);" class="btn" data-click="personal_update">Сохранить</a>
        <span class="mess_info personal_er"></span>
    </form>
    <div class="column">
        <div class="stub p-right p-right_desktop">
            @if(setting('site.baner_home'))
                <img src="{!! Voyager::image(setting('site.baner_home')) !!}" alt="" />
            @else
                Место для баннера или  какого-то предложения
            @endif
        </div>
    </div>
</div>