@if(!empty($list))
    <div class="footer__column footer__cites">
        <div class="footer__cites-title">ОСАГО в городах</div>
        <div class="footer__cites-list">
            @foreach($list as $item)
                <a href="/{{ $item->pages[0]->slug }}/{{ $item->slug }}" class="footer__cites-item">{{ $item->name }}</a>
            @endforeach
        </div>
    </div>
@endif
