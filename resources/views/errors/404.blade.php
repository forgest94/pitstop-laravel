@extends('layouts.template')
@section('content')
<div class="not-found">
    <img src="{!! config('app.dir_static') !!}/img/bg404.svg" alt="404" class="not-found__img">
    <p class="not-found__text">Такой страницы не существует. Вернитесь на главную</p>
</div>
@endsection