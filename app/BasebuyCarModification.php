<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BasebuyCarModification extends Model
{
    protected $fillable =
        [
            'id_car_modification',
            'id_car_serie',
            'id_car_model',
            'name',
            'start_production_year',
            'end_production_year',
            'id_car_type'
        ];
    public static function getListModification($model, $serie)
    {
        return static::where('id_car_serie', $serie)->where('id_car_model', $model)->orderBy('name', 'asc')->get();
    }
    public static function getByID($id)
    {
        return static::where('id_car_modification', $id)->first();
    }
}
