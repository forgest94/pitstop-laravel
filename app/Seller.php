<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Seller extends Model
{
    protected $fillable = [
        "yur_fiz",
        "S_PASSPORT_PATH_ADDRESS",
        "S_ADDITIONAL_PASSPORT_PATH_ADDRESS",
        "S_LAST_NAME",
        "S_FIRST_NAME",
        "S_MIDDLE_NAME",
        "S_DATE_BIRTH",
        "S_PLACE_BIRTH",
        "S_PASSPORT_SERIAL",
        "S_PASSPORT_NUMBER",
        "S_PASSPORT_DATE",
        "S_PASSPORT_WHERE",
        "S_PASSPORT_PATH_CITY",
        "S_PASSPORT_PATH_STREET",
        "S_PASSPORT_PATH_NUMBER",
        "S_NATIONALITY",
        "S_SEX",
        "S_INN",
        "S_LEGAL_NAME",
        "S_LEGAL_DATE",
        "S_LEGAL_INN",
        "S_LEGAL_ADDRESS",
        "S_LEGAL_TEL",
        "S_ADDITIONAL_LAST_NAME",
        "S_ADDITIONAL_FIRST_NAME", 
        "S_ADDITIONAL_MIDDLE_NAME", 
        "S_ADDITIONAL_DATE_BIRTH", 
        "S_ADDITIONAL_PLACE_BIRTH", 
        "S_ADDITIONAL_PASSPORT_SERIAL", 
        "S_ADDITIONAL_PASSPORT_NUMBER", 
        "S_ADDITIONAL_PASSPORT_DATE", 
        "S_ADDITIONAL_PASSPORT_WHERE", 
        "S_ADDITIONAL_PASSPORT_PATH_CITY", 
        "S_ADDITIONAL_PASSPORT_PATH_STREET", 
        "S_ADDITIONAL_PASSPORT_PATH_NUMBER", 
        "S_ADDITIONAL_NATIONALITY", 
        "S_ADDITIONAL_SEX",
        "S_ADDITIONAL_INN"
    ];
}
