<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Commission extends Model
{
    protected $fillable = [
        'commission',
        'type_commission',
        'check_commission',
        'user_id',
        'allinsurance_id',
        'delete'
    ];
    public static function findCommissionsUser($user)
    {
        return static::where('user_id', $user)->where('delete', 0)->orderBy('id', 'asc')->get();
    }
    public static function findCommissionsCompanyUser($user, $company)
    {
        return static::where('user_id', $user)->where('delete', 0)->where('allinsurance_id', $company)->orderBy('id', 'desc')->get();
    }
    public function UserCommission()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
    public function CompanyCommission()
    {
        return $this->hasMany('App\Allinsurance', 'id', 'allinsurance_id');
    }
    public function RegionsCommission()
    {
        return $this->belongsToMany('App\Region', 'regions_commissions')->withPivot([
            'commission_region',
            'type_commission_region',
            'region_id'
        ])->orderBy('id', 'asc');
    }
}
