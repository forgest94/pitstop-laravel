<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public static function getListQuestionsSec()
    {
        return static::where('active', 1)->orderBy('order', 'asc')->get();
    }
}
