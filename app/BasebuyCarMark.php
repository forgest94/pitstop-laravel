<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BasebuyCarMark extends Model
{
    protected $fillable = [
        'id_car_mark',
        'name',
        'name_rus',
        'id_car_type'
    ];
    public static function getAllMark()
    {
        return static::where('id_car_type', 1)->orderBy('name', 'asc')->get();
    }
    public static function getByID($id)
    {
        return static::where('id_car_mark', $id)->first();
    }
}
