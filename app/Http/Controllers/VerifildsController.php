<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VerifildsController extends Controller
{
    protected $key;

    public function __construct()
    {
        $this->key = config('app.key_rest');
    }
    public function VirifildApp(Request $request)
    {
        $query = $request->all();
        if($query["APP_KEY"] == $this->key){
            echo json_encode(array('BACK_KEY'=>csrf_token()));
        }else{
            echo json_encode(array('error'=>'Y'));
        }
    }
}
