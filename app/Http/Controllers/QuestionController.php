<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function addQuestion(Request $request)
    {
        $category = (int)$request->input('selectCat');
        $phone = $request->input('tel');
        $mail = $request->input('email');
        $quest = $request->input('question');
        if($category && $phone && $mail && $quest){
            $q_on = Question::where('id_category', $category)->where('phone', $phone)->where('email', $mail)->where('question', $quest)->first();
            if(empty($q_on->id)){
                Question::create([
                    'id_category' => $category,
                    'email' => $mail,
                    'phone' => $phone,
                    'question' => $quest
                ]);
                return response()->json(['success' => true, 'text'=>'Спасибо! Ваш вопрос принят.']);
            }else{
                return response()->json(['success' => false, 'text'=>'Дублирование отзывов.']);
            }
        }else{
            return response()->json(['success' => false, 'text'=>'Ввведите корректные данные.']);
        }
    }
}
