<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\Alfastrah\GetContractSigned;
use App\Http\Controllers\Api\Alfastrah\PartnersInteraction;
use App\Http\Controllers\Api\Ingostrah\IngostrahAllMethod;
use App\Http\Controllers\Api\Renins\ReninsController;
use App\Http\Controllers\Api\Soglasie\SoglasieController;
use App\Order;
use Illuminate\Http\Request;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DocsController extends Controller
{
    function insurableDocs(Request $request){
        $slug = $request->input('slugdoc');
        $policyId = $request->input('policeid');
        $dopId = $request->input('dopid');
        if(!empty($slug) && !empty($policyId)){
            $file = [];
            switch ($slug){
                case 'alfa':
                    $PartnersInteraction = new PartnersInteraction();
                    $infoContract = $PartnersInteraction->getContractId($dopId);
                    $docClass = new GetContractSigned();
                    $file = $docClass->GetContractSignedFun(["policeID"=>$policyId, "upid"=>$dopId]);
                    break;
                case 'soglasie':
                    $docClass = new SoglasieController();
                    $file = $docClass->PdfPolicyDownload($policyId);
                    break;
                case 'renins':
                    $docClass = new ReninsController();
                    $file = $docClass->docInitPdf($policyId);
                    break;
                case 'ingos':
                    $docClass = new IngostrahAllMethod();
                    $file = $docClass->GetPolicyDocuments($policyId);
                    break;
            }
            if(!empty($file)){
                return response()->json($file);
            }else{
                return response()->json([
                    'success' => false,
                    'error' => 'Ошибка печати'
                ]);
            }
        }else{
            return response()->json([
                'success' => false,
                'error' => 'Ошибка печати'
            ]);
        }
    }

    public function orderDocs($id){
        $orderInfo = Order::find($id);
        $pdf = PDF::loadView('pdf.doc_order', ["arFields" => $orderInfo]);
        $urlDoc = 'docs/doc_order.pdf';
        $pdf->save($urlDoc);
        header('Location: /'.$urlDoc);
    }
    public function allOrdersDocs(){
        $orders = Order::getListBuy();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Полис');
        $sheet->setCellValue('B1', 'Ф.И.О. клиента');
        $sheet->setCellValue('C1', 'Город');
        $sheet->setCellValue('D1', 'Телефон, e-mail');
        $sheet->setCellValue('E1', 'Агент');
        $sheet->setCellValue('F1', 'Компания');
        $sheet->setCellValue('G1', 'Стоимость');
        $i=1;
        foreach ($orders as $order) { $i++;
            $insurace_order = $order->Allinsurance()->first();
            $sheet->setCellValue('A'.$i, $order->polis_number);
            $sheet->setCellValue('B'.$i, $order->LastName_Owner.' '.$order->FirstName_Owner.' '.$order->MiddleName_Owner);
            $sheet->setCellValue('C'.$i, $order->CityOfUse);
            $sheet->setCellValue('D'.$i, !empty($order->phone)?$order->phone:(!empty($order->UserOrder[0]->phone)?$order->UserOrder[0]->phone:'').' '.!empty($order->mail)?$order->mail:(!empty($order->UserOrder[0]->email)?$order->UserOrder[0]->email:''));
            $sheet->setCellValue('E'.$i, '');
            $sheet->setCellValue('F'.$i, !empty($insurace_order->name)?$insurace_order->name:'');
            $sheet->setCellValue('G'.$i, $order->price?$order->price:0);
        }

        $writer = new Xlsx($spreadsheet);
        $urlDoc = 'docs/orders_list.xlsx';
        $writer->save($urlDoc);
        header('Location: /'.$urlDoc);
    }
}
