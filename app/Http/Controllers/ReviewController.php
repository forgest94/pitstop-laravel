<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function addReview(Request $request)
    {
        $rating = $request->input('rating');
        $city = $request->input('city');
        $name = $request->input('name');
        $review = $request->input('review');
        $mail = $request->input('mail');
        if($rating && $city && $name && $review && $mail){
            $rev_on = Review::where('name', $name)->where('city', $city)->where('rating', $rating)->where('body', $review)->first();
            if(empty($rev_on->id)){
                Review::create([
                    'name' => $name,
                    'city' => $city,
                    'rating' => $rating,
                    'body' => $review,
                    'mail' => $mail
                ]);
                return response()->json(['success' => true, 'text'=>'Спасибо! Ваш отзыв принят.']);
            }else{
                return response()->json(['success' => false, 'text'=>'Дублирование отзывов.']);
            }
        }else{
            return response()->json(['success' => false, 'text'=>'Ввведите корректные данные.']);
        }
    }
}
