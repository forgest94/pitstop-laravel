<?php

namespace App\Http\Controllers;

use App\Allinsurance;
use App\Commission;
use App\Order;
use App\OsagoListMark;
use App\OsagoListTransportCategorye;
use App\OsagoListTransportType;
use App\Payout;
use App\Region;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        switch (Auth::User()->role_id) {
            case 1:
                session_start();
                $where = [];
                if (!empty($request->input('datestart')) && !empty($request->input('dateend'))) {
                    $where["date"] = [
                        "start" => date('Y-m-d', strtotime($request->input('datestart'))),
                        "end" => date('Y-m-d', strtotime($request->input('dateend')))
                    ];
                }
                $count = 10;
                if (!empty($request->input('items')) || !empty($_SESSION["count_item"])) {
                    $_SESSION["count_item"] = !empty($request->input('items')) ? $request->input('items') : $_SESSION["count_item"];
                    $count = $_SESSION["count_item"];
                }
                $orders = Order::getListBuy($count, $where);
                $sumPriceOrders = Order::sumPriceOrders($where);
                $sumPriceOrdersAgents = Order::sumPriceOrdersAgents($where);
                $sumPayOrders = Order::sumPayOrders($where);

                if (!empty($where["date"])) {
                    $agentsOrderBD = DB::table('users')
                        ->where('role_id', 4)
                        ->join('orders', 'orders.manager_id', '=', 'users.id')
                        ->where('pay', 1)
                        ->where('orders.created_at', '>=', $where["date"]["start"])
                        ->where('orders.created_at', '<=', $where["date"]["end"])
                        ->get();
                } else {
                    $agentsOrderBD = DB::table('users')
                        ->where('role_id', 4)
                        ->join('orders', 'orders.manager_id', '=', 'users.id')
                        ->where('pay', 1)
                        ->get();
                }
                $arFieldsAgents = [];
                $arIdsAgent = [];
                foreach ($agentsOrderBD as $agentBD) {
                    $arFieldsAgents[$agentBD->manager_id]["items"][] = $agentBD;
                    $arFieldsAgents[$agentBD->manager_id]["sum"] = Order::sumOrderAgent($agentBD->manager_id, $where);
                    $arIdsAgent[] = $agentBD->manager_id;
                }
                $agentsOrder = [];
                if (!empty($arIdsAgent))
                    $agentsOrder = User::where('id', $arIdsAgent)->orderBy('id', 'desc')->paginate($count);

                $page = ['title' => 'Личный кабинет администратора', 'slug' => 'panel'];
                return view('partials.admin_panel.statistic', [
                    'page' => (object)$page,
                    'orders' => $orders,
                    'sumPriceOrders' => $sumPriceOrders,
                    'sumPayOrders' => $sumPayOrders,
                    'count_elem' => $count,
                    'sumPriceOrdersAgents' => $sumPriceOrdersAgents,
                    'agentsOrder' => $agentsOrder,
                    'fieldsAgentOrder' => $arFieldsAgents
                ]);
                break;
            default:
                $list_mark = OsagoListMark::getAllMark();
                $list_type = OsagoListTransportType::getListLTT();
                $list_cat = OsagoListTransportCategorye::getListTC();
                $user = User::find(Auth::User()->id);
                $cars = $user->AutoUsers;
                $person = $user->PeronsUser;
                $order = $user->Order;
                $arrCarsParse = array();
                /*if($cars){
                    $mark = array();
                    $model = array();
                    $auto = array();
                    $type_car = array();
                    foreach($cars as $item){
                        $auto = AutoUser::find($item->id);
                        if(!empty($auto->Marks))
                        $mark = $auto->Marks[0];
                        if(!empty($auto->Models))
                        $model = $auto->Models[0];
                        if(!empty($auto->TypeCar))
                        $type_car = $auto->TypeCar[0];
                        if(!empty($auto->CategoryCar))
                        $type_car = $auto->CategoryCar[0];

                        $arrCarsParse[]=array("mark"=>$mark, "model"=>$model, "type"=>$type_car, "item"=>$item);
                    }

                    $arModels = array();
                    $list_model = OsagoListModel::getListMoldel($list_mark[0]->id_mark);
                    if($list_model && is_object($mark))
                    $arModels[$mark->id_mark] = $list_model;
                }*/

                $years = array();
                $count = 30;
                $i2 = 0;
                for ($i = 0; $i < $count; $i++) {
                    $date = date('Y', strtotime('-' . $i . ' years'));
                    $i2++;
                    $years[$i2] = $date;
                }
                $page = ['title' => 'Личный кабинет', 'slug' => 'panel'];
                return view('home', ['page' => (object)$page, 'list_mark' => $list_mark, 'list_type' => $list_type, 'list_cat' => $list_cat, 'years' => $years, 'cars' => $cars, 'person' => $person, 'order' => $order/*, 'models'=>$arModels*/]);
                break;
        }
    }

    public function PageAgents(Request $request)
    {
        $count = 10;
        $where = [];
        if (!empty($request->input('items')) || !empty($_SESSION["count_item_agents"])) {
            session_start();
            $_SESSION["count_item_agents"] = !empty($request->input('items')) ? $request->input('items') : $_SESSION["count_item_agents"];
            $count = $_SESSION["count_item_agents"];
        }
        if (!empty($request->input('filter'))) {
            $where["filter"] = $request->input('filter');
        }
        $usersAgents = User::getListUsersRole(4, $count, $where);
        $regions = Region::getList();
        $insurance = Allinsurance::getAllInsurance();
        $page = ['title' => 'Личный кабинет', 'slug' => 'panel/company'];
        return view('partials.admin_panel.agents', ['page' => (object)$page, 'agents' => $usersAgents, 'count_elem' => $count, 'regions' => $regions, 'insurance' => $insurance]);
    }

    public function PageAgentsAdd(Request $request)
    {
        if ($request->isMethod('post')) {
            $name = $request->input('name');
            $mail = $request->input('email');
            $password = $request->input('password');
            $confirm_password = $request->input('confirm_password');
            $phone = $request->input('phone');
            $city = $request->input('city');
            $number_card = $request->input('number_card');
            $bank = $request->input('bank');
            $name_recipient = $request->input('name_recipient');
            $name_curator = $request->input('name_curator');
            $phone_curator = $request->input('phone_curator');
            $mail_curator = $request->input('mail_curator');
            $company_id = $request->input('company_id');
            $id_commission = $request->input('commission_id');

            $valid_mail = Validator::make(array('email' => $mail), array('email' => 'required|email'));
            $valid_password = Validator::make(array('password' => $password), array('password' => 'required|min:8'));
            if (empty($valid_mail->fails())) {
                $find_mail = User::where('email', $mail)->first();
                if (empty($find_mail)) {
                    if (empty($valid_password->fails())) {
                        if ($password == $confirm_password) {
                            $userAdd = User::create([
                                'name' => $name,
                                'email' => $mail,
                                'password' => \Hash::make($password),
                                'phone' => !empty($phone) ? $phone : '',
                                'city' => !empty($city) ? $city : '',
                                'role_id' => 4,
                                'number_card' => !empty($number_card) ? $number_card : '',
                                'bank' => !empty($bank) ? $bank : '',
                                'name_recipient' => !empty($name_recipient) ? $name_recipient : '',
                                'name_curator' => !empty($name_curator) ? $name_curator : '',
                                'phone_curator' => !empty($phone_curator) ? $phone_curator : '',
                                'mail_curator' => !empty($mail_curator) ? $mail_curator : ''
                            ]);
                            if ($userAdd) {
                                if (!empty($company_id[0])) {
                                    $edit = '';
                                    $arCompanyCommission = [];
                                    foreach ($company_id as $key => $company) {
                                        $commission_all = $request->input('commission_all_' . $company)[$key];
                                        $type_commission_all = $request->input('type_commission_all_' . $company)[$key];
                                        $check_all = $request->input('check_all_' . $company)[$key];
                                        $arRegions = [];
                                        //print_r($company);
                                        foreach ($request->input('regions_' . $company) as $key2 => $region) {
                                            //print_r($request->input('type_commission_regions_' . $company));
                                            $commission_region = $request->input('commission_regions_' . $company)[$key2];
                                            $type_commission_region = $request->input('type_commission_regions_' . $company)[$key2];
                                            /*if(!empty($id_commission[$key])){
                                                $isCommission = RegionsCommission::isAttach($region, $id_commission[$key]);
                                                if (!empty($isCommission->id)) {
                                                    $isCommission->commission_region = !empty($commission_region)?$commission_region:0;
                                                    $isCommission->type_commission_region = !empty($type_commission_region)?$type_commission_region:0;
                                                    $isCommission->save();
                                                }else{
                                                    $arRegions[$region] = [
                                                        "commission_region" => !empty($commission_region)?$commission_region:0,
                                                        "type_commission_region" => !empty($type_commission_region)?$type_commission_region:0
                                                    ];
                                                }
                                            }else{*/
                                            /*   $arRegions[$region] = [
                                                   "commission_region" => !empty($commission_region)?$commission_region:0,
                                                   "type_commission_region" => !empty($type_commission_region)?$type_commission_region:0
                                               ];
                                           }*/
                                            $arRegions[$region] = [
                                                "commission_region" => !empty($commission_region) ? $commission_region : false,
                                                "type_commission_region" => !empty($type_commission_region) ? $type_commission_region : false
                                            ];
                                        }
                                        /*if(!empty($id_commission[$key])){
                                            $commission = Commission::where("id", $id_commission[$key])->first();
                                            if(!empty($commission->id)){
                                                $commission->commission = !empty($commission_all)?$commission_all:0;
                                                $commission->type_commission = !empty($type_commission_all)?$type_commission_all:0;
                                                $commission->check_commission = !empty($check_all)?$check_all:0;
                                                $commission->allinsurance_id = $company;
                                                $commission->save();
                                                $edit = 'Y';
                                            }else{
                                                $commission = Commission::create([
                                                    "user_id" => $userAdd->id,
                                                    "commission" => !empty($commission_all)?$commission_all:0,
                                                    "type_commission" => !empty($type_commission_all)?$type_commission_all:0,
                                                    "check_commission" => !empty($check_all)?$check_all:0,
                                                    "allinsurance_id" => $company
                                                ]);
                                                $edit = 'Y';
                                            }
                                        }else{
                                            $commission = Commission::create([
                                                "user_id" => $userAdd->id,
                                                "commission" => !empty($commission_all)?$commission_all:0,
                                                "type_commission" => !empty($type_commission_all)?$type_commission_all:0,
                                                "check_commission" => !empty($check_all)?$check_all:0,
                                                "allinsurance_id" => $company
                                            ]);
                                            $edit = 'Y';
                                        }*/
                                        $commission = Commission::create([
                                            "user_id" => $userAdd->id,
                                            "commission" => !empty($commission_all) ? $commission_all : false,
                                            "type_commission" => !empty($type_commission_all) ? $type_commission_all : false,
                                            "check_commission" => !empty($check_all) ? $check_all : false,
                                            "allinsurance_id" => $company,
                                            "delete" => 0
                                        ]);
                                        $edit = 'Y';
                                        if (!empty($arRegions) && $commission) {
                                            $commission->RegionsCommission()->attach($arRegions);
                                            $edit = 'Y';
                                        }
                                        $arCompanyCommission[$company] = [
                                            "commission" => $request->input('commission_all_' . $company)[$key],
                                            "check" => $request->input('check_all_' . $company)[$key],
                                            "type" => $request->input('type_commission_all_' . $company)[$key],
                                            "regions" => $arRegions
                                        ];
                                    }
                                    if ($edit) {
                                        return response()->json([
                                            "success" => true,
                                            "message" => "",
                                            "redirect" => route('panel.page.agents', [], false),
                                            "input" => ""
                                        ]);
                                    } else {
                                        return response()->json([
                                            "success" => false,
                                            "message" => "Ошибка добавления регионов",
                                            "input" => ""
                                        ]);
                                    }
                                } else {
                                    return response()->json([
                                        "success" => true,
                                        "message" => "",
                                        "input" => ""
                                    ]);
                                }

                            } else {
                                return response()->json([
                                    "success" => false,
                                    "message" => "Ошибка добавления пользователя",
                                    "input" => ""
                                ]);
                            }
                        } else {
                            return response()->json([
                                "success" => false,
                                "message" => "Пароли не совпадают",
                                "input" => "password"
                            ]);
                        }
                    } else {
                        return response()->json([
                            "success" => false,
                            "message" => "Проверьте правильность пароля. Он должен иметь длину 8 символов",
                            "input" => "password"
                        ]);
                    }
                } else {
                    return response()->json([
                        "success" => false,
                        "message" => "Данный пользователь существует",
                        "input" => "email"
                    ]);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "message" => "Введите корректный Email",
                    "input" => "email"
                ]);
            }
        } else {
            $regions = Region::getList();
            $insurance = Allinsurance::getAllInsurance();
            $page = ['title' => 'Личный кабинет', 'slug' => 'panel/company'];
            return view('partials.admin_panel.agents_add', ['page' => (object)$page, 'regions' => $regions, 'insurance' => $insurance]);
        }
    }

    public function AgentsEdit(Request $request)
    {
        $agent_id = $request->input('agent_id');
        if (!empty($agent_id)) {
            $edit = '';
            $company_id = $request->input('company_id');
            if (!empty($request->input('delete'))) {
                foreach ($request->input('delete') as $delElem) {
                    $commission = Commission::find($delElem);
                    if ($commission) {
                        $commission->delete = 1;
                        $commission->save();
                        $edit = 'Y';
                    }
                }
            }
            if(!empty($request->input('commission_id'))){
                foreach ($request->input('commission_id') as $editElem) {
                    $commission = Commission::find($editElem);
                    if ($commission) {
                        $commission->delete = 1;
                        $commission->save();
                        $edit = 'Y';
                    }
                }
            }
            if (!empty($company_id)) {
                $arFields = [];
                foreach ($company_id as $company) {
                    $commission_all = $request->input('commission_all')[$company];
                    $check_all = $request->input('check_all')[$company];
                    $type_commission_all = $request->input('type_commission_all')[$company];
                    $arRegions = [];
                    if (!empty($request->input('regions')[$company])) {
                        foreach ($request->input('regions')[$company] as $key => $region) {
                            $arRegions[$region] = [
                                "commission_region" => $request->input('commission_regions')[$company][$key],
                                "type_commission_region" => $request->input('type_commission_regions')[$company][$key]
                            ];
                        }
                    }
                    /*$arFields[] = [
                        "company" => $company,
                        "commission_all" => $commission_all,
                        "check_all" => $check_all,
                        "type_commission_all" => $type_commission_all,
                        "regions" => $arRegions
                    ];*/
                    $commission = Commission::create([
                        "user_id" => $agent_id,
                        "commission" => !empty($commission_all) ? $commission_all : false,
                        "type_commission" => !empty($type_commission_all) ? $type_commission_all : false,
                        "check_commission" => !empty($check_all) ? $check_all : false,
                        "allinsurance_id" => $company,
                        "delete" => 0
                    ]);
                    if (!empty($arRegions) && $commission) {
                        $commission->RegionsCommission()->attach($arRegions);
                        $edit = 'Y';
                    }
                }
            }
            if ($edit) {
                return response()->json([
                    "success" => true,
                    "message" => "",
                    "input" => ""
                ]);
            } else {
                return response()->json([
                    "success" => false,
                    "message" => "",
                    "input" => ""
                ]);
            }

        } else {
            return response()->json([
                "success" => false,
                "message" => "Не выбран агент",
                "input" => ""
            ]);
        }
    }

    public function PagePayouts(Request $request)
    {
        $where = [];
        if (!empty($request->input('datestart')) && !empty($request->input('dateend'))) {
            $where["date"] = [
                "start" => date('Y-m-d', strtotime($request->input('datestart'))),
                "end" => date('Y-m-d', strtotime($request->input('dateend')))
            ];
        }
        if (!empty($request->input('select_agent'))) {
            $where["user_id"] = $request->input('select_agent');
        }
        $payouts = Payout::getList(10, $where);
        $usersAgents = User::getListUsersRole(4);
        $page = ['title' => 'Личный кабинет', 'slug' => 'panel/company'];
        return view('partials.admin_panel.payouts', ['page' => (object)$page, 'agents' => $usersAgents, 'payouts' => $payouts]);
    }

    public function PayoutsAgentSearch(Request $request)
    {
        if (!empty($request->input('payouts_agent'))) {
            $agent = User::find($request->input('payouts_agent'));
            $where = [];
            if (!empty($request->input('datestart') && !empty($request->input('dateend')))) {
                $where["date"] = [
                    "start" => date('Y-m-d h:i:s', strtotime($request->input('datestart'))),
                    "end" => date('Y-m-d h:i:s', strtotime($request->input('dateend')))
                ];
            }
            if (!empty($agent->id)) {
                $orders = [];
                $ordersPay = Order::getListBuyAgent(false, $where, $agent->id);
                if (!empty($ordersPay)) {
                    foreach ($ordersPay as $order) {
                        $orders[] = [
                            "order_id" => $order->id,
                            "insurance" => $order->Allinsurance[0]->name,
                            "income" => number_format(Order::sumIncomeOrdersAgent($where, $agent->id), 2, '.', ''),
                            "sum" => number_format(Order::sumPriceOrdersAgent($where, $agent->id), 2, '.', ''),
                            "date" => date('d.m.Y', strtotime($order->created_at))
                        ];
                    }
                }
                return response()->json([
                    "success" => true,
                    "fields" => [
                        "number_card" => $agent->number_card,
                        "bank" => $agent->bank,
                        "orders" => $orders
                    ],
                ]);
            } else {
                return response()->json([
                    "success" => false,
                    "message" => "Агент не найден",
                    "input" => "payouts_agent"
                ]);
            }
        } else {
            return response()->json([
                "success" => false,
                "message" => "Не выбран пользователь",
                "input" => "payouts_agent"
            ]);
        }
    }

    public function PayoutsEditStatus(Request $request)
    {
        if (!empty($request->input('payouts_id'))) {
            $payout = Payout::find($request->input('payouts_id'));
            if (!empty($payout->id)) {
                $payout->status = $request->input('status');
                $payout->save();
                return response()->json([
                    "success" => true,
                    "message" => "",
                    "input" => ""
                ]);
            } else {
                return response()->json([
                    "success" => false,
                    "message" => "Оплата не найдена",
                    "input" => ""
                ]);
            }
        } else {
            return response()->json([
                "success" => false,
                "message" => "Нет оплаты",
                "input" => ""
            ]);
        }
    }

    public function PagePayoutsAdd(Request $request)
    {
        if ($request->isMethod('post')) {
            $payouts_agent = $request->input('payouts_agent');
            $payouts_date = $request->input('payouts_date');
            $payouts_status = $request->input('payouts_status');
            $orders_id = $request->input('orders_id');
            if (!empty($payouts_agent) && !empty($payouts_date)) {
                $exDate = explode(' - ', $payouts_date);
                $payout = Payout::create([
                    "user_id" => $payouts_agent,
                    "date_start" => date('Y-m-d', strtotime($exDate[0])),
                    "date_end" => date('Y-m-d', strtotime($exDate[1])),
                    "status" => $payouts_status
                ]);
                if ($payout) {
                    if (!empty($orders_id)) {
                        $payout->OrdersPayouts()->attach($orders_id);
                    } else {
                        return response()->json([
                            "success" => false,
                            "message" => "Не выбраны заказы",
                            "input" => ""
                        ]);
                    }
                    return response()->json([
                        "success" => true,
                        "message" => "",
                        "redirect" => route('panel.page.payouts', [], false),
                        "input" => ""
                    ]);
                } else {
                    return response()->json([
                        "success" => false,
                        "message" => "Ошибка добавления оплаты",
                        "input" => ""
                    ]);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "message" => "Заполните поля",
                    "input" => "payouts_agent"
                ]);
            }
        } else {
            $usersAgents = User::getListUsersRole(4);
            $page = ['title' => 'Личный кабинет', 'slug' => 'panel/company'];
            return view('partials.admin_panel.payouts_add', ['page' => (object)$page, 'agents' => $usersAgents]);
        }
    }

    public function PageCompany(Request $request)
    {
        $insurance = Allinsurance::getAll(6);
        $regions = Region::getList();
        $commissionsUser = Commission::findCommissionsUser(Auth::user()->id);
        $arCommissionUser = [];
        if (!empty($commissionsUser)) {
            foreach ($commissionsUser as $commission) {
                $arRegionsUser = [];
                foreach ($commission->RegionsCommission as $regionsUser) {
                    $arRegionsUser[] = [
                        "region_id" => $regionsUser->pivot->region_id,
                        "commission" => $regionsUser->pivot->commission_region,
                        "type" => $regionsUser->pivot->type_commission_region
                    ];
                }
                $arCommissionUser[$commission->allinsurance_id] = [
                    "commission_fater" => $commission,
                    "regions" => $arRegionsUser
                ];
            }
        }

        $page = ['title' => 'Личный кабинет', 'slug' => 'panel/company'];
        return view('partials.admin_panel.company', ['page' => (object)$page, 'insurance' => $insurance, 'regions' => $regions, 'commissionUser' => $arCommissionUser]);
    }

    public function commissionCompany(Request $request)
    {
        $id_company = $request->input('id_company');
        $id_user = $request->input('id_user');
        $id_commission = $request->input('id_commission');
        $check_all_regions = $request->input('check_all_regions')[$id_company];
        $commission_all_regions = $request->input('commission_all_regions')[$id_company];
        $type_commission_all_regions = $request->input('type_commission_all_regions')[$id_company];
        $regions_list = $request->input('regions_list')[$id_company];
        $commission_regions = $request->input('commission_regions');
        $type_commission_region = $request->input('type_commission_region');
        if (!empty($id_company) && !empty($id_user)) {
            $findCompany = Allinsurance::getByID($id_company);
            if (!empty($findCompany->id)) {
                $arFieldRegions = [];
                $edit = '';
                if (!empty($regions_list)) {
                    foreach ($regions_list as $key => $regions) {
                        if(empty($regions))continue;
                        /*if(!empty($id_commission)){
                            $isCommission = RegionsCommission::isAttach($regions, $id_commission);
                            if (!empty($isCommission->id)) {
                                $isCommission->commission_region = $commission_regions[$key];
                                $isCommission->type_commission_region = $type_commission_region[$key];
                                $isCommission->save();
                            }else{
                                $arFieldRegions[$regions] = [
                                    'commission_region' => $commission_regions[$key],
                                    'type_commission_region' => $type_commission_region[$key], // 0 - % ; 1 - P
                                ];
                            }
                        }else{
                            $arFieldRegions[$regions] = [
                                'commission_region' => $commission_regions[$key],
                                'type_commission_region' => $type_commission_region[$key], // 0 - % ; 1 - P
                            ];
                        }*/
                        $arFieldRegions[$regions] = [
                            'commission_region' => !empty($commission_regions[$id_company][$key])?$commission_regions[$id_company][$key]:false,
                            'type_commission_region' => !empty($type_commission_region[$id_company][$key])?$type_commission_region[$id_company][$key]:false, // 0 - % ; 1 - P
                        ];
                    }
                }
                /*$commission = Commission::where("id", $id_commission)->first();
                if(empty($commission->id)){
                    $commission = Commission::create([
                        "user_id" => $id_user,
                        "commission" => $commission_all_regions,
                        "type_commission" => $type_commission_all_regions,
                        "check_commission" => $check_all_regions,
                        "allinsurance_id" => $findCompany->id
                    ]);
                    $edit = 'Y';
                }else{
                    $commission->commission = $commission_all_regions;
                    $commission->type_commission = $type_commission_all_regions;
                    $commission->check_commission = $check_all_regions;
                    $commission->allinsurance_id = $findCompany->id;
                    $commission->save();
                    $edit = 'Y';
                }*/
                $commission = Commission::create([
                    "user_id" => $id_user,
                    "commission" => $commission_all_regions,
                    "type_commission" => $type_commission_all_regions,
                    "check_commission" => $check_all_regions,
                    "allinsurance_id" => $findCompany->id,
                    "delete" => 0
                ]);
                $edit = 'Y';
                if (!empty($arFieldRegions) && $commission) {
                    //$findCompany->RegionsInsurance()->attach($arFieldRegions);
                    $commission->RegionsCommission()->attach($arFieldRegions);
                    $edit = 'Y';
                }

                if ($edit) {
                    return response()->json([
                        'success' => true
                    ]);
                } else {
                    return response()->json([
                        'success' => true
                    ]);
                }
            }
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function editCompanyActive(Request $request)
    {
        $idElem = $request->input('id_elem');
        $active = $request->input('active');
        if (!empty($idElem)) {
            foreach ($idElem as $key => $id) {
                $isCompany = Allinsurance::getByID($id);
                if (!empty($isCompany->id)) {
                    $isCompany->active = $active[$key];
                    $isCompany->save();
                }
            }
            return ["success" => true];
        } else {
            return ["success" => false];
        }
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
        ];
        $validator = Validator::make($data, [
            'current-password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',
        ], $messages);
        return $validator;
    }

    public function updateUser(Request $request)
    {
        if (Auth::Check()) {
            $phone = $request->input('phone');
            $mail = $request->input('mail');
            $password = $request->input('password');
            $password_confirmation = $request->input('password_confirmation');
            $currentpassword = $request->input('current-password');
            $user = Auth::user();
            $arInfoGood = array();
            if ($phone && $user->phone != $phone) {
                $user->phone = $phone;
                $user->save();
                $arInfoGood["good"][] = 'phone';
            }
            if ($mail && $user->email != $mail) {
                $find_mail = User::where('email', $mail)->first();
                $valid_mail = Validator::make(array('email' => $mail), array('email' => 'required|email'));
                if (empty($find_mail) && empty($valid_mail->fails())) {
                    $user->email = $mail;
                    $user->save();
                    $arInfoGood["good"][] = 'mail';
                } else {
                    return response()->json(array('error' => 'mail'));
                }
            }
            if ($currentpassword && $password && $password_confirmation && ($password_confirmation == $password)) {
                $request_data = $request->All();
                $validator = $this->admin_credential_rules($request_data);
                if ($validator->fails()) {
                    return response()->json(array('error' => 'password'));
                } else {
                    $current_password = Auth::User()->password;
                    if (\Hash::check($request_data['current-password'], $current_password)) {
                        $user_id = Auth::User()->id;
                        $obj_user = User::find($user_id);
                        $obj_user->password = \Hash::make($request_data['password']);;
                        $obj_user->save();
                        $arInfoGood["good"][] = 'password';
                    } else {
                        $error = array('current-password' => 'Please enter correct current password');
                        return response()->json(array('error' => 'current-password'));
                    }
                }
            }
            if (!empty($arInfoGood))
                return response()->json($arInfoGood);
        } else {
            return response()->json(array('error' => 'no user'));
        }
    }

    public function addMoreHome(Request $request)
    {
        $type = $request->input('type');
        $user = User::find(Auth::User()->id);
        switch ($type) {
            case 'person':
                $person = $user->PeronsUser()->create([
                    'FirstName' => $request->input('FIRST_NAME_AUTO'),
                    'LastName' => $request->input('LAST_NAME_AUTO'),
                    'MiddleName' => $request->input('MIDDLE_NAME_AUTO'),
                    'BirthDate' => date('Y-m-d', strtotime($request->input('DATE_BIRTH_AUTO'))),
                    'docser_numb' => $request->input('DRIVE_DOCUMENT_AUTO'),
                    'ExperienceDate' => date('Y-m-d', strtotime($request->input('DATE_DRIVE_DOCUMENT_AUTO')))
                ]);
                if ($person->id) {
                    //$persons = $user->PeronsUser;
                    $personFild = array('text1' => $person->LastName, 'text2' => $person->FirstName, 'text3' => $person->MiddleName, 'id' => $person->id);
                    return response()->json(array('success' => true, 'text' => 'Персона успешно добавлена', 'elems' => $personFild));
                } else {
                    return response()->json(array('success' => false, 'text' => 'Ошибка добавления, проверьте данные'));
                }
                break;
            case 'auto':
                $type = '';
                if ($request->input('category_id')) {
                    $exCat = explode('|', $request->input('category_id'));
                    $typeInfo = OsagoListTransportType::getListLTT($exCat[0]);
                    $arT = array();
                    foreach ($typeInfo as $type) {
                        $arT[] = $type;
                    }
                    $type = $arT[0]->id_type . '|' . $arT[0]->name_type;
                }
                $arCarUp = [
                    'mark' => $request->input('AUTO_MAKE'),
                    'model' => $request->input('FIRST_MODEL'),
                    'type_car' => $type,
                    'category' => $request->input('category_id'),
                    'gos_reg' => $request->input('REG_NUMBER'),
                    'year' => $request->input('YEAR'),
                    'power' => $request->input('AUTO_POWER'),

                ];
                $exDocTS = explode(' ', $request->input('SERIA_NUMBER_TS'));
                if ($request->input('doc_ts') == 'pts') {
                    $arCarUp['seria_pts'] = $exDocTS[0];
                    $arCarUp['number_pts'] = $exDocTS[0];
                    $arCarUp['date_pts'] = date('Y-m-d', strtotime($request->input('DATE_TS')));
                } else {
                    $arCarUp['seria_sts'] = $exDocTS[0];
                    $arCarUp['number_sts'] = $exDocTS[0];
                    $arCarUp['date_sts'] = date('Y-m-d', strtotime($request->input('DATE_TS')));
                }
                if ($request->input('number_type') == 'vin') {
                    $arCarUp['vin'] = $request->input('VIN');
                } elseif ($request->input('number_type') == 'bodywork') {
                    $arCarUp['number_bodywork'] = $request->input('bodywork');
                } elseif ($request->input('number_type') == 'chassis') {
                    $arCarUp['number_chassis'] = $request->input('chassis');
                }
                $car = $user->AutoUsers()->create($arCarUp);
                if ($car->mark) {
                    $carFild = array('text1' => explode('|', $car->mark)[1], 'text2' => explode('|', $car->model)[1], 'text3' => $car->year, 'id' => $car->id);
                    return response()->json(array('success' => true, 'text' => 'Автомобиль успешно изменен', 'elems' => $carFild));
                } else {
                    return response()->json(array('success' => false, 'text' => 'Ошибка добавления, проверьте данные'));
                }
                break;
        }
    }

    public function editMoreHome(Request $request)
    {
        $type = $request->input('type');
        $id_elem = $request->input('id');
        $user = User::find(Auth::User()->id);
        switch ($type) {
            case 'person':
                if ($id_elem) {
                    $person = $user->PeronsUser()->find($id_elem);
                    if ($person) {
                        $person->update([
                            'FirstName' => $request->input('FIRST_NAME_AUTO_edit'),
                            'LastName' => $request->input('LAST_NAME_AUTO_edit'),
                            'MiddleName' => $request->input('MIDDLE_NAME_AUTO_edit'),
                            'BirthDate' => date('Y-m-d', strtotime($request->input('DATE_BIRTH_AUTO_edit'))),
                            'docser_numb' => $request->input('DRIVE_DOCUMENT_AUTO_edit'),
                            'ExperienceDate' => date('Y-m-d', strtotime($request->input('DATE_DRIVE_DOCUMENT_AUTO_edit')))
                        ]);
                        return response()->json(array('success' => true, 'text' => 'Персона успешно изменена', 'name' => $person->LastName . ' ' . $person->FirstName . ' ' . $person->MiddleName));
                    } else {
                        return response()->json(array('success' => false, 'text' => 'Ошибка добавления, проверьте данные'));
                    }
                }
                break;
            case 'auto':
                if ($id_elem) {
                    $car = $user->AutoUsers()->find($id_elem);
                    if ($car) {
                        $type = '';
                        if ($request->input('category_edit')) {
                            $exCat = explode('|', $request->input('category_edit'));
                            $typeInfo = OsagoListTransportType::getListLTT($exCat[0]);
                            $arT = array();
                            foreach ($typeInfo as $type) {
                                $arT[] = $type;
                            }
                            $type = $arT[0]->id_type . '|' . $arT[0]->name_type;
                        }
                        $arCarUp = [
                            'mark' => $request->input('AUTO_MAKE_edit'),
                            'model' => $request->input('FIRST_MODEL_edit'),
                            'type_car' => $type,
                            'category' => $request->input('category_edit'),
                            'gos_reg' => $request->input('REG_NUMBER_edit'),
                            'year' => $request->input('YEAR_edit'),
                            'power' => $request->input('AUTO_POWER_edit'),

                        ];
                        $exDocTS = explode(' ', $request->input('doc_ts_edit'));
                        if ($request->input('doc_ts_edit') == 'pts') {
                            $arCarUp['seria_pts'] = $exDocTS[0];
                            $arCarUp['number_pts'] = $exDocTS[0];
                            $arCarUp['date_pts'] = date('Y-m-d', strtotime($request->input('DATE_TS_edit')));
                        } else {
                            $arCarUp['seria_sts'] = $exDocTS[0];
                            $arCarUp['number_sts'] = $exDocTS[0];
                            $arCarUp['date_sts'] = date('Y-m-d', strtotime($request->input('DATE_TS_edit')));
                        }
                        if ($request->input('doc_ts_edit') == 'vin') {
                            $arCarUp['vin'] = $request->input('VIN_edit');
                        } elseif ($request->input('doc_ts_edit') == 'bodywork') {
                            $arCarUp['number_bodywork'] = $request->input('bodywork_edit');
                        } elseif ($request->input('doc_ts_edit') == 'chassis') {
                            $arCarUp['number_chassis'] = $request->input('chassis_edit');
                        }
                        $car->update($arCarUp);
                        return response()->json(array('success' => true, 'text' => 'Автомобиль успешно изменен', 'name' => explode('|', $car->mark)[1] . ' ' . explode('|', $car->model)[1] . ' ' . $car->year));
                    } else {
                        return response()->json(array('success' => false, 'text' => 'Ошибка добавления, проверьте данные'));
                    }
                }
                break;
        }
    }

    public function deleteMoreHome(Request $request)
    {
        $type = $request->input('type');
        $id_elem = $request->input('id');
        $user = User::find(Auth::User()->id);
        switch ($type) {
            case 'person':
                if ($id_elem) {
                    $personFind = $user->PeronsUser()->find($id_elem);
                    if ($personFind) {
                        $user->PeronsUser()->detach($id_elem);
                        $personFind->delete();
                        $persons = $user->PeronsUser;
                        return response()->json(array('success' => true, 'text' => 'Персона успешно удалена', 'count' => $persons->count()));
                    }
                }
                break;
            case 'auto':
                if ($id_elem) {
                    $autoFind = $user->AutoUsers()->find($id_elem);
                    if ($autoFind) {
                        $user->AutoUsers()->detach($id_elem);
                        $autoFind->delete();
                        $cars = $user->AutoUsers;
                        return response()->json(array('success' => true, 'text' => 'Автомобиль успешно удален', 'count' => $cars->count()));
                    }
                }
                break;
        }
    }
}
