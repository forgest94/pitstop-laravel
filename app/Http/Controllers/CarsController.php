<?php

namespace App\Http\Controllers;

use App\BasebuyCarGeneration;
use App\BasebuyCarModel;
use App\BasebuyCarModification;
use App\BasebuyCarSeries;


class CarsController extends Controller
{
    public function parseYears($arYears)
    {
        $arParseYear = [];
        if ($arYears["year_begin"] && $arYears["year_end"])
            for ($i = $arYears["year_begin"]; $i <= $arYears["year_end"]; $i++) {
                $arParseYear[$arYears["id"]][] = $i;
            }
        return $arParseYear;
    }

    public function getModels($id_mark)
    {
        $arModels = [];
        $arModelsDb = BasebuyCarModel::getListModel($id_mark);
        if (!empty($arModelsDb))
            foreach ($arModelsDb as $model) {
                $isModel = BasebuyCarGeneration::isModel($model->id_car_model);
                if (!empty($isModel->id_car_generation)) {
                    $arModels[] = [
                        "value" => $model->id_car_model . '|' . $model->name,
                        "name" => $model->name
                    ];
                }
            }
        return $arModels;
    }

    public function getGenerations($id_model)
    {
        $arGenerations = [];
        $arGenerationsDb = BasebuyCarGeneration::getListGeneration($id_model);
        if (!empty($arGenerationsDb))
            foreach ($arGenerationsDb as $generation) {
                $isGeneration = BasebuyCarSeries::isGeneration($id_model, $generation->id_car_generation);
                if (!empty($isGeneration->id_car_serie)) {
                    $years = $this->parseYears(["year_begin" => $generation->year_begin, "year_end" => $generation->year_end, "id" => $generation->id_car_generation]);
                    if (!empty($years)) {
                        $yearsAp = $years[$generation->id_car_generation];
                        foreach ($yearsAp as $year) {
                            $arGenerations[(int)$year] = [
                                "value" => $generation->id_car_generation . '|' . $year,
                                "name" => (int)$year
                            ];
                        }
                    }
                }
            }
        krsort($arGenerations);
        return $arGenerations;
    }

    public function getModifications($id_model, $id_generation)
    {
        $arModifications = [];
        $arSeriesDb = BasebuyCarSeries::getListSeries($id_model, $id_generation);
        foreach ($arSeriesDb as $series) {
            if (!empty($series->id_car_serie)) {
                $arModifDb = BasebuyCarModification::getListModification($id_model, $series->id_car_serie);
                if (!empty($arModifDb))
                    foreach ($arModifDb as $modification) {
                        if (!empty(explode('(', $modification->name)[1]))
                            $parseName = explode(' л.с.)', explode('(', $modification->name)[1])[0];
                        if (!empty($parseName))
                            $arModifications[$parseName] = [
                                "value" => $modification->id_car_modification . "|" . $parseName,
                                "name" => $parseName . ' л.с.'
                            ];
                    }
            }
        }
        return $arModifications;
    }
}
