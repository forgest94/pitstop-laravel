<?php

namespace App\Http\Controllers;

use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;
use PDF;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $mail_to;

    public function __construct ()
    {
        $this->mail_to = config('app.mail_to_all');
    }

    public function about_form(Request $request)
    {
        if ($request->input("name") && $request->input("phone")) {
            $mess = 'Имя: ' . $request->input("name") . ' ' . PHP_EOL;
            $mess .= 'Телефон: ' . $request->input("phone") . ' ' . PHP_EOL;
            if ($request->input("comment"))
                $mess .= 'Текст: ' . $request->input("comment") . ' ' . PHP_EOL;
            Mail::raw($mess, function ($message) {
                $message->from('noreply@pitstop-group.ru', 'Форма на странице "О компании"');
                $message->to($this->mail_to);//->cc('bar@example.com');
                $message->subject('PitStop');
            });
            return response()->json(array("success" => true));
        }
    }

    public function phone_form(Request $request)
    {
        if ($request->input("name") && $request->input("phone")) {
            $mess = 'Имя: ' . $request->input("name") . ' ' . PHP_EOL;
            $mess .= 'Телефон: ' . $request->input("phone") . ' ' . PHP_EOL;
            $mess .= 'Страница: ' . $request->input("page") . ' ' . PHP_EOL;
            Mail::raw($mess, function ($message) {
                $message->from('noreply@pitstop-group.ru', 'Модалка "Заказать звонок"');
                $message->to($this->mail_to);//->cc('bar@example.com');
                $message->subject('PitStop');
            });
            return response()->json(array("success" => true));
        }
    }

    public function calc_kbm(Request $request)
    {
        //$request->input("LAST_NAME") $request->input("MIDDLE_NAME") $request->input("PLACE_BIRTH")
        if ($request->input("FIRST_NAME") && $request->input("TEL")) {
            /*$mess='Фамилия: '.$request->input("LAST_NAME").' '.PHP_EOL;
            $mess.='Отчество: '.$request->input("MIDDLE_NAME").' '.PHP_EOL;
            $mess.='Почта: '.$request->input("PLACE_BIRTH").' '.PHP_EOL;*/

            $mess = 'Имя: ' . $request->input("FIRST_NAME") . ' ' . PHP_EOL;
            $mess .= 'Телефон: ' . $request->input("TEL") . ' ' . PHP_EOL;

            Mail::raw($mess, function ($message) {
                $message->from('noreply@pitstop-group.ru', 'Форма "КБМ онлайн"');
                $message->to($this->mail_to);//->cc('bar@example.com');
                $message->subject('PitStop');
            });
            return response()->json(array("success" => true));
        }
    }

    public function calc_diag(Request $request)
    {
        if ($request->input("FIRST_NAME") && $request->input("DATE_BIRTH") && $request->input("TEL") && $request->input("category")) {
            //$mess='Фамилия: '.$request->input("LAST_NAME").' '.PHP_EOL;
            $mess = 'Имя: ' . $request->input("FIRST_NAME") . ' ' . PHP_EOL;
            $mess .= 'Телефон: ' . $request->input("TEL") . ' ' . PHP_EOL;
            $mess .= 'Желаемая дата: ' . $request->input("DATE_BIRTH") . ' ' . PHP_EOL;
            $mess .= 'Время: ' . $request->input("DATE_BIRTH_TIME") . ' ' . PHP_EOL;
            $mess .= 'Категория: ' . $request->input("category") . ' ' . PHP_EOL;
            /*if($request->input("horns")){
                $mess.='от 1,5 тонн'.PHP_EOL;
            }else{
                $mess.='до 1,5 тонн'.PHP_EOL;
            }*/
            Mail::raw($mess, function ($message) {
                $message->from('noreply@pitstop-group.ru', 'Форма "Техосмотр онлайн"');
                $message->to($this->mail_to);//->cc('bar@example.com');
                $message->subject('PitStop');
            });
            return response()->json(array("success" => true));
        }
    }

    public function kladr(Request $request)
    {
        $query = $request->input('query');
        $type = $request->input('type');
        $parent = $request->input('pagent');
        $parent_v = $request->input('pagent_v');
        $kladr = [];
        $filtr = [];
        switch ($type) {
            case 'address':
                $filtr = ["query" => $query, "count" => 10, "from_bound" => ["value" => "city"], "to_bound" => ["value" => "house"]];
                break;
            case 'citys':
                $filtr = ["query" => $query, "count" => 10, "from_bound" => ["value" => "city"], "to_bound" => ["value" => "settlement"]];
                switch ($parent) {
                    case 'region':
                        $filtr["locations"] = ["region" => $parent_v];
                        $filtr["restrict_value"] = true;
                        break;
                }
                break;
            case 'regions':
                $filtr = ["query" => $query, "count" => 10, "from_bound" => ["value" => "region"], "to_bound" => ["value" => "region"]];
                break;
            default:
                $filtr = ["query" => $query, "count" => 10];
                break;
        }
        $result = DadataSuggest::suggest("address", $filtr);

        foreach ($result["suggestions"] as $val) {
            if (strpos('----' . $val["value"], $val["data"]["region_with_type"]) === false) {
                $result = $val["data"]["region_with_type"] . ', ' . $val["value"];
            } else {
                $result = $val["value"];
            }

            /*if($type == 'address') {
                $result = $val["value"];
            }else{
                if(!empty($val["data"]["city_with_type"])) {
                    $result = $val["data"]["city_with_type"];
                }elseif(!empty($val["data"]["settlement_with_type"])){
                    $result = $val["data"]["settlement_with_type"];
                }else{
                    $result = $val["value"];
                }
            }*/
            $kladr[] = [
                'kladr' => substr($val["data"]["kladr_id"], 0, -2),
                'name' => $result,
                'result' => $result,
                'city' => !empty($val["data"]["city_with_type"]) ? $val["data"]["city_with_type"] : $val["data"]["settlement_with_type"],
                'street' => $val["data"]["street_with_type"],
                'house' => $val["data"]["house"],
                'country' => $val["data"]["country"],
                'region' => $val["data"]["region_with_type"],
                'house_kladr' => $val["data"]["street_kladr_id"]
            ];
        }
        return json_encode($kladr);
    }

    public function domPDF(Request $request)
    {
        $slug = $request->input('slug');
        //return view('pdf.contract',['pdfClass'=>$pdfClass]);
        if ($slug) {
            $pdf = PDF::loadView('pdf.' . $slug, []);
            return $pdf->stream('invoice.pdf');
        }
    }

    public function testMail(Request $request)
    {
        if ($request->input('mail')) {
            $mail = $request->input('mail');
            $mess = 'Вы зарегистрированы на сайте PitStop.' . PHP_EOL;
            $mess .= 'Ваши данные для входа в личный кабинет: ' . PHP_EOL;
            $mess .= 'Логин: ' . $mail . PHP_EOL;
            $mess .= 'Пароль: ';
            Mail::send(['raw' => $mess], ['mail' => $mail], function ($message) use ($mail) {
                $message->from('noreply@pitstop-group.ru', 'Регистрация после оформления ОСАГО"');
                $message->to($mail);//->cc('bar@example.com');
                $message->subject('PitStop');
            });
        }
    }
}
