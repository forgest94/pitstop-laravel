<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\SmsRu\SmsRu;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function registerFirstSend(Request $request)
    {
        if (!Auth::Check()) {
            $name = $request->input('name');
            $mail = $request->input('email');
            $phone = preg_replace('/[^0-9]/', '', $request->input('phone'));
            $password = $request->input('password');
            $password_confirmation = $request->input('password_confirmation');
            $valid_mail = Validator::make(array('email' => $mail), array('email'=>['required', 'string', 'email', 'max:255']));
            if (empty($valid_mail->fails())) {
                $find_mail = User::where('email', $mail)->first();
                $find_phone = User::where('phone', $phone)->first();
                if (empty($find_mail->id) && empty($find_phone->id)) {
                    if (!empty($phone)) {
                        if (iconv_strlen($password) >= 8) {
                            if ($password == $password_confirmation) {
                                $code = rand(1000, 9999);
                                /*return response()->json([
                                    "success" => true,
                                    "djej" => md5(base64_encode($code)),
                                    "code" => $code
                                ]);*/
                                $sms = new SmsRu();
                                $resul = $sms->sendSms([
                                    "phone" => $phone,
                                    "text" => "Ваш код подтверждения:" . $code,
                                ]);
                                if ($resul["success"]) {
                                    return response()->json([
                                        "success" => true,
                                        "djej" => md5(base64_encode($code))
                                    ]);
                                } else {
                                    return response()->json([
                                        "success" => false,
                                        "error" => $resul["info"]
                                    ]);
                                }
                            } else {
                                return response()->json([
                                    "success" => false,
                                    "id_elem" => [
                                        "password",
                                        "password_confirmation"
                                    ],
                                    "error" => "Вы ввели разные пароли"
                                ]);
                            }
                        } else {
                            return response()->json([
                                "success" => false,
                                "id_elem" => [
                                    "password"
                                ],
                                "error" => "Ваш пароль меньше 8 символов"
                            ]);
                        }
                    } else {
                        return response()->json([
                            "success" => false,
                            "id_elem" => [
                                "phone"
                            ],
                            "error" => "Введите корректный телефон"
                        ]);
                    }
                } else {
                    return response()->json([
                        "success" => false,
                        "id_elem" => [
                            "email"
                        ],
                        "error" => "Этот пользователь уже существует"
                    ]);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "id_elem" => [
                        "email"
                    ],
                    "error" => "Введите корректный Email"
                ]);
            }
        } else {
            return response()->json([
                "success" => false,
                "error" => "Вы уже авторизованны"
            ]);
        }
    }

    public function addUser(Request $request){
        $code = (int)$request->input('confirmCode');
        $hash = $request->input('djej');
        if(!empty($code) && !empty($hash)){
            if($hash == md5(base64_encode($code))){
                $name = $request->input('name');
                $mail = $request->input('email');
                $phone = preg_replace('/[^0-9]/', '', $request->input('phone'));
                $password = $request->input('password');
                $find_mail = User::where('email', $mail)->where('phone', $phone)->first();
                if (empty($find_mail->id)) {
                    $addUser = User::create([
                        'name' => $name,
                        'email' => $mail,
                        'phone' => $phone,
                        'password' => Hash::make($password),
                    ]);
                    if (!empty($addUser)) {
                        Auth::login($addUser, true);
                        return response()->json([
                            "success" => true
                        ]);
                    } else {
                        return response()->json([
                            "success" => false,
                            "error" => "Ошибка регистрации"
                        ]);
                    }
                }else{
                    return response()->json([
                        "success" => false,
                        "id_elem" => [
                            "email"
                        ],
                        "error" => "Этот пользователь уже существует"
                    ]);
                }
            }else{
                return response()->json([
                    "success" => false,
                    "error" => "Не верный код"
                ]);
            }
        }else{
            return response()->json([
                "success" => false,
                "error" => "Не верный код"
            ]);
        }
    }
}
