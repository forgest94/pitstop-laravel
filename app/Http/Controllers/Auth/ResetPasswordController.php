<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\SmsRu\SmsRu;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function isPhone(Request $request){
        $phone = preg_replace('/[^0-9]/', '', $request->input('phone'));
        if (!empty($phone)) {
            $find_phone = User::where('phone', $phone)->first();
            if(!empty($find_phone->id)){
                $code = rand(1000, 9999);
                /*return response()->json([
                    "success" => true,
                    "djej" => md5(base64_encode($code)),
                    "phone" => $request->input('phone'),
                    "code" => $code
                ]);*/
                $sms = new SmsRu();
                $resul = $sms->sendSms([
                    "phone" => $phone,
                    "text" => "Ваш код подтверждения:" . $code,
                ]);
                if ($resul["success"]) {
                    return response()->json([
                        "success" => true,
                        "djej" => md5(base64_encode($code)),
                        "phone" => $request->input('phone')
                    ]);
                } else {
                    return response()->json([
                        "success" => false,
                        "error" => $resul["info"]
                    ]);
                }
            }else{
                return response()->json([
                    "success" => false,
                    "id_elem" => [
                        "phone"
                    ],
                    "error" => "Пользователь не найден"
                ]);
            }
        }else{
            return response()->json([
                "success" => false,
                "id_elem" => [
                    "phone"
                ],
                "error" => "Введите корректный телефон"
            ]);
        }
    }

    public function isCode(Request $request){
        $code = (int)$request->input('confirmCode');
        $hash = $request->input('djej');
        if(!empty($code) && !empty($hash)){
            if($hash == md5(base64_encode($code))){
                return response()->json([
                    "success" => true
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "id_elem" => [
                      "confirmCode"
                    ],
                    "error" => "Не верный код"
                ]);
            }
        }else{
            return response()->json([
                "success" => false,
                "id_elem" => [
                    "confirmCode"
                ],
                "error" => "Не верный код"
            ]);
        }
    }

    public function sendReset(Request $request)
    {
        $password = $request->input('password');
        $password_confirmation = $request->input('password_confirmation');
        $phone = preg_replace('/[^0-9]/', '', $request->input('phone'));
        if (iconv_strlen($password) >= 8) {
            if ($password == $password_confirmation) {
                $find_mail = User::where('phone', $phone)->first();
                if(!empty($find_mail->id)) {
                    $uppUser = User::where('phone', $phone)->update(['password' => Hash::make($password)]);
                    if ($uppUser) {
                        return response()->json([
                            "success" => true,
                            "message" => "Данные изменены, попробуйте войти"
                        ]);
                    } else {
                        return response()->json([
                            "success" => false,
                            "error" => "Ошибка востановления пароля"
                        ]);
                    }
                }else{
                    return response()->json([
                        "success" => false,
                        "id_elem" => [
                            "phone"
                        ],
                        "error" => "Пользователь не найден"
                    ]);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "id_elem" => [
                        "password",
                        "password_confirmation"
                    ],
                    "error" => "Вы ввели разные пароли"
                ]);
            }
        }else{
            return response()->json([
                "success" => false,
                "id_elem" => [
                    "password"
                ],
                "error" => "Ваш пароль меньше 8 символов"
            ]);
        }
    }
}
