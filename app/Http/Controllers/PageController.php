<?php

namespace App\Http\Controllers;

use App\BasebuyCarMark;
use App\Category;
use App\City;
use App\OsagoListTransportCategorye;
use App\OsagoListTransportType;
use App\Page;
use App\Partner;
use App\Question;
use App\Review;
use App\SilderPolisplise;
use App\User;
use App\Wiki;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class PageController extends Controller
{
    public function __construct()
    {
        /*if(config('app.env') == 'production')
        $this->middleware('reconstruct');*/
    }

    public function show(Request $request, $slug, $city = false)
    {
        $page = Page::ViewBySlug($slug);
        $partners = Partner::getListPartner(10);
        if ($page) {
            switch ($slug) {
                case 'reviews':
                    $sort["date"] = '';
                    $sort["rating"] = '';
                    $url = array();
                    $class = array();
                    if ($request->input('date') || $request->input('rating')) {
                        if ($request->input('date')) {

                            $sort["date"] = $request->input('date');

                            $url["date"] = $sort["date"] == 'asc' ? '?date=desc' : '?date=asc';
                            $class["date"] = $sort["date"] == 'asc' ? 'sorting__item sorting__item_active sorting__item_upper' : 'sorting__item sorting__item_active';
                            $url["rating"] = '?rating=desc';
                            $class["rating"] = 'sorting__item';

                        } elseif ($request->input('rating')) {

                            $sort["rating"] = $request->input('rating');

                            $url["date"] = '?date=desc';
                            $class["date"] = 'sorting__item';
                            $url["rating"] = $sort["rating"] == 'desc' ? '?rating=asc' : '?rating=desc';
                            $class["rating"] = $sort["rating"] == 'asc' ? 'sorting__item sorting__item_active sorting__item_upper' : 'sorting__item sorting__item_active';

                        }
                    } else {
                        $url["date"] = '?date=asc';
                        $class["date"] = 'sorting__item sorting__item_active';
                        $url["rating"] = '?rating=asc';
                        $class["rating"] = 'sorting__item';
                    }
                    $reviews = Review::getListReview(10, $sort);
                    if (empty($reviews[0]->name))
                        abort(404, 'Page not found');
                    return view('custom_page.reviews', ['page' => $page, 'reviews' => $reviews, 'url_sort' => $url, 'class_sort' => $class, 'partners' => $partners]);
                    break;
                case 'about':
                    $list_city = City::getList();
                    $arCity = [];
                    foreach ($list_city as $item){
                        $arCity[substr(trim($item->name), 0, 2)][] = $item;
                    }
                    ksort($arCity);
                    return view('custom_page.about', ['page' => $page, 'partners' => $partners, 'list_city' => $arCity]);
                    break;
                case 'questions':
                    $categories = Category::getListQuestionsSec();
                    $category_slug = '';
                    if ($request->input('cat')) {
                        $category_slug = $request->input('cat');
                    }
                    $questions = Question::getListQuestion($category_slug);
                    return view('custom_page.questions', ['page' => $page, 'categories' => $categories, 'questions' => $questions, 'category_slug' => $category_slug, 'partners' => $partners]);
                    break;
                case 'kbm':
                    $reviews = Review::getListReview(3);
                    $icon_page = config('app.dir_static') . '/img/calc-welcome-kbm.svg';
                    return view('custom_page.calc-land', ['page' => $page, 'reviews' => $reviews, 'icon' => $icon_page, 'text_button' => 'Расчитать КБМ', 'calc' => 'kbm', 'partners' => $partners]);
                    break;
                case 'gbdd':
                    $reviews = Review::getListReview(3);
                    $list_mark = BasebuyCarMark::getAllMark();
                    $list_type = OsagoListTransportType::getListLTT();
                    $list_cat = OsagoListTransportCategorye::getListTC();
                    $years18 = strtotime('- 18 years');
                    $icon_page = config('app.dir_static') . '/img/calc-welcome-gibdd.svg';
                    $years = array();
                    $count = 50;
                    $i2 = 0;
                    for ($i = 0; $i < $count; $i++) {
                        $date = date('Y', strtotime('-' . $i . ' years'));
                        $i2++;
                        $years[$i2] = $date;
                    }
                    return view('custom_page.calc-land', [
                        'page' => $page,
                        'list_mark' => $list_mark,
                        'list_type' => $list_type,
                        'list_cat' => $list_cat,
                        'years' => $years,
                        'reviews' => $reviews,
                        'years18' => $years18,
                        'icon' => $icon_page,
                        'text_button' => 'Сформировать документы',
                        'calc' => 'doc',
                        'partners' => $partners
                    ]);
                    break;
                case 'inspection':
                    $reviews = Review::getListReview(3);
                    $begin = new DateTime("9:00");
                    $end = new DateTime("17:30");
                    $list_time = array();
                    for ($i = $begin; $i <= $end; $i->modify('+30 minute')) {
                        $list_time[] = $i->format("H:i");
                    }
                    $icon_page = config('app.dir_static') . '/img/calc-welcome-seen.svg';
                    return view('custom_page.calc-land', [
                        'page' => $page,
                        'reviews' => $reviews,
                        'icon' => $icon_page,
                        'text_button' => 'Записаться на техосмотр',
                        'calc' => 'diag',
                        'list_time' => $list_time,
                        'partners' => $partners
                    ]);
                    break;
                case 'insurance':
                    $reviews = Review::getListReview(3);
                    $list_mark = BasebuyCarMark::getAllMark();
                    $list_type = OsagoListTransportType::getListLTT();
                    $list_cat = OsagoListTransportCategorye::getListTC(false, false, true);
                    $wiki = Wiki::getAllWiki();
                    $years = array();
                    $count = 50;
                    $i2 = 0;
                    for ($i = 0; $i < $count; $i++) {
                        $date = date('Y', strtotime('-' . $i . ' years'));
                        $i2++;
                        $years[$i2] = $date;
                    }
                    $years18 = strtotime('- 18 years');
                    /*$years2 = strtotime('+ 1 years + 72 hours - 1 day');
                    $hours = strtotime('+ 72 hours');
                    if(date('N', strtotime(gmdate('Y-m-d', $hours))) == 6){
                      $hours = strtotime('+ 72 hours + 48 hours');
                    }else if(date('N', strtotime(gmdate('Y-m-d', $hours))) == 7){
                      $hours = strtotime('+ 72 hours + 24 hours');
                    }*/
                    $years2 = strtotime('+ 1 years + 2 day');
                    $hours = strtotime('+ 4 day');
                    if (date('N', strtotime(gmdate('Y-m-d', $hours))) == 6) {
                        $hours = strtotime('+ 6 day');
                    } else if (date('N', strtotime(gmdate('Y-m-d', $hours))) == 7) {
                        $hours = strtotime('+ 5 day');
                    }
                    $arCars = array();
                    $arPersons = array();
                    if (Auth::check()) {
                        $user = User::find(Auth::User()->id);
                        $autos = $user->AutoUsers;
                        if ($autos->count())
                            $arCars = $autos;

                        $user = User::find(Auth::User()->id);
                        $persons = $user->PeronsUser;
                        if ($persons->count())
                            $arPersons = $persons;
                    }
                    $isCity = [];
                    if (!empty($city)) {
                        $isCity = City::isCity($city);
                    }
                    return view('custom_page.calc-osago', [
                        'page' => $page,
                        'cityInfo' => $isCity,
                        'list_mark' => $list_mark,
                        'list_type' => $list_type,
                        'list_cat' => $list_cat,
                        'years' => $years,
                        'reviews' => $reviews,
                        'wiki' => $wiki,
                        'years18' => $years18,
                        'years2' => $years2,
                        'hours' => $hours,
                        'arCars' => $arCars,
                        'arPersons' => $arPersons,
                        'dateTest' => date('N', strtotime(gmdate('Y-m-d', $hours))),
                        'partners' => $partners
                    ]);
                    break;
            }
            return view('page', ['page' => $page]);
        } else {
            abort(404, 'Page not found');
        }
    }

    public function policePlease($slug = false)
    {
        $partners = Partner::getListPartner(10);
        $sliders = SilderPolisplise::getListSlider(10);
        $list_mark = BasebuyCarMark::getAllMark();
        $list_cat = OsagoListTransportCategorye::getListTC(false, false, true);
        $wiki = Wiki::getAllWiki();
        $years = array();
        $count = 50;
        $i2 = 0;
        for ($i = 0; $i < $count; $i++) {
            $date = date('Y', strtotime('-' . $i . ' years'));
            $i2++;
            $years[$i2] = $date;
        }
        $years18 = strtotime('- 18 years');
        $years2 = strtotime('+ 1 years + 2 day');
        $hours = strtotime('+ 4 day');
        if (date('N', strtotime(gmdate('Y-m-d', $hours))) == 6) {
            $hours = strtotime('+ 6 day');
        } else if (date('N', strtotime(gmdate('Y-m-d', $hours))) == 7) {
            $hours = strtotime('+ 5 day');
        }
        $arCars = array();
        $arPersons = array();
        if (Auth::check()) {
            $user = User::find(Auth::User()->id);
            $autos = $user->AutoUsers;
            if ($autos->count())
                $arCars = $autos;

            $user = User::find(Auth::User()->id);
            $persons = $user->PeronsUser;
            if ($persons->count())
                $arPersons = $persons;
        }
        $page = [
            'title' => 'Полис плиз',
            'page_title' => 'Полис плиз',
            'slug' => 'index',
            'meta_description' => Voyager::setting('site.description'),
            'meta_keywords' => Voyager::setting('site.keywords'),
            'seo_title' => Voyager::setting('site.seo_title'),
            'seo_text' => Voyager::setting('site.seo_text')
        ];
        return view('layouts.police-please', [
            'page' => (object)$page,
            'list_mark' => $list_mark,
            'list_cat' => $list_cat,
            'years' => $years,
            'sliders' => $sliders,
            'wiki' => $wiki,
            'years18' => $years18,
            'years2' => $years2,
            'hours' => $hours,
            'slug' => $slug,
            'arCars' => $arCars,
            'arPersons' => $arPersons,
            'dateTest' => date('N', strtotime(gmdate('Y-m-d', $hours))),
            'partners' => $partners
        ]);
    }

    public function index()
    {
        $reviews = Review::getListReview(3);
        $partners = Partner::getListPartner(10);
        $page = [
            'title' => Voyager::setting('site.title'),
            'slug' => 'index',
            'meta_description' => Voyager::setting('site.description'),
            'meta_keywords' => Voyager::setting('site.keywords'),
            'seo_title' => Voyager::setting('site.seo_title'),
            'seo_text' => Voyager::setting('site.seo_text')
        ];
        return view('custom_page.index', ['page' => (object)$page, 'reviews' => $reviews, 'partners' => $partners]);
    }

    public function test(Request $request)
    {
        echo 'test';
    }
}
