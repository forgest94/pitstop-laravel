<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\Alfastrah\GetContractSigned;
use App\Http\Controllers\Api\Ingostrah\IngostrahAllMethod;
use App\Http\Controllers\Api\Renins\ReninsController;
use App\Http\Controllers\Api\Soglasie\SoglasieController;
use App\Order;


class UpdateStatusOrder extends Controller
{
    public function updateStatus()
    {
        $listOrders = Order::getListAll();
        foreach ($listOrders as $order) {
            if(empty($order->id_allinsuranses))continue;
            $insurance_order = $order->Allinsurance()->first();
            if (!empty($insurance_order->slug) && !empty($order->polis_number)) {
                //if ($insurance_order->slug != 'soglasie') continue;
                /*switch ($insurance_order->slug) {
                    case 'ingos':
                        $ingos = new IngostrahAllMethod;
                        $infoIngos = $ingos->GetPolicyDocuments($order->polis_number);
                        if ($infoIngos["success"]) {
                            $order->update(["pay" => 1]);
                        }
                        break;
                    case 'soglasie':
                        $soglasie = new SoglasieController();
                        $infoSoglasie = $soglasie->PdfPolicyDownload($order->polis_number);
                        if ($infoSoglasie["success"]) {
                            $order->update(["pay" => 1]);
                        }
                        break;
                    case 'renins':
                        $renins = new ReninsController();
                        $infoRenins = $renins->infoStatusDoc($order->polis_number);
                        if ($infoRenins["success"]) {
                            $order->update(["pay" => 1]);
                        }
                        break;
                }*/
                switch ($insurance_order->slug) {
                    case 'alfa':
                        if (!empty($order->dop_code_strah)) {
                            $contractAlfaSerivce = new GetContractSigned;
                            $infoAlfa = $contractAlfaSerivce->GetContractSignedFun(["upid" => $order->dop_code_strah, "policeID" => $order->polis_number]);
                            if($infoAlfa["success"]){
                                $order->update(["pay" => 1]);
                            }
                        }
                        break;
                    case 'ingos':
                        $ingos = new IngostrahAllMethod;
                        $infoIngos = $ingos->GetPolicyDocuments($order->polis_number, $order);
                        if ($infoIngos["success"]) {
                            $order->update(["pay" => 1]);
                        }
                        break;
                    case 'soglasie':
                        $soglasie = new SoglasieController();
                        $infoSoglasie = $soglasie->PdfPolicyDownload($order->polis_number);
                        if ($infoSoglasie["success"]) {
                            $order->update(["pay" => 1]);
                        }
                        break;
                    case 'renins':
                        $renins = new ReninsController();
                        //$infoRenins = $renins->infoStatusDoc($order->polis_number);
                        $infoRenins = $renins->docInitPdf($order->polis_number);
                        if ($infoRenins["success"]) {
                            $order->update(["pay" => 1]);
                        }
                        break;
                }
            } else {
                continue;
            }
        }
    }
}
