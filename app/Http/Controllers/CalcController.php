<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 10000);
session_start();

use App\Allinsurance;
use App\AutoOrder;
use App\AutoUser;
use App\Http\Controllers\Api\Alfastrah\Cross;
use App\Http\Controllers\Api\Alfastrah\EOsagoRelaunch;
use App\Http\Controllers\Api\Alfastrah\MerchantServices;
use App\Http\Controllers\Api\Alfastrah\OsagoCalcRelaunch;
use App\Http\Controllers\Api\Alfastrah\PartnersInteraction;
use App\Http\Controllers\Api\Alfastrah\PaymentCross;
use App\Http\Controllers\Api\Ingostrah\IngostrahAllMethod;
use App\Http\Controllers\Api\Makc\MakcController;
use App\Http\Controllers\Api\Renins\ReninsController;
use App\Http\Controllers\Api\SmsRu\SmsRu;
use App\Http\Controllers\Api\Soglasie\SoglasieController;
use App\PeronsUser;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PDF;
use TCG\Voyager\Facades\Voyager;


class CalcController extends Controller
{
    protected $env_type;

    public function __construct()
    {
        $this->env_type = config('app.env');
    }

    public function array_multisort_value()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row) {
                    $tmp[$key] = $row[$field];
                }
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    public function inpForm($request)
    {
        /* Auto */
        $vin = '';
        $number_bodywork = '';
        $number_chassis = '';
        $id_auto = 0;
        if ($request->input('SLECT_AUTO') > 0) {
            $autoFind = AutoUser::find($request->input('SLECT_AUTO'));
            $id_auto = $autoFind->id;
        }
        if ($id_auto > 0) {
            $mark = $autoFind->mark;
            $mark_ex = explode('|', $mark);
            $model = $autoFind->model;
            $model_ex = explode('|', $model);
            $type = $autoFind->type_car;
            $type_ex = explode('|', $type);
            $category = $autoFind->category;
            $category_ex = explode('|', $category);

            $vin = $autoFind->vin;
            $year_ts = $autoFind->year;
            $year_ts_ex = explode('|', $year_ts);
            $power = $autoFind->power;
            $power_ex = explode('|', $power);
            $number_bodywork = $autoFind->number_bodywork;
            $number_chassis = $autoFind->number_chassis;
        } else {
            $mark = $request->input('AUTO_MAKE');
            $mark_ex = explode('|', $mark);
            $model = $request->input('FIRST_MODEL');
            $model_ex = explode('|', $model);
            $type = '2|Легковой а/м';//$request->input('AUTO_TYPE');
            $type_ex = explode('|', $type);
            $category = $request->input('category');
            $category_ex = explode('|', $category);
            $year_ts = $request->input('YEAR');
            $year_ts_ex = explode('|', $year_ts);
            $power = $request->input('AUTO_POWER');
            $power_ex = explode('|', $power);
        }
        switch ($request->input('number_type')) {
            case 'vin':
                $vin = $request->input('VIN');
                break;
            case 'bodywork':
                $number_bodywork = $request->input('bodywork');
                break;
            case 'chassis':
                $number_chassis = $request->input('chassis');
                break;
        }
        $regn = $request->input('REG_NUMBER');
        $seriarN = $request->input('SERIA_NUMBER_TS');
        if ($seriarN) {
            $exSerNDoc = explode(' ', $seriarN);
            $seria_ts = $exSerNDoc[0];
            $number_ts = $exSerNDoc[1];
        } else {
            $seria_ts = '';
            $number_ts = '';
        }
        $datets_ts = $request->input('DATE_TS');

        $city = $request->input('CITY_SOB_DOP');
        $region = $request->input('REGION_SOB');
        $email = $request->input('MAIL_PERSON');
        $phone = $request->input('PHONE_PERSON');

        $dateStart = $request->input('DATE_START');

        $count_drive = '';

        if ($request->input('LAST_NAME_AUTO') || ($request->input('SELECT_PERSON') > 0))
            $count_drive = $request->input('driver_numb');

        if (Auth::check()) {
            /*$phone = Auth::user()->phone;
            $email = Auth::user()->email;*/
            $id_user = Auth::user()->id;
        } else {
            /*$phone = '79999999999';
            $email = 'test@test.ru';*/
            $id_user = 0;
        }
        $arFildsCalc = array(
            'kladr_address' => substr($request->input('HOME_STRAH_KLADR'), 0, 15),
            'ContractType' => 'Первоначальный',
            'RegionOfUse' => $region,
            'CityOfUse' => $city,
            'city_sob' => $request->input('CITY_SOB_DOP'),
            'kladr_city' => $request->input('CITY_SOB_KLADR'),
            'kladr_region' => $request->input('REGION_SOB_KLADR'),
            'email' => $email,
            'old_seria_police' => $request->input('OLD_SERIA_POLICE'),
            'old_number_police' => $request->input('OLD_NUMBER_POLICE'),
            'phone' => $phone,
            'id_user' => $id_user,
            'offer_accepted' => 'Y',
            'doc_ts' => $request->input('doc_ts'),
            'DriversRestriction' => $count_drive ? true : false,
            'LicensePlate' => $regn,
            'PolicyBeginDate' => $dateStart,
            'VIN' => $vin,
            'cart_diag' => $request->input('CART_DIAG'),
            'date_cart_diag' => date("Y-m-d", strtotime($request->input('DATE_CART_DIAG'))),
            'id_auto' => $id_auto,
            'Mark' => $mark_ex[1],
            'Model' => $model_ex[1],
            'number_bodywork' => $number_bodywork,
            'number_chassis' => $number_chassis,
            'YearIssue' => !empty($year_ts_ex[1]) ? $year_ts_ex[1] : $year_ts,
            'Category' => $category_ex[1],
            'Type' => $type_ex[1],
            'Power' => !empty($power_ex[1]) ? $power_ex[1] : $power,
            'DocCarSerial' => $seria_ts,
            'DocCarNumber' => $number_ts,
            'DocumentCarDate' => $datets_ts,
            'type_doc_ts' => $request->input('doc_ts'),
            'EngCap' => !empty($power_ex[1]) ? $power_ex[1] : $power,
            'model_ingos' => array($mark_ex[0], $model_ex[1]),
            'Drivers' => array()
        );
        $seria_pass2 = '';
        $number_pass2 = '';
        if (!empty($request->input('SERIA_SOB'))) {
            $pass_doc_ex2 = explode(' ', $request->input('SERIA_SOB'));
            if (!empty($pass_doc_ex[0]))
                $seria_pass2 = $pass_doc_ex2[0];
            if (!empty($pass_doc_ex[1]))
                $number_pass2 = $pass_doc_ex2[1];
        }
        $arFildsCalc["Owner_calc"] = array( // Собственник
            'LastName' => $request->input('LAST_NAME_SOB'),
            'FirstName' => $request->input('FIRST_NAME_SOB'),
            'MiddleName' => $request->input('MIDDLE_NAME_SOB'),
            'BirthDate' => date("Y-m-d", strtotime($request->input('BRIT_SOB'))),
            'BirthDate_old' => $request->input('BRIT_SOB'),
            'Number' => $number_pass2,
            'Seria' => $seria_pass2
        );
        $seria_pass = '';
        $number_pass = '';
        if (!empty($request->input('SERIA_STRAH'))) {
            $pass_doc_ex = explode(' ', $request->input('SERIA_STRAH'));
            if (!empty($pass_doc_ex[0]))
                $seria_pass = $pass_doc_ex[0];
            if (!empty($pass_doc_ex[1]))
                $number_pass = $pass_doc_ex[1];
        }
        $arFildsCalc["Owner"] = array( // Собственник
            'LastName' => $request->input('LAST_NAME_STRAH'),
            'FirstName' => $request->input('FIRST_NAME_STRAH'),
            'MiddleName' => $request->input('MIDDLE_NAME_STRAH'),
            'BirthDate' => date("Y-m-d", strtotime($request->input('BRIT_STRAH'))),
            'BirthDate_old' => $request->input('BRIT_STRAH'),
            'Number' => $number_pass,
            'Seria' => $seria_pass,
            'Date_pass' => $request->input('DATE_DOC_STRAH'),
            'Kod_doc_pass' => $request->input('KOD_DOC_STRAH'),
            'For_doc_pass' => $request->input('FOR_DOC_STRAH'),
            'address' => [
                "region" => $request->input('REGION_STRAH_DOP'),
                "city" => $request->input('CITY_STRAH'),
                "street" => $request->input('STREET_STRAH'),
                "home" => $request->input('HOME_STRAH'),
                "apartament" => $request->input('APARTAMENT_STRAH'),
                "country" => $request->input('COUNTRY_STRAH'),
                //"building"=>$request->input('BUILDING_STRAH')
            ],
        );
        $arFildsCalc["Insurer"] = array( // Страхователь
            'LastName' => $request->input('LAST_NAME_STRAH'),
            'FirstName' => $request->input('FIRST_NAME_STRAH'),
            'MiddleName' => $request->input('MIDDLE_NAME_STRAH'),
            'BirthDate' => date("Y-m-d", strtotime($request->input('BRIT_STRAH'))),
            'BirthDate_old' => $request->input('BRIT_STRAH'),
            'Number' => $number_pass,
            'Seria' => $seria_pass,
            'Date_pass' => $request->input('DATE_DOC_STRAH'),
            'Kod_doc_pass' => $request->input('KOD_DOC_STRAH'),
            'For_doc_pass' => $request->input('FOR_DOC_STRAH'),
            'address' => [
                "region" => $request->input('REGION_STRAH_DOP'),
                "city" => $request->input('CITY_STRAH'),
                "street" => $request->input('STREET_STRAH'),
                "home" => $request->input('HOME_STRAH'),
                "apartament" => $request->input('APARTAMENT_STRAH'),
                "country" => $request->input('COUNTRY_STRAH'),
                //"building"=>$request->input('BUILDING_STRAH')
            ],//$request->input('ADDRESS_STRAH'),
            //'Apartment_doc_pass'=>$request->input('APARTAMENT_STRAH')
        );
        if (!empty($request->input('REGION_STRAH_DOP'))) {
            $arFildsCalc["Owner"]["address"]["region"] = $request->input('REGION_STRAH_DOP');
            $arFildsCalc["Insurer"]["address"]["region"] = $request->input('REGION_STRAH_DOP');
        }
        if (!empty($request->input('CITY_STRAH'))) {
            $arFildsCalc["Owner"]["address"]["city"] = $request->input('CITY_STRAH');
            $arFildsCalc["Insurer"]["address"]["city"] = $request->input('CITY_STRAH');
        }
        if (!empty($request->input('STREET_STRAH'))) {
            $arFildsCalc["Owner"]["address"]["street"] = $request->input('STREET_STRAH');
            $arFildsCalc["Insurer"]["address"]["street"] = $request->input('STREET_STRAH');
        }
        if (!empty($request->input('HOME_STRAH'))) {
            $arFildsCalc["Owner"]["address"]["home"] = $request->input('HOME_STRAH');
            $arFildsCalc["Insurer"]["address"]["home"] = $request->input('HOME_STRAH');
        }
        if (!empty($request->input('APARTAMENT_STRAH'))) {
            $arFildsCalc["Owner"]["address"]["apartament"] = $request->input('APARTAMENT_STRAH');
            $arFildsCalc["Insurer"]["address"]["apartament"] = $request->input('APARTAMENT_STRAH');
        }
        if (!empty($request->input('REGION_STRAH'))) {
            $arFildsCalc["Owner"]["address_full"] = $request->input('REGION_STRAH');
            $arFildsCalc["Insurer"]["address_full"] = $request->input('REGION_STRAH');
        }
        $arFildsCalc['auto_fild'] = array(
            'LicensePlate' => $regn,
            'VIN' => $vin,
            'Mark' => $mark,
            'Model' => $model,
            'YearIssue' => !empty($year_ts_ex[1]) ? $year_ts_ex[1] : $year_ts,
            'Category' => $category,
            'Type' => $type,
            'Power' => !empty($power_ex[1]) ? $power_ex[1] : $power,
            'DocCarSerial' => $seria_ts,
            'DocCarNumber' => $number_ts,
            'DocumentCarDate' => $datets_ts,
            'number_bodywork' => $number_bodywork,
            'number_chassis' => $number_chassis
        );

        $arDriv = array();
        if ($count_drive) {
            $person_panel = '';
            if ($request->input('SELECT_PERSON') > 0) {
                $prsonsFind = PeronsUser::find($request->input('SELECT_PERSON'));
                if ($prsonsFind) {
                    $srn = explode(' ', $prsonsFind->docser_numb);
                    $number_drive = $srn[1];
                    $seria_drive = $srn[0];
                    $lastName_fdriver = $prsonsFind->LastName;
                    $firstName_fdriver = $prsonsFind->FirstName;
                    $middleName_fdriver = $prsonsFind->MiddleName;
                    $birthDate_fdriver = $prsonsFind->BirthDate;
                    $expierienceDate_fdriver = $prsonsFind->ExperienceDate;
                    $person_panel = 'Y';
                }
            } else {
                $srn = explode(' ', $request->input('DRIVE_DOCUMENT_AUTO'));
                $number_drive = $srn[1];
                $seria_drive = $srn[0];
                /*$seria_drive = $request->input('DRIVE_DOCUMENT_AUTO');
                $number_drive = $request->input('DRIVE_DOCUMENT_AUTO2');*/
                $lastName_fdriver = $request->input('LAST_NAME_AUTO');
                $firstName_fdriver = $request->input('FIRST_NAME_AUTO');
                $middleName_fdriver = $request->input('MIDDLE_NAME_AUTO');
                $birthDate_fdriver = date("Y-m-d", strtotime($request->input('DATE_BIRTH_AUTO')));
                $expierienceDate_fdriver = date("Y-m-d", strtotime($request->input('DATE_DRIVE_DOCUMENT_AUTO')));
            }
            if ($count_drive == 1) {
                if ($lastName_fdriver)
                    $arFildsCalc["Drivers"]['Driver'][] = array(
                        'person_panel' => $person_panel,
                        'LastName' => $lastName_fdriver, // Фамилия водителя
                        'FirstName' => $firstName_fdriver, // Имя водителя
                        'MiddleName' => $middleName_fdriver, // Отчество водителя
                        'BirthDate' => $birthDate_fdriver, // Дата рождения водителя
                        'ExperienceDate' => $expierienceDate_fdriver, // Дата начала стажа вождения
                        'DriverDocument' => array( // Атрибуты по водительскому удостоверению ЛДУ
                            'Number' => $number_drive, // Номер водительского удостоверения ЛДУ
                            'Seria' => $seria_drive, // Серия водительского удостоверения ЛДУ
                            'DateIssue' => $expierienceDate_fdriver
                        ),
                    );
            } else {
                if ($lastName_fdriver)
                    $arFildsCalc["Drivers"]['Driver'][] = array(
                        'person_panel' => $person_panel,
                        'LastName' => $lastName_fdriver, // Фамилия водителя
                        'FirstName' => $firstName_fdriver, // Имя водителя
                        'MiddleName' => $middleName_fdriver, // Отчество водителя
                        'BirthDate' => $birthDate_fdriver, // Дата рождения водителя
                        'ExperienceDate' => $expierienceDate_fdriver, // Дата начала стажа вождения
                        'DriverDocument' => array( // Атрибуты по водительскому удостоверению ЛДУ
                            'Number' => $number_drive, // Номер водительского удостоверения ЛДУ
                            'Seria' => $seria_drive, // Серия водительского удостоверения ЛДУ
                            'DateIssue' => $expierienceDate_fdriver
                        ),
                    );
                for ($i = 2; $i <= $count_drive; $i++) {
                    $person_panel2 = '';
                    if ($request->input('SELECT_PERSON_' . $i) > 0) {
                        $prsonsFind2 = PeronsUser::find($request->input('SELECT_PERSON_' . $i));
                        if ($prsonsFind2) {
                            $srn2 = explode(' ', $prsonsFind2->docser_numb);
                            $number_ddrive = $srn2[1];
                            $seria_ddrive = $srn2[0];
                            $lastName_ddriver = $prsonsFind2->LastName;
                            $firstName_ddriver = $prsonsFind2->FirstName;
                            $middleName_ddriver = $prsonsFind2->MiddleName;
                            $birthDate_ddriver = $prsonsFind2->BirthDate;
                            $expierienceDate_ddriver = $prsonsFind2->ExperienceDate;
                            $person_panel2 = 'Y';
                        }
                    } else {
                        if (!$request->input('LAST_NAME_AUTO_' . $i)) continue;
                        $srn2 = explode(' ', $request->input('DRIVE_DOCUMENT_AUTO_' . $i));
                        $number_ddrive = $srn2[1];
                        $seria_ddrive = $srn2[0];
                        /*$seria_ddrive = $request->input('DRIVE_DOCUMENT_AUTO_'.$i);
                        $number_ddrive = $request->input('DRIVE_DOCUMENT_AUTO2_'.$i);*/
                        $lastName_ddriver = $request->input('LAST_NAME_AUTO_' . $i);
                        $firstName_ddriver = $request->input('FIRST_NAME_AUTO_' . $i);
                        $middleName_ddriver = $request->input('MIDDLE_NAME_AUTO_' . $i);
                        $birthDate_ddriver = date("Y-m-d", strtotime($request->input('DATE_BIRTH_AUTO_' . $i)));
                        $expierienceDate_ddriver = date("Y-m-d", strtotime($request->input('DATE_DRIVE_DOCUMENT_AUTO_' . $i)));
                    }
                    if ($lastName_ddriver)
                        $arFildsCalc["Drivers"]['Driver'][] = array(
                            'person_panel' => $person_panel2,
                            'LastName' => $lastName_ddriver, // Фамилия водителя
                            'FirstName' => $firstName_ddriver, // Имя водителя
                            'MiddleName' => $middleName_ddriver, // Отчество водителя
                            'BirthDate' => $birthDate_ddriver, // Дата рождения водителя
                            'ExperienceDate' => $expierienceDate_ddriver, // Дата начала стажа вождения
                            'DriverDocument' => array( // Атрибуты по водительскому удостоверению ЛДУ
                                'Number' => $number_ddrive, // Номер водительского удостоверения ЛДУ
                                'Seria' => $seria_ddrive, // Серия водительского удостоверения ЛДУ
                                'DateIssue' => $expierienceDate_ddriver
                            ),
                        );
                }
            }
            $arDriv = $arFildsCalc["Drivers"]['Driver'];
        }
        return array('filds' => $arFildsCalc, 'countdr' => $count_drive);
    }

    public function sectionStrah(Request $request)
    {
        $formInp = $this::inpForm($request);
        $arFildsCalc = $formInp["filds"];
        $count_drive = $formInp["countdr"];
        $slugElem = $request->input('slug');
        $inStrah = Allinsurance::getAllInsurance($slugElem);
        $arDrivTech = [];
        if ($inStrah) {
            $sum = 0;
            $kbm = 0;
            $Coefficients["Kbm"] = 0;
            $error = '<td></td>';
            switch ($inStrah->slug) {
                case 'alfa': // AlfaStrah
                    $PartnersInteraction = new PartnersInteraction();
                    $OsagoCalcRelaunch = new OsagoCalcRelaunch();
                    $upid = $PartnersInteraction->getUPID();
                    $arFildsCalc['UPID'] = $upid;
                    $arFildsCalc['type_calc'] = "min";
                    //$CalcEOsago = $OsagoCalcRelaunch->CalcEOsagoMin($arFildsCalc);
                    $CalcEOsago = $OsagoCalcRelaunch->PartnerCalcEOsago($arFildsCalc);
                    if ($CalcEOsago["success"]) {
                        $Coefficients = $CalcEOsago["info"]["Coefficients"];
                        if (!empty($count_drive) && $CalcEOsago["info"]["Drivers"]["Driver"]) {
                            if ($count_drive == 1) {
                                $arDrivTech[] = $CalcEOsago["info"]["Drivers"]["Driver"];
                            } else {
                                $arDrivTech = $CalcEOsago["info"]["Drivers"]["Driver"];
                            }
                        }
                        if ($CalcEOsago["info"]["InsuranceBonus"] > 0) {
                            $sum = $CalcEOsago["info"]["InsuranceBonus"];
                            $_SESSION["CalcRef"] = $CalcEOsago["info"]["RefCalcId"];
                        } else {
                            $_SESSION["CalcRef"] = '';
                        }
                    } else {
                        $_SESSION["CalcRef"] = '';
                        $error = '<td style="color:red;font-size: 15px;">' . $CalcEOsago["error"] . '</td>';
                    }
                    $arElemsCalcSrah = array(
                        'img' => Voyager::image($inStrah->img),
                        'Coefficients' => $Coefficients,
                        'rating' => $inStrah->rating,
                        'sum' => $sum,
                        'slug' => $inStrah->slug,
                        'error' => $error
                    );
                    break;
                case 'ingos': // Ingostrah
                    $Ingostrah = new IngostrahAllMethod();
                    $token = $Ingostrah->Login();
                    if ($token) {
                        $arFildsCalc["token"] = $token;
                        $GetTariffMin = $Ingostrah->GetTariffMin($arFildsCalc);
                        if ($GetTariffMin["success"]) {
                            $Coefficients = $GetTariffMin["Coefficients"];
                            $Coefficients["Kbm"] = $Coefficients["Kbm"] ?? 0;
                            $sum = $GetTariffMin["sum"] ?? 0;
                        } else {
                            $error = '<td style="color:red;font-size: 15px;">' . $GetTariffMin["error"] . '</td>';
                        }
                    } else {
                        $error = '<td style="color:red;font-size: 15px;">Cервис временно не доступен</td>';
                    }
                    $arElemsCalcSrah = array(
                        'img' => Voyager::image($inStrah->img),
                        'Coefficients' => $Coefficients,
                        'rating' => $inStrah->rating,
                        'sum' => $sum,
                        'slug' => $inStrah->slug,
                        'error' => $error
                    );
                    break;
                case 'renins':
                    $renins = new ReninsController();
                    $marks = $renins->marksInfo($arFildsCalc);
                    if ($marks["success"]) {
                        $calcRen = $renins->calculate($arFildsCalc, "min");
                        if ($calcRen["success"]) {
                            $Coefficients = $calcRen["info"]["Coefficients"];
                            $sum = $calcRen["info"]["sum"] ?? 0;
                        } else {
                            $error = '<td style="color:red;font-size: 15px;">' . $calcRen["error"] . '</td>';
                        }
                    } else {
                        $error = '<td style="color:red;font-size: 15px;">' . $marks["error"] . '</td>';
                    }
                    $arElemsCalcSrah = array(
                        'img' => Voyager::image($inStrah->img),
                        'Coefficients' => $Coefficients,
                        'rating' => $inStrah->rating,
                        'sum' => $sum,
                        'slug' => $inStrah->slug,
                        'error' => $error
                    );
                    break;
                case 'soglasie':
                    $soglasie = new SoglasieController();
                    $arFildsCalc["type_calc"] = 'min';
                    $calcSoglasie = $soglasie->calc($arFildsCalc);
                    $Coefficients = [];
                    if ($calcSoglasie["success"]) {
                        $Coefficients = $calcSoglasie["info"]["Coefficients"];
                        $sum = $calcSoglasie["info"]["sum"];
                    } else {
                        $error = $calcSoglasie["error"];
                    }
                    $arElemsCalcSrah = array(
                        'img' => Voyager::image($inStrah->img),
                        'Coefficients' => $Coefficients,
                        'rating' => $inStrah->rating,
                        'sum' => $sum,
                        'slug' => $inStrah->slug,
                        'error' => $error
                    );
                    break;
                case 'sk-maks':
                    $makc = new MakcController();
                    $calcMakc = $makc->minCalc($arFildsCalc);
                    $Coefficients = [];
                    if ($calcMakc["success"]) {
                        $Coefficients = $calcMakc["info"]["Coefficients"];
                        $sum = $calcMakc["info"]["sum"];
                    } else {
                        $error = $calcMakc["error"];
                    }
                    $arElemsCalcSrah = array(
                        'img' => Voyager::image($inStrah->img),
                        'Coefficients' => $Coefficients,
                        'rating' => $inStrah->rating,
                        'sum' => $sum,
                        'slug' => $inStrah->slug,
                        'error' => $error
                    );
                    break;
            }
        }
        if (!empty($arElemsCalcSrah)) {
            /*
            if(empty($arDrivTech) && !empty($arFildsCalc["Drivers"])){
                $arDrivTech = $arFildsCalc["Drivers"]["Driver"];
            }*/
            $thisyear = date('Y');
            $sumYear = $thisyear - $arFildsCalc["YearIssue"];
            return response()->json([
                'success' => true,
                'drivers' => $arDrivTech,
                'strahInfo' => $arElemsCalcSrah
            ]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function AllUpPrice(Request $request)
    {
        $formInp = $this::inpForm($request);
        $arFildsCalc = $formInp["filds"];
        $count_drive = $formInp["countdr"];

        $arElemsCalcSrah = array();
        $arDrivTech = array();
        $listStrah = Allinsurance::getAllInsurance();
        $inNull = '';
        foreach ($listStrah as $item) {
            $sum = 0;
            $kbm = 0;
            $error = '<td></td>';
            $arElemsCalcSrah[] = array(
                'img' => Voyager::image($item->img),
                'kbm' => $kbm,
                'rating' => $item->rating,
                'sum' => $sum,
                'slug' => $item->slug,
                'error' => $error,
                'api' => $item->api
            );
        }
        if (!empty($arElemsCalcSrah)) {
            $thisyear = date('Y');
            $sumYear = $thisyear - $arFildsCalc["YearIssue"];
            $drivers = [];
            if (!empty($arFildsCalc["Drivers"]['Driver'])) {
                $drivers = $arFildsCalc["Drivers"]['Driver'];
            }
            return response()->json([
                'success' => true,
                'drivers' => $drivers,
                'year_car' => $arFildsCalc["YearIssue"],
                'mark' => $arFildsCalc["Mark"],
                'model' => $arFildsCalc["Model"],
                'city' => $arFildsCalc["CityOfUse"],
                'power' => $arFildsCalc["EngCap"],
                'liststrah' => $arElemsCalcSrah,
                'date_start' => $arFildsCalc["PolicyBeginDate"],
                'date_end' => gmdate('d.m.Y', strtotime($arFildsCalc["PolicyBeginDate"] . ' + 12 month - 1 day')),
                'sumYear' => $sumYear
            ]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function onOrder(Request $request, $arFields = [])
    {
        $arFildsCalc = $this::inpForm($request)["filds"];
        $arFildsCalc["type_site"] = $request->input('type_site');
        $strahID = !empty($arFields["strahID"]) ? $arFields["strahID"] : $request->input('strahID');
        $policeID = !empty($arFields["policeID"]) ? $arFields["policeID"] : $request->input('policeID');
        $crossCheck = [];
        if (!empty($arFields["crossList"]))
            $crossCheck = $arFields["crossList"];

        $priceStrah = $request->input('priceStrah');
        $phone = preg_replace('/[^0-9]/', '', $arFildsCalc["phone"]);
        $mail = $arFildsCalc["email"];
        $auto = '';
        $order = '';
        $user = '';
        $persons = [];
        $arFildCreateAuto = [];
        $arFildCreatePersons = [];
        $arFulPers = [];
        if ($arFildsCalc["auto_fild"]["Mark"]) {
            $arFildCreateAuto = [
                'mark' => $arFildsCalc["auto_fild"]["Mark"],
                'model' => $arFildsCalc["auto_fild"]["Model"],
                'type_car' => $arFildsCalc["auto_fild"]["Type"],
                'category' => $arFildsCalc["auto_fild"]["Category"],
                'gos_reg' => $arFildsCalc["auto_fild"]["LicensePlate"],
                'vin' => $arFildsCalc["auto_fild"]["VIN"],
                'year' => $arFildsCalc["auto_fild"]["YearIssue"],
                'power' => $arFildsCalc["auto_fild"]["Power"],
                'number_bodywork' => $arFildsCalc["auto_fild"]["number_bodywork"],
                'number_chassis' => $arFildsCalc["auto_fild"]["number_chassis"]
            ];
            if ($request->input('doc_ts') == 'sts') {
                $exTsDoc = explode(' ', $request->input('SERIA_NUMBER_TS'));
                $arFildCreateAuto["seria_sts"] = $exTsDoc[0];
                $arFildCreateAuto["number_sts"] = $exTsDoc[1];
                $arFildCreateAuto["date_sts"] = date('Y-m-d', strtotime($arFildsCalc["auto_fild"]["DocumentCarDate"]));
            } else {
                $exTsDoc = explode(' ', $request->input('SERIA_NUMBER_TS'));
                $arFildCreateAuto["seria_pts"] = $exTsDoc[0];
                $arFildCreateAuto["number_pts"] = $exTsDoc[1];
                $arFildCreateAuto["date_pts"] = date('Y-m-d', strtotime($arFildsCalc["auto_fild"]["DocumentCarDate"]));
            }
        }
        if (!empty($arFildsCalc["Drivers"])) {
            $arFildCreatePersons["on_pers"] = [];
            $arFildCreatePersons["no_pers"] = [];
            foreach ($arFildsCalc["Drivers"]["Driver"] as $v) {
                if ($v["person_panel"]) {
                    $arFildCreatePersons["on_pers"][] = [
                        'LastName' => $v["LastName"],
                        'FirstName' => $v["FirstName"],
                        'MiddleName' => $v["MiddleName"],
                        'BirthDate' => date('Y-m-d', strtotime($v["BirthDate"])),
                        'ExperienceDate' => date('Y-m-d', strtotime($v["ExperienceDate"])),
                        'docser_numb' => $v["DriverDocument"]["Seria"] . ' ' . $v["DriverDocument"]["Number"]
                    ];
                } else {
                    $arFildCreatePersons["no_pers"][] = [
                        'LastName' => $v["LastName"],
                        'FirstName' => $v["FirstName"],
                        'MiddleName' => $v["MiddleName"],
                        'BirthDate' => date('Y-m-d', strtotime($v["BirthDate"])),
                        'ExperienceDate' => date('Y-m-d', strtotime($v["ExperienceDate"])),
                        'docser_numb' => $v["DriverDocument"]["Seria"] . ' ' . $v["DriverDocument"]["Number"]
                    ];
                }
            }
            $arFulPers = array_merge($arFildCreatePersons["no_pers"], $arFildCreatePersons["on_pers"]);
        }
        if ($arFildCreateAuto && $arFildsCalc["id_user"] > 0) { // если авторизован
            $userFind = User::find($arFildsCalc["id_user"]);
            $user = $userFind;
            if ($arFildsCalc["id_auto"] > 0) {
                $auto = $arFildsCalc["id_auto"];
            } else {
                $auto = $userFind->AutoUsers()->create($arFildCreateAuto);
            }
            if (!empty($arFildCreatePersons["no_pers"])) {
                foreach ($arFildCreatePersons["no_pers"] as $v) {
                    $id_pers = $userFind->PeronsUser()->create($v);
                    $persons[] = $id_pers->id;
                }
            }
        } elseif ($arFildCreateAuto && $mail) { // если без авторизации
            $userWhere = User::where('email', $mail)->first();
            $passwPers = $request->input('PASSWORD_PERSON');
            if ($userWhere && !empty($passwPers) && $passwPers != ' ' && $passwPers != '') { // проверка наличия пользователя
                if (\Hash::check($passwPers, $userWhere->password)) {
                    $user = $userWhere;
                    $auto = $userWhere->AutoUsers()->create($arFildCreateAuto);
                    foreach ($arFulPers as $v) {
                        $id_pers = $userWhere->PeronsUser()->create($v);
                        $persons[] = $id_pers->id;
                    }
                    $arFildsCalc["id_user"] = $user;
                } else {
                    return response()->json([
                        'success' => false,
                        'type' => 'pass',
                        'error' => 'Не верный пароль'
                    ]);
                }
            } elseif ($userWhere) {
                return response()->json([
                    'success' => false,
                    'type' => 'pass',
                    'error' => 'Такой пользователь существует<br/>Введите пароль'
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'type' => 'input',
                'error' => 'Введите верные данные'
            ]);
        }
        if (!$user && !empty($arFildsCalc)) {
            $motdepasse = str_random(8);
            $userCreate = User::create([
                'email' => $mail,
                'phone' => $phone,
                'name' => $arFildsCalc["Insurer"]["FirstName"],
                'password' => bcrypt($motdepasse)
            ]);
            $user = $userCreate;
            if ($user) {
                if ($arFildsCalc["type_site"] != 'polise-please') {
                    $mess = 'Вы зарегистрированы на сайте PitStop.' . PHP_EOL;
                    $mess .= 'Ваши данные для входа в личный кабинет: ' . PHP_EOL;
                    $mess .= 'Логин: ' . $mail . PHP_EOL;
                    $mess .= 'Пароль: ' . $motdepasse;
                    Mail::send(['raw' => $mess], ['mail' => $mail], function ($message) use ($mail) {
                        $message->from('noreply@pitstop-group.ru', 'Регистрация после оформления ОСАГО"');
                        $message->to($mail);//->cc('bar@example.com');
                        $message->subject('PitStop');
                    });
                }
                $auto = $userCreate->AutoUsers()->create($arFildCreateAuto);
                foreach ($arFulPers as $v) {
                    $id_pers = $userCreate->PeronsUser()->create($v);
                    $persons[] = $id_pers->id;
                }
                $arFildsCalc["id_user"] = $user;
            }
        }
        $autoOrder = new AutoOrder;
        $autoOrderFin = $autoOrder->create($arFildCreateAuto);
        if (!empty($autoOrderFin->id) && $auto && !empty($strahID) && $user) {
            if (!empty(Auth::user()->role_id) && (Auth::user()->role_id == 4 || Auth::user()->role_id == 1)) {
                $manager = Auth::user();
            } else {
                $manager = User::find(1);
            }
            $commission = '';
            $price_commission = 0;
            if (!empty($manager->id)) {
                if (!empty($manager->CommissionsCompany[0])) {
                    $commission = $manager->CommissionsCompany[0];
                    $commission_type = 0;
                    $commission_numb = 0;
                    if ($commission->check_commission == 1) {
                        $commission_type = $commission->type_commission;
                        $commission_numb = $commission->commission;
                    } elseif (!empty($arFildsCalc["RegionOfUse"])) {
                        if (!empty($commission->RegionsCommission)) {
                            foreach ($commission->RegionsCommission as $regionCommission) {
                                if (!strpos('----.' . strtolower($arFildsCalc["RegionOfUse"]), strtolower($regionCommission->name)) === false) {
                                    $commission_type = $regionCommission->pivot->type_commission_region;
                                    $commission_numb = $regionCommission->pivot->commission_region;
                                }
                            }
                        }
                    }
                    if ($commission_type == 0) {
                        $koef = (float)$priceStrah / 100;
                        $price_commission = $koef * (float)$commission_numb;
                    } else {
                        $price_commission = $commission_numb;
                    }
                }
            }

            $order = $user->Order()->create([
                'id_allinsuranses' => $strahID,
                'polis_number' => $policeID,
                'price' => $priceStrah,
                'mail' => $mail,
                'phone' => $phone,
                'id_auto' => $autoOrderFin->id,
                'RegionOfUse' => $arFildsCalc["RegionOfUse"],
                'CityOfUse' => $arFildsCalc["CityOfUse"],
                'DriversRestriction' => $arFildsCalc["DriversRestriction"],
                'LastName_Owner' => $arFildsCalc["Owner"]["LastName"],
                'FirstName_Owner' => $arFildsCalc["Owner"]["FirstName"],
                'MiddleName_Owner' => $arFildsCalc["Owner"]["MiddleName"],
                'BirthDate_Owner' => date('Y-m-d', strtotime($arFildsCalc["Owner"]["BirthDate_old"])),
                'Number_pass' => $arFildsCalc["Owner"]["Number"],
                'Seria_pass' => $arFildsCalc["Owner"]["Seria"],
                'LastName_Insurer' => $arFildsCalc["Insurer"]["LastName"],
                'FirstName_Insurer' => $arFildsCalc["Insurer"]["FirstName"],
                'MiddleName_Insurer' => $arFildsCalc["Insurer"]["MiddleName"],
                'BirthDate_Insurer' => date('Y-m-d', strtotime($arFildsCalc["Insurer"]["BirthDate_old"])),
                'Number_pas_Insurer' => $arFildsCalc["Insurer"]["Number"],
                'Seria_pas_Insurer' => $arFildsCalc["Insurer"]["Seria"],
                'Date_pass_Insurer' => date('Y-m-d', strtotime($arFildsCalc["Insurer"]["Date_pass"])),
                'Kod_doc_pass_Insurer' => $arFildsCalc["Insurer"]["Kod_doc_pass"],
                'For_doc_pass_Insurer' => $arFildsCalc["Insurer"]["For_doc_pass"],
                'region_insurer' => $arFildsCalc["Insurer"]["address"]["region"],
                'city_insurer' => !empty($arFildsCalc["Insurer"]["address"]["city"]) ? $arFildsCalc["Insurer"]["address"]["city"] : '',
                'street_insurer' => !empty($arFildsCalc["Insurer"]["address"]["street"]) ? $arFildsCalc["Insurer"]["address"]["street"] : '',
                'home_insurer' => !empty($arFildsCalc["Insurer"]["address"]["home"]) ? $arFildsCalc["Insurer"]["address"]["home"] : '',
                'building_insurer' => !empty($arFildsCalc["Insurer"]["address"]["building"]) ? $arFildsCalc["Insurer"]["address"]["building"] : '',
                'Apartment_doc_pass_Insurer' => !empty($arFildsCalc["Insurer"]["address"]["apartament"]) ? $arFildsCalc["Insurer"]["address"]["apartament"] : '',
                'date_start' => date('Y-m-d', strtotime($arFildsCalc["PolicyBeginDate"])),
                'kv' => !empty($arFields['kv']) ? $arFields['kv'] : 0,
                'manager_id' => !empty($manager->id) ? $manager->id : false,
                'commission_id' => !empty($commission->id) ? $commission->id : false,
                'price_commission' => $price_commission
            ]);
            if ((!empty($persons) || !empty($arFulPers)) && $order) {
                foreach ($arFulPers as $v) {
                    $order->PeronsOrder()->create($v);
                }
            }
            if (!empty($crossCheck)) {
                foreach ($crossCheck as $cross) {
                    $order->CrossOrder()->create([
                        "name" => $cross["name"],
                        "id_allinsurances" => $strahID,
                        "number_cross" => $cross["id"],
                        "price" => $cross["price"]
                    ]);
                }
            }
        }

        if (!empty($order)) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function CrossOsagoFunctions(Request $request)
    {
        $slug = $request->input('slug');
        $crossInfo = $request->input('crossInfo');
        $crossInfoParse = json_decode($crossInfo, true);
        $strahInfo = $request->input('strahInfo');
        $crossCheck = $request->input('crossList');
        $crossListParse = json_decode($crossCheck, true);
        $strahInfoParse = json_decode($strahInfo, true);
        $listStrah = Allinsurance::getAllInsurance($slug);
        if (!empty($listStrah->slug)) {
            switch ($listStrah->slug) {
                case 'ingos':
                    $Ingostrah = new IngostrahAllMethod();
                    if (!empty($crossListParse)) {
                        $crossOn = [];
                        foreach ($crossListParse as $cross) {
                            $arFields = [
                                "AgrISN" => $crossInfoParse['crossPolice'],
                                "token" => $crossInfoParse['token'],
                                "id" => $cross["id"],
                                "name" => $cross["name"],
                                "price" => $cross["price"]
                            ];
                            $crossIngosSave = $Ingostrah->CreateCrossAgreementRequestIngos($arFields);
                            $agrArray = [];
                            if ($crossIngosSave["success"]) {
                                $crossOn[] = $crossIngosSave;
                                $agrArray[] = $crossIngosSave["AgrID"];
                            }
                        }
                        $agrArray[] = $strahInfoParse["AgrID"];
                        if (!empty($crossOn[0])) {
                            $strahInfoParse["AgrISN"] = $crossOn[count($crossOn) - 1]["AgrISN"];
                            $strahInfoParse["AgrID"] = $agrArray;//$crossOn[count($crossOn) - 1]["AgrID"];
                            $CreateBill = $Ingostrah->CreateBill($strahInfoParse);
                            if ($CreateBill["success"]) {
                                $strahInfoParse["BillISN"] = $CreateBill["BillISN"];
                                $CreateOnlineBill = $Ingostrah->CreateOnlineBill($strahInfoParse);
                                if ($CreateOnlineBill["success"]) {
                                    $strahInfoParse["policeID"] = $strahInfoParse[count($strahInfoParse) - 1]["AgrID"];
                                    $strahInfoParse["strahID"] = $listStrah->id;
                                    $strahInfoParse["crossList"] = $crossOn;
                                    $this->onOrder($request, $strahInfoParse);
                                    return response()->json([
                                        'success' => true,
                                        'urlp' => !empty($CreateOnlineBill["urlPay"]) ? $CreateOnlineBill["urlPay"] : '',
                                        'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . !empty($CreateOnlineBill["urlPay"]) ? $CreateOnlineBill["urlPay"] : '',
                                        'policeID' => $strahInfoParse["policeID"],
                                        'cross_good' => $crossOn,
                                        'dopID' => "",
                                        'strahID' => $listStrah->id,
                                    ]);
                                }
                            } else {
                                return response()->json([
                                    'success' => false,
                                    'method' => 'CreateBill',
                                    'error' => $CreateBill["error"]
                                ]);
                            }
                        } else {
                            return response()->json([
                                'success' => false,
                                'method' => 'saveCross',
                                'error' => 'Кросс предложения не прошли, попробуйте выбрать другие или перейти к оплате'
                            ]);
                        }
                    } else {
                        $CreateBill = $Ingostrah->CreateBill($strahInfoParse);
                        if ($CreateBill["success"]) {
                            $strahInfoParse["BillISN"] = $CreateBill["BillISN"];
                            $CreateOnlineBill = $Ingostrah->CreateOnlineBill($strahInfoParse);
                            if ($CreateOnlineBill["success"]) {
                                $strahInfoParse["policeID"] = $strahInfoParse["AgrID"];
                                $this->onOrder($request, $strahInfoParse);
                                return response()->json([
                                    'success' => true,
                                    'urlp' => !empty($CreateOnlineBill["urlPay"]) ? $CreateOnlineBill["urlPay"] : '',
                                    'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . !empty($CreateOnlineBill["urlPay"]) ? $CreateOnlineBill["urlPay"] : '',
                                    'policeID' => $strahInfoParse["AgrID"],
                                    'dopID' => "",
                                    'strahID' => $listStrah->id,
                                ]);
                            }
                        } else {
                            return response()->json([
                                'success' => false,
                                'method' => 'CreateBill',
                                'error' => $CreateBill["error"]
                            ]);
                        }
                    }
                    break;
                case 'alfa':
                    $MerchantServices = new MerchantServices;
                    if (!empty($crossListParse)) {
                        $cross = new Cross;
                        $crossSave = $cross->saveCross([
                            "crossList" => $crossListParse,
                            "contractId" => $strahInfoParse["contract_number"],
                            "calculationRequestId" => $crossInfoParse["calculationRequestId"],
                            "strahFields" => $strahInfoParse,
                        ]);
                        if ($crossSave["success"]) {
                            $statusCrosses = $cross->crossStatus($crossSave["saveID"]);
                            if ($statusCrosses["success"]) {
                                $crossOn = [];
                                foreach ($crossListParse as $key => $crossCheckCalc) {
                                    if (!empty($statusCrosses["cross_good_status"][$crossCheckCalc["id"]])) {
                                        $crossCheckCalc["contractId"] = $statusCrosses["cross_good_status"][$crossCheckCalc["id"]]["contractId"];
                                        $crossOn[] = $crossCheckCalc;
                                    }
                                }
                                $paymentCross = new PaymentCross;
                                $payReceipt = $paymentCross->payReceipt($strahInfoParse["merchantOrderNumber"], $crossOn);
                                if ($payReceipt["success"]) {
                                    $strahInfoParse["merchantOrderNumber"] = $payReceipt["infoReceipt"]["id"];
                                    $strahInfoParse["contract_number"] = $payReceipt["infoReceipt"]["number"];
                                    $pay = $MerchantServices->pay($strahInfoParse);
                                    if ($pay["url"]) {
                                        $strahInfoParse["crossList"] = $crossOn;
                                        $strahInfoParse["policeID"] = $strahInfoParse["contract_number"];
                                        $this->onOrder($request, $strahInfoParse);
                                        return response()->json([
                                            'success' => true,
                                            'urlp' => !empty($pay["url"]) ? $pay["url"] : '',
                                            'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $pay["url"],
                                            'policeID' => $strahInfoParse["contract_number"],
                                            'dopID' => !empty($pay["upid"]) ? $pay["upid"] : '',
                                            'cross_good' => $crossOn,
                                            'strahID' => $listStrah->id,
                                        ]);
                                    } else {
                                        return response()->json([
                                            'success' => false,
                                            'method' => 'url'
                                        ]);
                                    }
                                }
                            } else {
                                return response()->json([
                                    'success' => false,
                                    'method' => 'statusCross'
                                ]);
                            }
                        } else {
                            return response()->json([
                                'success' => false,
                                'method' => 'saveCross',
                                'error' => 'Кросс предложения не прошли, попробуйте выбрать другие или перейти к оплате'
                            ]);
                        }
                    } else {
                        $pay = $MerchantServices->pay($strahInfoParse);
                        if ($pay["url"]) {
                            $strahInfoParse["policeID"] = $strahInfoParse["contract_number"];
                            $this->onOrder($request, $strahInfoParse);
                            return response()->json([
                                'success' => true,
                                'urlp' => !empty($pay["url"]) ? $pay["url"] : '',
                                'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $pay["url"],
                                'policeID' => $strahInfoParse["contract_number"],
                                'dopID' => !empty($pay["upid"]) ? $pay["upid"] : '',
                                'strahID' => $listStrah->id,
                            ]);
                        }
                    }
                    break;
            }
            /*if (!empty($crossOn)) {
                if ($crossOn["success"]) {
                    $this->onOrder($request);
                } else {
                    return response()->json($crossOn);
                }
            }*/
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function sendCalc(Request $request)
    {
        $slug = $request->input('slug');
        if ($slug) {
            $arFildsCalc = $this::inpForm($request)["filds"];
            $arFildsCalc["type_site"] = $request->input('type_site');
            $count_drive = $this::inpForm($request)["countdr"];
            $listStrah = Allinsurance::getAllInsurance($slug);
            /*return response()->json([
                'success' => true,
                'urlp' => '/',
                'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=',
                'policeID' => 1111,
                'dopID' => 22222,
                'strahID' => 1,
            ]);*/
            if ($listStrah->slug) {
                //$login = $listStrah->login;
                //$pass = $listStrah->password;
                if (!empty($arFildsCalc)) {
                    switch ($listStrah->slug) {
                        case 'alfa':
                            $PartnersInteraction = new PartnersInteraction();
                            $OsagoCalcRelaunch = new OsagoCalcRelaunch();
                            $upid = $PartnersInteraction->getUPID();
                            if ($upid) {
                                $arFildsCalc['UPID'] = $upid;
                                $arFildsCalc['type_calc'] = "big";
                                //$CalcEOsago = $OsagoCalcRelaunch->CalcEOsago($arFildsCalc);
                                $CalcEOsago = $OsagoCalcRelaunch->PartnerCalcEOsago($arFildsCalc);
                                if ($CalcEOsago["success"]) {
                                    $arAlfa = $arFildsCalc;
                                    if (!empty($CalcEOsago["info"]["kv"]))
                                        $arAlfa["kv"] = $CalcEOsago["info"]["kv"];
                                    $EOsagoRelaunch = new EOsagoRelaunch();

                                    $loadVehicle = $EOsagoRelaunch->loadVehicle($arAlfa);
                                    $arAlfa["IDCheckTS"] = $loadVehicle["IDCheckTS"];

                                    $arAlfa["type"] = 'Owner';
                                    $Owner = $EOsagoRelaunch->loadInsurerOwner($arAlfa);
                                    if (!empty($Owner["IDCheckInsurerOwner"]))
                                        $arAlfa["Owner"]["IDCheckInsurerOwner"] = $Owner["IDCheckInsurerOwner"];

                                    $arAlfa["type"] = 'Insurer';
                                    $Insurer = $EOsagoRelaunch->loadInsurerOwner($arAlfa);
                                    if (!empty($Insurer["IDCheckInsurerOwner"]))
                                        $arAlfa["Insurer"]["IDCheckInsurerOwner"] = $Insurer["IDCheckInsurerOwner"];

                                    if ($Owner["success"] && $Insurer["success"] && !empty($Insurer["IDCheckInsurerOwner"]) && !empty($Owner["IDCheckInsurerOwner"])) {
                                        $arAlfa["CalcRef"] = $CalcEOsago["info"]["RefCalcId"];

                                        if (!empty($count_drive)) {
                                            foreach ($arAlfa["Drivers"]['Driver'] as $driver) {
                                                $drivInfo = $EOsagoRelaunch->loadDriver($driver);
                                                if (!empty($drivInfo["IDCheckDriver"])) {
                                                    $driver["IDCheckDriver"] = $drivInfo["IDCheckDriver"];
                                                    $arAlfa["DriversChck"]['Driver'][] = $driver;
                                                }
                                            }
                                        }
                                        $contract = $EOsagoRelaunch->loadContractEosago($arAlfa);
                                        if ($contract["success"]) {
                                            $MerchantServices = new MerchantServices;
                                            //$arAlfa['login'] = $login;
                                            $arAlfa['contract_number'] = $contract["ns2ContractNumber"];
                                            $login = $MerchantServices->onLogin($arAlfa);
                                            if ($login) {
                                                $arAlfa["merchantOrderNumber"] = $contract["ns2ContractId"];
                                                //if ($this->env_type == 'local') {
                                                $cross = new Cross;
                                                $getCross = $cross->getCross($arAlfa);
                                                //}
                                                $arrCross = [];
                                                $onCalc = '';
                                                $calculationRequestId = '';
                                                if (!empty($getCross["success"]) && !empty($getCross["list"])) {
                                                    $arrCross = $getCross["list"];
                                                    $calculationRequestId = $getCross["calculationRequestId"];
                                                    $onCalc = 'Y';
                                                } else {
                                                    $pay = $MerchantServices->pay($arAlfa);
                                                    if ($pay["url"]) {
                                                        $onCalc = 'Y';
                                                    }
                                                }
                                                if ($onCalc) {
                                                    $urlPay = '';
                                                    $dopID = $arAlfa["UPID"];
                                                    if (!empty($pay["url"])) {
                                                        $urlPay = $pay["url"];
                                                    }
                                                    if (!empty($pay["upid"])) {
                                                        $dopID = $pay["upid"];
                                                    }
                                                    return response()->json([
                                                        'success' => true,
                                                        'urlp' => $urlPay,
                                                        'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $urlPay,
                                                        'policeID' => $arAlfa["contract_number"],
                                                        'dopID' => $dopID,
                                                        'crossInfo' => [
                                                            'arrayCross' => $arrCross,
                                                            'calculationRequestId' => $calculationRequestId,
                                                            'merchantOrderNumber' => $arAlfa["merchantOrderNumber"],
                                                            'contract_number' => $arAlfa["contract_number"],
                                                            'id_user' => $arAlfa["id_user"],
                                                            'upid' => $dopID
                                                        ],
                                                        'strahInfo' => $arAlfa,
                                                        'strahID' => $listStrah->id,
                                                        'price' => $CalcEOsago["info"]["InsuranceBonus"]
                                                    ]);
                                                }
                                            }
                                        } else {
                                            return response()->json([
                                                'success' => false,
                                                'method' => 'loadContractEosago',
                                                'error' => $contract["error"]
                                            ]);
                                        }
                                    }
                                } else {
                                    return response()->json([
                                        'success' => false,
                                        'method' => 'PartnerCalcEOsago',
                                        'error' => 'Сервис временно не доступен'
                                    ]);
                                }
                            } else {
                                return response()->json([
                                    'success' => false,
                                    'method' => 'UPID',
                                    'error' => 'Сервис временно не доступен'
                                ]);
                            }
                            break;
                        case 'ingos': // Ingostrah
                            $Ingostrah = new IngostrahAllMethod();
                            $token = $Ingostrah->Login();
                            if ($token) {
                                $arEditIngos = $arFildsCalc;
                                $arEditIngos["token"] = $token;
                                $GetTariff = $Ingostrah->GetTariff($arEditIngos);
                                if ($GetTariff["success"]) {
                                    $CreateAgreement = $Ingostrah->CreateAgreement($arEditIngos);
                                    if ($CreateAgreement["success"]) {
                                        $arEditIngos["AgrID"] = $CreateAgreement["AgrID"];
                                        $GetAgreementRequest = $Ingostrah->GetAgreementRequest($arEditIngos);
                                        if ($GetAgreementRequest["success"]) {
                                            $arEditIngos["AgrISN"] = $GetAgreementRequest["General"]["ISN"];
                                            $MakeEOsago = $Ingostrah->MakeEOsago($arEditIngos);
                                            if ($MakeEOsago["success"]) {
                                                //if ($this->env_type == 'local') { //кроссы
                                                $getCross = $Ingostrah->CrossProductListRequest($arEditIngos);
                                                //}
                                                $onCalc = '';
                                                $arrCross = [];
                                                if (!empty($getCross["success"]) && !empty($getCross["list"])) {
                                                    $arrCross = $getCross["list"];
                                                    $onCalc = 'Y';
                                                } else {
                                                    $CreateBill = $Ingostrah->CreateBill($arEditIngos);
                                                    if ($CreateBill["success"]) {
                                                        $arEditIngos["BillISN"] = $CreateBill["BillISN"];
                                                        $CreateOnlineBill = $Ingostrah->CreateOnlineBill($arEditIngos);
                                                        if ($CreateOnlineBill["success"]) {
                                                            $onCalc = 'Y';
                                                        }
                                                    } else {
                                                        return response()->json([
                                                            'success' => false,
                                                            'method' => 'CreateBill',
                                                            'error' => $CreateBill["error"]
                                                        ]);
                                                    }
                                                }
                                                if ($onCalc) {
                                                    $urlPay = '';
                                                    if (!empty($CreateOnlineBill["urlPay"])) {
                                                        $urlPay = $CreateOnlineBill["urlPay"];
                                                    }
                                                    return response()->json([
                                                        'success' => true,
                                                        'urlp' => $urlPay,
                                                        'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $urlPay,
                                                        'policeID' => $arEditIngos["AgrID"],
                                                        'crossInfo' => [
                                                            'crossPolice' => $arEditIngos["AgrISN"],
                                                            'token' => $arEditIngos["token"],
                                                            'arrayCross' => $arrCross
                                                        ],
                                                        'dopID' => "",
                                                        'strahInfo' => $arEditIngos,
                                                        'strahID' => $listStrah->id,
                                                        'price' => $GetTariff["sum"]
                                                    ]);
                                                } else {
                                                    return response()->json([
                                                        'success' => false,
                                                        'method' => 'CreateOnlineBill'
                                                    ]);
                                                }
                                            } else {
                                                return response()->json([
                                                    'success' => false,
                                                    'method' => 'MakeEOsago',
                                                    'error' => $MakeEOsago["error"]
                                                ]);
                                            }
                                        } else {
                                            return response()->json([
                                                'success' => false,
                                                'method' => 'GetAgreementRequest',
                                                'error' => $GetAgreementRequest["error"]
                                            ]);
                                        }
                                    } else {
                                        return response()->json([
                                            'success' => false,
                                            'method' => 'CreateAgreement',
                                            'error' => $CreateAgreement["error"]
                                        ]);
                                    }
                                } else {
                                    return response()->json([
                                        'success' => false,
                                        'method' => 'GetTariff',
                                        'error' => $GetTariff["error"]
                                    ]);
                                }
                            }
                            break;
                        case 'renins':
                            $renins = new ReninsController();
                            $onContract = $renins->runCalc($arFildsCalc);
                            if ($onContract["success"]) {
                                if ($onContract["url"]) {
                                    return response()->json([
                                        'success' => true,
                                        'urlp' => $onContract["url"],
                                        'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $onContract["url"],
                                        'policeID' => $onContract["policeID"],
                                        'dopID' => "",
                                        'crossInfo' => [
                                            'arrayCross' => [],
                                        ],
                                        'strahID' => $listStrah->id,
                                        'price' => $onContract["sum"]
                                    ]);
                                } else {
                                    return response()->json($onContract);
                                }
                            } else {
                                return response()->json($onContract);
                            }
                            break;
                        case 'soglasie':
                            $soglasie = new SoglasieController();
                            $arFildsCalc["type_calc"] = 'big';
                            $calcSoglasie = $soglasie->calc($arFildsCalc);
                            if ($calcSoglasie["success"]) {
                                return response()->json([
                                    'success' => true,
                                    'urlp' => $calcSoglasie["url"],
                                    'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $calcSoglasie["url"],
                                    'policeID' => $calcSoglasie["policyId"],
                                    'dopID' => "",
                                    'crossInfo' => [
                                        'arrayCross' => [],
                                    ],
                                    'strahID' => $listStrah->id,
                                    'price' => $calcSoglasie["sum"]
                                ]);
                            } else {
                                return response()->json($calcSoglasie);
                            }
                            break;
                        case 'sk-maks':
                            $makc = new MakcController();
                            $calcMakc = $makc->finalCalc($arFildsCalc);
                            if ($calcMakc["success"]) {
                                return response()->json([
                                    'success' => true,
                                    'urlp' => $calcMakc["url"],
                                    'qr' => 'https://api.qrserver.com/v1/create-qr-code/?size=116x116&data=' . $calcMakc["url"],
                                    'policeID' => $calcMakc["policyId"],
                                    'dopID' => "",
                                    'crossInfo' => [
                                        'arrayCross' => [],
                                    ],
                                    'strahID' => $listStrah->id,
                                    'price' => $calcMakc["sum"]
                                ]);
                            } else {
                                return response()->json($calcMakc);
                            }
                            break;
                    }
                }
            }
        }
    }

    public function isCodePhone(Request $request)
    {
        if ($this->env_type == 'local') {
            return response()->json([
                "success" => true
            ]);
        } else {
            $code = (int)$request->input('confirmCode');
            $hash = $request->input('jedj');
            if (!empty($code) && !empty($hash)) {
                if ($hash != md5(base64_encode($code))) {
                    return response()->json([
                        "success" => false,
                        "id_elem" => [
                            "confirmCode"
                        ],
                        "error" => "Не верный код"
                    ]);
                } else {
                    return response()->json([
                        "success" => true
                    ]);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "id_elem" => [
                        "confirmCode"
                    ],
                    "error" => "Не верный код"
                ]);
            }
        }
    }

    public function sendSmsCode(Request $request)
    {
        $arFildsCalc = $this::inpForm($request)["filds"];
        if ($arFildsCalc["id_user"] <= 0 && !empty($arFildsCalc["email"])) { // проверка пользователя
            $userWhere = User::where('email', $arFildsCalc["email"])->first();
            $passwPers = $request->input('PASSWORD_PERSON');
            if ($userWhere && !empty($passwPers) && $passwPers != ' ' && $passwPers != '') { // проверка наличия пользователя
                if (\Hash::check($passwPers, $userWhere->password) == false) {
                    return response()->json([
                        'success' => false,
                        'type' => 'pass',
                        'error' => 'Не верный пароль'
                    ]);
                } else {
                    Auth::login($userWhere);
                }
            } elseif ($userWhere) {
                return response()->json([
                    'success' => false,
                    'type' => 'pass',
                    'error' => 'Такой пользователь существует. Введите пароль'
                ]);
            }
        } elseif (empty($arFildsCalc["email"])) {
            return response()->json([
                'success' => false,
                'type' => 'input',
                'error' => 'Введите верные данные'
            ]);
        }

        if ($this->env_type == 'local') {
            return response()->json([
                "success" => true,
                "jedj" => 'test',
                "phone" => $request->input('PHONE_PERSON')
            ]);
        } else {
            $vinError = false; // проверка vin
            if (!empty($arFildsCalc["auto_fild"]["VIN"])) {
                $this_auto = AutoOrder::isVin($arFildsCalc["auto_fild"]["VIN"]);
                foreach ($this_auto as $autoOrder) {
                    if ($autoOrder->vin == $arFildsCalc["auto_fild"]["VIN"]) {
                        if ($autoOrder->created_at->format('m.Y') == date('m.Y')) {
                            $vinError = true;
                        }
                    }
                }
            }
            if ($vinError)
                return response()->json([
                    'success' => false,
                    'type' => 'vin',
                    'error' => 'VIN - повторяется более чем в одном оформленном полисе в отчетном месяце'
                ]);
            $phone = preg_replace('/[^0-9]/', '', $request->input('PHONE_PERSON'));
            if (!empty($phone)) {
                $code = rand(1000, 9999);
                $sms = new SmsRu();
                $resul = $sms->sendSms([
                    "phone" => $phone,
                    "text" => "Ваш код подтвержения:" . $code,
                ]);
                if ($resul["success"]) {
                    return response()->json([
                        "success" => true,
                        "jedj" => md5(base64_encode($code)),
                        "phone" => $request->input('PHONE_PERSON')
                    ]);
                } else {
                    return response()->json([
                        "success" => false,
                        "error" => "Ошибка отправки сообщения",
                        "info" => $resul
                    ]);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "id_elem" => [
                        "PHONE_PERSON"
                    ],
                    "error" => "Введите корректный телефон"
                ]);
            }
        }
    }

    public function calc_doc(Request $request) // Документы ГБДД
    {
        $error = '';
        $mess = '';
        $arFild = [];
        /* proc */
        if ($request->input('horns')) {
            $mess .= 'Покупатель: Юридическое лицо' . PHP_EOL;
            $arFild["buyer"]["yur_fiz"] = true;
        } else {
            $arFild["buyer"]["yur_fiz"] = false;
            $mess .= 'Покупатель: Физическое лицо' . PHP_EOL;
        }
        $arFizReq = array(
            "head" => "Данные покупателя:",
            "LAST_NAME" => "Фамилия",
            "FIRST_NAME" => "Имя",
            "MIDDLE_NAME" => "Отчество",
            "DATE_BIRTH" => "Дата рождения",
            "PLACE_BIRTH" => "Место рождения",
            "PASSPORT_SERIAL" => "Серия",
            "PASSPORT_NUMBER" => "Номер",
            "PASSPORT_DATE" => "Дата выдачи",
            "PASSPORT_WHERE" => "Кем и когда выдан",
            "PASSPORT_PATH_ADDRESS" => "Место жительства",
            /*"PASSPORT_PATH_CITY" => "Населенный пункт",
            "PASSPORT_PATH_STREET" => "Улица",
            "PASSPORT_PATH_NUMBER" => "Номер дома",*/
            "NATIONALITY" => "Гражданство",
            "SEX" => "Пол"//,
            //"INN" => "ИНН"
        );
        $arYurReq = array(
            "head" => "Данные покупателя:",
            "LEGAL_NAME" => "Наименование",
            "LEGAL_DATE" => "Дата регистрации",
            "LEGAL_INN" => "ИНН",
            "LEGAL_ADDRESS" => "Адрес регистрации",
            "LEGAL_TEL" => "Телефон"
        );
        $arFildsReqDop = array(
            "ADDITIONAL_LAST_NAME" => "Фамилия",
            "ADDITIONAL_FIRST_NAME" => "Имя",
            "ADDITIONAL_MIDDLE_NAME" => "Отчество",
            "ADDITIONAL_DATE_BIRTH" => "Дата рождения",
            "ADDITIONAL_PLACE_BIRTH" => "Место рождения",
            "ADDITIONAL_PASSPORT_SERIAL" => "Серия",
            "ADDITIONAL_PASSPORT_NUMBER" => "Номер",
            "ADDITIONAL_PASSPORT_DATE" => "Дата выдачи",
            "ADDITIONAL_PASSPORT_WHERE" => "Кем и когда выдан",
            "ADDITIONAL_PASSPORT_PATH_ADDRESS" => "Место жительства",
            /*"ADDITIONAL_PASSPORT_PATH_CITY" => "Населенный пункт",
            "ADDITIONAL_PASSPORT_PATH_STREET" => "Улица",
            "ADDITIONAL_PASSPORT_PATH_NUMBER" => "Номер дома", */
            "ADDITIONAL_NATIONALITY" => "Гражданство",
            "ADDITIONAL_SEX" => "Пол'"//,
            //"ADDITIONAL_INN" => "ИНН"
        );
        if ($request->input('LAST_NAME')) {
            foreach ($arFizReq as $key => $name) {
                $value = $request->input($key);
                if ($value == '' && $key != 'head') {
                    $error = 'Y';
                } elseif ($key == 'head') {
                    $mess .= PHP_EOL . '---' . $name . '---' . PHP_EOL;
                } else {
                    $arFild["buyer"][$key] = $value;
                    $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
                }
            }
        } elseif ($request->input('LEGAL_NAME')) {
            foreach ($arYurReq as $key => $name) {
                $value = $request->input($key);
                if ($value == '' && $key != 'head') {
                    $error = 'Y';
                } elseif ($key == 'head') {
                    $mess .= PHP_EOL . '---' . $name . '---' . PHP_EOL;
                } else {
                    $arFild["buyer"][$key] = $value;
                    $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
                }
            }
        } else {
            $error = 'Y';
        }
        if ($request->input("ADDITIONAL_LAST_NAME")) {
            $mess .= PHP_EOL . '---Представитель у покупателя---' . PHP_EOL;
            foreach ($arFildsReqDop as $key => $name) {
                $value = $request->input($key);
                $arFild["buyer"][$key] = $value;
                if ($value != '') {
                    $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
                }
            }
        }
        /* prod */
        if ($request->input('S_horns')) {
            $mess .= PHP_EOL . 'Продавец: Юридическое лицо' . PHP_EOL;
            $arFild["seller"]["yur_fiz"] = true;
        } else {
            $arFild["seller"]["yur_fiz"] = false;
            $mess .= PHP_EOL . 'Продавец: Физическое лицо' . PHP_EOL;
        }
        $arFizReq2 = array(
            "head" => "Данные продавца:",
            "S_LAST_NAME" => "Фамилия",
            "S_FIRST_NAME" => "Имя",
            "S_MIDDLE_NAME" => "Отчество",
            "S_DATE_BIRTH" => "Дата рождения",
            "S_PLACE_BIRTH" => "Место рождения",
            "S_PASSPORT_SERIAL" => "Серия",
            "S_PASSPORT_NUMBER" => "Номер",
            "S_PASSPORT_DATE" => "Дата выдачи",
            "S_PASSPORT_WHERE" => "Кем и когда выдан",
            "S_PASSPORT_PATH_ADDRESS" => "Место жительства",
            /*"S_PASSPORT_PATH_CITY" => "Населенный пункт",
            "S_PASSPORT_PATH_STREET" => "Улица",
            "S_PASSPORT_PATH_NUMBER" => "Номер дома",*/
            "S_NATIONALITY" => "Гражданство",
            "S_SEX" => "Пол'",
            //"S_INN" => "ИНН'"
        );
        $arYurReq2 = array(
            "head" => "Данные продавца:",
            "S_LEGAL_NAME" => "Наименование",
            "S_LEGAL_DATE" => "Дата регистрации",
            "S_LEGAL_INN" => "ИНН",
            "S_LEGAL_ADDRESS" => "Адрес регистрации",
            "S_LEGAL_TEL" => "Телефон"
        );
        $arFildsReqDop2 = array(
            "S_ADDITIONAL_LAST_NAME" => "Фамилия",
            "S_ADDITIONAL_FIRST_NAME" => "Имя",
            "S_ADDITIONAL_MIDDLE_NAME" => "Отчество",
            "S_ADDITIONAL_DATE_BIRTH" => "Дата рождения",
            "S_ADDITIONAL_PLACE_BIRTH" => "Место рождения",
            "S_ADDITIONAL_PASSPORT_SERIAL" => "Серия",
            "S_ADDITIONAL_PASSPORT_NUMBER" => "Номер",
            "S_ADDITIONAL_PASSPORT_DATE" => "Дата выдачи",
            "S_ADDITIONAL_PASSPORT_WHERE" => "Кем и когда выдан",
            "S_ADDITIONAL_PASSPORT_PATH_ADDRESS" => "Место жительства",
            /*"S_ADDITIONAL_PASSPORT_PATH_CITY" => "Населенный пункт",
            "S_ADDITIONAL_PASSPORT_PATH_STREET" => "Улица",
            "S_ADDITIONAL_PASSPORT_PATH_NUMBER" => "Номер дома", */
            "S_ADDITIONAL_NATIONALITY" => "Гражданство",
            "S_ADDITIONAL_SEX" => "Пол'",
            //"S_ADDITIONAL_INN" => "ИНН"
        );
        if ($request->input('S_LAST_NAME')) {
            foreach ($arFizReq2 as $key => $name) {
                $value = $request->input($key);
                if ($value == '' && $key != 'head') {
                    $error = 'Y';
                } elseif ($key == 'head') {
                    $mess .= PHP_EOL . '---' . $name . '---' . PHP_EOL;
                } else {
                    $arFild["seller"][$key] = $value;
                    $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
                }
            }
        } elseif ($request->input('S_LEGAL_NAME')) {
            foreach ($arYurReq2 as $key => $name) {
                $value = $request->input($key);
                if ($value == '' && $key != 'head') {
                    $error = 'Y';
                } elseif ($key == 'head') {
                    $mess .= PHP_EOL . '---' . $name . '---' . PHP_EOL;
                } else {
                    $arFild["seller"][$key] = $value;
                    $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
                }
            }
        } else {
            $error = 'Y';
        }
        if ($request->input("S_ADDITIONAL_LAST_NAME")) {
            $mess .= PHP_EOL . '---Представитель у продавеца---' . PHP_EOL;
            foreach ($arFildsReqDop2 as $key => $name) {
                $value = $request->input($key);
                $arFild["seller"][$key] = $value;
                if ($value != '') {
                    $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
                }
            }
        }
        /* auto */
        $arFildsReqAuto = array(
            "head" => "Данные автомобиля:",
            "AUTO_MAKE" => "Марка автомобиля",
            "FIRST_MODEL" => "Модель",
            "AUTO_TYPE" => "Тип транспортного средства",
            //"AUTO_NUMBER" => "Гос. рег. знак",
            "AUTO_VIN" => "VIN номер",
            "AUTO_CREATOR_COUNTRY" => "Орг. изготовитель",
            "AUTO_CATEGORY" => "Категория",
            "AUTO_YEAR" => "Год выпуска",
            "AUTO_CHASSIS_NUMBER" => "Номер шасси (рамы)",
            "AUTO_BODY_NUMBER" => "Номер кузова",
            "AUTO_COLOR" => "Цвет",
            "AUTO_POWER" => "Мощность двиг. (л.с.)",
            "AUTO_ECO_CLASS" => "Экологический класс",
            "AUTO_DOCUMENT" => "Тип документа",
            "AUTO_STS_SERIAL" => "Серия",
            "AUTO_STS_NUMBER" => "Номер",
            "AUTO_STS_DATE" => "Дата выдачи",
            "AUTO_PRICE" => "Стоимость ТС",
        );
        foreach ($arFildsReqAuto as $key => $name) {
            $value = $request->input($key);

            if ($value == '' && $key != 'head') {
                $error = 'Y';
            } elseif ($key == 'head') {
                $mess .= PHP_EOL . '---' . $name . '---' . PHP_EOL;
            } else {
                if ($key == "AUTO_MAKE" || $key == "FIRST_MODEL") {
                    if (!strpos($request->input($key), '|') === false) {
                        $mm_ex = explode('|', $request->input($key));
                        $value = $mm_ex[1];
                    } else {
                        $value = $request->input($key);
                    }
                }
                $arFild["auto"][$key] = $value;
                $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
            }
        }
        if ($request->input("AUTO_NUMBER")) {
            $mess .= 'Гос. рег. знак: ' . $request->input("AUTO_NUMBER") . ' ' . PHP_EOL;
        }

        $arFildsReqDKP = array(
            "head" => "Данные ДКП:",
            "DOC_ADDRESS" => "Место составления",
            "DOC_DATE" => "Дата составления",
            "DOC_USER_MAIL" => "Почта",
            "DOC_USER_PHONE" => "Телефон"
        );
        foreach ($arFildsReqDKP as $key => $name) {
            $value = $request->input($key);
            if ($value == '' && $key != 'head') {
                $error = 'Y';
            } elseif ($key == 'head') {
                $mess .= PHP_EOL . '---' . $name . '---' . PHP_EOL;
            } else {
                $arFild["auto"][$key] = $value;
                $mess .= $name . ': ' . $value . ' ' . PHP_EOL;
            }
        }
        if (!$error && $mess) {
            $id_buyer = '';
            $id_saller = '';
            $orderDoc = '';
            /*$id_buyer = Buyer::create($arFild["buyer"]);
            $id_saller = Seller::create($arFild["seller"]);
            if($id_buyer && $id_saller){
                $arFild["auto"]["id_buyer"] = $id_buyer->id;
                $arFild["auto"]["id_seller"] = $id_saller->id;
                $orderDoc = CalcDoc::create($arFild["auto"]);
            }*/
            if (!$orderDoc) {
                /*Mail::raw($mess, function($message)
                {
                    $message->from('pit-stop.help@yandex.ru', 'Форма "Документы ГИБДД онлайн"');
                    $message->to('deshor777@gmail.com');//->cc('bar@example.com');
                    $message->subject('PitStop');
                });*/

                $pdf = PDF::loadView('pdf.contract', ["arFild" => $arFild]);
                //$urlDoc = 'docs/'.$orderDoc->id.'_'.str_random(8).'.pdf';
                //$orderDoc->update(['doc_url'=>url($urlDoc)]);
                $urlDoc = 'docs/test.pdf';
                $pdf->save($urlDoc);

                $pdf2 = PDF::loadView('pdf.statement', ["arFild" => $arFild]);
                //$urlDoc2 = 'docs/'.$orderDoc->id.'_'.str_random(8).'.pdf';
                //$orderDoc->update(['doc_url_statement'=>url($urlDoc2)]);
                $urlDoc2 = 'docs/test2.pdf';
                $pdf2->save($urlDoc2);

                //return response()->json(array("success"=>true));
            }
        }
    }
}
