<?php

namespace App\Http\Controllers\Api\Avtocod;

use App\Http\Controllers\Controller;
use Avtocod\B2BApi\Client;
use Avtocod\B2BApi\Laravel\Connections\ConnectionsFactoryInterface as ConnectionsFactoryInterface;
use Avtocod\B2BApi\Laravel\ReportTypes\RepositoryInterface as RepositoryInterface;
use Avtocod\B2BApi\Settings;
use Illuminate\Http\Request;

class AvtocodController extends Controller
{
    protected $report_types;
    private static $start_date;
    protected $auth_token;

    public function __construct(RepositoryInterface $report_types, ConnectionsFactoryInterface $connections)
    {
        //$this->start_date = 1;
        $this->report_types = $report_types;
        $this->connections = $connections;
        $this->auth_token = config('app.avtocode_token');
    }

    /*static function startDate()
    {
        self::$start_date = date('Y-m-d H:i:s');
    }

    static function endDate()
    {
        $date1 = strtotime(self::$start_date);
        $date2 = strtotime(date('Y-m-d H:i:s'));
        return abs($date1 - $date2);
    }*/

    public function getInfoGRZ(Request $request)
    {
        $number = $request->input('query');
        if (!empty($number)) {
                $client = new Client(new Settings($this->auth_token));
            $report_types = $this->report_types;
            $uid = $report_types->default()->getUid();
            $report_uid = $client
                ->userReportMake($uid, 'GRZ', $number, null, true)
                ->first()
                ->getReportUid();
            $arResponse = [];
            if (!empty($report_uid)) {
                while (true) {
                    if ($client->userReport($report_uid, false)->first()->isCompleted()) {
                        break;
                    }
                    \sleep(1);
                }
                $infoAuto = $client->userReport($report_uid)->first()->getContent();
                if (!empty($infoAuto->getByPath('tech_data'))) {
                    $originalName = $infoAuto->getByPath('tech_data.brand.name.original');
                    $power = $infoAuto->getByPath('tech_data.engine.power.hp');
                    $year = $infoAuto->getByPath('tech_data.year');
                    $category = $infoAuto->getByPath('additional_info.vehicle.category.code');
                    if (!empty($originalName)) {
                        $normalName = $infoAuto->getByPath('tech_data.brand.name.normalized');
                        $normalNameModel = $infoAuto->getByPath('tech_data.model.name.normalized');
                        if (!empty($normalName))
                            $arResponse["mark"] = $normalName;
                        if (!empty($normalNameModel)) {
                            $exModel = explode(' «', $normalNameModel);
                            if (!empty($exModel[0])) {
                                $arResponse["model"] = $exModel[0];
                            } else {
                                $arResponse["model"] = $normalNameModel;
                            }
                        } else {
                            $modelMark = explode(' ', $originalName);
                            $arResponse["model"] = $modelMark[1];
                        }
                        $arResponse["reg_number"] = $number;
                    }
                    if (!empty($power)) {
                        $arResponse["power"] = round($power);
                        $arResponse["power2"] = $power;
                    }
                    if (!empty($year)) {
                        $arResponse["year"] = $year;
                    }
                    if (!empty($category)) {
                        $arResponse["category"] = $category;
                    }
                    return response()->json([
                        'success' => true,
                        'info' => $arResponse
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'mess' => 'Данные не найдены'
                    ]);
                }
            }
            /*
            $report_types = $this->report_types;
            $connections = $this->connections;
            $uid = $report_types->default()->getUid(); // Get default report type UID
            $arResponse = [];
            $report_uid = $connections->default()
                ->userReportMake($uid, 'GRZ', $number)
                ->first()
                ->getReportUid();
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/avtocode/report_grz.txt', print_r($report_uid, true), FILE_APPEND);
            try {
                if (!empty($report_uid)) {
                    $infoAuto = $connections->default()
                        ->userReport($report_uid)
                        ->first()
                        ->getContent()->getContent();
                    file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/avtocode/info_grz.txt', print_r($infoAuto, true), FILE_APPEND);
                    if (!empty($infoAuto)) {
                        if (!empty($infoAuto["tech_data"]["brand"]["name"]["original"])) {
                            if (!empty($infoAuto["tech_data"]["brand"]["name"]["normalized"]))
                                $arResponse["mark"] = $infoAuto["tech_data"]["brand"]["name"]["normalized"];
                            if (!empty($infoAuto["tech_data"]["model"]["name"]["normalized"])) {
                                $exModel = explode(' «', $infoAuto["tech_data"]["model"]["name"]["normalized"]);
                                if (!empty($exModel[0])) {
                                    $arResponse["model"] = $exModel[0];
                                } else {
                                    $arResponse["model"] = $infoAuto["tech_data"]["model"]["name"]["normalized"];
                                }
                            } else {
                                $modelMark = explode(' ', $infoAuto["tech_data"]["brand"]["name"]["original"]);
                                $arResponse["model"] = $modelMark[1];
                            }
                            $arResponse["reg_number"] = $number;
                        }
                        if (!empty($infoAuto["tech_data"]["engine"]["power"]["hp"])) {
                            $arResponse["power"] = round($infoAuto["tech_data"]["engine"]["power"]["hp"]);
                            $arResponse["power2"] = $infoAuto["tech_data"]["engine"]["power"]["hp"];
                        }
                        if (!empty($infoAuto["tech_data"]["year"])) {
                            $arResponse["year"] = $infoAuto["tech_data"]["year"];
                        }
                        if (!empty($infoAuto["additional_info"]["vehicle"]["category"]["code"])) {
                            $arResponse["category"] = $infoAuto["additional_info"]["vehicle"]["category"]["code"];
                        }
                        return response()->json([
                            'success' => true,
                            'info' => $arResponse
                        ]);
                    } else {
                        $this->startDate();
                        $endDate = $this->endDate();
                        if ($endDate < 60) {
                            sleep(5);
                            $this->getInfoGRZ($request);
                        } elseif ($endDate >= 61 && $endDate < 300) {
                            sleep(30);
                            $this->getInfoGRZ($request);
                        } elseif($endDate > 300 && $endDate < 900){
                            sleep(600);
                            $this->getInfoGRZ($request);
                        }elseif($endDate > 900){
                            return response()->json([
                                'success' => false,
                                'mess' => 'Данные не найдены'
                            ]);
                        }
                        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/avtocode/time.txt', print_r([$endDate, $number], true), FILE_APPEND);
                    }
                } else {
                    return response()->json([
                        'success' => false,
                        'mess' => 'Данные не найдены'
                    ]);
                }
            } catch (RequestException $e) {
                return response()->json([
                    'success' => false,
                    'mess' => 'Данные не найдены'
                ]);
            }
            /*$arResponse = [
                "mark"=>"Mercedes-Benz",
                "model"=>"G-klasse",
                "reg_number"=>"Н777НН22",
                "power"=>245,
                "power2"=>245,
                "year"=>2016,
                "category"=>"B"
            ];*/
        } else {
            return response()->json([
                'success' => false,
                'mess' => 'Пустое значение'
            ]);
        }
    }

    public function getInfoVIN(Request $request)
    {
        $number = $request->input('query');
        if (!empty($number)) {
            $uid_nv = 'pit_stop_report_identifikatory_TS'; // Get default report type UID
            $client = new Client(new Settings($this->auth_token));
            $report_uid = $client
                ->userReportMake($uid_nv, 'GRZ', $number, null, true)
                ->first()
                ->getReportUid();
            $arResponse = [];
            if (!empty($report_uid)) {
                while (true) {
                    if ($client->userReport($report_uid, false)->first()->isCompleted()) {
                        break;
                    }
                    \sleep(1);
                }
                $infoAuto = $client->userReport($report_uid)->first()->getContent();
                if (!empty($infoAuto->getByPath('identifiers.vehicle'))) {
                    $body = $infoAuto->getByPath('identifiers.vehicle.body');
                    $chassis = $infoAuto->getByPath('identifiers.vehicle.chassis');
                    $vin = $infoAuto->getByPath('identifiers.vehicle.vin');
                    $sts = $infoAuto->getByPath('identifiers.vehicle.sts');
                    $pts = $infoAuto->getByPath('identifiers.vehicle.pts');
                    if (!empty($body))
                        $arResponse["bodywork"] = $body;
                    if (!empty($vin))
                        $arResponse["vin"] = $vin;
                    if (!empty($chassis))
                        $arResponse["chassis"] = $chassis;
                    if (!empty($sts)) {
                        $seria = mb_substr($sts, 0, 4);
                        $number = mb_substr($sts, 4);
                        $arResponse["sts"] = $seria . ' ' . $number;
                    }
                    if (!empty($pts)) {
                        $seria = mb_substr($pts, 0, 4);
                        $number = mb_substr($pts, 4);
                        $arResponse["pts"] = $seria . ' ' . $number;
                    }
                    return response()->json([
                        'success' => true,
                        'info' => $arResponse
                    ]);
                }else{
                    return response()->json([
                        'success' => false,
                        'mess' => 'Данные не найдены'
                    ]);
                }
            }else{
                return response()->json([
                    'success' => false,
                    'mess' => 'Данные не найдены'
                ]);
            }
            /*$connections = $this->connections;
            $report_uid_nv = $connections->default()
                ->userReportMake($uid_nv, 'GRZ', $number)
                ->first()
                ->getReportUid();
            $arResponse = [];
            if ($report_uid_nv) {
                $infoAuto_nv = $connections->default()
                    ->userReport($report_uid_nv)
                    ->first()
                    ->getContent()->getContent();
                if (!empty($infoAuto_nv)) {
                    if (!empty($infoAuto_nv["identifiers"]["vehicle"])) {
                        if (!empty($infoAuto_nv["identifiers"]["vehicle"]["body"]))
                            $arResponse["bodywork"] = $infoAuto_nv["identifiers"]["vehicle"]["body"];
                        if (!empty($infoAuto_nv["identifiers"]["vehicle"]["vin"]))
                            $arResponse["vin"] = $infoAuto_nv["identifiers"]["vehicle"]["vin"];
                        if (!empty($infoAuto_nv["identifiers"]["vehicle"]["chassis"]))
                            $arResponse["chassis"] = $infoAuto_nv["identifiers"]["vehicle"]["chassis"];
                        if (!empty($infoAuto_nv["identifiers"]["vehicle"]["sts"])) {
                            $seria = mb_substr($infoAuto_nv["identifiers"]["vehicle"]["sts"], 0, 4);
                            $number = mb_substr($infoAuto_nv["identifiers"]["vehicle"]["sts"], 4);
                            $arResponse["sts"] = $seria . ' ' . $number;
                        }
                        if (!empty($infoAuto_nv["identifiers"]["vehicle"]["pts"])) {
                            $seria = mb_substr($infoAuto_nv["identifiers"]["vehicle"]["pts"], 0, 4);
                            $number = mb_substr($infoAuto_nv["identifiers"]["vehicle"]["pts"], 4);
                            $arResponse["pts"] = $seria . ' ' . $number;
                        }
                    }
                    return response()->json([
                        'success' => true,
                        'info' => $arResponse
                    ]);
                } else {
                    if ($time < 60) {
                        sleep(5);
                        $time = $time + 5;
                    } elseif ($time >= 61 && $time < 300) {
                        sleep(30);
                        $time = $time + 30;
                    } elseif($time < 700) {
                        sleep(600);
                        $time = $time + 600;
                    }
                    if ($time > 1 && $time < 700) {
                        $this->getInfoVIN($request, $time);
                    }else{
                        return response()->json([
                            'success' => false,
                            'mess' => 'Данные не найдены'
                        ]);
                    }
                }
            } else {
                return response()->json([
                    'success' => false,
                    'mess' => 'Данные не найдены'
                ]);
            }*/
        }
    }

    public function getBalance(){
        $nowUtc = new \DateTime( 'now');
        $date = $nowUtc->format('Y-m-d\TH:i:s.v\Z');
        $url = "https://b2bapi.avtocod.ru/b2b/api/v1/system/balance/quote";
        $data = [
            "data" => [
                ["stamp" => $date],
            ],
        ];
        $data_string = json_encode($data);
        print_r($data_string);
        $header = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: AR-REST Y2VudGVyX29rcEBwaXRfc3RvcDoxNDgzNjM0NzIzOjk5OTk5OTk5OToxQUJNdlF1UE9EZnhOa3ppSktCTTVnPT0='
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $resp = json_decode($response, true);
        echo '<pre>';
        print_r($resp);
        echo '</pre>';
        /*$report_types = $this->report_types;
            $connections = $this->connections;
            $uid = $report_types->default()->getUid(); // Get default report type UID
            $arResponse = [];
            $report_uid = $connections->default()->userBalance($uid)->getRawResponseContent();
            echo '<pre>';
            print_r($report_uid);
            echo '</pre>';*/
    }
}
