<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Api\Alfastrah\AuthSoap;
use Illuminate\Routing\Controller;

require_once($_SERVER["DOCUMENT_ROOT"]."/soap_security.php");

class EOsagoRelaunch extends Controller /* Сервис EOsagoRelaunch */
{
    protected $url_servic;
    protected $login;
    protected $pass;

    public function __construct ()
    {
        if(config('app.env') == 'local'){
            $this->url_servic = "https://b2b-test2.alfastrah.ru/cxf/partner/EOsagoRelaunch?wsdl";
        }else{
            $this->url_servic = "https://b2b.alfastrah.ru/cxf/EOsagoRelaunch?wsdl"; // wss-wssecurity - Включен
        }

        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }

    public function loadDriver($arFilds) // Проверка данных по ЛДУ
    {
        $Surname = $arFilds["LastName"];//$request->input('Surname');
        $Name = $arFilds["FirstName"];
        $Patronymic = $arFilds["MiddleName"];
        $BirthDate = $arFilds["BirthDate"];
        $Serial = $arFilds["DriverDocument"]["Seria"];
        $Number = $arFilds["DriverDocument"]["Number"];
        $DriverDocDate = $arFilds["ExperienceDate"];

        $arDriver = array(
            'DriverRequest'=>array(
            'DriverRequestValue'=>array(
                'DriverInfoRequest'=>array(
                    'Surname' => $Surname, // Фамилия
                    'Name' => $Name, // Имя
                    'Patronymic' => $Patronymic, // Отчество
                    'BirthDate' => $BirthDate, // Дата рождения (1983-12-26)
                    'DriverDocument'=>array( // Атрибуты по водительскому удостоверению ЛДУ
                        'Serial' => $Serial, // Номер
                        'Number' => $Number, // Серия
                    ),
                    'DriverDocDate' => $DriverDocDate, // Дата выдачи первого водительского удостоверения
                ),
                'DateRequest' => gmdate('Y-m-d\TH:i:s'),
            ),
        ));
        $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $result->__soapCall("loadDriver", $arDriver);
        $req = $result->__last_response;
        $resp = $result->__last_request;
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);

        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadDriver_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadDriver_response.xml', print_r($req, true));

        $info["Code"] = 0;
        if(!empty($responseArray["soapBody"]["ns2DriverStatusResponse"]["ErrorList"]["ErrorInfo"]))
        $info = $responseArray["soapBody"]["ns2DriverStatusResponse"]["ErrorList"]["ErrorInfo"];

        if(($info["Code"] == 3 || $info["Code"] == 2) && !empty($responseArray["soapBody"]["ns2DriverStatusResponse"]["IDCheckDriver"])){
            $IDCheckDriver = $responseArray["soapBody"]["ns2DriverStatusResponse"]["IDCheckDriver"];
            return array('success' => true, 'info'=>$info, 'IDCheckDriver'=>$IDCheckDriver);
        }else{
            return array('success' => false, 'errors'=>$info);
        }
    }
    public function loadInsurerOwner($arFilds) // Проверка данных по страхователю/собственнику - одному пользователю
    {

        $type = $arFilds["type"];
        if(!strpos('---' . $arFilds['RegionOfUse'], 'г ') === false){
            $Region = explode('г ', $arFilds["RegionOfUse"])[1];
        }else{
            $Region = $arFilds["RegionOfUse"];
        }
        $City = $arFilds["CityOfUse"];

        $Surname = $arFilds["Owner"]["LastName"];
        $Name = $arFilds["Owner"]["FirstName"];
        $Patronymic = $arFilds["Owner"]["MiddleName"];
        $BirthDate = $arFilds["Owner"]["BirthDate"];
        $Seria = $arFilds["Owner"]["Seria"];
        $Number = $arFilds["Owner"]["Number"];

        $Surname2 = $arFilds["Insurer"]["LastName"];
        $Name2 = $arFilds["Insurer"]["FirstName"];
        $Patronymic2 = $arFilds["Insurer"]["MiddleName"];
        $BirthDate2 = $arFilds["Insurer"]["BirthDate"];
        $Seria2 = $arFilds["Insurer"]["Seria"];
        $Number2 = $arFilds["Insurer"]["Number"];
        $Apartment = $arFilds["Insurer"]["address"]["apartament"];
        if(!empty((int)$Seria) && !empty((int)$Seria2))
        switch($type){
            case 'Owner':
              /*  if(!empty($arFilds["Drivers"]["Driver"])){
                    $driver_one = $arFilds["Drivers"]["Driver"][0];
                    $arOwner = array(
                        'InsurerOwnerRequest'=>array(
                            'InsurerOwnerRequestValue'=>array(
                                'CheckedPerson' => 'Owner', // Тип проверяемого (Для проверки собственника передавать Owner Для проверки страхователя передавать Insurer)
                                'PhysicalPersonInfoRequest'=>array(
                                    'Country' => 643, // Наименование страны
                                    'Region' => $Region, // Регион
                                    'City' => $City, // Город
                                    'Surname' => $driver_one["LastName"], // Фамилия
                                    'Name' => $driver_one["FirstName"], // Имя
                                    'Patronymic' => $driver_one["MiddleName"], // Отчество
                                    'BirthDate' => $driver_one["BirthDate"], // Дата рождения
                                    'PersonDocument' => array(
                                        'DocPerson' => 20, // Тип документа (Код из справочника Типы документов РСА У нас это всегда паспорт. Код паспорта = "12")
                                        'Seria' => $driver_one["DriverDocument"]["Seria"], // Серия паспорта
                                        'Number' => $driver_one["DriverDocument"]["Number"] // Номер паспорта
                                    ),
                                ),
                                'DateRequest' => gmdate('Y-m-d\TH:i:s'), // Дата и время запроса
                            ),
                        ),
                    );

                }else{ }*/
                    $arOwner = array(
                        'InsurerOwnerRequest'=>array(
                            'InsurerOwnerRequestValue'=>array(
                                'CheckedPerson' => 'Owner', // Тип проверяемого (Для проверки собственника передавать Owner Для проверки страхователя передавать Insurer)
                                'PhysicalPersonInfoRequest'=>array(
                                    'Country' => 643, // Наименование страны
                                    'Region' => $arFilds["Owner"]["address"]["region"], // Регион
                                    'City' => $arFilds["Owner"]["address"]["city"], // Город
                                    'Street' => $arFilds["Owner"]["address"]["street"], // Улица
                                    'House' => $arFilds["Owner"]["address"]["home"], // Дом
                                    'Apartment' => !empty($arFilds["Owner"]["address"]["apartament"])?$arFilds["Owner"]["address"]["apartament"]:'', // Квартира
                                    'Surname' => $Surname, // Фамилия
                                    'Name' => $Name, // Имя
                                    'Patronymic' => $Patronymic, // Отчество
                                    'BirthDate' => $BirthDate, // Дата рождения
                                    'PersonDocument' => array(
                                        'DocPerson' => 12, // Тип документа (Код из справочника Типы документов РСА У нас это всегда паспорт. Код паспорта = "12")
                                        'Serial' => $Seria, // Серия паспорта
                                        'Number' => $Number, // Номер паспорта
                                    ),
                                ),
                                'DateRequest' => gmdate('Y-m-d\TH:i:s'), // Дата и время запроса
                            ),
                        ),
                    );

                    /*echo '<pre>';
                    print_r($arOwner);
                    echo '</pre>';*/

                $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
                $result->__setAuth($this->login, $this->pass);
                $result->__soapCall("loadInsurerOwner", $arOwner);
                $req = $result->__last_response;
                $resp = $result->__last_request;
                $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                $xml = simplexml_load_string($xml);
                $json = json_encode($xml);
                $responseArray = json_decode($json,true);

                file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadInsurerOwner_request.xml', print_r($resp, true));
                file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadInsurerOwner_response.xml', print_r($req, true));
                $info["Code"] = 0;
                if(!empty($responseArray["soapBody"]["ns2InsurerOwnerStatusResponse"]["ErrorList"]["ErrorInfo"]))
                $info = $responseArray["soapBody"]["ns2InsurerOwnerStatusResponse"]["ErrorList"]["ErrorInfo"];

                if($info["Code"] == 3 || $info["Code"] == 2){
                    $IDCheckInsurerOwner = $responseArray["soapBody"]["ns2InsurerOwnerStatusResponse"]["IDCheckInsurerOwner"];
                    return array('success' => true, 'info'=>$info, 'IDCheckInsurerOwner'=>$IDCheckInsurerOwner);
                }else{
                    return array('success' => false, 'errors'=>$info);
                }
            break;
            case 'Insurer':
                $arInsurer = array(
                    'InsurerOwnerRequest'=>array(
                        'InsurerOwnerRequestValue'=>array(
                            'CheckedPerson' => 'Insurer', // Тип проверяемого (Для проверки собственника передавать Owner Для проверки страхователя передавать Insurer)
                            'PhysicalPersonInfoRequest'=>array(
                                'Country' => 643, // Наименование страны
                                'Region' => $arFilds["Insurer"]["address"]["region"], // Регион
                                'City' => $arFilds["Insurer"]["address"]["city"], // Город
                                'Street' => $arFilds["Insurer"]["address"]["street"], // Улица
                                'House' => $arFilds["Insurer"]["address"]["home"], // Дом
                                'Apartment' => !empty($arFilds["Insurer"]["address"]["apartament"])?$arFilds["Insurer"]["address"]["apartament"]:'', // Квартира
                                'Surname' => $Surname2, // Фамилия
                                'Name' => $Name2, // Имя
                                'Patronymic' => $Patronymic2, // Отчество
                                'BirthDate' => $BirthDate2, // Дата рождения
                                'PersonDocument' => array(
                                    'DocPerson' => 12, // Тип документа (Код из справочника Типы документов РСА У нас это всегда паспорт. Код паспорта = "12")
                                    'Serial' => $Seria2, // Серия паспорта
                                    'Number' => $Number2, // Номер паспорта
                                ),
                            ),
                            'DateRequest' => gmdate('Y-m-d\TH:i:s'), // Дата и время запроса
                        ),
                    ),
                );
                $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
                $result->__setAuth($this->login, $this->pass);
                $result->__soapCall("loadInsurerOwner", $arInsurer);
                $req = $result->__last_response;
                $resp = $result->__last_request;
                $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                $xml = simplexml_load_string($xml);
                $json = json_encode($xml);
                $responseArray = json_decode($json,true);

                file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadInsurerOwner2_request.xml', print_r($resp, true));
                file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadInsurerOwner2_response.xml', print_r($req, true));

                $info["Code"] = 0;
                if(!empty($responseArray["soapBody"]["ns2InsurerOwnerStatusResponse"]["ErrorList"]["ErrorInfo"]))
                $info = $responseArray["soapBody"]["ns2InsurerOwnerStatusResponse"]["ErrorList"]["ErrorInfo"];

                if($info["Code"] == 3 || $info["Code"] == 2){
                    $IDCheckInsurerOwner = $responseArray["soapBody"]["ns2InsurerOwnerStatusResponse"]["IDCheckInsurerOwner"];
                    return array('success' => true, 'info'=>$info, 'IDCheckInsurerOwner'=>$IDCheckInsurerOwner);
                }else{
                    return array('success' => false, 'errors'=>$info);
                }
            break;
            default:
                abort(404, 'Page not found');
            break;
        }
    }
    public function loadVehicle($arrFild) // Проверка данных по страхуемому ТС
    {
        $LicensePlate = $arrFild["LicensePlate"];
        $VIN = $arrFild["VIN"];
        $number_bodywork = $arrFild["number_bodywork"];
        $number_chassis = $arrFild["number_chassis"];
        $Mark = $arrFild["Mark"];
        $Model = $arrFild["Model"];
        $YearIssue = $arrFild["YearIssue"];
        $DocCarSerial = $arrFild["DocCarSerial"];
        $DocCarNumber = $arrFild["DocCarNumber"];
        $DocumentCarDate = date("Y-m-d", strtotime($arrFild["DocumentCarDate"]));
        $EngCap = $arrFild["EngCap"];

        $arTSRequest = array(
            'CountryCar' => 1, // Страна регистрации ТС (0 – не РФ, 1 – РФ)
            'Mark' => $Mark, // Марка
            'Model' => $Model, // Модель
            'YearIssue' => $YearIssue, // Год выпуска
            'CarIdent' => array( // Атрибуты, позволяющие однозначно идентифицировать страхуемое ТС
                'LicensePlate' => $LicensePlate, // Регистрационный номер ТС
            ),
            'DocumentCar' => $arrFild["doc_ts"]=="pts"?30:31, // Тип документа на автомобиль (Код из справочника "Типы документов")
            'DocCarSerial' => $DocCarSerial, // Серия документа ТС
            'DocCarNumber' => $DocCarNumber, // Номер документа ТС
            'DocumentCarDate' => $DocumentCarDate, // Дата выдачи документа ТС
            'EngCap' => $EngCap, // Мощность двигателя Для ТС категории "B"
        );
        if($VIN){
            $arTSRequest['CarIdent']['VIN'] = $VIN;
        }else if($number_bodywork){
            $arTSRequest['CarIdent']['BodyNumber'] = $number_bodywork;
        }else if($number_chassis){
            $arTSRequest['CarIdent']['ChassisNumber'] = $number_chassis;
        }

        $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $result->__soapCall("loadVehicle", array(
            'TSRequest'=>array(
                'TSRequestValue'=>array(
                    'TSInfoRequest'=>$arTSRequest,
                    'DateRequest' => gmdate('Y-m-d\TH:i:s'), // Дата и время запроса
                ),
            ),
        ));

        $req = $result->__last_response;
        $resp = $result->__last_request;
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);

        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadVehicle_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadVehicle_response.xml', print_r($req, true));

        $info["Code"] = 0;
        if(!empty($responseArray["soapBody"]["ns2TSStatusResponse"]["ErrorList"]["ErrorInfo"]))
        $info = $responseArray["soapBody"]["ns2TSStatusResponse"]["ErrorList"]["ErrorInfo"];

        if(($info["Code"] == 3 || $info["Code"] == 2) && !empty($responseArray["soapBody"]["ns2TSStatusResponse"]["IDCheckTS"])){
            $IDCheckTS = $responseArray["soapBody"]["ns2TSStatusResponse"]["IDCheckTS"];
            return array('success' => true, 'info'=>$info, 'IDCheckTS'=>$IDCheckTS);
        }else{
            return array('success' => false, 'errors'=>$info); // array elems = Message
        }
    }
    public function loadContractEosago($arFilds) // Загрузка проекта договора в РСА после успешного прохождения расчёта и прохождения проверок
    {
            $hours = strtotime('+ 72 hours');
            $hours2 = strtotime('+ 72 hours + 1 years - 1 day');
            //$dateStart = gmdate('Y-m-d\TH:i:s', $hours);
            $dateStart = date("Y-m-d\TH:i:s", strtotime($arFilds["PolicyBeginDate"]));
            $dateEnd = gmdate('Y-m-d\TH:i:s', strtotime($arFilds["PolicyBeginDate"].' + 1 years - 1 day'));
            $DateRevision = gmdate('Y-m-d\TH:i:s');
            $VIN = $arFilds["VIN"];
            $number_bodywork = $arFilds["number_bodywork"];
            $number_chassis = $arFilds["number_chassis"];
            $arChD = array();
            if(!empty($arFilds["DriversChck"]) && $arFilds["DriversChck"]["Driver"]){
                $DriversRestriction = true;
                $arChD["Drivers"]=array();
                foreach($arFilds["DriversChck"]["Driver"] as $v){
                    $arChD['Drivers']['Driver'][]=array(
                            'IDCheckDriver' => $v["IDCheckDriver"],
                            'CountryCode' => 643,
                            'CountryName' => 'Россия',
                            'DriverDocument'=>array(
                                'Serial' => $v["DriverDocument"]["Seria"],
                                'Number' => $v["DriverDocument"]["Number"],
                                'DateIssue' => $v["DriverDocument"]["DateIssue"],
                            ),
                            'DriverDocDate' => $v["ExperienceDate"],
                            'Surname' => $v["LastName"],
                            'Name' => $v["FirstName"],
                            'Patronymic' => $v["MiddleName"],
                            'BirthDate' => $v["BirthDate"],
                    );
                }
                    $driver_one = $arFilds["DriversChck"]["Driver"][0];
                    /*$arChD['Owner']=array(
                        'PhysicalPerson'=>array(
                            'IDCheckInsurerOwner' => $arFilds["Owner"]["IDCheckInsurerOwner"],
                            'Surname' => $driver_one["LastName"],
                            'Name' => $driver_one["FirstName"],
                            'Patronymic' => $driver_one["MiddleName"],
                            'BirthDate' => $driver_one["BirthDate"],
                            'PersonDocument'=>array(
                                'DocPerson' => 20,
                                'Serial' => $driver_one["DriverDocument"]["Seria"],
                                'Number' => $driver_one["DriverDocument"]["Number"],
                            ),
                            'CountryCode' => 643,
                            'CountryName' => 'Россия',
                            'State' => $arFilds["RegionOfUse"],
                            'City' => $arFilds["CityOfUse"],
                        ),
                    );*/
            }else{
                $arChD["Drivers"]=array();
                $DriversRestriction = false;
            }
                $arChD['Owner']=array(
                    'PhysicalPerson'=>array(
                        'IDCheckInsurerOwner' => $arFilds["Owner"]["IDCheckInsurerOwner"],
                        'Surname' => $arFilds["Owner"]["LastName"],
                        'Name' => $arFilds["Owner"]["FirstName"],
                        'Patronymic' => $arFilds["Owner"]["MiddleName"],
                        'BirthDate' => $arFilds["Owner"]["BirthDate"],
                        'Phone' => $arFilds["phone"],
                        'Email' => $arFilds["email"],
                        'PersonDocument'=>array(
                            'DocPerson' => 12,
                            'Serial' => $arFilds["Owner"]["Seria"],
                            'Number' => $arFilds["Owner"]["Number"],
                        ),
                        'CountryCode' => 643,
                        'CountryName' => 'Россия',
                        'State' => !empty($arFilds["Owner"]["address"]["region"])?$arFilds["Owner"]["address"]["region"]:'',
                        'City' => !empty($arFilds["Owner"]["address"]["city"])?$arFilds["Owner"]["address"]["city"]:'',
                        'Street' => !empty($arFilds["Owner"]["address"]["street"])?$arFilds["Owner"]["address"]["street"]:'',
                        'House' => !empty($arFilds["Owner"]["address"]["home"])?$arFilds["Owner"]["address"]["home"]:'',
                        'Apartment' => !empty($arFilds["Owner"]["address"]["apartament"])?$arFilds["Owner"]["address"]["apartament"]:'',
                    ),
                );

        $LoadContractRequest = array(
                'CalcRef' => $arFilds["CalcRef"],
                'DateRevision' => $DateRevision,
                'DateActionBeg' => $dateStart,
                'DateActionEnd' => $dateEnd,
                'DriversRestriction' => $DriversRestriction,
                'IsTransCar' => false,
                'IsTrailerAllowed' => 0,
                'Car'=>array(
                    'IDCheckTS' => $arFilds["IDCheckTS"],
                    'CountryCar' => 1,
                    'LicensePlate' => $arFilds["LicensePlate"],
                    'Mark' => $arFilds["Mark"],
                    'Model' => $arFilds["Model"],
                    'YearIssue' => $arFilds["YearIssue"],
                    'DocumentCar' => $arFilds["doc_ts"]=="pts"?30:31,
                    'DocCarSerial' => $arFilds["DocCarSerial"],
                    'DocCarNumber' => $arFilds["DocCarNumber"],
                    'DocumentCarDate' => date("Y-m-d", strtotime($arFilds["DocumentCarDate"])),
                    'EngCap' => $arFilds["EngCap"],
                    'Purpose' => 'Личные',
                ),
                'Insurer'=>array(
                    'PhysicalPerson'=>array(
                        'IDCheckInsurerOwner' => $arFilds["Insurer"]["IDCheckInsurerOwner"],
                        'Surname' => $arFilds["Insurer"]["LastName"],
                        'Name' => $arFilds["Insurer"]["FirstName"],
                        'Patronymic' => $arFilds["Insurer"]["MiddleName"],
                        'BirthDate' => $arFilds["Insurer"]["BirthDate"],
                        'Phone' => $arFilds["phone"],
                        'Email' => $arFilds["email"],
                        'PersonDocument'=>array(
                            'DocPerson' => 12,
                            'Serial' => $arFilds["Insurer"]["Seria"],
                            'Number' => $arFilds["Insurer"]["Number"],
                        ),
                        'CountryCode' => 643,
                        'CountryName' => 'Россия',
                        'State' => !empty($arFilds["Owner"]["address"]["region"])?$arFilds["Owner"]["address"]["region"]:'',
                        'City' => !empty($arFilds["Owner"]["address"]["city"])?$arFilds["Owner"]["address"]["city"]:'',
                        'Street' => !empty($arFilds["Owner"]["address"]["street"])?$arFilds["Owner"]["address"]["street"]:'',
                        'House' => !empty($arFilds["Owner"]["address"]["home"])?$arFilds["Owner"]["address"]["home"]:'',
                        'Apartment' => !empty($arFilds["Owner"]["address"]["apartament"])?$arFilds["Owner"]["address"]["apartament"]:'',
                    ),
                ),
                'AgentBlock'=>[
                    'AgentContractId' => 815927,
                    'ManagerId' => 343424545,
                    'DepartmentId' => 53225815,
                    'ChannelSaleId' => 20071
                ],
        );
            if($arChD["Drivers"]){
                $LoadContractRequest["Drivers"] = $arChD["Drivers"];
            }
            if($arChD['Owner']){
                $LoadContractRequest["Owner"] = $arChD['Owner'];
            }
        if($arChD["Drivers"]){
            $LoadContractRequest["Drivers"] = $arChD["Drivers"];
        }

        if($arFilds["cart_diag"]){
            $LoadContractRequest["Car"]['TicketCar'] = 53;
            $LoadContractRequest["Car"]["TicketCarNumber"] = $arFilds["cart_diag"];
            $LoadContractRequest["Car"]["TicketDiagnosticDate"] = $arFilds["date_cart_diag"];
            //$LoadContractRequest["Car"]["TicketCarMonth"] = '';
        }
        if($VIN){
            $LoadContractRequest['Car']['CarIdent']['VIN'] = $VIN;
        }else if($number_bodywork){
            $LoadContractRequest['Car']['CarIdent']['BodyNumber'] = $number_bodywork;
        }else if($number_chassis){
            $LoadContractRequest['Car']['CarIdent']['ChassisNumber'] = $number_chassis;
        }

        $result = new AuthSoap($this->url_servic, array('trace' => true, 'exceptions' => true,'cache_wsdl'=>WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $result->__soapCall("LoadContractEosago", array(
            'PartnerInfo'=>array(
                'UPID' => $arFilds["UPID"],
                'LoadContractRequest'=>$LoadContractRequest,
            ),
        ));

        /*if (is_soap_fault($result)) {
            return array("success"=>false, "error"=>$result->faultstring);
        }*/
            $req = $result->__last_response;
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/LoadContractEosago_response.xml', print_r($req, true));
            $resp = $result->__last_request;
            file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/loadContractEosago_request.xml', print_r($resp, true));

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);

        if(!empty($responseArray["soapBody"]["soapFault"]["faultstring"])){
            return array('success'=>false, "error"=>$responseArray["soapBody"]["soapFault"]["faultstring"]);
        }elseif(!empty($responseArray["soapBody"]["ns2LoadContractEosagoResponse"])){
            $body = $responseArray["soapBody"]["ns2LoadContractEosagoResponse"];
            return array('success'=>true, 'ns2ContractId'=>$body["ns2ContractId"], 'ns2ContractNumber'=>$body["ns2ContractNumber"], 'ns2ContractPremium'=>$body["ns2ContractPremium"]);
        }
    }
}
