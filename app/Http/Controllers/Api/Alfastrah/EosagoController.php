<?php

namespace App\Http\Controllers\Api\Alfastrah;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class EosagoController extends Controller
{
    protected $login;
    protected $pass;

    public function __construct ()
    {
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }
    public function userPartner(Request $request) // В рамках продажи ЕОСАГО необходимо создать ЛК клиента страхователя
    {
       /*
        • email - Email. Обязательный.
        • last_name - Фамилия. Обязательный.
        • name - Имя. Обязательный.
        • second_name - Отчество. Обязательный.
        • password - Пароль. Не обязательный, в случае отсутствия пароль будет сгенерирован.
        • confirm_password - Повтор пароля.
        • phone - Телефон. В любом формате, главное чтобы были все 11 цифр. Обязательный. (71071071717)
        • birthday - Дата рождения. Формат: `дд.мм.гггг`. Обязательный.
        • contract_number - Номер договора.
        • offer_accepted - Флаг принятия оферты. Возможные знаения: `Y`/`N`.
        • partner_name - Название партнера. Обязательный.
       */
        $email = $request->input('email');
        $last_name = $request->input('last_name');
        $name = $request->input('name');
        $second_name = $request->input('second_name');
        $phone = $request->input('phone');
        $birthday = $request->input('birthday');
        $contract_number = $request->input('contract_number');
        $offer_accepted = $request->input('offer_accepted');

        $url = 'https://www.alfastrah.ru/api/userPartner/';
        //параметры которые необходимо передать
        $params = array(
            'email' => $email,
            'last_name' => $last_name,
            'name' => $name,
            'second_name' => $second_name,
            'password' => '',
            'confirm_password' => '',
            'phone' => $phone,
            'birthday' => $birthday,
            'contract_number' => $contract_number,
            'offer_accepted' => $offer_accepted,
            'partner_name' => $this->login,
        );
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/userPartner_request.xml', print_r($params, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/userPartner_response.xml', print_r($result, true));
        return $result;
    }
}
