<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Api\Alfastrah\AuthSoap;
use Illuminate\Routing\Controller;

require($_SERVER["DOCUMENT_ROOT"]."/soap_security.php");

class PartnersInteraction extends Controller /* Сервис PartnersInteraction */
{
    protected $url_servic;
    protected $login;
    protected $pass;
    protected $callerCode;

    public function __construct ()
    {
        if(config('app.env') == 'local'){
            $this->url_servic = "https://b2b-test2.alfastrah.ru/cxf/partner/PartnersInteraction?wsdl";
            $this->callerCode = "Дружба";
        }else{
            $this->url_servic = "https://b2b.alfastrah.ru/cxf/PartnersInteraction?wsdl"; // wss-wssecurity - Включен
            $this->callerCode = "E_OKSHIN";
        }
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }

    public function getUPID() // Вызывается для каждой попытки оформления договора ЕОСАГО для получения уникального идентификатора
    {
        $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $result->__soapCall("getUPID", array('UPIDRequest'=> array('callerCode'=>$this->callerCode)));
        $req = $result->__last_response;
        $resp = $result->__last_request;

        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/getUPID_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/getUPID_response.xml', print_r($req, true));

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);
        //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/UPID.txt', print_r($responseArray, true), FILE_APPEND);
        $upid = false;
        if(!empty($responseArray["soapBody"]["ns2UPIDResponse"]["UPID"]))
        $upid = $responseArray["soapBody"]["ns2UPIDResponse"]["UPID"]; //Идентификатор попытки заключения договора

        return $upid;
    }
    public function getContractId($upid) // Вызывается партнёром для определения статуса договора и извлечения его идентификатора в случае успешного заключения.
    {
        $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $result->__soapCall("getContractId", array('getPayedContractResponse'=> array('UPID'=>$upid)));
        $req = $result->__last_response;
        $resp = $result->__last_request;

        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/getContractId_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/getContractId_response.xml', print_r($req, true));

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);
        /*
            [soapBody] => Array
            (
                [ns2getPayedContractResponse] => Array
                    (
                        [code] => 0
                        [message] => Договор еще не создан в системе
                    )

            )
         */
        print_r($responseArray);
    }
}
