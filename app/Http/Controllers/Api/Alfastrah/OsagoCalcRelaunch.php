<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Api\Alfastrah\AuthSoap;
use Illuminate\Routing\Controller;

ini_set('default_socket_timeout', '600');


class OsagoCalcRelaunch extends Controller /* Сервис OsagoCalcRelaunch */
{
    protected $url_servic;
    protected $login;
    protected $pass;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            $this->url_servic = "https://b2b-test2.alfastrah.ru/cxf/partner/OsagoCalcRelaunch?wsdl";
        } else {
            $this->url_servic = "https://b2b.alfastrah.ru/cxf/OsagoCalcRelaunch?wsdl"; // wss-wssecurity - Включен
        }
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }

    public function CalcEOsagoMin($arFilds) // Используется для расчёта страховой премии по договору
    {
        $UPID = $arFilds["UPID"];
        $ContractType = $arFilds["ContractType"];
        $InsuranceMonth = 12;
        $RegionOfUse = $arFilds["RegionOfUse"];
        $CityOfUse = $arFilds["CityOfUse"];
        $DriversRestriction = $arFilds["DriversRestriction"];
        $FollowToRegistration = false;
        $Mark = $arFilds["Mark"];
        $Model = $arFilds["Model"];
        $Category = $arFilds["Category"];
        $Type = $arFilds["Type"];
        $RegistrationRussia = true;
        $Power = $arFilds["Power"];
        $Purpose = 'Личные';
        $LicensePlate = $arFilds["LicensePlate"];

        $VIN = $arFilds["VIN"];
        $number_bodywork = $arFilds["number_bodywork"];
        $number_chassis = $arFilds["number_chassis"];

        $WithUseTrailer = false;
        $YearIssue = $arFilds["YearIssue"];

        $LastName = $arFilds["Owner_calc"]["LastName"];
        $FirstName = $arFilds["Owner_calc"]["FirstName"];
        $MiddleName = $arFilds["Owner_calc"]["MiddleName"];
        $BirthDate = $arFilds["Owner_calc"]["BirthDate"];
        $NumberPass = $arFilds["Owner_calc"]["Number"];
        $SeriaPass = $arFilds["Owner_calc"]["Seria"];

        $dateStart = date("Y-m-d\TH:i:s", strtotime($arFilds["PolicyBeginDate"]));

        $hours = strtotime('+ 72 hours');
        $CalcEOsago = array(
            'PartnerInfo' => array(
                'UPID' => $UPID, // Идентификатор попытки заключения договора
            ),
            'Transport' => array( // Атрибуты страхуемого ТС
                'Mark' => $Mark, // Марка ТС
                'Model' => $Model,  // Модель ТС
                'Category' => $Category, // Категория ТС : B - легковые
                'Type' => $Type,  // Тип ТС
                'RegistrationRussia' => $RegistrationRussia, // Булевый признак признак регистрации ТС в России
                'Power' => $Power, // Мощность (л.с.)
                'Purpose' => $Purpose, // Как планируется использовать
                'LicensePlate' => $LicensePlate, // Регистрационный номер ТС
                'WithUseTrailer' => $WithUseTrailer,  // Признак использования данным ТС прицепа
                'YearIssue' => $YearIssue, // Год выпуска авто
            ),
            'ContractType' => $ContractType, // Тип договора
            'PolicyBeginDate' => $dateStart, // Дата начала действия данного договора с учётом добавления 72 часов от даты выдачи полиса
            'InsuranceMonth' => $InsuranceMonth, // Длительность действия данного договора в месяцах
            'RegionOfUse' => $RegionOfUse, // Субъект РФ, в котором будет использоваться страхуемое ТС
            'CityOfUse' => $CityOfUse, // Город РФ, в котором будет использоваться страхуемое ТС
            'DriversRestriction' => $DriversRestriction, // Булевый признак ограничения на лиц допущенных к управлению
            'FollowToRegistration' => $FollowToRegistration, // Булевый признак следования ТС до места регистрации
        );
        if ($VIN) {
            $CalcEOsago['Transport']['VIN'] = $VIN;
        } else if ($number_bodywork) {
            $CalcEOsago['Transport']['BodyNumber'] = $number_bodywork;
        } else if ($number_chassis) {
            $CalcEOsago['Transport']['ChassisNumber'] = $number_chassis;
        }

        if ($LastName) {
            $CalcEOsago['TransportOwner'] = array( // Атрибуты по собственникуфизику
                'LastName' => $LastName, // Фамилия собственника
                'FirstName' => $FirstName, // Имя собственника
                'MiddleName' => $MiddleName, // Отчество собственника
                'BirthDate' => $BirthDate, // Дата рождения собственника : 1986-03-22
                'PersonDocument' => array( // Атрибуты по паспорту собственника
                    'Number' => $NumberPass, // Номер паспорта собственника
                    'Seria' => $SeriaPass, // Серия паспорта собственника
                ),
            );
        }
        if ($arFilds["Drivers"]) {
            $driverFirst = $arFilds["Drivers"]["Driver"][0];
            $CalcEOsago['TransportOwner'] = array( // Атрибуты по собственникуфизику
                'LastName' => $driverFirst["LastName"], // Фамилия собственника
                'FirstName' => $driverFirst["FirstName"], // Имя собственника
                'MiddleName' => $driverFirst["MiddleName"], // Отчество собственника
                'BirthDate' => $driverFirst["BirthDate"], // Дата рождения собственника : 1986-03-22
                'DriverDocument' => $driverFirst["DriverDocument"], // Атрибуты по водительскому собственника
            );
            $CalcEOsago['Drivers'] = $arFilds["Drivers"];
        }
        $CalcEOsago['UnwantedClient'] = true; // Признак переданного нам стороннего клиента (Заполняется в рамках процесса СК1СК2 в случае обработки стороннего договора)
        $CalcEOsago['DateRequest'] = gmdate('Y-m-d\TH:i:s'); // Дата и время запроса
        if ($CalcEOsago) {
            $result = new AuthSoap($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
            $result->__setAuth($this->login, $this->pass);
            $result->__soapCall("CalcEOsago", array(
                'CalcEOsago' => $CalcEOsago,
            ));
            $req = $result->__last_response;
            $resp = $result->__last_request;

            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);

            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/CalcEOsago_response.xml', print_r($req, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/CalcEOsago_request.xml', print_r($resp, true));

        }
        if (!empty($responseArray["soapBody"]["CalcEOsagoResponse"])) {
            $info = $responseArray["soapBody"]["CalcEOsagoResponse"];
            return array('success' => true, 'info' => $info);
            //return response()->json(['success' => true, 'info'=>$info]);
        } else {
            return array('success' => false, 'error' => 'Сервис временно не доступен');
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/CalcEOsago-error.xml', print_r($responseArray, true));
        }
        /*
        При получении ответа по методу расчёта СП договора необходимо анализировать выходной
        параметр InsuranceBonus:
        ❖ если InsuranceBonus=0, то завершать процесс
        ❖ если InsuranceBonus<>0, то вызывать метод loadContractEosago (с указанием UPID,
        IDCheckTS, двух IDCheckInsurerOwner, IDCheckDriver и CalcRef из ответов
        соответствующих методов)
        */
    }

    public function CalcEOsago($arFilds) // Используется для расчёта страховой премии по договору
    {
        $UPID = $arFilds["UPID"];
        $ContractType = $arFilds["ContractType"];
        $InsuranceMonth = 12;
        $RegionOfUse = $arFilds["RegionOfUse"];
        $CityOfUse = $arFilds["CityOfUse"];
        $DriversRestriction = $arFilds["DriversRestriction"];
        $FollowToRegistration = false;
        $Mark = $arFilds["Mark"];
        $Model = $arFilds["Model"];
        $Category = $arFilds["Category"];
        $Type = $arFilds["Type"];
        $RegistrationRussia = true;
        $Power = $arFilds["Power"];
        $Purpose = 'Личные';
        $LicensePlate = $arFilds["LicensePlate"];

        $VIN = $arFilds["VIN"];
        $number_bodywork = $arFilds["number_bodywork"];
        $number_chassis = $arFilds["number_chassis"];

        $WithUseTrailer = false;
        $YearIssue = $arFilds["YearIssue"];

        $LastName = $arFilds["Owner"]["LastName"];
        $FirstName = $arFilds["Owner"]["FirstName"];
        $MiddleName = $arFilds["Owner"]["MiddleName"];
        $BirthDate = $arFilds["Owner"]["BirthDate"];
        $NumberPass = $arFilds["Owner"]["Number"];
        $SeriaPass = $arFilds["Owner"]["Seria"];

        $dateStart = date("Y-m-d\TH:i:s", strtotime($arFilds["PolicyBeginDate"]));

        $hours = strtotime('+ 72 hours');
        $CalcEOsago = array(
            'PartnerInfo' => array(
                'UPID' => $UPID, // Идентификатор попытки заключения договора
            ),
            'Transport' => array( // Атрибуты страхуемого ТС
                'Mark' => $Mark, // Марка ТС
                'Model' => $Model,  // Модель ТС
                'Category' => $Category, // Категория ТС : B - легковые
                'Type' => $Type,  // Тип ТС
                'RegistrationRussia' => $RegistrationRussia, // Булевый признак признак регистрации ТС в России
                'Power' => $Power, // Мощность (л.с.)
                'Purpose' => $Purpose, // Как планируется использовать
                'LicensePlate' => $LicensePlate, // Регистрационный номер ТС
                'WithUseTrailer' => $WithUseTrailer,  // Признак использования данным ТС прицепа
                'YearIssue' => $YearIssue, // Год выпуска авто
            ),
            'ContractType' => $ContractType, // Тип договора
            'PolicyBeginDate' => $dateStart, // Дата начала действия данного договора с учётом добавления 72 часов от даты выдачи полиса
            'InsuranceMonth' => $InsuranceMonth, // Длительность действия данного договора в месяцах
            'RegionOfUse' => $RegionOfUse, // Субъект РФ, в котором будет использоваться страхуемое ТС
            'CityOfUse' => $CityOfUse, // Город РФ, в котором будет использоваться страхуемое ТС
            'DriversRestriction' => $DriversRestriction, // Булевый признак ограничения на лиц допущенных к управлению
            'FollowToRegistration' => $FollowToRegistration, // Булевый признак следования ТС до места регистрации
        );
        if ($VIN) {
            $CalcEOsago['Transport']['VIN'] = $VIN;
        } else if ($number_bodywork) {
            $CalcEOsago['Transport']['BodyNumber'] = $number_bodywork;
        } else if ($number_chassis) {
            $CalcEOsago['Transport']['ChassisNumber'] = $number_chassis;
        }

        if ($LastName) {
            $CalcEOsago['TransportOwner'] = array( // Атрибуты по собственникуфизику
                'LastName' => $LastName, // Фамилия собственника
                'FirstName' => $FirstName, // Имя собственника
                'MiddleName' => $MiddleName, // Отчество собственника
                'BirthDate' => $BirthDate, // Дата рождения собственника : 1986-03-22
                'PersonDocument' => array( // Атрибуты по паспорту собственника
                    'Number' => $NumberPass, // Номер паспорта собственника
                    'Seria' => $SeriaPass, // Серия паспорта собственника
                ),
            );
        }
        if ($arFilds["Drivers"]) {
            $driverFirst = $arFilds["Drivers"]["Driver"][0];
            $CalcEOsago['TransportOwner'] = array( // Атрибуты по собственникуфизику
                'LastName' => $driverFirst["LastName"], // Фамилия собственника
                'FirstName' => $driverFirst["FirstName"], // Имя собственника
                'MiddleName' => $driverFirst["MiddleName"], // Отчество собственника
                'BirthDate' => $driverFirst["BirthDate"], // Дата рождения собственника : 1986-03-22
                'DriverDocument' => $driverFirst["DriverDocument"], // Атрибуты по водительскому собственника
            );
            $CalcEOsago['Drivers'] = $arFilds["Drivers"];
        }
        $CalcEOsago['UnwantedClient'] = false; // Признак переданного нам стороннего клиента (Заполняется в рамках процесса СК1СК2 в случае обработки стороннего договора)
        $CalcEOsago['DateRequest'] = gmdate('Y-m-d\TH:i:s'); // Дата и время запроса
        if ($CalcEOsago) {
            $result = new AuthSoap($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
            $result->__setAuth($this->login, $this->pass);
            $result->__soapCall("CalcEOsago", array(
                'CalcEOsago' => $CalcEOsago,
            ));
            $req = $result->__last_response;
            $resp = $result->__last_request;

            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);

            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/CalcEOsago_response.xml', print_r($req, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/CalcEOsago_request.xml', print_r($resp, true));

        }
        if (!empty($responseArray["soapBody"]["CalcEOsagoResponse"])) {
            $info = $responseArray["soapBody"]["CalcEOsagoResponse"];
            return array('success' => true, 'info' => $info);
            //return response()->json(['success' => true, 'info'=>$info]);
        } else {
            return array('success' => false, 'error' => 'Сервис временно не доступен');
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/CalcEOsago-error.xml', print_r($responseArray, true));
        }
        /*
        При получении ответа по методу расчёта СП договора необходимо анализировать выходной
        параметр InsuranceBonus:
        ❖ если InsuranceBonus=0, то завершать процесс
        ❖ если InsuranceBonus<>0, то вызывать метод loadContractEosago (с указанием UPID,
        IDCheckTS, двух IDCheckInsurerOwner, IDCheckDriver и CalcRef из ответов
        соответствующих методов)
        */
    }

    public function PartnerCalcEOsago($arFilds)
    {
        //$autoAlfa = new OSAGOlists;
        $UPID = $arFilds["UPID"];
        $ContractType = $arFilds["ContractType"];
        $InsuranceMonth = 12;
        if (!strpos('---' . $arFilds['RegionOfUse'], 'г ') === false) {
            $RegionOfUse = explode('г ', $arFilds["RegionOfUse"])[1];
        } else {
            $RegionOfUse = $arFilds["RegionOfUse"];
        }
        $CityOfUse = $arFilds["CityOfUse"];
        if(!strpos('----' . $CityOfUse, 'г ') === false){
            $exCity = explode(' ', $CityOfUse);
            $CityOfUse = $exCity[0].'. '.$exCity[1];
        }
        $DriversRestriction = $arFilds["DriversRestriction"];
        $FollowToRegistration = false;
        $Mark = strtoupper($arFilds["Mark"]);
        $Model = strtoupper($arFilds["Model"]);
        $Category = $arFilds["Category"];
        $Type = $arFilds["Type"];
        /*$markInfo = $autoAlfa->listMark('get', $Mark);
        if($markInfo["success"]){
            $Mark = $markInfo["mark"]["value"];
            $modelInfo = $autoAlfa->listModel('get', $markInfo["mark"]["id"], $Model);
            if($modelInfo["success"]){
                $Model = $modelInfo["model"]["value"];
                if(!empty($modelInfo["model"]["category"])){
                    $Category = $modelInfo["model"]["category"];
                }
                if(!empty($modelInfo["model"]["type"])){
                    $Type = $modelInfo["model"]["type"];
                }
            }else{
                return array('success' => false, 'error' => 'Не правильная модель');
            }
        }else{
            return array('success' => false, 'error' => 'Не правильная марка');
        }*/
        $RegistrationRussia = true;
        $Power = $arFilds["Power"];
        $Purpose = 'Личные';
        $LicensePlate = $arFilds["LicensePlate"];

        $VIN = $arFilds["VIN"];
        $number_bodywork = $arFilds["number_bodywork"];
        $number_chassis = $arFilds["number_chassis"];

        $WithUseTrailer = false;
        $YearIssue = $arFilds["YearIssue"];

        $documentsType  = '';
        if ($arFilds["type_calc"] == 'min') {
            if (!empty($arFilds["Drivers"])) {
                $driverFirst = $arFilds["Drivers"]["Driver"][0];
                $LastName = $driverFirst["LastName"];
                $FirstName = $driverFirst["FirstName"];
                $MiddleName = $driverFirst["MiddleName"];
                $BirthDate = $driverFirst["BirthDate"];
                $expDate = $driverFirst["ExperienceDate"];
                $NumberPass = $driverFirst["DriverDocument"]["Number"];
                $SeriaPass = $driverFirst["DriverDocument"]["Seria"];
                $documentsType = 'DriverDocument';
            } else {
                $LastName = $arFilds["Owner_calc"]["LastName"];
                $FirstName = $arFilds["Owner_calc"]["FirstName"];
                $MiddleName = $arFilds["Owner_calc"]["MiddleName"];
                $BirthDate = $arFilds["Owner_calc"]["BirthDate"];
                $NumberPass = $arFilds["Owner_calc"]["Number"];
                $SeriaPass = $arFilds["Owner_calc"]["Seria"];
                $expDate = '';
                $documentsType = 'PersonDocument';
            }
        } else {
            $LastName = $arFilds["Owner"]["LastName"];
            $FirstName = $arFilds["Owner"]["FirstName"];
            $MiddleName = $arFilds["Owner"]["MiddleName"];
            $BirthDate = $arFilds["Owner"]["BirthDate"];
            $NumberPass = $arFilds["Owner"]["Number"];
            $SeriaPass = $arFilds["Owner"]["Seria"];
            $documentsType = 'PersonDocument';
            if (!empty($arFilds["Drivers"])) {
                $expDate = $arFilds["Drivers"]["Driver"][0]["ExperienceDate"];
            } else {
                $expDate = '';
            }
        }

        $PolicyBeginDate = date("Y-m-d\TH:i:s", strtotime($arFilds["PolicyBeginDate"]));
        $dateStart = date("Y-m-d", strtotime($arFilds["PolicyBeginDate"]));
        $years = strtotime($arFilds["PolicyBeginDate"] . ' + 12 month - 1 day');
        $dateEnd = gmdate('Y-m-d', $years);

        $mailCalc = 'test2Calc@mail.ru';
        if(!empty($arFilds["email"])){
            $mailCalc = $arFilds["email"];
        }

        $CalcEOsago = [
            "PartnerInfo" => [
                "UPID" => $UPID,
            ],
            "ContractType" => $ContractType,
            "PrevPolicySerial" => $arFilds["old_seria_police"],
            "PrevPolicyNumber" => $arFilds["old_number_police"],
            "PolicyBeginDate" => $PolicyBeginDate,
            "InsuranceMonth" => $InsuranceMonth,
            "RegionOfUse" => $RegionOfUse,
            "CityOfUse" => $CityOfUse,
            "DriversRestriction" => $DriversRestriction,
            "FollowToRegistration" => $FollowToRegistration,
            "FlagrantViolation" => false,
            "Transport" => [
                "Mark" => strtoupper($Mark),
                "Model" => strtoupper($Model),
                "Category" => $Category,
                "Type" => $Type,
                "RegistrationRussia" => $RegistrationRussia,
                "Power" => $Power,
                "Purpose" => $Purpose,
                "IsTaxi" => false,
                "LicensePlate" => $LicensePlate,
                "WithUseTrailer" => $WithUseTrailer,
                "YearIssue" => $YearIssue,
            ],
            "TransportInsurer" => [
                "LastName" => $LastName,
                "FirstName" => $FirstName,
                "MiddleName" => $MiddleName,
                "BirthDate" => $BirthDate,
                "email" => $mailCalc,
                $documentsType => [
                    "Number" => $NumberPass,
                    "Seria" => $SeriaPass
                ],
            ],
            "TransportOwner" => [
                "LastName" => $LastName,
                "FirstName" => $FirstName,
                "MiddleName" => $MiddleName,
                "BirthDate" => $BirthDate,
                $documentsType => [
                    "Number" => $NumberPass,
                    "Seria" => $SeriaPass
                ],
            ],
            "PeriodList" => [
                "Period" => [
                    "StartDate" => $dateStart,
                    "EndDate" => $dateEnd,
                ],
            ],
            "UnwantedClient" => false,
        ];
        if ($VIN) {
            $CalcEOsago['Transport']['VIN'] = $VIN;
        } else if ($number_bodywork) {
            $CalcEOsago['Transport']['BodyNumber'] = $number_bodywork;
        } else if ($number_chassis) {
            $CalcEOsago['Transport']['ChassisNumber'] = $number_chassis;
        }
        if (!empty($arFilds["Drivers"])) {
            $CalcEOsago['Drivers'] = $arFilds["Drivers"];
        }
        if ($arFilds["type_calc"] != 'min') {
            if (!empty($arFilds["Drivers"])) {
                $driverFirst = $arFilds["Drivers"]["Driver"][0];
                $CalcEOsago['TransportOwner']['DriverDocument']['Number'] = $driverFirst["DriverDocument"]["Number"];
                $CalcEOsago['TransportOwner']['DriverDocument']['Seria'] = $driverFirst["DriverDocument"]["Seria"];
            }
        }

        if ($CalcEOsago) {
            $result = new AuthSoap($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
            $result->__setAuth($this->login, $this->pass);
            $result->__soapCall("PartnerCalcEOsago", array(
                "PartnerCalc" => $CalcEOsago
            ));
            $req = $result->__last_response;
            $resp = $result->__last_request;

            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);

            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/PartnerCalcEOsago_response.xml', print_r($req, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/PartnerCalcEOsago_request.xml', print_r($resp, true));
            //print_r($responseArray);
            if (!empty($responseArray["soapBody"]["PartnerCalcResponse"])) {
                $info = $responseArray["soapBody"]["PartnerCalcResponse"];
                return array('success' => true, 'info' => $info);
            } else {
                return array('success' => false, 'error' => 'Сервис временно не доступен');
                //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/PartnerCalcEOsago-error.xml', print_r($responseArray, true));
            }
        }
    }
}
