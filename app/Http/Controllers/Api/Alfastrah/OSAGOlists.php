<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\OsagoListMark;
use App\OsagoListModel;
use App\OsagoListTransportCategorye;
use App\OsagoListTransportPurpose;
use App\OsagoListTransportType;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class OSAGOlists extends Controller /* Сервис OSAGOlists */
{
    protected $url_servic = "https://b2b.alfastrah.ru/cxf/OSAGOlists?wsdl"; // wss-wssecurity - Отключен
    protected $login;

    public function __construct()
    {
        $this->login = config('app.login_alfa');
    }

    public function listMark($type = false, $markName = false)
    {
        switch ($type) {
            case 'update': //Раз в день вызывается с последующим запоминанием ответа сервиса. Используется для формирования выпадающего списка с известными нам марками ТС
                $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                $result->__soapCall("listMark", array('listMark' => array('login' => $this->login, 'is_eosago' => true)));
                $req = $result->__last_response;
                $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                $xml = simplexml_load_string($xml);
                $json = json_encode($xml);
                $responseArray = json_decode($json, true);

                $marks = array();
                if (!empty($responseArray["soapBody"]["ns2listMarkResponse"]["markEntry"]))
                    foreach ($responseArray["soapBody"]["ns2listMarkResponse"]["markEntry"] as $value) {
                        if (!empty($value["id"]) && !empty($value["value"])) {
                            $mark_on = OsagoListMark::where('id_mark', (int)$value["id"])->where('value', $value["value"])->first();
                            if (empty($mark_on->id_mark)) {
                                $marks[] = OsagoListMark::create([
                                    'id_mark' => (int)$value["id"],
                                    'value' => $value["value"],
                                ]);
                            }
                        }
                    }
                if (!empty($marks)) {
                    foreach ($marks as $value) {
                        if (!empty($value->id_mark))
                            $this::listModel('update', $value->id_mark);
                    }
                    return response()->json(['success' => true, 'marks' => $marks]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
            case 'elems':
                $list = OsagoListMark::getAllMark();
                return response()->json($list);
                break;
            case 'get':
                if (!empty($markName)) {
                    $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                    $result->__soapCall("listMark", array('listMark' => array('login' => $this->login, 'is_eosago' => true)));
                    $req = $result->__last_response;
                    $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                    $xml = simplexml_load_string($xml);
                    $json = json_encode($xml);
                    $responseArray = json_decode($json, true);
                    $markInfo = [];
                    if (!empty($responseArray["soapBody"]["ns2listMarkResponse"]["markEntry"]))
                        foreach ($responseArray["soapBody"]["ns2listMarkResponse"]["markEntry"] as $value) {
                            if (strtoupper($value["value"]) == strtoupper($markName)) {
                                $markInfo = $value;
                            }
                        }
                    if(!empty($markInfo)){
                        return ["success"=>true, "mark"=>$markInfo];
                    }else{
                        return ["success"=>false];
                    }
                } else {
                    return ["success"=>false];
                }
                break;
            default:
                abort(404, 'Page not found');
                break;
        }
    }

    public function listModel($type = false, $markId = false, $modelName = false) // , Request $request
    {
        /*if (empty($type)) {
            $type = $request->input('type');
        }
        if (empty($markId)) {
            $exParse = explode('|', $request->input('value'));
            if ($exParse[0]) {
                $markId = $exParse[0];
            } else {
                $markId = $request->input('value');
            }
        }*/
        switch ($type) {
            case 'update': //Раз в день вызывается с последующим запоминанием ответа сервиса. Используется для формирования выпадающего списка с известными нам моделями ТС при условии выбора марки из аналогичного списка
                //$markId=50011; //марка из списка альфа.
                if ($markId) {
                    $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                    $result->__soapCall("listModel", array('listModel' => array('login' => $this->login, 'markId' => $markId, 'is_eosago' => true)));
                    $req = $result->__last_response;
                    $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                    $xml = simplexml_load_string($xml);
                    $json = json_encode($xml);
                    $responseArray = json_decode($json, true);

                    $model = array();
                    if (!empty($responseArray["soapBody"]["ns2listModelResponse"]["modelEntry"]))
                        foreach ($responseArray["soapBody"]["ns2listModelResponse"]["modelEntry"] as $value) {
                            if (!empty($value["id"]) && !empty($value["value"])) {
                                $model_on = OsagoListModel::where('id_model', (int)$value["id"])->where('value', $value["value"])->where('id_mark', $markId)->first();
                                if (empty($model_on->id_model)) {
                                    $model[] = OsagoListModel::create([
                                        'id_model' => (int)$value["id"],
                                        'id_mark' => $markId,
                                        'value' => $value["value"],
                                        'category' => $value["category"],
                                        'type' => $value["type"],
                                    ]);
                                }
                            }
                        }
                } else {
                    $list_mark = OsagoListMark::getAllMark();
                    foreach ($list_mark as $mark) {
                        $markId = $mark->id_mark;
                        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                        $result->__soapCall("listModel", array('listModel' => array('login' => $this->login, 'markId' => $markId, 'is_eosago' => true)));
                        $req = $result->__last_response;
                        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                        $xml = simplexml_load_string($xml);
                        $json = json_encode($xml);
                        $responseArray = json_decode($json, true);

                        $model = array();
                        if (!empty($responseArray["soapBody"]["ns2listModelResponse"]["modelEntry"]))
                            foreach ($responseArray["soapBody"]["ns2listModelResponse"]["modelEntry"] as $value) {
                                if (!empty($value["id"]) && !empty($value["value"])) {
                                    $model_on = OsagoListModel::where('id_model', (int)$value["id"])->where('value', $value["value"])->where('id_mark', (int)$markId)->first();
                                    if (empty($model_on->id_model)) {
                                        $model[] = OsagoListModel::create([
                                            'id_model' => (int)$value["id"],
                                            'id_mark' => (int)$markId,
                                            'value' => $value["value"],
                                            'category' => $value["category"],
                                            'type' => $value["type"],
                                        ]);
                                    }
                                }
                            }
                    }
                }

                if (!empty($model)) {
                    return response()->json(['success' => true, 'models' => $model]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
            case 'elems':
                $list = OsagoListModel::getListMoldel($markId);
                return response()->json($list);
                break;
            case 'get':
                if(!empty($modelName)){
                    $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                    $result->__soapCall("listModel", array('listModel' => array('login' => $this->login, 'markId' => $markId, 'is_eosago' => true)));
                    $req = $result->__last_response;
                    $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                    $xml = simplexml_load_string($xml);
                    $json = json_encode($xml);
                    $responseArray = json_decode($json, true);

                    $modelInfo = [];
                    if (!empty($responseArray["soapBody"]["ns2listModelResponse"]["modelEntry"]))
                        foreach ($responseArray["soapBody"]["ns2listModelResponse"]["modelEntry"] as $value) {
                            if (strtoupper($value["value"]) == strtoupper($modelName)) {
                                $modelInfo = $value;
                            }
                        }
                    if(!empty($modelInfo)){
                        return ["success"=>true, "model"=>$modelInfo];
                    }else{
                        return ["success"=>false];
                    }
                } else {
                    return ["success"=>false];
                }
                break;
            default:
                abort(404, 'Page not found');
                break;
        }
    }

    public function listTransportCategory($type = false, $category = false)
    {
        switch ($type) {
            case 'update': //Раз в день вызывается с последующим запоминанием ответа сервиса. Используется для формирования выпадающего списка с перечнем категорий ТС
                //$category=8; //Идентификатор категории ТС
                $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                if ($category) {
                    $result->__soapCall("listTransportCategory", array('listTransportCategory' => array('login' => $this->login, 'categoryId' => $category)));
                } else {
                    $result->__soapCall("listTransportCategory", array('listTransportCategory' => array('login' => $this->login)));
                }
                $req = $result->__last_response;
                $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                $xml = simplexml_load_string($xml);
                $json = json_encode($xml);
                $responseArray = json_decode($json, true);

                $tc = array();
                if (!empty($responseArray["soapBody"]["ns2listTransportCategoryResponse"]["TransportCategory"]))
                    foreach ($responseArray["soapBody"]["ns2listTransportCategoryResponse"]["TransportCategory"] as $value) {
                        if ((!empty($value["categoryId"]) && !empty($value["categoryName"])) || $value["categoryId"] == 0) {
                            $tc_on = OsagoListTransportCategorye::where('id_category', (int)$value["categoryId"])->where('name_category', $value["categoryName"])->first();
                            if (!$tc_on) {
                                $tc[] = OsagoListTransportCategorye::create([
                                    'id_category' => (int)$value["categoryId"],
                                    'name_category' => $value["categoryName"],
                                ]);
                            }
                        }
                    }
                if (!empty($tc)) {
                    return response()->json(['success' => true, 'tc' => $tc]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
            case 'elems':
                $list = OsagoListTransportCategorye::getListTC($category);
                return response()->json($list);
                break;
            default:
                abort(404, 'Page not found');
                break;
        }
    }

    public function listTransportPurpose($type = false, $purposeId = false)
    {
        switch ($type) {
            case 'update': //Используется для формирования выпадающего списка с перечнем целей использования данного ТС
                //$purposeId=8; //Идентификатор цели использования ТС
                $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                if ($purposeId) {
                    $result->__soapCall("listTransportPurpose", array('listTransportPurpose' => array('login' => $this->login, 'purposeId' => $purposeId)));
                } else {
                    $result->__soapCall("listTransportPurpose", array('listTransportPurpose' => array('login' => $this->login)));
                }
                $req = $result->__last_response;
                $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                $xml = simplexml_load_string($xml);
                $json = json_encode($xml);
                $responseArray = json_decode($json, true);

                $tp = array();
                if (!empty($responseArray["soapBody"]["ns2listTransportPurposeResponse"]["TransportPurpose"]))
                    foreach ($responseArray["soapBody"]["ns2listTransportPurposeResponse"]["TransportPurpose"] as $value) {
                        if (!empty($value["purposeId"]) && !empty($value["purposeName"])) {
                            $tp_on = OsagoListTransportPurpose::where('id_purpose', (int)$value["purposeId"])->where('name_purpose', $value["purposeName"])->first();
                            if (!$tp_on) {
                                $tp[] = OsagoListTransportPurpose::create([
                                    'id_purpose' => (int)$value["purposeId"],
                                    'name_purpose' => $value["purposeName"],
                                ]);
                            }
                        }
                    }
                if (!empty($tp)) {
                    return response()->json(['success' => true, 'tp' => $tp]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
            case 'elems':
                $list = OsagoListTransportPurpose::getListTP($purposeId);
                return response()->json($list);
                break;
            default:
                abort(404, 'Page not found');
                break;
        }
    }

    public function listTransportType($type = false, $categoryId = false, Request $request)
    {
        if (empty($type)) {
            $type = $request->input('type');
        }
        if (empty($categoryId)) {
            $exParse = explode('|', $request->input('id_cat'));
            if ($exParse[0]) {
                $categoryId = $exParse[0];
            } else {
                $categoryId = $request->input('id_cat');
            }
        }
        switch ($type) {
            case 'update': //Раз в день вызывается с последующим запоминанием ответа сервиса. Используется для формирования выпадающего списка с перечнем доступных категорий ТС
                //$categoryId=8; //Идентификатор категории ТС из справочника АльфаСтрахования
                $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                if ($categoryId) {
                    $result->__soapCall("listTransportType", array('listTransportType' => array('login' => $this->login, 'categoryId' => $categoryId)));
                } else {
                    $result->__soapCall("listTransportType", array('listTransportType' => array('login' => $this->login)));
                }
                $req = $result->__last_response;
                $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
                $xml = simplexml_load_string($xml);
                $json = json_encode($xml);
                $responseArray = json_decode($json, true);

                $ltt = array();
                if (!empty($responseArray["soapBody"]["ns2listTransportTypeResponse"]["TransportType"]))
                    foreach ($responseArray["soapBody"]["ns2listTransportTypeResponse"]["TransportType"] as $value) {
                        if ((!empty($value["typeId"]) && !empty($value["typeName"])) || $value["typeId"] == 0) {
                            $ltt_on = OsagoListTransportType::where('id_category', (int)$value["categoryId"])->where('id_type', (int)$value["typeId"])->where('name_type', $value["typeName"])->first();
                            if (!$ltt_on) {
                                $ltt[] = OsagoListTransportType::create([
                                    'id_category' => (int)$value["categoryId"],
                                    'id_type' => (int)$value["typeId"],
                                    'name_type' => $value["typeName"],
                                ]);
                            }
                        }
                    }
                if (!empty($ltt)) {
                    return response()->json(['success' => true, 'ltt' => $ltt]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
            case 'elems':
                $list = OsagoListTransportType::getListLTT($categoryId);
                return response()->json($list);
                break;
            default:
                abort(404, 'Page not found');
                break;
        }
    }
}
