<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Api\Alfastrah\AuthSoap;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

require_once($_SERVER["DOCUMENT_ROOT"] . "/soap_security.php");

class MerchantServices extends Controller // Используется для оплаты полиса клиентом
{
    protected $url_servic;
    protected $login;
    protected $pass;
    protected $urlSite;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            $this->url_servic = "https://b2b-test2.alfastrah.ru/cxf/partner/MerchantServices?wsdl";
        } else {
            $this->url_servic = "https://b2b.alfastrah.ru/cxf/MerchantServices?wsdl"; // wss-wssecurity - Включен
        }
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
        $this->urlSite = config('app.url');
    }

    public function onLogin($arFild)
    {
        /*
        · email - Email. Обязательный.
        · last_name - Фамилия. Обязательный.
        · name - Имя. Обязательный.
        · second_name - Отчество. Обязательный.
        · password - Пароль. Не обязательный, в случае отсутствия пароль будет сгенерирован.
        · confirm_password - Повтор пароля.
        · phone - Телефон. В любом формате, главное чтобы были все 11 цифр. Обязательный.
        · birthday - Дата рождения. Формат: `дд.мм.гггг`. Обязательный.
        · contract_number - Номер договора.
        · offer_accepted - Флаг принятия оферты. Возможные знаения: `Y`/`N`.
        · partner_name - Название партнера. Обязательный.
        */
        $url = 'https://www.alfastrah.ru/api/userPartner/';
        $params = array(
            'email' => $arFild["email"],
            'last_name' => $arFild["Insurer"]["LastName"],
            'name' => $arFild["Insurer"]["FirstName"],
            'second_name' => $arFild["Insurer"]["MiddleName"],
            'password' => '',
            'confirm_password' => '',
            'phone' => $arFild['phone'],
            'birthday' => $arFild["Insurer"]['BirthDate_old'],
            'contract_number' => $arFild['contract_number'],
            'offer_accepted' => $arFild['offer_accepted'],
            'partner_name' => $this->login
        );
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/userPartner_request.xml', print_r($params, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/userPartner_response.xml', print_r($result, true));

        return $result;
    }

    public function pay($arFild)
    {
        $merchantOrderNumber = $arFild["merchantOrderNumber"];
        $contract_number = $arFild['contract_number'];
        $result = new AuthSoap($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $minute = strtotime('+ 20 minutes');
        $result->__soapCall("registerOrder", array(
            "order" => array(
                "merchantOrderNumber" => $merchantOrderNumber,
                "description" => "Оплата страхового полиса " . $contract_number,
                "expirationDate" => gmdate('Y-m-d\TH:i:s', $minute),
                "returnUrl" => $this->urlSite . "/alfa/infoPay/good/" . $contract_number,
                "failUrl" => $this->urlSite . "/alfa/infoPay/false/" . $contract_number
            ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/registerOrder_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/registerOrder_response.xml', print_r($req, true));

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        if (!empty($responseArray["soapBody"]["ns1registerOrderResponse"]["return"]["formUrl"])) {
            $url = $responseArray["soapBody"]["ns1registerOrderResponse"]["return"]["formUrl"];
            return ['url' => $url, 'upid' => $arFild["UPID"]];
        } else {
            return ['url' => false];
        }
    }

    public function infoPayPage($slug, $contract, Request $request)
    {
        if (!empty($slug)) {
            $page = [];
            switch ($slug) {
                case 'good':
                    $page = [
                        'title' => 'Информация об оплате Альфа-страхования',
                        'slug' => 'pay',
                        'page_title' => 'Оплата прошла успешно'
                    ];
                    if (!empty($contract)) {
                        $find_order = Order::findOrderPolice($contract);
                        $find_order->update(["pay" => 1]);
                    }
                    break;
                case 'false':
                    $page = [
                        'title' => 'Информация об оплате Альфа-страхования',
                        'slug' => 'pay',
                        'page_title' => 'Ошибка оплаты'
                    ];
                    break;
                default:
                    abort(404, 'Page not found');
                    break;
            }
            if (!empty($page))
                return view('custom_page.payInfo', ['page' => (object)$page]);
        } else {
            abort(404, 'Page not found');
        }
    }
}
