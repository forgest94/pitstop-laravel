<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Controller;

class PaymentCross extends Controller
{
    protected $url_servic;
    protected $authUrl;
    protected $login;
    protected $pass;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            $this->url_servic = "https://b2b-test2.alfastrah.ru/msrv/payment/receipt";
            $this->authUrl = "https://b2b-test2.alfastrah.ru/msrv/oauth/token";
        } else {
            $this->url_servic = "https://b2b.alfastrah.ru/msrv/payment/receipt";
            $this->authUrl = "https://b2b.alfastrah.ru/msrv/oauth/token";
        }
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }

    public function OAuth()
    {
        $fieldsRequest = [
            "grant_type" => "password",
            "username" => $this->login,
            "password" => $this->pass
        ];
        $ch = curl_init($this->authUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $authTwo = curl_exec($ch);
        curl_close($ch);
        $authObj = json_decode($authTwo);
        return $authObj;
    }

    public function payReceipt($masterContractId, $crossList)
    {
        if (!empty($crossList) && !empty($masterContractId)) {
            $idsContracts = [];
            foreach ($crossList as $cross) {
                $idsContracts[] = $cross["contractId"];
            }
            $oAuth = $this->OAuth();
            $fieldsRequest = [
                "masterContractId" => $masterContractId,
                "contractList" => $idsContracts
            ];
            $header = [
                'Authorization: Bearer ' . $oAuth->access_token,
                'Content-type: application/json',
                'Content-length: ' . strlen(json_encode($fieldsRequest))
            ];
            $ch = curl_init($this->url_servic);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fieldsRequest));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            $payRec = curl_exec($ch);
            curl_close($ch);
            $payRecObj = json_decode($payRec, true);
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/receipt_fieldsRequest.txt', print_r($fieldsRequest, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/receipt_headerRequest.txt', print_r($header, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Alfastrah/receipt_resp.txt', print_r($payRecObj, true));
            if (!empty($payRecObj["id"])) {
                return ["success" => true, "infoReceipt" => $payRecObj];
            } else {
                return ["success" => false];
            }
        } else {
            return ["success" => false];
        }
    }

}
