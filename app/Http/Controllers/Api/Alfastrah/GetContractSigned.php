<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Api\Alfastrah\AuthSoap;
use App\Http\Controllers\Controller;

require_once($_SERVER["DOCUMENT_ROOT"]."/soap_security.php");

class GetContractSigned extends Controller
{
    protected $url_servic;
    protected $login;
    protected $pass;

    public function __construct ()
    {
        if(config('app.env') == 'local'){
            $this->url_servic = "https://b2b-test2.alfastrah.ru/cxf/partner/GetContractSigned?wsdl";
        }else{
            $this->url_servic = "https://b2b.alfastrah.ru/cxf/GetContractSigned?wsdl"; // wss-wssecurity - Включен
        }
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }

    public function GetContractSignedFun($arFilds){
        /*$arFilds["upid"] = '020243ba-6b2b-4519-92e9-66c809d57d43';
        $arFilds["policeID"] = '46164828';*/
        $result = new AuthSoap($this->url_servic, array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
        $result->__setAuth($this->login, $this->pass);
        $result->__soapCall("GetContractSigned", array(
            "GetContractSignedRequest"=>array(
                "UPID"=>$arFilds["upid"],
                "ContractId"=>$arFilds["policeID"],
            ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/GetContractSigned_response.xml', print_r($req, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"].'/logs/Alfastrah/GetContractSigned_request.xml', print_r($resp, true));
        echo '<pre>';
        print_r($responseArray);
        echo '</pre>';
        if(empty($responseArray["soapBody"]["soapFault"]["faultcode"])){

        }else{
            return ["success"=>false, "error"=>$responseArray["soapBody"]["soapFault"]["faultcode"]];
        }

    }
}
