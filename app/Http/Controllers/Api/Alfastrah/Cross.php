<?php

namespace App\Http\Controllers\Api\Alfastrah;

use App\Http\Controllers\Controller;

class Cross extends Controller
{
    protected $url_servic;
    protected $url_status;
    protected $authUrl;
    protected $url_save;
    protected $login;
    protected $pass;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            $this->url_servic = "https://b2b-test2.alfastrah.ru/msrv/cross/united/cross";
            $this->url_save = "https://b2b-test2.alfastrah.ru/msrv/cross/united/save-cross";
            $this->url_status = "https://b2b-test2.alfastrah.ru/msrv/cross/united/crossStatus";
            $this->authUrl = "https://b2b-test2.alfastrah.ru/msrv/oauth/token";
        } else {
            $this->url_servic = "https://b2b.alfastrah.ru/msrv/cross/united/cross";
            $this->url_save = "https://b2b.alfastrah.ru/msrv/cross/united/save-cross";
            $this->url_status = "https://b2b.alfastrah.ru/msrv/cross/united/crossStatus";
            $this->authUrl = "https://b2b.alfastrah.ru/msrv/oauth/token";
        }
        $this->login = config('app.login_alfa');
        $this->pass = config('app.pass_alfa');
    }

    public function OAuth()
    {
        $fieldsRequest = [
            "grant_type" => "password",
            "username" => $this->login,
            "password" => $this->pass
        ];
        $ch = curl_init($this->authUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $authTwo = curl_exec($ch);
        curl_close($ch);
        $authObj = json_decode($authTwo);
        return $authObj;
    }

    public function getCross($arFields)
    {
        /*
         * $client = new Client; //GuzzleHttp\Client
         * print_r($fieldsRequest);
        $auth = $client->post( 'https://b2b-test2.alfastrah.ru/msrv/oauth/token', $fieldsRequest);
        print_r($auth);*/
        /*$result = $client->get($this->url_servic, [
            'header' => [
                'Authorization'=> 'Bearer '.$authObj->access_token
            ],
            'contractId' => $arFields["merchantOrderNumber"]
        ]);*/
        $oAuth = $this->OAuth();
        $ch = curl_init($this->url_servic . '?contractId=' . $arFields["merchantOrderNumber"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $oAuth->access_token
        ));
        $cross = curl_exec($ch);
        curl_close($ch);
        $crossObj = json_decode($cross);
        if (!empty($crossObj->crossProductList)) {
            $arrCross = [];
            foreach ($crossObj->crossProductList as $itemBlockCross) {
                if (!empty($itemBlockCross->calculationList)) {
                    $arItemsCross = [];
                    foreach ($itemBlockCross->calculationList as $itemCross) {
                        $arItemsCross[] = [
                            "id" => $itemCross->id,
                            "name" => $itemCross->name,
                            "price" => $itemCross->price,
                        ];
                    }
                    if (!empty($arItemsCross))
                        $arrCross[] = [
                            "name_block" => $itemBlockCross->name,
                            "crossList" => $arItemsCross
                        ];
                }
            }
            if (!empty($arrCross)) {
                return ["success" => true, "list" => $arrCross, "calculationRequestId" => $crossObj->calculationRequestId];
            } else {
                return ["success" => false];
            }
        } else {
            return ["success" => false];
        }
    }

    public function saveCross($arFields)
    {
        if (!empty($arFields["crossList"])) {
            $idsCross = [];
            foreach ($arFields["crossList"] as $cross) {
                $idsCross[] = $cross["id"];
            }
            $oAuth = $this->OAuth();
            $fieldsRequest = [
                "calculationRequestId" => $arFields["calculationRequestId"],
                "calculationIdList" => $idsCross,
                "agent" => [
                    /*"contractId" => $arFields["contractId"],
                    "managerId" => ""*/
                    "contractId" => 815927,
                    "managerId" => 343424545
                ],
            ];
            $ch = curl_init($this->url_save);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fieldsRequest));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $oAuth->access_token,
                'Content-type: application/json',
                'Content-length: ' . strlen(json_encode($fieldsRequest))
            ));
            $cross = curl_exec($ch);
            curl_close($ch);
            $crossObj = json_decode($cross);
            if (!empty($crossObj->saveRequestId)) {
                return ["success" => true, "saveID" => $crossObj->saveRequestId];
            } else {
                return ["success" => false];
            }
        } else {
            return ["success" => false];
        }
    }

    public function crossStatus($saveRequestId)
    {
        if (!empty($saveRequestId)) {
            $oAuth = $this->OAuth();
            $ch = curl_init($this->url_status . '?saveRequestId=' . $saveRequestId);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $oAuth->access_token
            ));
            $cross = curl_exec($ch);
            curl_close($ch);
            $crossObj = json_decode($cross, true);
            if (!empty($crossObj["crossStatuses"])) {
                $waitArray = [];
                foreach ($crossObj["crossStatuses"] as $crossStatus) {
                    if($crossStatus["statusName"] == 'На сохранении'){
                        $waitArray[] = $crossStatus;
                    }
                }
                if(!empty($waitArray)){
                    $recursStatus = $this::crossStatus($saveRequestId);
                    return $recursStatus;
                }else{
                    $crossStatusArray = [];
                    foreach ($crossObj["crossStatuses"] as $crossStatus) {
                        if ($crossStatus["statusName"] == 'Сохранен'){
                            $crossStatusArray[$crossStatus["calculationId"]] = $crossStatus;
                        }
                    }
                    if (!empty($crossStatusArray)) {
                        return ["success" => true, "cross_good_status" => $crossStatusArray];
                    } else {
                        return ["success" => false];
                    }
                }
            } else {
                return ["success" => false];
            }
        } else {
            return ["success" => false];
        }
    }
}
