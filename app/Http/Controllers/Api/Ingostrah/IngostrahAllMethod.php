<?php

namespace App\Http\Controllers\Api\Ingostrah;

use Illuminate\Routing\Controller;

ini_set('default_socket_timeout', '600');

class IngostrahAllMethod extends Controller
{
    protected $url_servic;
    protected $login;
    protected $pass;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            //$this->url_servic = "https://aisws.ingos.ru/sales-vsv2/SalesService.svc?wsdl";
            $this->url_servic = "https://aisws.ingos.ru/sales-test/SalesService.svc?wsdl";
        } else {
            $this->url_servic = "https://aisws.ingos.ru/sales/SalesService.svc?wsdl";
        }
        $this->login = config('app.login_ingo');
        $this->pass = config('app.pass_ingo');
    }

    public function kladr($query, $type)
    {
        $kladr = '';
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://kladr-api.ru/api.php?contentType=' . $type . '&query=' . $query);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);
            $result_kl = (array)json_decode($out)->result[1];
            $kladr = substr($result_kl["id"], 0, -2);
            curl_close($curl);
        }
        return $kladr;
    }

    public function Login()
    {
        /*
        Для возможности работы на сервисе, необходимо пройти процедуру авторизации. Для этого
        используется функция Login. Необходимо вызывать в начале каждого сеанса работы с сервисом. В
        ответ пользователь получает ключ сессии (Token), который необходимо указывать при вызове
        каждого последующего метода в рамках текущей сессии.
        */
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("Login", array('LoginRequest' => array('User' => $this->login, 'Password' => $this->pass)));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/Login_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/Login_response.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);

        /*echo '<pre>';
        print_r($responseArray);
        echo '</pre>';*/

        $info = $responseArray["sBody"]["LoginResponse"];

        $error = $info["ResponseStatus"]["ErrorCode"];
        if (!empty($info) && $error == 0) {
            return $info["ResponseData"]["SessionToken"];
        } else {
            return false;
        }
    }

    public function GetDicti($type = false, $value = false)
    {
        $token = $this::Login();
        if (!$token) return false;
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("GetDicti", array('GetDictiRequest' => array(
            'SessionToken' => $token,
            'Product' => 753518300,
        )
        ));
        $req = $result->__last_response;
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);

        if ($responseArray["sBody"]["GetDictiResponse"]["ResponseStatus"]["ErrorCode"] > 0) {
            return array('success' => false, 'error' => 'Сервис временно не доступен');
        } else {
            $arModelF = $responseArray["sBody"]["GetDictiResponse"]["ResponseData"]["Dictionary"]["ModelList"]["Model"]["Model"];
            $arType = array();
            $arMark = array();
            $arModels = array();

            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/models_arr.xml', print_r($arModelF, true));
            foreach ($arModelF as $item) {
                foreach ($item as $key => $item2) {
                    if ($key == 'Model') {
                        foreach ($item2 as $item3) {
                            foreach ($item3 as $key2 => $item4) {
                                if ($key2 == 'Model') {
                                    foreach ($item4 as $item5) {
                                        if (key($item5) == '@attributes') {
                                            $arModels[$item3["@attributes"]["ISN"]][] = $item5["@attributes"];
                                        }
                                    }
                                }
                            }
                            $arMark[$item["@attributes"]["ISN"]][] = $item3["@attributes"];
                        }
                    }
                }
                $arType[] = $item["@attributes"];
            }
        }
        if ($type) {
            switch ($type) {
                case 'category':
                    $result = array();
                    foreach ($arType as $type) {
                        //$alfaType = OsagoListTransportCategorye::whereRaw("name_category like '%$input'")->where('name_category', $value)->orderBy('id_category', 'asc')->first();
                        //if (empty($alfaType->name_category)) continue;
                        if (!strpos('-----' . mb_strtolower($value), mb_strtolower($type["Name"])) === false)
                            $result[] = $type["ISN"];
                    }

                    if (!empty($result)) {
                        return $result[0];
                    } else {
                        return false;
                    }
                    break;
                case 'mark':
                    $result = array();
                    foreach ($arMark[$value[0]] as $mark) {
                        $input = str_replace(' ', '', preg_replace('![^\w\d\sа-яА-Я]*!', '', mb_strtolower($mark["Name"])));
                        $mark_en = str_replace(' ', '', preg_replace('![^\w\d\sа-яА-Я]*!', '', mb_strtolower($value[1])));
                        if ($input == '' || strlen($input) == 1) continue;
                        //$alfaMark = OsagoListMark::whereRaw("value like '%$input'")->where('value', $value[1])->orderBy('id_mark', 'asc')->first();
                        //if(empty($alfaMark->value))continue;
                        if ($mark_en == $input)
                            $result[] = $mark["ISN"];
                    }

                    if (!empty($result)) {
                        return $result[0];
                    } else {
                        return false;
                    }
                    break;
                case 'model':
                    $result = array();
                    foreach ($arModels[$value[0]] as $model) {
                        $input = str_replace(' ', '', preg_replace('![^\w\d\sа-яА-Я]*!', '', mb_strtolower($model["Name"])));
                        $model_en = str_replace(' ', '', preg_replace('![^\w\d\sа-яА-Я]*!', '', mb_strtolower($value[1])));
                        if ($input == '' || strlen($input) == 1) continue;
                        //$alfaModel = OsagoListModel::whereRaw("value like '%$input'")->where('value', $value[1])->orderBy('id_model', 'asc')->first();
                        //if(empty($alfaModel->value))continue;
                        if ($model_en == $input)
                            $result[] = $model["ISN"];
                    }
                    if (!empty($result)) {
                        return $result[0];
                    } else {
                        return false;
                    }
                    break;
            }
        } else {
            echo '<pre>';
            print_r($responseArray["sBody"]);
            echo '</pre>';
            return array('success' => false, 'error' => 'Указаны неверные данные');
        }
    }

    public function GetTariffMin($arFields = false)
    {
        $token = $arFields["token"];


        $cat = array();
        $mark = array();
        $model = array();
        $kbm = 0;
        $sum = 0;

        $cat = $this::GetDicti('category', $arFields["Category"]);
        if ($cat)
            $mark = $this::GetDicti('mark', array($cat, $arFields["Mark"]));
        if ($mark)
            $model = $this::GetDicti('model', array($mark, $arFields["Model"]));


        $kladrCity = $arFields["kladr_city"];
        if (!empty($model) && $kladrCity) {

            $dateStart = strtotime($arFields["PolicyBeginDate"]);
            $years = strtotime($arFields["PolicyBeginDate"] . ' + 12 month - 1 day');

            if (!empty($arFields["Drivers"]['Driver'])) {
                $risk = '<RiskCtg>28966116</RiskCtg>';
                $DriverList = '<DriverList>';
                $SubjectList = '<SubjectList>';
                $i = 0;
                foreach ($arFields["Drivers"]['Driver'] as $val) {
                    $i++;
                    $DriverList .= '
                    <Driver>
                        <SbjRef>' . $i . '</SbjRef>
                        <DriverLicense>
                            <DocType>765912000</DocType>
                            <Serial>' . $val["DriverDocument"]["Seria"] . '</Serial>
                            <Number>' . $val["DriverDocument"]["Number"] . '</Number>
                        </DriverLicense>
                        <DrvDateBeg>' . $val["ExperienceDate"] . '</DrvDateBeg>
                    </Driver>
                    ';
                    $SubjectList .= '
                    <Subject SbjKey="' . $i . '">
                        <SbjType>Ф</SbjType>
                        <SbjResident>Y</SbjResident>
                        <CountryCode>643</CountryCode>
                        <FullName>' . $val["LastName"] . ' ' . $val["FirstName"] . ' ' . $val["MiddleName"] . '</FullName>
                        <BirthDate>' . $val["BirthDate"] . '</BirthDate>
                        <Address>
                            <CityCode>' . $kladrCity . '</CityCode>
                        </Address>
                    </Subject>
                    ';
                }
                $SubjectList .= '</SubjectList>';
                $DriverList .= '</DriverList>';

            } else {
                $DriverList = '';
                $SubjectList = '<SubjectList>';
                $risk = '<RiskCtg>28966316</RiskCtg>';
                $SubjectList .= '<Subject SbjKey="1">
                    <SbjType>Ф</SbjType>
                    <SbjResident>Y</SbjResident>
                    <CountryCode>643</CountryCode>
                    <FullName>' . $arFields["Owner_calc"]["LastName"] . ' ' . $arFields["Owner_calc"]["FirstName"] . ' ' . $arFields["Owner_calc"]["MiddleName"] . '</FullName>
                    <BirthDate>' . $arFields["Owner_calc"]["BirthDate"] . '</BirthDate>
                    <Address>
                        <CityCode>' . $kladrCity . '</CityCode>
                    </Address>
                </Subject>';
                $SubjectList .= '</SubjectList>';
            }
            $VIN = $arFields["VIN"];
            $number_bodywork = $arFields["number_bodywork"];
            $number_chassis = $arFields["number_chassis"];
            $nuber_auto = '';
            if ($VIN) {
                $nuber_auto = '<VIN>' . $VIN . '</VIN>';
            } else if ($number_bodywork) {
                $nuber_auto = '<BodyNum>' . $number_bodywork . '</BodyNum>';
            } else if ($number_chassis) {
                $nuber_auto = '<ChassisNum>' . $number_chassis . '</ChassisNum>';
            }
            $propertyArr = new \SoapVar('
            <TariffParameters>
                <Agreement>
                    <General>
                    <Product>753518300</Product>
                    <DateBeg>' . gmdate('Y-m-d\TH:i:s', $dateStart) . '</DateBeg>
                    <DateEnd>' . gmdate('Y-m-d', $years) . '</DateEnd>
                    <CitySales>' . $kladrCity . '</CitySales>
                    <Individual>N</Individual>
                    <DISetting>3</DISetting>
                    </General>
                    ' . $SubjectList . '
                    <Insurer>
                        <SbjRef>1</SbjRef>
                    </Insurer>
                    <Owner>
                        <SbjRef>1</SbjRef>
                    </Owner>
                    ' . $DriverList . '
                    <Vehicle>
                        ' . $nuber_auto . '
                        <Model>' . $model . '</Model>
                        <Category>' . explode(' - ', $arFields["Category"])[0] . '</Category>
                        <EnginePowerHP>' . $arFields["EngCap"] . '</EnginePowerHP>
                        <Constructed>' . $arFields["YearIssue"] . '-01-01</Constructed>
                    </Vehicle>
                    <Condition>
                    <Liability>
                        <UseWithTrailer>N</UseWithTrailer>
                        ' . $risk . '
                        <UsageType>1381850903</UsageType>
                        <UsageTarget>
                            <Personal>Y</Personal>
                        </UsageTarget>
                        <PeriodList>
                        <Period>
                            <DateBeg>' . gmdate('Y-m-d', $dateStart) . '</DateBeg>
                            <DateEnd>' . gmdate('Y-m-d', $years) . '</DateEnd>
                        </Period>
                        </PeriodList>
                    </Liability>
                    </Condition>
                </Agreement>
            </TariffParameters>
            ', XSD_ANYXML);
            $result = new \SoapClient($this->url_servic, array(
                'trace' => true,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
            ));
            $result->__soapCall("GetTariff", array('GetTariffRequest' => array(
                'NeedList' => 1,
                'SessionToken' => $token,
                'TariffParameters' => $propertyArr,
            ),
            ));
            $req = $result->__last_response;
            $resp = $result->__last_request;
            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);


            /*echo '<pre>';
            print_r($responseArray);
            echo '</pre>';*/

            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetTariff_calculation_request.xml', print_r($resp, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetTariff_calculation_response.xml', print_r($req, true));

            if (empty($responseArray) || !$responseArray) {
                return array('success' => false, 'error' => 'Указаны неверные данные');
            }
            if ($responseArray["sBody"]["GetTariffResponse"]["ResponseStatus"]["ErrorCode"] == 0) {
                $tariff = $responseArray["sBody"]["GetTariffResponse"]["ResponseData"]["Tariff"];
                $CalcInfo = $tariff["CalcPremData"]["Result"]["CalcInfo"];
                $sum = $tariff["PremiumAmount"];
                if (!empty($tariff["CalcPremData"]["Result"]["ObjectList"]["Object"]["Risk"]["ElementList"]["Element"][13])) {
                    $infoCoeff = $tariff["CalcPremData"]["Result"]["ObjectList"]["Object"]["Risk"]["ElementList"]["Element"];
                    $coefficient = [];
                    foreach ($infoCoeff as $coeff) {
                        if (!strpos('sss' . $coeff["@attributes"]["Value"], '.') === false && strpos('sss' . $coeff["@attributes"]["Value"], '0.') === false && strpos('sss' . $coeff["@attributes"]["Value"], '1.') === false) {
                            $value_item = '0' . $coeff["@attributes"]["Value"];
                        } else {
                            $value_item = $coeff["@attributes"]["Value"];
                        }
                        switch ($coeff["@attributes"]["Name"]) {
                            case 'TRF_CL_BASE':
                                $coefficient["Tb"] = $value_item;
                                break;
                            case 'TRF_CL_BONUSMALUS':
                                $coefficient["Kbm"] = $value_item;
                                break;
                            case 'TRF_CL_POWER':
                                $coefficient["Km"] = $value_item;
                                break;
                            case 'TRF_CL_PERIOD':
                                $coefficient["Ks"] = $value_item;
                                $coefficient["Kp"] = $value_item;
                                break;
                            case 'TRF_CL_MULTIDRIVE':
                                $coefficient["Ko"] = $value_item;
                                break;
                            case 'TRF_CL_ABUSE':
                                $coefficient["Kn"] = $value_item;
                                break;
                            case 'TRF_CL_ZONE':
                                $coefficient["Kt"] = $value_item;
                                break;
                            case 'FR_USEWITHTRAILER':
                                $coefficient["Kpr"] = $value_item;
                                break;
                            case 'KVS_OSAGO':
                                $coefficient["Kvs"] = $value_item;
                                break;
                        }
                    }

                    return array('success' => true, 'Coefficients' => $coefficient, 'sum' => $sum);
                }
                /*if($tariff["CalcPremData"]["Result"]["SubjBonusList"]){
                    if($tariff["CalcPremData"]["Result"]["SubjBonusList"]["SubjBonus"]){
                        if(count($arFields["Drivers"]['Driver']) > 1){
                            $kbm = '0.'.explode('0',$tariff["CalcPremData"]["Result"]["SubjBonusList"]["SubjBonus"][0]["KBM"])[1];
                        }else{
                            $exKbm = explode('0',$tariff["CalcPremData"]["Result"]["SubjBonusList"]["SubjBonus"]["KBM"]);
                            if(count($exKbm) > 1){
                                $kbm = '0'.implode(".", $exKbm);
                            }else{
                                $exKbm = explode('1',$tariff["CalcPremData"]["Result"]["SubjBonusList"]["SubjBonus"]["KBM"]);
                                $kbm = '1'.implode(".", $exKbm);
                            }
                        }
                    }
                }*/
                return array('success' => false, 'error' => 'Сервис верменно не доступен');
            } else {
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetTariff_error_request-' . date('d_m_Y__H_i_s') . '.xml', print_r($resp, true));
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetTariff_error_response-' . date('d_m_Y__H_i_s') . '.xml', print_r($req, true));
                return array('success' => false, 'error' => $responseArray["sBody"]["GetTariffResponse"]["ResponseStatus"]["ErrorMessage"]);
            }
        } elseif (empty($model)) {
            return array('success' => false, 'error' => 'Марка или модель автомобиля не найдена в сервисе');
        } else {
            return array('success' => false, 'error' => 'Указаны неверные данные');
        }
    }

    public function GetTariff($arFields)
    {
        $token = $arFields["token"];
        $cat = array();
        $mark = array();
        $model = array();
        $kbm = 0;
        $sum = 0;

        $cat = $this::GetDicti('category', $arFields["Category"]);
        if ($cat)
            $mark = $this::GetDicti('mark', array($cat, $arFields["Mark"]));
        if ($mark)
            $model = $this::GetDicti('model', array($mark, $arFields["Model"]));


        $kladrCity = $arFields["kladr_city"];

        if (!empty($model) && $kladrCity) {
            $dateStart = strtotime($arFields["PolicyBeginDate"]);
            $years = strtotime($arFields["PolicyBeginDate"] . ' + 12 month - 1 day');
            $SubjectList = '';
            if (!empty($arFields["Drivers"]['Driver'])) {
                $risk = '<RiskCtg>28966116</RiskCtg>';
                $DriverList = '<DriverList>';
                $i = 0;
                foreach ($arFields["Drivers"]['Driver'] as $key => $val) {
                    $i++;
                    $DriverList .= '
                    <Driver>
                        <SbjRef>' . $i . '</SbjRef>
                        <DriverLicense>
                            <DocType>765912000</DocType>
                            <Serial>' . $val["DriverDocument"]["Seria"] . '</Serial>
                            <Number>' . $val["DriverDocument"]["Number"] . '</Number>
                        </DriverLicense>
                        <DrvDateBeg>' . $val["ExperienceDate"] . '</DrvDateBeg>
                    </Driver>
                    ';
                    if ($i > 1) {
                        $SubjectList .= '
                        <Subject SbjKey="' . $i . '">
                            <SbjType>Ф</SbjType>
                            <SbjResident>Y</SbjResident>
                            <FullName>' . $val["LastName"] . ' ' . $val["FirstName"] . ' ' . $val["MiddleName"] . '</FullName>
                            <BirthDate>' . $val["BirthDate"] . '</BirthDate>
                            <CountryCode>643</CountryCode>
                            <IdentityDocument>
                                <DocType>765912000</DocType>
                                <Serial>' . $val["DriverDocument"]["Seria"] . '</Serial>
                                <Number>' . $val["DriverDocument"]["Number"] . '</Number>
                            </IdentityDocument>
                            <Address>
                                <CountryCode>643</CountryCode>
                                <CityCode>' . $kladrCity . '</CityCode>
                            </Address>
                        </Subject>
                        ';
                    }
                }
                $DriverList .= '</DriverList>';

            } else {
                $DriverList = '';
                $risk = '<RiskCtg>28966316</RiskCtg>';
                /*$SubjectList = '
                <Subject SbjKey="2">
                    <SbjType>Ф</SbjType>
                    <SbjResident>Y</SbjResident>
                    <FullName>'.$arFields["Owner"]["LastName"].' '.$arFields["Owner"]["FirstName"].' '.$arFields["Owner"]["MiddleName"].'</FullName>
                    <BirthDate>'.$arFields["Owner"]["BirthDate"].'</BirthDate>
                    <CountryCode>643</CountryCode>
                        <IdentityDocument>
                            <DocType>30363316</DocType>
                            <Serial>'.$arFields["Owner"]["Seria"].'</Serial>
                            <Number>'.$arFields["Owner"]["Number"].'</Number>
                        </IdentityDocument>
                    <Address>
                        <CountryCode>643</CountryCode>
                        <CityCode>'.$kladrCity.'</CityCode>
                    </Address>
                </Subject>';*/
            }
            if ($arFields["doc_ts"] == 'pts') {
                $type_doc_ts = 34709216;
            } else {
                $type_doc_ts = 34709116;
            }
            $VIN = $arFields["VIN"];
            $number_bodywork = $arFields["number_bodywork"];
            $number_chassis = $arFields["number_chassis"];
            $nuber_auto = '';
            if ($VIN) {
                $nuber_auto = '<VIN>' . $VIN . '</VIN>';
            } else if ($number_bodywork) {
                $nuber_auto = '<BodyNum>' . $number_bodywork . '</BodyNum>';
            } else if ($number_chassis) {
                $nuber_auto = '<ChassisNum>' . $number_chassis . '</ChassisNum>';
            }
            $street = '';
            $home = '';
            $apartament = '';
            if (!empty($arFields["kladr_address"]))
                $street = '<StreetCode>' . $arFields["kladr_address"] . '</StreetCode>';
            if ($arFields["Insurer"]["address"]["home"])
                $home = '<House>' . $arFields["Insurer"]["address"]["home"] . '</House>';
            if (!empty($arFields["Insurer"]["address"]["apartament"]))
                $apartament = '<Flat>' . $arFields["Insurer"]["address"]["apartament"] . '</Flat>';

            $propertyArr = new \SoapVar('
            <TariffParameters>
                <Agreement>
                        <General>
                            <Product>753518300</Product>
                            <DateBeg>' . gmdate('Y-m-d\TH:i:s', $dateStart) . '</DateBeg>
                            <DateEnd>' . gmdate('Y-m-d', $years) . '</DateEnd>
                            <CitySales>' . $kladrCity . '</CitySales>
                            <Individual>N</Individual>
                        </General>
                        <Insurer>
                            <SbjRef>1</SbjRef>
                            <MobilePhone>' . $arFields["phone"] . '</MobilePhone>
                            <Email>' . $arFields["email"] . '</Email>
                        </Insurer>
                        <Owner>
                            <SbjRef>1</SbjRef>
                        </Owner>
                        <SubjectList>
                            <Subject SbjKey="1">
                                <SbjType>Ф</SbjType>
                                <SbjResident>Y</SbjResident>
                                <FullName>' . $arFields["Insurer"]["LastName"] . ' ' . $arFields["Insurer"]["FirstName"] . ' ' . $arFields["Insurer"]["MiddleName"] . '</FullName>
                                <BirthDate>' . $arFields["Insurer"]["BirthDate"] . '</BirthDate>
                                <CountryCode>643</CountryCode>
                                    <IdentityDocument>
                                        <DocType>30363316</DocType>
                                        <Serial>' . chunk_split($arFields["Insurer"]["Seria"], 2, ' ') . '</Serial>
                                        <Number>' . $arFields["Insurer"]["Number"] . '</Number>
                                        <DocDate>' . gmdate('Y-m-d', strtotime($arFields["Insurer"]["Date_pass"])) . '</DocDate>
                                        <DocIssuedBy>' . $arFields["Insurer"]["For_doc_pass"] . '</DocIssuedBy>
                                    </IdentityDocument>
                                <Address>
                                    <CountryCode>643</CountryCode>
                                    <CityCode>' . $kladrCity . '</CityCode>
                                    ' . $street . '
                                    ' . $home . '
                                    ' . $apartament . '
                                </Address>
                            </Subject>
                            ' . $SubjectList . '
                        </SubjectList>
                        <Vehicle>
                            <Model>' . $model . '</Model>
                            <RegNum>' . $arFields["LicensePlate"] . '</RegNum>
                            <Constructed>' . $arFields["YearIssue"] . '-01-01</Constructed>
                            ' . $nuber_auto . '
                            <Category>' . explode(' - ', $arFields["Category"])[0] . '</Category>
                            <EnginePowerHP>' . $arFields["EngCap"] . '</EnginePowerHP>
                            <Document>
                                <DocType>' . $type_doc_ts . '</DocType>
                                <Serial>' . $arFields["DocCarSerial"] . '</Serial>
                                <Number>' . $arFields["DocCarNumber"] . '</Number>
                                <DocDate>' . gmdate('Y-m-d', strtotime($arFields["DocumentCarDate"])) . '</DocDate>
                            </Document>
                        </Vehicle>
                        <Condition>
                            <Liability>
                                ' . $risk . '
                                <UsageTarget>
                                    <Personal>Y</Personal>
                                </UsageTarget>
                                <UseWithTrailer>N</UseWithTrailer>
                                <UsageType>1381850903</UsageType>
                                <PeriodList>
                                <Period>
                                    <DateBeg>' . gmdate('Y-m-d', $dateStart) . '</DateBeg>
                                    <DateEnd>' . gmdate('Y-m-d', $years) . '</DateEnd>
                                </Period>
                                </PeriodList>
                            </Liability>
                        </Condition>
                        ' . $DriverList . '
                </Agreement>
            </TariffParameters>
            ', XSD_ANYXML);
            $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
            $result->__soapCall("GetTariff", array('GetTariffRequest' => array(
                'NeedList' => 0,
                'SessionToken' => $token,
                'TariffParameters' => $propertyArr,
            ),
            ));
            $req = $result->__last_response;
            $resp = $result->__last_request;
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetTariff_request.xml', print_r($resp, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetTariff_response.xml', print_r($req, true));
            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);
            if ($responseArray["sBody"]["GetTariffResponse"]["ResponseStatus"]["ErrorCode"] == 0) {
                $tariff = $responseArray["sBody"]["GetTariffResponse"]["ResponseData"]["Tariff"];
                $sum = $tariff["PremiumAmount"];
                return array("success" => true, "sum" => $sum);
            } else {
                return array('success' => false, 'error' => $responseArray["sBody"]["GetTariffResponse"]["ResponseStatus"]["ErrorMessage"]);
            }
        }
    }

    public function CreateAgreement($arFields)
    {
        $token = $arFields["token"];
        $cat = array();
        $mark = array();
        $model = array();
        $kbm = 0;
        $sum = 0;

        $cat = $this::GetDicti('category', $arFields["Category"]);
        if ($cat)
            $mark = $this::GetDicti('mark', array($cat, $arFields["Mark"]));
        if ($mark)
            $model = $this::GetDicti('model', array($mark, $arFields["Model"]));


        $kladrCity = $arFields["kladr_city"];

        if (!empty($model) && $kladrCity) {
            $dateStart = strtotime($arFields["PolicyBeginDate"]);
            $years = strtotime($arFields["PolicyBeginDate"] . ' + 12 month - 1 day');
            $SubjectList = '';
            if (!empty($arFields["Drivers"]['Driver'])) {
                $risk = '<RiskCtg>28966116</RiskCtg>';
                $DriverList = '<DriverList>';
                $i = 0;
                foreach ($arFields["Drivers"]['Driver'] as $val) {
                    $i++;
                    $DriverList .= '
                    <Driver>
                        <SbjRef>' . $i . '</SbjRef>
                        <DriverLicense>
                            <DocType>765912000</DocType>
                            <Serial>' . $val["DriverDocument"]["Seria"] . '</Serial>
                            <Number>' . $val["DriverDocument"]["Number"] . '</Number>
                        </DriverLicense>
                        <DrvDateBeg>' . $val["ExperienceDate"] . '</DrvDateBeg>
                    </Driver>
                    ';
                    if ($i > 1) {
                        $SubjectList .= '
                        <Subject SbjKey="' . $i . '">
                            <SbjType>Ф</SbjType>
                            <SbjResident>Y</SbjResident>
                            <FullName>' . $val["LastName"] . ' ' . $val["FirstName"] . ' ' . $val["MiddleName"] . '</FullName>
                            <BirthDate>' . $val["BirthDate"] . '</BirthDate>
                            <CountryCode>643</CountryCode>
                        </Subject>
                        ';
                    }
                }
                $DriverList .= '</DriverList>';

            } else {
                $DriverList = '';
                $risk = '<RiskCtg>28966316</RiskCtg>';
                /*$SubjectList = '
                <Subject SbjKey="2">
                    <SbjType>Ф</SbjType>
                    <SbjResident>Y</SbjResident>
                    <FullName>'.$arFields["Owner"]["LastName"].' '.$arFields["Owner"]["FirstName"].' '.$arFields["Owner"]["MiddleName"].'</FullName>
                    <BirthDate>'.$arFields["Owner"]["BirthDate"].'</BirthDate>
                    <CountryCode>643</CountryCode>
                    <IdentityDocument>
                        <DocType>30363316</DocType>
                        <Serial>'.$arFields["Owner"]["Seria"].'</Serial>
                        <Number>'.$arFields["Owner"]["Number"].'</Number>
                    </IdentityDocument>
                    <Address>
                        <CountryCode>643</CountryCode>
                        <CityCode>'.$kladrCity.'</CityCode>
                    </Address>
                </Subject>';*/
            }
            if ($arFields["doc_ts"] == 'pts') {
                $type_doc_ts = 34709216;
            } else {
                $type_doc_ts = 34709116;
            }
            $VIN = $arFields["VIN"];
            $number_bodywork = $arFields["number_bodywork"];
            $number_chassis = $arFields["number_chassis"];
            $nuber_auto = '';
            if ($VIN) {
                $nuber_auto = '<VIN>' . $VIN . '</VIN>';
            } else if ($number_bodywork) {
                $nuber_auto = '<BodyNum>' . $number_bodywork . '</BodyNum>';
            } else if ($number_chassis) {
                $nuber_auto = '<ChassisNum>' . $number_chassis . '</ChassisNum>';
            }
            $street = '';
            $home = '';
            $apartament = '';
            if (!empty($arFields["Insurer"]["address"]["street"]))
                $street = '<StreetName>' . $arFields["Insurer"]["address"]["street"] . '</StreetName>';
            if ($arFields["Insurer"]["address"]["home"])
                $home = '<House>' . $arFields["Insurer"]["address"]["home"] . '</House>';
            if (!empty($arFields["Insurer"]["address"]["apartament"]))
                $apartament = '<Flat>' . $arFields["Insurer"]["address"]["apartament"] . '</Flat>';

            $diag = '';
            if ($arFields["cart_diag"] && $arFields["date_cart_diag"]) {
                $di = '<DISetting>1</DISetting>';
                $diag = '<DocInspection>
                <DocType>3507627803</DocType>
                <Number>' . $arFields["cart_diag"] . '</Number>
                <DateEnd>' . $arFields["date_cart_diag"] . '</DateEnd>
                </DocInspection>';
            } else {
                $di = '<DISetting>3</DISetting>';
            }

            $propertyArr = new \SoapVar('
            <Agreement>
            <General>
              <Product>753518300</Product>
                <DateBeg>' . gmdate('Y-m-d\TH:i:s', $dateStart) . '</DateBeg>
                <DateEnd>' . gmdate('Y-m-d', $years) . '</DateEnd>
              <CitySales>' . $kladrCity . '</CitySales>
              <Individual>N</Individual>
              ' . $di . '
            </General>
            <Insurer>
              <SbjRef>1</SbjRef>
              <MobilePhone>' . preg_replace("/[^,.0-9]/", '', $arFields["phone"]) . '</MobilePhone>
              <Email>' . $arFields["email"] . '</Email>
            </Insurer>
            <Owner>
              <SbjRef>1</SbjRef>
            </Owner>
            <SubjectList>
                <Subject SbjKey="1">
                    <SbjType>Ф</SbjType>
                    <SbjResident>Y</SbjResident>
                    <FullName>' . $arFields["Insurer"]["LastName"] . ' ' . $arFields["Insurer"]["FirstName"] . ' ' . $arFields["Insurer"]["MiddleName"] . '</FullName>
                    <BirthDate>' . $arFields["Insurer"]["BirthDate"] . '</BirthDate>
                    <CountryCode>643</CountryCode>
                        <IdentityDocument>
                            <DocType>30363316</DocType>
                            <Serial>' . chunk_split($arFields["Insurer"]["Seria"], 2, ' ') . '</Serial>
                            <Number>' . $arFields["Insurer"]["Number"] . '</Number>
                            <DocDate>' . gmdate('Y-m-d', strtotime($arFields["Insurer"]["Date_pass"])) . '</DocDate>
                            <DocIssuedBy>' . $arFields["Insurer"]["For_doc_pass"] . '</DocIssuedBy>
                        </IdentityDocument>
                    <Address>
                        <CountryCode>643</CountryCode>
                        <CityCode>' . $kladrCity . '</CityCode>
                        ' . $street . '
                        ' . $home . '
                        ' . $apartament . '
                    </Address>
                </Subject>
                ' . $SubjectList . '
            </SubjectList>
            <Vehicle>
              <Model>' . $model . '</Model>
              <RegNum>' . $arFields["LicensePlate"] . '</RegNum>
              <Constructed>' . $arFields["YearIssue"] . '-01-01</Constructed>
              ' . $nuber_auto . '
              <Category>' . explode(' - ', $arFields["Category"])[0] . '</Category>
              <EnginePowerHP>' . $arFields["EngCap"] . '</EnginePowerHP>
              <Document>
                <DocType>' . $type_doc_ts . '</DocType>
                <Serial>' . $arFields["DocCarSerial"] . '</Serial>
                <Number>' . $arFields["DocCarNumber"] . '</Number>
                <DocDate>' . gmdate('Y-m-d', strtotime($arFields["DocumentCarDate"])) . '</DocDate>
              </Document>
              ' . $diag . '
            </Vehicle>
            <Condition>
              <Liability>
              ' . $risk . '
                <UsageTarget>
                  <Personal>Y</Personal>
                </UsageTarget>
                <UseWithTrailer>N</UseWithTrailer>
                <UsageType>1381850903</UsageType>
                <PeriodList>
                  <Period>
                  <DateBeg>' . gmdate('Y-m-d', $dateStart) . '</DateBeg>
                  <DateEnd>' . gmdate('Y-m-d', $years) . '</DateEnd>
                  </Period>
                </PeriodList>
              </Liability>
            </Condition>
            ' . $DriverList . '
          </Agreement>
            ', XSD_ANYXML);
            if ($risk) {
                $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
                $result->__soapCall("CreateAgreement", array('CreateAgreementRequest' => array(
                    'NeedList' => 'Y',
                    'SessionToken' => $token,
                    'Agreement' => $propertyArr
                ),
                ));
                $req = $result->__last_response;
                $resp = $result->__last_request;
            }
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CreateAgreement_request.xml', print_r($resp, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CreateAgreement_response.xml', print_r($req, true));
            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);
            $error = $responseArray["sBody"]["CreateAgreementResponse"]["ResponseStatus"]["ErrorCode"];
            if ($error == 0) {
                return array("success" => true, "AgrID" => $responseArray["sBody"]["CreateAgreementResponse"]["ResponseData"]["AgrID"]);
            } else {
                return array("success" => false, "error" => $responseArray["sBody"]["CreateAgreementResponse"]["ResponseStatus"]["ErrorMessage"]);
            }
        }
    }

    public function GetAgreementRequest($arFields)
    {
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("GetAgreement", array('GetAgreementRequest' => array(
            'SessionToken' => $arFields["token"],
            'PolicyNumber' => $arFields["AgrID"],
        ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetAgreementRequest_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetAgreementRequest_response.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        $error = $responseArray["sBody"]["GetAgreementResponse"]["ResponseStatus"]["ErrorCode"];
        if ($error == 0) {
            return array("success" => true, "General" => $responseArray["sBody"]["GetAgreementResponse"]["ResponseData"]["Agreement"]["General"]);
        } else {
            return array("success" => false, "error" => $responseArray["sBody"]["GetAgreementResponse"]["ResponseStatus"]["ErrorMessage"]);
        }
    }

    public function MakeEOsago($arFields)
    {
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("MakeEOsago", array('MakeEOsagoRequest' => array(
            'AgrISN' => $arFields["AgrISN"],
            'SessionToken' => $arFields["token"]
        ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/MakeEOsago_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/MakeEOsago_response.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        $error = $responseArray["sBody"]["MakeEOsagoResponse"]["ResponseStatus"]["ErrorCode"];
        if ($error == 0) {
            return array("success" => true, "Bso" => $responseArray["sBody"]["MakeEOsagoResponse"]["ResponseData"]["Bso"]);
        } else {
            return array("success" => false, "error" => $responseArray["sBody"]["MakeEOsagoResponse"]["ResponseStatus"]["ErrorMessage"]);
        }
    }

    public function CreateBill($arFields)
    {
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("CreateBill", array('CreateBillRequest' => array(
            'SessionToken' => $arFields["token"],
            'PaymentType' => 114916,
            'Payer' => 'Customer',
            'AgreementList' => array(
                'AgrID' => $arFields["AgrID"]
            )
        ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CreateBill_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CreateBill_response.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        $error = $responseArray["sBody"]["CreateBillResponse"]["ResponseStatus"]["ErrorCode"];
        if ($error == 0) {
            return array("success" => true, "BillISN" => $responseArray["sBody"]["CreateBillResponse"]["ResponseData"]["BillISN"]);
        } else {
            return array("success" => false, "error" => $responseArray["sBody"]["CreateBillResponse"]["ResponseStatus"]["ErrorMessage"]);
        }
    }

    public function CreateOnlineBill($arFields)
    {
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("CreateOnlineBill", array('CreateOnlineBillRequest' => array(
            'SessionToken' => $arFields["token"],
            'Bill' => array(
                'BillISN' => $arFields["BillISN"],
                'Client' => array(
                    'Email' => $arFields["email"],
                    'DigitalPolicyEmail' => $arFields["email"],
                    'Phone' => preg_replace("/[^,.0-9]/", '', $arFields["phone"]),
                    'PaymentNotification' => 'Y',
                    'SendByEmail' => 'Y',
                    'SendBySms' => 'Y'
                )
            )
        ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CreateOnlineBill_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CreateOnlineBill_response.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        $error = $responseArray["sBody"]["CreateOnlineBillResponse"]["ResponseStatus"]["ErrorCode"];
        if ($error == 0) {
            return array(
                "success" => true,
                "urlPay" => $responseArray["sBody"]["CreateOnlineBillResponse"]["ResponseData"]["PayURL"]
            );
        } else {
            return array("success" => false, "error" => $responseArray["sBody"]["CreateOnlineBillResponse"]["ResponseStatus"]["ErrorMessage"]);
        }
        // В статус «действует» переводить не надо
    }

    public function GetPolicyDocuments($policeID, $order = false)
    {
        $checkAgr = [];
        if (!empty($order->id)) {
            $Vehicle = [];
            if (!empty($order->AutoOrder[0]->id)) {
                if (!empty($order->AutoOrder[0]->vin)) {
                    $Vehicle["VIN"] = $order->AutoOrder[0]->vin;
                } elseif (!empty($order->AutoOrder[0]->number_bodywork)) {
                    $Vehicle["BodyNum"] = $order->AutoOrder[0]->number_bodywork;
                } elseif (!empty($order->AutoOrder[0]->number_chassis)) {
                    $Vehicle["ChassisNum"] = $order->AutoOrder[0]->number_chassis;
                }
            }
            $checkAgr = [
                'Insurer' => [
                    'FIO' => $order->LastName_Insurer . ' ' . $order->FirstName_Insurer . ' ' . $order->MiddleName_Insurer,
                    'Document' => [
                        'Serial' => $order->Seria_pas_Insurer,
                        'Number' => $order->Number_pas_Insurer
                    ],
                ],
                'Vehicle' => $Vehicle,
            ];
        }
        $token = $this::Login();
        $result = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result->__soapCall("GetPolicyDocuments", array('GetPolicyDocumentsRequest' => array(
            'SessionToken' => $token,
            'PolicyNumber' => $policeID,
            'Draft' => 0,
            'CheckAgr' => $checkAgr,
        ),
        ));
        $req = $result->__last_response;
        $resp = $result->__last_request;
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $req);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetPolicyDocuments_request.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/GetPolicyDocuments_response.xml', print_r($req, true));
        $error = $responseArray["sBody"]["GetPolicyDocumentsResponse"]["ResponseStatus"]["ErrorCode"];

        if ($error == 0) {
            if (!empty($responseArray["sBody"]["GetPolicyDocumentsResponse"]["ResponseData"]["DocumentList"]["Document"][0])) {
                return array("success" => true, "doc" => $responseArray["sBody"]["GetPolicyDocumentsResponse"]["ResponseData"]["DocumentList"]["Document"][2]["DocumentImage"]);
            } else {
                return ["success" => false];
            }
        } else {
            return array("success" => false, "error" => $responseArray["sBody"]["GetPolicyDocumentsResponse"]["ResponseStatus"]["ErrorMessage"]);
        }
    }

    public function CrossProductListRequest($arFields)
    {
        $result_cross = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result_cross->__soapCall("GetCrossProductListSecond", array('CrossProductListRequest' => array(
            'AgrISN' => $arFields["AgrISN"],
            'SessionToken' => $arFields["token"]
        ),
        ));
        $resp_cross = $result_cross->__last_response;
        $req_cross = $result_cross->__last_request;
        $xml_cross = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp_cross);
        $xml_cross = simplexml_load_string($xml_cross);
        $json_cross = json_encode($xml_cross);
        $responseArray_cross = json_decode($json_cross, true);
        $error = $responseArray_cross["sBody"]["CrossProductListResponse"]["ResponseStatus"]["ErrorCode"];
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CrossProductListRequest_request.xml', print_r($req_cross, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/Ingos/CrossProductListRequest_response.xml', print_r($resp_cross, true));
        /*$list = [
            [
                "id" => 1,
                "name" => 'test',
                "price" => 1000
            ],
            [
                "id" => 2,
                "name" => 'test2',
                "price" => 1001
            ],
            [
                "id" => 3,
                "name" => 'test3',
                "price" => 1002
            ]
        ];
        return ["success" => true, "list" => $list];*/
        if ($error == 0) {
            if (!empty($responseArray_cross["sBody"]["CrossProductListResponse"]["ResponseData"]["CrossProductList"]["Product"])) {
                $list = [];
                $products = $responseArray_cross["sBody"]["CrossProductListResponse"]["ResponseData"]["CrossProductList"]["Product"];
                if (count($products) > 1) {
                    foreach ($responseArray_cross["sBody"]["CrossProductListResponse"]["ResponseData"]["CrossProductList"]["Product"] as $itemCross) {
                        $list[] = [
                            "id" => $itemCross["CrossType"],
                            "name" => $itemCross["CrossName"],
                            "price" => $itemCross["PremSum"]
                        ];
                    }
                } else {
                    $list[] = [
                        "id" => $products["CrossType"],
                        "name" => $products["CrossName"],
                        "price" => $products["PremSum"]
                    ];
                }
                return ["success" => true, "list" => $list];
            } else {
                return ["success" => false];
            }
        } else {
            return ["success" => false, "error" => $responseArray_cross["sBody"]["CrossProductListResponse"]["ResponseStatus"]["ErrorMessage"]];
        }
    }

    public function CreateCrossAgreementRequestIngos($arFields)
    {
        $result_cross = new \SoapClient($this->url_servic, array('trace' => true, 'exceptions' => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        $result_cross->__soapCall("CreateCrossAgreementSecond", array('CreateCrossAgreementRequest' => array(
            'AgrISN' => $arFields["AgrISN"],
            'SessionToken' => $arFields["token"],
            'CrossType' => $arFields["id"],
        ),
        ));
        $resp_cross = $result_cross->__last_response;
        $req_cross = $result_cross->__last_request;
        $xml_cross = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp_cross);
        $xml_cross = simplexml_load_string($xml_cross);
        $json_cross = json_encode($xml_cross);
        $responseArray_cross = json_decode($json_cross, true);
        $error = $responseArray_cross["sBody"]["CreateCrossAgreementResponse"]["ResponseStatus"]["ErrorCode"];
        if ($error == 0) {
            $CreateCrossAgrResult = $responseArray_cross["sBody"]["CreateCrossAgreementResponse"]["ResponseData"]["CreateCrossAgrResult"];
            $arFields["success"] = true;
            $arFields["AgrISN"] = $CreateCrossAgrResult["AgrISN"];
            $arFields["AgrID"] = $CreateCrossAgrResult["AgrID"];
            return $arFields;
        } else {
            return ["success" => false];
        }
    }
}
