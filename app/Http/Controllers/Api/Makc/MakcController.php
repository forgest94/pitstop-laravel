<?php

namespace App\Http\Controllers\Api\Makc;


use App\Http\Controllers\Controller;


ini_set('default_socket_timeout', '600');

class MakcController extends Controller
{
    protected $url_servic;
    protected $login;
    protected $pass;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            $this->url_servic = "http://unitest.makc.ru:80/XmlDocumExchangeWS/XmlDocumExchangeWebService?wsdl";
        } else {
            $this->url_servic = "http://uni2.makc.ru/XmlDocumExchangeWS/XmlDocumExchangeWebService?wsdl";
        }
        $this->login = config('app.login_makc');
        $this->pass = config('app.pass_makc');
    }

    public function minCalc($arFields)
    {
        $arFields["Owner"] = $arFields["Owner_calc"];
        $kbm = $this->kbm($arFields);
        if ($kbm["success"]) {
            $arFields["kbm"] = $kbm["kbm"];
            $arFields["drivers_kbm"] = $kbm["drivers"];
            $calcOsago = $this->calc_eosago($arFields);
            return $calcOsago;
        } else {
            return ["success" => false, "error" => "Ошибка расчета кбм"];
        }
    }

    public function finalCalc($arFields)
    {
        $kbm = $this->kbm($arFields);
        if ($kbm["success"]) {
            $arFields["kbm"] = $kbm["kbm"];
            $arFields["kbm_info"] = $kbm["kbm_info"];
            $arFields["drivers_kbm"] = $kbm["drivers"];
            $calcOsago = $this->calc_eosago($arFields);
            if ($calcOsago["success"]) {
                $arFields["calc_info"] = $calcOsago;
                $saveCalc = $this->saveCalc($arFields);
                if ($saveCalc["success"]) {
                    $arFields["saveCalcInfo"] = $saveCalc;
                    $issuseOsago = $this->issuseOsago($arFields);
                } else {
                    return $saveCalc;
                }
            } else {
                return $calcOsago;
            }
        } else {
            return ["success" => false, "error" => "Ошибка расчета кбм"];
        }
    }

    public function guides($arFields = false)
    {
        $result = new \SoapClient($this->url_servic,
            array(
                'trace' => true,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'login' => $this->login,
                'password' => $this->pass,
            )
        );
        $mark = '';
        if (!empty($arFields["type"])) {
            $type = $arFields["type"];
            $value = $arFields["value"];
            if (!empty($arFields["mark"]))
                $mark = $arFields["mark"];
        } else {
            $type = $request->input('type');
            $mark = $request->input('mark');
            $value = $request->input('value');
        }
        switch ($type) {
            case 'mark':
                $result->documentExchangeL(array('arg0' => '
                 <root>
                  <gate>
                    <action> 
                      GET_MARKA_TS
                    </action>
                  </gate>
                </root>
               '));
                break;
            case 'model':
                $result->documentExchangeL(array('arg0' => '
                 <root>
                  <gate>
                    <action> 
                      GET_MODEL_TS
                    </action>
                  </gate>
                  <marka_id>' . $mark . '</marka_id>
                </root>
               '));
                break;
            case 'type_ts':
                $result->documentExchangeL(array('arg0' => '
                 <root>
                  <gate>
                    <action> 
                      GET_TS_TYPE
                    </action>
                  </gate>
                </root>
               '));
                break;
        }

        $resp = $result->__last_response;
        $req = $result->__last_request;
        //file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/Login_request.xml', print_r($resp, true));
        //file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/Login_response.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        if (!empty($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"])) {
            $xml = str_replace('&', '&amp;', $responseArray["SBody"]["ns2documentExchangeLResponse"]["return"]);
            $items = simplexml_load_string($xml);
            $findParams = [];
            foreach ($items as $item) {
                if (strtolower($item->name) == strtolower($value)) {
                    $findParams = $item;
                }
            }
            if (!empty($findParams)) {
                return ["success" => true, "info" => $findParams];
            } else {
                return ["success" => false];
            }
        }
    }

    public function kbm($arFields = false)
    {
        if (!empty($arFields["Drivers"]['Driver'])) {
            $drivers = '<drivers>';
            foreach ($arFields["Drivers"]['Driver'] as $driver) {
                $drivers .= '
                <driver>
                    <lastName>' . $driver["LastName"] . '</lastName>
                    <firstName>' . $driver["FirstName"] . '</firstName>
                    <middleName>' . $driver["MiddleName"] . '</middleName>
                    <birthday>' . date('d.m.Y', strtotime($driver["BirthDate"])) . '</birthday>
                    <Serial>' . $driver["DriverDocument"]["Seria"] . '</Serial>
                    <Number>' . $driver["DriverDocument"]["Number"] . '</Number>
                </driver>
                ';
            }
            $drivers .= '</drivers>';
            $driveType = 1;
        } else {
            $driveType = 0;
            $drivers = '
              <ownerType>1</ownerType>
              <ownerLastName>' . $arFields["Owner"]["LastName"] . '</ownerLastName>
              <ownerFirstName>' . $arFields["Owner"]["FirstName"] . '</ownerFirstName>
              <ownerMiddleName>' . $arFields["Owner"]["MiddleName"] . '</ownerMiddleName>
              <ownerBirthDate>' . date('d.m.Y', strtotime($arFields["Owner"]["BirthDate"])) . '</ownerBirthDate>
              <DocType>1</DocType>
              <ownerDocSer>' . $arFields["Owner"]["Seria"] . '</ownerDocSer>
              <ownerDocNum>' . $arFields["Owner"]["Number"] . '</ownerDocNum>
            ';
        }
        $params = '
        <root>
          <gate>
            <action>CALC_KBM</action>
          </gate>
          <constrainCheckTO>1</constrainCheckTO>
          <constrainCheckKBM>1</constrainCheckKBM>
          <constrainNumDrivers>' . $driveType . '</constrainNumDrivers>
          <dateKbm>' . date('d.m.Y', strtotime($arFields["PolicyBeginDate"])) . '</dateKbm>
          ' . $drivers . '
          <regNum>' . $arFields["LicensePlate"] . '</regNum>
          <VIN>' . $arFields["VIN"] . '</VIN>
          <chassisNum>' . $arFields["number_chassis"] . '</chassisNum>
        </root>
        ';
        $result = new \SoapClient($this->url_servic,
            array(
                'trace' => true,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'login' => $this->login,
                'password' => $this->pass,
            )
        );
        $result->documentExchangeL(array('arg0' => $params));
        $resp = $result->__last_response;
        $req = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/CALC_KBM_response.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/CALC_KBM_request.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        if (!empty($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"])) {
            $items = simplexml_load_string($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"]);
            $drivers = [];
            if (!empty($items->objects->driver) && !empty($arFields["Drivers"]['Driver'])) {
                $i = 0;
                foreach ($items->objects->driver as $key => $driver) {
                    $drivers[] = ["drive" => $arFields["Drivers"]['Driver'][$i], "kbm_drive" => $driver];
                    $i++;
                }
            }
            if (!empty($items->KBM_value)) {
                return [
                    "success" => true,
                    "kbm" => (string)$items->KBM_value,
                    "kbm_info" => $items,
                    "drivers" => $drivers
                ];
            } else {
                return ["success" => false];
            }
        }
    }

    public function calc_eosago($arFields)
    {
        function getAge($datePerson, $date)
        {
            if ($datePerson[1] > $date[1] || $datePerson[1] == $date[1] && $datePerson[2] > $date[0])
                return ($date[2] - $datePerson[0] - 1);
            else
                return ($date[2] - $datePerson[0]);
        }

        $driveType = 1;
        $age = '';
        $staz = '';
        if (!empty($arFields["Drivers"]['Driver'])) {
            $driveType = 0;
            $yearsUser = [];
            $experience = [];
            foreach ($arFields["Drivers"]['Driver'] as $driver) {
                $yearsUser[] = getAge(explode('-', $driver["BirthDate"]), explode('.', $arFields["PolicyBeginDate"])) + 1;
                $experience[] = getAge(explode('-', $driver["ExperienceDate"]), explode('.', $arFields["PolicyBeginDate"])) + 1;
            }
            if ($yearsUser)
                $age = min($yearsUser);
            if ($experience)
                $staz = min($experience);
        }
        $params = '
        <root>
            <gate>
                <action>CALC_OSAGO</action>
            </gate>
            <TYPE_TC>2</TYPE_TC>
            <POWER_TC>' . $arFields["Power"] . '</POWER_TC>
            <OWNER_TP>PHYSICAL_PERSON</OWNER_TP>
            <TRAILER_TC >0</TRAILER_TC>
            <TRANSP_TC>0</TRANSP_TC>
            <TARGET_TC>1</TARGET_TC>
            <VIOLATION>0</VIOLATION>
            <PERIOD_TC>5</PERIOD_TC>
            <TERRA_TC>' . $arFields["kladr_city"] . '</TERRA_TC>
            <AGE>' . $age . '</AGE>
            <STAZ>' . $staz . '</STAZ>
            <CAPACITY_TC></CAPACITY_TC>
            <MAX_MASS></MAX_MASS>
            <KBM>' . $arFields["kbm"] . '</KBM>
            <TRANSPORT_FREE>' . $driveType . '</TRANSPORT_FREE>
        </root>
        ';
        $result = new \SoapClient($this->url_servic,
            array(
                'trace' => true,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'login' => $this->login,
                'password' => $this->pass,
            )
        );
        $result->documentExchangeL(array('arg0' => $params));
        $resp = $result->__last_response;
        $req = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/CALC_OSAGO_response.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/CALC_OSAGO_request.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        if (!empty($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"])) {
            $items = simplexml_load_string($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"]);
            if (!empty((string)$items->product_amount)) {
                $coefficient = [
                    "Tb" => (string)$items->tarif_tb,
                    "Kt" => (string)$items->tarif_kt,
                    "Kbm" => $arFields["kbm"],
                    "Ko" => (string)$items->tarif_ko,
                    "Kn" => (string)$items->tarif_kn,
                    "Kp" => (string)$items->tarif_kp,
                    "Ks" => (string)$items->tarif_ks,
                    "Km" => (string)$items->tarif_km,
                    "Kvs" => (string)$items->tarif_kvs,
                    "Kpr" => (string)$items->tarif_kpc,
                ];
                return [
                    'success' => true,
                    'info' => [
                        "sum" => (string)$items->product_amount,
                        "Coefficients" => $coefficient
                    ]
                ];
            } else {
                return ['success' => false, 'error' => 'Ошибка расчета параметров'];
            }
        }
    }

    public function gender($name)
    {
        $male = 'M';
        $female = 'F';

        $nameEnds = (mb_substr($name, -1, 1));
        if (mb_strtolower($name) == 'саша' || mb_strtolower($name) == 'женя' || mb_strtolower($name) == 'слава') {
            return $male;
        } elseif ($nameEnds == 'а' || $nameEnds == 'я') {
            return $female;
        } else {
            return $male;
        }
    }

    public function saveCalc($arFields)
    {
        $isMark = $this->guides([
            "type" => "mark",
            "value" => $arFields["Mark"]
        ]);
        if ($isMark["success"]) {
            $isModel = $this->guides([
                "type" => "model",
                "value" => $arFields["Model"],
                "mark" => (int)$isMark["info"]->value
            ]);
            if (!$isModel["success"]) {
                return ["success" => false, "error" => "Модель не найдена"];
            }
        } else {
            return ["success" => false, "error" => "Марка не найдена"];
        }
        $diagCart = '';
        $numberDiag = '';
        if (!empty($arFields["cart_diag"])) {
            $numberDiag = 48;
            $diagCart = '
                <tech_doc_ser></tech_doc_ser> 
                <tech_doc_num>' . $arFields["cart_diag"] . '</tech_doc_num> 
                <tech_doc_issue_month>' . date('m', strtotime($arFields["date_cart_diag"])) . '</tech_doc_issue_month> 
                <tech_doc_issue_year>' . date('Y', strtotime($arFields["date_cart_diag"])) . '</tech_doc_issue_year> 
            ';
        }
        $docTs = '';
        switch ($arFields["type_doc_ts"]) {
            case 'sts':
                $docTs = 12;
                break;
            case 'pts':
                $docTs = 11;
                break;
        }
        $drivers = '<drivers>';
        if (!empty($arFields["Drivers"]['Driver'])) {
            $drivers .= '<drv_is_ins>0</drv_is_ins>';
            foreach ($arFields["Drivers"]['Driver'] as $key => $driver) {
                $infoKbmDriver = $arFields["drivers_kbm"][$key];
                $drivers .= '
                <driver>
                  <drv_is_ins></drv_is_ins> 
                  <drv_is_own></drv_is_own>
                  <last_name>' . $driver["LastName"] . '</last_name> 
                  <first_name>' . $driver["FirstName"] . '</first_name>  
                  <middle_name>' . $driver["MiddleName"] . '</middle_name> 
                  <sex>' . $this->gender($driver["FirstName"]) . '</sex> 
                  <birth_day>' . date('d', strtotime($driver["BirthDate"])) . '</birth_day>
                  <birth_month>' . date('m', strtotime($driver["BirthDate"])) . '</birth_month> 
                  <birth_year>' . date('Y', strtotime($driver["BirthDate"])) . '</birth_year>
                  <doc_ser>' . $driver["DriverDocument"]["Seria"] . '</doc_ser>
                  <doc_num>' . $driver["DriverDocument"]["Number"] . '</doc_num>
                  <driving_day>' . date('d', strtotime($driver["DriverDocument"]["DateIssue"])) . '</driving_day>
                  <driving_month>' . date('m', strtotime($driver["DriverDocument"]["DateIssue"])) . '</driving_month>
                  <driving_year>' . date('Y', strtotime($driver["DriverDocument"]["DateIssue"])) . '</driving_year>
                  <bonus_malus>' . $infoKbmDriver["kbm_drive"]->KBM_value . '</bonus_malus>
                </driver>
                ';
            }
        } else {
            $drivers .= '<drv_is_ins>1</drv_is_ins>';
        }
        $drivers .= '</drivers>';
        $dateEnd = strtotime($arFields["PolicyBeginDate"] . ' + 12 month - 1 day');
        $country = 'Россия';
        if (!empty($arFields["Insurer"]["address"]["country"])) {
            $country = $arFields["Insurer"]["address"]["country"];
        }
        $params_dop = $drivers . '
          <insurer>
            <phys_jur>0</phys_jur> 
            <last_name>' . $arFields["Insurer"]["LastName"] . '</last_name>  
            <first_name>' . $arFields["Insurer"]["FirstName"] . '</first_name>  
            <middle_name>' . $arFields["Insurer"]["MiddleName"] . '</middle_name> 
            <sex>' . $this->gender($arFields["Insurer"]["FirstName"]) . '</sex> 
            <birth_day>' . date('d', strtotime($arFields["Insurer"]["BirthDate_old"])) . '</birth_day>
            <birth_month>' . date('m', strtotime($arFields["Insurer"]["BirthDate_old"])) . '</birth_month>
            <birth_year>' . date('Y', strtotime($arFields["Insurer"]["BirthDate_old"])) . '</birth_year>
            <jur_name></jur_name> 
            <jur_form></jur_form>
            <jur_inn></jur_inn>
            <doc_type>1</doc_type> 
            <doc_ser>' . $arFields["Insurer"]["Seria"] . '</doc_ser> 
            <doc_num>' . $arFields["Insurer"]["Number"] . '</doc_num> 
            <doc_issue_day>' . date('d', strtotime($arFields["Insurer"]["Date_pass"])) . '</doc_issue_day>
            <doc_issue_month>' . date('m', strtotime($arFields["Insurer"]["Date_pass"])) . '</doc_issue_month> 
            <doc_issue_year>' . date('Y', strtotime($arFields["Insurer"]["Date_pass"])) . '</doc_issue_year> 
            <addr_kladr>' . $arFields["kladr_address"] . '</addr_kladr>
            <addr_country>' . $country . '</addr_country> 
            <addr_state>' . $arFields["RegionOfUse"] . '</addr_state> 
            <addr_city>' . $arFields["CityOfUse"] . '</addr_city> 
            <addr_region>' . $arFields["RegionOfUse"] . '</addr_region> 
            <addr_street>' . $arFields["Insurer"]["address"]["street"] . '</addr_street> 
            <addr_house>' . $arFields["Insurer"]["address"]["home"] . '</addr_house> 
            <addr_building></addr_building> 
            <addr_apartment>' . $arFields["Insurer"]["address"]["apartament"] . '</addr_apartment>
            <contact_phone1>' . $arFields["phone"] . '</contact_phone1> 
            <contact_email>' . $arFields["email"] . '</contact_email> 
          </insurer>
          <owner>
            <own_is_ins>1</own_is_ins>
            <phys_jur>0</phys_jur> 
            <last_name>' . $arFields["Owner"]["LastName"] . '</last_name>  
            <first_name>' . $arFields["Owner"]["FirstName"] . '</first_name>  
            <middle_name>' . $arFields["Owner"]["MiddleName"] . '</middle_name> 
            <sex>' . $this->gender($arFields["Owner"]["FirstName"]) . '</sex> 
            <birth_day>' . date('d', strtotime($arFields["Owner"]["BirthDate_old"])) . '</birth_day>
            <birth_month>' . date('m', strtotime($arFields["Owner"]["BirthDate_old"])) . '</birth_month>
            <birth_year>' . date('Y', strtotime($arFields["Owner"]["BirthDate_old"])) . '</birth_year>
            <jur_name></jur_name> 
            <jur_form></jur_form>
            <jur_inn></jur_inn>
            <doc_type>1</doc_type> 
            <doc_ser>' . $arFields["Owner"]["Seria"] . '</doc_ser> 
            <doc_num>' . $arFields["Owner"]["Number"] . '</doc_num> 
            <doc_issue_day>' . date('d', strtotime($arFields["Owner"]["Date_pass"])) . '</doc_issue_day>
            <doc_issue_month>' . date('m', strtotime($arFields["Owner"]["Date_pass"])) . '</doc_issue_month> 
            <doc_issue_year>' . date('Y', strtotime($arFields["Owner"]["Date_pass"])) . '</doc_issue_year> 
            <addr_kladr>' . $arFields["kladr_address"] . '</addr_kladr>
            <addr_country>' . $country . '</addr_country> 
            <addr_state>' . $arFields["RegionOfUse"] . '</addr_state> 
            <addr_city>' . $arFields["CityOfUse"] . '</addr_city> 
            <addr_region>' . $arFields["RegionOfUse"] . '</addr_region> 
            <addr_street>' . $arFields["Owner"]["address"]["street"] . '</addr_street> 
            <addr_house>' . $arFields["Owner"]["address"]["home"] . '</addr_house> 
            <addr_building></addr_building> 
            <addr_apartment>' . $arFields["Owner"]["address"]["apartament"] . '</addr_apartment>
            <contact_phone1>' . $arFields["phone"] . '</contact_phone1>  
          </owner>  
          <vehicle>
            <reg_num>' . $arFields["LicensePlate"] . '</reg_num> 
            <VIN>' . $arFields["VIN"] . '</VIN>
            <brand_id>' . (int)$isMark["info"]->value . '</brand_id> 
            <brand_name>' . (string)$isMark["info"]->name . '</brand_name> 
            <model_id>' . (int)$isModel["info"]->value . '</model_id> 
            <model_name>' . (string)$isModel["info"]->name . '</model_name> 
            <modif></modif>
            <type>2</type> 
            <category></category> 
            <year>' . $arFields["YearIssue"] . '</year> 
            <chassis_num>' . $arFields["number_chassis"] . '</chassis_num> 
            <body_num>' . $arFields["number_bodywork"] . '</body_num> 
            <power_hp>' . $arFields["Power"] . '</power_hp> 
            <max_mass></max_mass> 
            <passenger_num></passenger_num> 
            <doc_foreign>0</doc_foreign> 
            <doc_type>' . $docTs . '</doc_type> 
            <doc_ser>' . $arFields["DocCarSerial"] . '</doc_ser> 
            <doc_num>' . $arFields["DocCarNumber"] . '</doc_num> 
            <doc_issue_day>' . date('d', strtotime($arFields["DocumentCarDate"])) . '</doc_issue_day> 
            <doc_issue_month>' . date('m', strtotime($arFields["DocumentCarDate"])) . '</doc_issue_month> 
            <doc_issue_year>' . date('Y', strtotime($arFields["DocumentCarDate"])) . '</doc_issue_year> 
            <tech_doc_type>' . $numberDiag . '</tech_doc_type> 
            ' . $diagCart . ' 
            <purpose>1</purpose> 
            <usage_id>5</usage_id> 
              <osago_periods>
                <osago_period>
                  <osago_period_number>1</osago_period_number> 
                  <usage_start_day>' . date('d', strtotime($arFields["PolicyBeginDate"])) . '</usage_start_day> 
                  <usage_start_month>' . date('m', strtotime($arFields["PolicyBeginDate"])) . '</usage_start_month> 
                  <usage_start_year>' . date('Y', strtotime($arFields["PolicyBeginDate"])) . '</usage_start_year> 
                  <usage_end_day>' . date('d', $dateEnd) . '</usage_end_day> 
                  <usage_end_month>' . date('m', $dateEnd) . '</usage_end_month> 
                  <usage_end_year>' . date('Y', $dateEnd) . '</usage_end_year> 
                </osago_period>
              </osago_periods>
            <bonus_malus>' . $arFields["kbm"] . '</bonus_malus> 
            <trailer>0</trailer> 
          </vehicle>
          <kbm_data>
            <kbm_doc_number>' . (int)$arFields["kbm_info"]->kbm_doc_number . '</kbm_doc_number>
            <kbm_calc_number>' . (int)$arFields["kbm_info"]->kbm_calc_number . '</kbm_calc_number>
          </kbm_data>
          <premium>
            <PaymentType>2</PaymentType> 
          </premium>
        ';
        $params = '
        <root>
          <gate>
            <action>SAVE_OSAGO</action>
          </gate>
          <general>
          <bankParticipation>0</bankParticipation> 
          <bankParticipationID></bankParticipationID>
          <blanks/>
          <period_id>8</period_id> 
          <period_start_hour>' . date('H', strtotime($arFields["PolicyBeginDate"])) . '</period_start_hour> 
          <period_start_min>' . date('i', strtotime($arFields["PolicyBeginDate"])) . '</period_start_min>
          <period_start_day>' . date('d', strtotime($arFields["PolicyBeginDate"])) . '</period_start_day> 
          <period_start_month>' . date('m', strtotime($arFields["PolicyBeginDate"])) . '</period_start_month> 
          <period_start_year>' . date('Y', strtotime($arFields["PolicyBeginDate"])) . '</period_start_year> 
          <period_end_day>' . date('d', $dateEnd) . '</period_end_day> 
          <period_end_month>' . date('m', $dateEnd) . '</period_end_month> 
          <period_end_year>' . date('Y', $dateEnd) . '</period_end_year> 
          <stat_day>' . date('d') . '</stat_day> 
          <stat_month>' . date('m') . '</stat_month> 
          <stat_year>' . date('Y') . '</stat_year> 
          <is_eosago>1</is_eosago>
          </general>
          ' . $params_dop . '
        </root>
        ';
        $result = new \SoapClient($this->url_servic,
            array(
                'trace' => true,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'login' => $this->login,
                'password' => $this->pass,
            )
        );
        $result->documentExchangeL(array('arg0' => $params));
        $resp = $result->__last_response;
        $req = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/SAVE_OSAGO_response.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/SAVE_OSAGO_request.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        if (!empty($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"])) {
            $items = simplexml_load_string($responseArray["SBody"]["ns2documentExchangeLResponse"]["return"]);
            if (!empty((string)$items->policy_number)) {
                return ["success" => true, "params" => $params_dop, "policy_number" => (string)$items->policy_number, "contract_id" => (int)$items->contract_id];
            } else {
                return ["success" => false, "error" => "Ошибка расчета полиса policy_number", "dop_info"=>$items];
            }
        } else {
            return ["success" => false, "error" => "Ошибка расчета полиса", "dop_info"=>$responseArray];
        }

    }

    public function issuseOsago($arFields)
    {
        $dateEnd = strtotime($arFields["PolicyBeginDate"] . ' + 12 month - 1 day');
        $params = '
        <root>
          <gate>
            <action>ISSUE_OSAGO</action>
          </gate>
          <general>
          <contract_id>' . $arFields["saveCalcInfo"]["contract_id"] . '</contract_id>
          <bankParticipation>0</bankParticipation> 
          <bankParticipationID></bankParticipationID>
          <blanks/>
          <period_id>8</period_id> 
          <period_start_hour>' . date('H', strtotime($arFields["PolicyBeginDate"])) . '</period_start_hour> 
          <period_start_min>' . date('i', strtotime($arFields["PolicyBeginDate"])) . '</period_start_min>
          <period_start_day>' . date('d', strtotime($arFields["PolicyBeginDate"])) . '</period_start_day> 
          <period_start_month>' . date('m', strtotime($arFields["PolicyBeginDate"])) . '</period_start_month> 
          <period_start_year>' . date('Y', strtotime($arFields["PolicyBeginDate"])) . '</period_start_year> 
          <period_end_day>' . date('d', $dateEnd) . '</period_end_day> 
          <period_end_month>' . date('m', $dateEnd) . '</period_end_month> 
          <period_end_year>' . date('Y', $dateEnd) . '</period_end_year> 
          <stat_day>' . date('d') . '</stat_day> 
          <stat_month>' . date('m') . '</stat_month> 
          <stat_year>' . date('Y') . '</stat_year> 
          <is_eosago>1</is_eosago>
          </general>
          ' . $arFields["saveCalcInfo"]["params"] . '
        </root>';
        $result = new \SoapClient($this->url_servic,
            array(
                'trace' => true,
                'exceptions' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'login' => $this->login,
                'password' => $this->pass,
            )
        );
        $result->documentExchangeL(array('arg0' => $params));
        $resp = $result->__last_response;
        $req = $result->__last_request;
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/ISSUE_OSAGO_response.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/makc/ISSUE_OSAGO_request.xml', print_r($req, true));
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);
        print_r($responseArray);
    }
}
