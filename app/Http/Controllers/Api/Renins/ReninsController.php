<?php

namespace App\Http\Controllers\Api\Renins;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use PDF;

class ReninsController extends Controller
{
    protected $url_servic = "https://api-eosago.renins.com";
    protected $key_renins;
    protected $urlSite;

    public function __construct()
    {
        if (config('app.env') == 'local') {
            $this->key_renins = 'SECRET_KEY_DEMO';
        } else {
            $this->key_renins = config('app.key_renins');
        }
        $this->urlSite = config('app.url');
    }


    public function runCalc($arFields)
    {
        $calcInfo = $this::calculate($arFields);
        if ($calcInfo["success"]) {
            $arFields["infoCalc"] = $calcInfo["infoCalc"];
            $arFields["sum"] = $calcInfo["info"]["sum"];
            $crate = $this::createContract($arFields, "Y");
            if ($crate["success"]) {
                $status = $this::infoContract($crate["id_create"], $arFields);
                if ($status["success"]) {
                    return $status;
                } else {
                    return $status;
                }
            } else {
                return $crate;
            }
        } else {
            return $calcInfo;
        }
    }

    public function calculate($arFields, $min = false)
    {

        $car = $this::marksInfo($arFields);

        $dateStart = gmdate("Y-m-d", strtotime($arFields["PolicyBeginDate"]));
        $dateEnd = gmdate('Y-m-d', strtotime($arFields["PolicyBeginDate"] . ' + 12 month - 1 day'));
        $arPost = array(
            'key' => $this->key_renins,
            'dateStart' => $dateStart,
            'period' => '12',
            'limitDrivers' => $arFields["DriversRestriction"] ? '1' : '0',
            'trailer' => '0',
            'isJuridical' => '0',
            'codeKladr' => $arFields["kladr_city"],
            'codeOkato' => '',
            'car[make]' => $car["info"]["mark"],
            'car[model]' => $car["info"]["model"],
            'car[category]' => explode(' - ', $arFields["Category"])[0],
            'car[power]' => $arFields["Power"],
            'car[documents][registrationNumber]' => $arFields["LicensePlate"],
            'car[documents][chassisNumber]' => $arFields["number_chassis"],
            'car[documents][carcaseNumber]' => $arFields["number_bodywork"],
            'car[documents][vin]' => $arFields["VIN"],
            'usagePeriod[0][dateStart]' => $dateStart,
            'usagePeriod[0][dateEnd]' => $dateEnd,
        );
        if ($min) {
            if ($arFields["Owner_calc"]["LastName"]) {
                $arPost['owner[name]'] = $arFields["Owner_calc"]['FirstName'];
                $arPost['owner[lastname]'] = $arFields["Owner_calc"]['LastName'];
                $arPost['owner[middlename]'] = $arFields["Owner_calc"]['MiddleName'];
                $arPost['owner[birthday]'] = $arFields["Owner_calc"]['BirthDate'];
                $arPost['owner[document][dateIssue]'] = $arFields["Owner"]['Date_pass'];
                $arPost['owner[document][issued]'] = $arFields["Owner"]['For_doc_pass'];
                $arPost['owner[document][number]'] = $arFields["Owner"]['Number'];
                $arPost['owner[document][series]'] = $arFields["Owner"]['Seria'];
                $arPost['owner[organization][name]'] = '';
                $arPost['owner[organization][inn]'] = '';
                $arPost['owner[organization][kpp]'] = '';
            } elseif (!empty($arFields["Drivers"]['Driver'])) {
                foreach ($arFields["Drivers"]['Driver'] as $key => $driver) {
                    if ($key == 0) {
                        $arPost['owner[name]'] = $driver["FirstName"];
                        $arPost['owner[lastname]'] = $driver["LastName"];
                        $arPost['owner[middlename]'] = $driver["MiddleName"];
                        $arPost['owner[birthday]'] = $driver["BirthDate"];
                        $arPost['owner[document][dateIssue]'] = $driver["ExperienceDate"];
                        $arPost['owner[document][issued]'] = '';
                        $arPost['owner[document][number]'] = $driver["DriverDocument"]["Number"];
                        $arPost['owner[document][series]'] = $driver["DriverDocument"]["Seria"];
                        $arPost['owner[organization][name]'] = '';
                        $arPost['owner[organization][inn]'] = '';
                        $arPost['owner[organization][kpp]'] = '';
                    }
                    $arPost['drivers[' . $key . '][name]'] = $driver["FirstName"];
                    $arPost['drivers[' . $key . '][lastname]'] = $driver["LastName"];
                    $arPost['drivers[' . $key . '][middlename]'] = $driver["MiddleName"];
                    $arPost['drivers[' . $key . '][birthday]'] = $driver["BirthDate"];
                    $arPost['drivers[' . $key . '][license][dateBeginDrive]'] = $driver["ExperienceDate"];
                    $arPost['drivers[' . $key . '][license][dateIssue]'] = $driver["ExperienceDate"];
                    $arPost['drivers[' . $key . '][license][number]'] = $driver["DriverDocument"]["Number"];
                    $arPost['drivers[' . $key . '][license][series]'] = $driver["DriverDocument"]["Seria"];
                }
            }
        } else {
            $arPost['owner[name]'] = $arFields["Insurer"]['FirstName'];
            $arPost['owner[lastname]'] = $arFields["Insurer"]['LastName'];
            $arPost['owner[middlename]'] = $arFields["Insurer"]['MiddleName'];
            $arPost['owner[birthday]'] = $arFields["Insurer"]['BirthDate'];
            $arPost['owner[document][dateIssue]'] = $arFields["Insurer"]["Date_pass"];
            $arPost['owner[document][issued]'] = $arFields["Insurer"]["For_doc_pass"];
            $arPost['owner[document][number]'] = $arFields["Insurer"]['Number'];
            $arPost['owner[document][series]'] = $arFields["Insurer"]['Seria'];
            $arPost['owner[organization][name]'] = '';
            $arPost['owner[organization][inn]'] = '';
            $arPost['owner[organization][kpp]'] = '';

            if (!empty($arFields["Drivers"]['Driver'])) {
                foreach ($arFields["Drivers"]['Driver'] as $key => $driver) {
                    $arPost['drivers[' . $key . '][name]'] = $driver["FirstName"];
                    $arPost['drivers[' . $key . '][lastname]'] = $driver["LastName"];
                    $arPost['drivers[' . $key . '][middlename]'] = $driver["MiddleName"];
                    $arPost['drivers[' . $key . '][birthday]'] = $driver["BirthDate"];
                    $arPost['drivers[' . $key . '][license][dateBeginDrive]'] = $driver["ExperienceDate"];
                    $arPost['drivers[' . $key . '][license][dateIssue]'] = $driver["ExperienceDate"];
                    $arPost['drivers[' . $key . '][license][number]'] = $driver["DriverDocument"]["Number"];
                    $arPost['drivers[' . $key . '][license][series]'] = $driver["DriverDocument"]["Seria"];
                }
            }
        }
        $err = '';
        $response = '';
        if ($arPost) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url_servic . "/calculate/?fullInformation=true",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $arPost,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
        }

        $resp = json_decode($response);
        //print_r($resp);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/calculate_request.txt', print_r($arPost, true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/calculate_response.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'calculate_response', 'dop_mess' => $err, 'method' => '/calculate/'];
        } elseif ($response) {
            if (!empty($resp->data[0])) {
                $data = $resp->data[0];

                if ($data->id) {
                    $premium = $this::premiumInfo($data->id);
                    if ($premium["success"]) {
                        return ['success' => true, 'info' => $premium["info"], 'infoCalc' => $data];
                    } else {
                        return ['success' => false, 'error' => $premium["error"], 'method' => '/calculate/'];
                    }
                }
            } elseif (!empty($resp->message)) {
                return ['success' => false, 'error' => $resp->message, 'method' => '/calculate/'];
            }
        }
    }

    public function premiumInfo($id)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/calculate/" . $id . "/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $resp = json_decode($response);
        /*echo '<pre>';
        print_r($resp);
        echo '</pre>';*/
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/calculate_id_request.txt', print_r(array($this->url_servic . "/calculate/" . $id . "/?key=" . $this->key_renins), true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/calculate_id_response.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'calculate_id_response', 'dop_mess' => $err, 'method' => '/calculate/:id'];
        } else {
            if (is_object($resp->data)) {
                if (!empty($resp->data)) {
                    $data = $resp->data;

                    /*$listDrive = $data->response->KBM;
                    $max = null;
                    foreach ($listDrive as $item) {
                        if($item->KBM > $max || $max === null)
                            $max = $item->KBM;
                    }*/
                    $coefficient = [];
                    if (!empty($data)) {
                        if (!empty($data->response)) {
                            $coeff = $data->response->Details->Coefficient;
                            $coefficient = [
                                "Tb" => $coeff[0]->value,
                                "Kt" => $coeff[1]->value,
                                "Kbm" => $coeff[2]->value,
                                "Ko" => $coeff[3]->value,
                                "Kn" => $coeff[4]->value,
                                "Kp" => $coeff[5]->value,
                                "Ks" => $coeff[6]->value,
                                "Km" => $coeff[7]->value,
                                "Kvs" => $coeff[8]->value,
                                "Kpr" => $coeff[9]->value,
                            ];
                        } else {
                            return ['success' => false, 'error' => $resp->message, 'method' => '/calculate/:id'];
                        }
                    } else {
                        return ['success' => false, 'error' => $resp->message, 'method' => '/calculate/:id'];
                    }
                    return [
                        'success' => true,
                        'info' => [
                            "sum" => $data->premium,
                            "Coefficients" => $coefficient
                        ]
                    ];
                } elseif (!empty($resp->message)) {
                    return ['success' => false, 'error' => $resp->message, 'method' => '/calculate/:id'];
                }
            } elseif (!strpos(mb_strtolower($resp->data), 'расчет не окончен') === false) {
                sleep(5);
                $premiumInfo = $this::premiumInfo($id);
                return $premiumInfo;
            }
        }
    }

    public function marksInfo($arMark = false)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/models/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ['success' => false, 'error' => 'Mark info', 'dop_mess' => $err];
        } else {
            $resp = json_decode($response);

            if ($arMark) {
                $arFind = [];
                if (!empty($resp->data)) {
                    foreach ($resp->data as $mAr) {
                        if (strtolower($mAr->make) == strtolower($arMark["Mark"]) || stripos(strtolower($mAr->make), strtolower($arMark["Mark"])) !== false) {
                            if (!empty($mAr->models)) {
                                foreach ($mAr->models as $model) {
                                    if (str_replace(' ', '', strtolower($model->model)) == str_replace(' ', '', strtolower($arMark["Model"]))) {
                                        $arFind["mark"] = $mAr->make;
                                        $arFind["model"] = $model->model;
                                    } else {
                                        continue;
                                    }
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }
                } else {
                    return ['success' => false, 'error' => 'mark_data'];
                }
                if (!empty($arFind)) {
                    return ['success' => true, 'info' => $arFind];
                } else {
                    return ['success' => false, 'error' => 'Марка или модель автомобиля не найдена в сервисе'];
                }
            } else {
                echo '<pre>';
                print_r($resp);
                echo '</pre>';
            }
        }
    }

    public function createContract($arFields, $sigment = false)
    {
        $korp = '';
        if (!empty($arFields["Insurer"]["address"]["building"])) {
            $korp = $arFields["Insurer"]["address"]["building"];
        }
        $calcID = $arFields["infoCalc"]->id;

        $arProps = array(
            'calculationId' => $calcID,
            'cabinet[email]' => $arFields["email"],
            'cabinet[password]' => '11111111',
            'owner[addressJuridical][country]' => 'Россия',
            'owner[addressJuridical][zip]' => '',
            'owner[addressJuridical][city]' => $arFields["Insurer"]["address"]["city"],
            'owner[addressJuridical][street]' => $arFields["Insurer"]["address"]["street"],
            'owner[addressJuridical][home]' => $arFields["Insurer"]["address"]["home"] . $korp,
            'owner[addressJuridical][flat]' => !empty($arFields["Insurer"]["address"]["apartament"]) ? $arFields["Insurer"]["address"]["apartament"] : '',
            'owner[addressJuridical][kladr]' => $arFields["kladr_city"],
            'owner[addressJuridical][okato]' => '',
            'owner[addressFact][country]' => 'Россия',
            'owner[addressFact][zip]' => '',
            'owner[addressFact][city]' => $arFields["Insurer"]["address"]["city"],
            'owner[addressFact][street]' => $arFields["Insurer"]["address"]["street"],
            'owner[addressFact][home]' => $arFields["Insurer"]["address"]["home"] . $korp,
            'owner[addressFact][flat]' => !empty($arFields["Insurer"]["address"]["apartament"]) ? $arFields["Insurer"]["address"]["apartament"] : '',
            'owner[addressFact][kladr]' => $arFields["kladr_city"],
            'owner[addressFact][okato]' => '',
            'owner[email]' => $arFields["email"],
            'owner[phone]' => preg_replace("/[^,.0-9]/", '', $arFields["phone"]),
            'isInsurerJuridical' => '0',
            'insurer[name]' => $arFields["Insurer"]["FirstName"],
            'insurer[lastname]' => $arFields["Insurer"]["LastName"],
            'insurer[middlename]' => $arFields["Insurer"]["MiddleName"],
            'insurer[birthday]' => $arFields["Insurer"]["BirthDate"],
            'insurer[document][dateIssue]' => $arFields["Insurer"]["Date_pass"],
            'insurer[document][issued]' => $arFields["Insurer"]["For_doc_pass"],
            'insurer[document][number]' => $arFields["Insurer"]["Number"],
            'insurer[document][series]' => $arFields["Insurer"]["Seria"],
            'insurer[organization][name]' => '',
            'insurer[organization][inn]' => '',
            'insurer[organization][kpp]' => '',
            'insurer[addressJuridical][country]' => 'Россия',
            'insurer[addressJuridical][zip]' => '',
            'insurer[addressJuridical][city]' => $arFields["Insurer"]["address"]["city"],
            'insurer[addressJuridical][street]' => $arFields["Insurer"]["address"]["street"],
            'insurer[addressJuridical][home]' => $arFields["Insurer"]["address"]["home"] . $korp,
            'insurer[addressJuridical][flat]' => !empty($arFields["Insurer"]["address"]["apartament"]) ? $arFields["Insurer"]["address"]["apartament"] : '',
            'insurer[addressJuridical][kladr]' => $arFields["kladr_city"],
            'insurer[addressJuridical][okato]' => '',
            'insurer[addressFact][country]' => 'Россия',
            'insurer[addressFact][zip]' => '',
            'insurer[addressFact][city]' => $arFields["Insurer"]["address"]["city"],
            'insurer[addressFact][street]' => $arFields["Insurer"]["address"]["street"],
            'insurer[addressFact][home]' => $arFields["Insurer"]["address"]["home"] . $korp,
            'insurer[addressFact][flat]' => !empty($arFields["Insurer"]["address"]["apartament"]) ? $arFields["Insurer"]["address"]["apartament"] : '',
            'insurer[addressFact][kladr]' => $arFields["kladr_city"],
            'insurer[addressFact][okato]' => '',
            'insurer[email]' => $arFields["email"],
            'insurer[phone]' => preg_replace("/[^,.0-9]/", '', $arFields["phone"]),
            'car[year]' => $arFields["YearIssue"],
            'car[isNew]' => '0',
            'car[' . $arFields["type_doc_ts"] . '][serie]' => $arFields["DocCarSerial"],
            'car[' . $arFields["type_doc_ts"] . '][number]' => $arFields["DocCarNumber"],
            'car[' . $arFields["type_doc_ts"] . '][dateIssue]' => gmdate('Y-m-d', strtotime($arFields["DocumentCarDate"])),
            'key' => $this->key_renins,
            'diagnostic[number]' => '',//$arFields["cart_diag"] ? $arFields["cart_diag"] : '',
            'diagnostic[validDate]' => '',//$arFields["date_cart_diag"] ? $arFields["date_cart_diag"] : '',
            //'CheckSegment' => 1
        );
        if ($sigment) {
            $arProps['CheckSegment'] = 1;
        } else {
            $arProps['CheckSegment'] = 0;
        }
        if ($arProps) {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url_servic . "/create/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $arProps,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
        }
        $resp = json_decode($response);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/create_request.txt', print_r($arProps, true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/create_response.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'CreateContract', 'dop_mess' => $err, 'method' => '/create/'];
        } else {
            if (!empty($resp->data->policyId)) {
                return ['success' => true, 'id_create' => $resp->data->policyId];
            } elseif (!empty($resp->message)) {
                return ['success' => false, 'error' => $resp->message, 'method' => '/create/'];
            }
        }
    }

    public function infoContract($id, $arFields)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $id . "/status/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $resp = json_decode($response);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/status_request.txt', print_r(array($this->url_servic . "/policy/" . $id . "/status/?key=" . $this->key_renins), true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/status_response.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'status_response', 'dop_mess' => $err, 'method' => '/status/'];
        } else {
            if (!empty($resp->data->result) || $resp->message == "ok") {
                if (!empty($resp->data->return)) {
                    if ($resp->data->return->Status == 'wait') {
                        sleep(15);
                        $st = $this::infoContract($id, $arFields);
                        return $st;
                    } else {
                        return ['success' => false, 'error' => $resp->message, 'method' => '/status/'];
                    }
                } elseif (!empty($resp->data->Status)) {
                    $status_mess = $resp->data->Status;
                    if ($status_mess == "ok") {
                        $calcInfo = $this::calculate($arFields);
                        if ($calcInfo["success"]) {
                            $arFields["infoCalc"] = $calcInfo["infoCalc"];
                            $crate = $this::createContract($arFields);
                            if ($crate["success"]) {
                                $confirm = $this::statusConfirm($crate["id_create"]);
                                $confirm["sum"] = $arFields["sum"];
                                return $confirm;
                            } else {
                                return $crate;
                            }
                        }
                    } else {
                        return ['success' => false, 'error' => $status_mess, 'method' => '/status/'];
                    }
                } elseif (!empty($resp->message)) {
                    return ['success' => false, 'error' => $resp->message, 'method' => '/status/'];
                }
            } elseif (!empty($resp->message)) {
                return ['success' => false, 'error' => $resp->message, 'method' => '/status/'];
            }
        }
    }

    public function statusConfirm($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $id . "/status/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $resp = json_decode($response);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/status_request2.txt', print_r(array($this->url_servic . "/policy/" . $id . "/status/?key=" . $this->key_renins), true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/status_response2.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'status_response2', 'dop_mess' => $err, 'method' => '/status2/'];
        } else {
            if (!empty($resp->data->result) || $resp->message == "ok") {
                if (!empty($resp->data->return)) {
                    if ($resp->data->return->Status == 'wait') {
                        sleep(15);
                        $st = $this::statusConfirm($id);
                        return $st;
                    } else {
                        return ['success' => false, 'error' => $resp->data->return->Status];
                    }
                } elseif (!empty($resp->data->Status)) {
                    $status_mess = $resp->data->Status;
                    if ($status_mess == "ok") {
                        $list_pay = $this::listPay($id);
                        if ($list_pay["success"]) {
                            if (!empty($list_pay["list"][0]->id)) {
                                $pay = $this::pay($id, $list_pay["list"][0]->id);
                                if ($pay["success"]) {
                                    return ['success' => true, 'url' => $pay["url_pay"], 'order_id' => $pay["order_id"], 'policeID' => $id];
                                    //return ['success' => false, 'error' => 'Проверка расчетов'];
                                } else {
                                    return $pay;
                                }
                            }
                        } else {
                            return $list_pay;
                        }
                    } else {
                        return ['success' => false, 'error' => $status_mess, 'method' => '/status2/'];
                    }
                } elseif (strpos($resp->message, 'Неизвестная ошибка')) {
                    sleep(15);
                    $st = $this::statusConfirm($id);
                    return $st;
                } elseif (!empty($resp->message)) {
                    return ['success' => false, 'error' => $resp->message, 'method' => '/status2/'];
                }
            } elseif (strpos($resp->message, 'Неизвестная ошибка')) {
                sleep(15);
                $st = $this::statusConfirm($id);
                return $st;
            } elseif (!empty($resp->message)) {
                return ['success' => false, 'error' => $resp->message, 'method' => '/status2/'];
            }
        }
    }

    public function listPay($id_contract)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $id_contract . "/acquiring/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $resp = json_decode($response);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/acquiring_request.txt', print_r(array($this->url_servic . "/policy/" . $id_contract . "/acquiring/?key=" . $this->key_renins), true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/acquiring_response.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'acquiring_response', 'dop_mess' => $err, 'method' => '/acquiring/'];
        } else {
            if (!empty($resp->data->acquirings)) {
                //print_r(json_decode($response));
                return ['success' => true, 'list' => $resp->data->acquirings];
            } elseif (!empty($resp->message)) {
                return ['success' => false, 'error' => $resp->message, 'method' => '/acquiring/'];
            }
        }
    }

    public function pay($id_contract, $code_pay)
    {

        $url_back = $this->urlSite . '/renins/infoPay?id=' . $id_contract;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $id_contract . "/acquiring/" . $code_pay . "/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('fail_url' => $url_back, 'success_url' => $url_back),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $resp = json_decode($response);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/pay_request.txt', print_r(array($this->url_servic . "/policy/" . $id_contract . "/acquiring/" . $code_pay . "/?key=" . $this->key_renins), true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/pay_response.txt', print_r($resp, true), FILE_APPEND);

        if ($err) {
            return ['success' => false, 'error' => 'pay_request', 'dop_mess' => $err, 'method' => '/pay/'];
        } else {
            if (!empty($resp->data->url)) {
                return ['success' => true, 'url_pay' => $resp->data->url, 'order_id' => $resp->data->orderId];
            } elseif (!empty($resp->message)) {
                return ['success' => false, 'error' => $resp->message, 'method' => '/pay/'];
            }
        }
    }

    public function docInitPdf($id)
    {
        $isPay = $this->isPay($id);
        if ($isPay["success"]) {

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url_servic . "/policy/" . $id . "/pdf/?key=" . $this->key_renins,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $resp = json_decode($response);
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/doc_pdf_request.txt', print_r(array($this->url_servic . "/policy/" . $id . "/pdf/?key=" . $this->key_renins), true), FILE_APPEND);
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/renins/doc_pdf_response.txt', print_r($resp, true), FILE_APPEND);

            if ($err) {
                return ['success' => false, 'error' => 'Ошибка получения документа', 'dop_mess' => $err];
            } else {
                if (!empty($resp->data)) {
                    if (!empty($resp->data->return)) {
                        //$urlFile = "/docs/renins_".$id.".pdf";
                        //file_put_contents($urlFile, base64_encode($resp->data->return));
                        //$file = $this::base64ToFile($resp->data->return,$urlFile);
                        return ['success' => true, 'base64' => $resp->data->return];

                    } else {
                        return ['success' => false, 'error' => $resp->data->message];
                    }
                } else {
                    return ['success' => false, 'error' => $resp->message];
                }
            }
        } else {
            return $isPay;
        }
    }

    public function docInitApplication($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $id . "/application/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ['success' => false, 'error' => 'Ошибка получения документа', 'dop_mess' => $err];
        } else {
            $resp = json_decode($response);
            if (!empty($resp->data)) {
                if (!empty($resp->data->return)) {
                    //$urlFile = "/docs/renins_".$id.".pdf";
                    //file_put_contents($urlFile, base64_encode($resp->data->return));
                    //$file = $this::base64ToFile($resp->data->return,$urlFile);
                    return ['success' => true, 'base64' => $resp->data->return];
                } else {
                    return ['success' => false, 'error' => $resp->message];
                }
            } else {
                return ['success' => false, 'error' => $resp->message];
            }
        }
    }

    public function isPay($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $id . "/info/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ['success' => false, 'error' => 'Сервис временно не доступен', 'dop_mess' => $err];
        } else {
            $resp = json_decode($response);
            if (!empty($resp->data)) {
                if (!empty($resp->data->return)) {
                    if (!empty($resp->data->return->StatusPay) && $resp->data->return->StatusPay == 'Не оплачен') {
                        return ['success' => false, 'error' => $resp->data->return->StatusPay];
                    } elseif (!empty($resp->data->return->StatusPay)) {
                        return ['success' => true, 'info' => $resp->data->return->StatusPay];
                    } else {
                        return ['success' => false, 'error' => 'Сервис временно не доступен', 'dop_mess' => $err];
                    }
                } else {
                    return ['success' => false, 'error' => $resp->message];
                }
            } else {
                return ['success' => false, 'error' => $resp->message];
            }
        }
    }

    public function infoStatusDoc($police)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_servic . "/policy/" . $police . "/info/?key=" . $this->key_renins,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return ['success' => false, 'error' => $err];
        } else {
            $resp = json_decode($response);
            if (!empty($resp->data)) {
                if (!empty($resp->data->return)) {
                    if ($resp->data->return->StatusPay == 'Оплачен') {
                        return ["success" => true, "doc" => ""];
                    } else {
                        return ["success" => false, "error" => $resp->message];
                    }
                } else {
                    return ["success" => false, "error" => $resp->message];
                }
            } else {
                return ["success" => false, "error" => $resp->message];
            }
        }
    }

    public function infoPayPage(Request $request)
    {
        if (!empty($request->input('id'))) {
            $id = $request->input('id');

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url_servic . "/policy/" . $id . "/info/?key=" . $this->key_renins,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);


            $arrMess = [
                'title' => 'Информация об оплате Ренессанс страхование',
                'slug' => 'pay',
                'page_title' => 'Информация об оплате'
            ];
            if ($err) {
                $arrMess = ['error' => 'Сервис временно не доступен', 'dop_mess' => $err];
            } else {
                $resp = json_decode($response);
                if (!empty($resp->data)) {
                    if (!empty($resp->data->return)) {
                        $arrMess['body'] = '
                            <p>Статус договора: ' . $resp->data->return->Status . '</p>
                            <p>Статус оплаты: ' . $resp->data->return->StatusPay . '</p>
                        ';
                        if ($resp->data->return->StatusPay == 'Оплачен') {
                            $find_order = Order::findOrderPolice($id);
                            $find_order->update(["pay" => 1]);
                            $doc = $this::docInitPdf($id);
                            if ($doc["success"]) {
                                $arrMess['body'] .= '<p><a class="btn" href="/renins/file?type=pdf&id=' . $id . '" target="_blank">Скачать документ договора</a></p>';
                            }
                            $doc2 = $this::docInitApplication($id);
                            if ($doc2["success"]) {
                                $arrMess['body'] .= '<p><a class="btn" href="/renins/file?type=application&id=' . $id . '" target="_blank">Скачать документ заявения</a></p>';
                            }
                        }
                    } else {
                        $arrMess['body'] = '<p>' . $resp->message . '</p>';
                    }
                } else {
                    $arrMess['body'] = '<p>' . $resp->message . '</p>';
                }
            }
            if ($arrMess)
                return view('custom_page.payInfo', ['page' => (object)$arrMess]);
        } else {
            abort(404, 'Page not found');
        }
    }

    public function docPdf(Request $request)
    {
        if (!empty($request->input('id')) && !empty($request->input('type'))) {
            $id = $request->input('id');
            $type = $request->input('type');
            if ($type == 'pdf') {
                $doc = $this::docInitPdf($id);
            } elseif ($type == 'application') {
                $doc = $this::docInitApplication($id);
            }
            if ($doc["success"]) {
                /*$pdf = PDF::loadView('pdf.base64', ['file' => $doc["base64"]]);
                return $pdf->stream('test.pdf');*/
                return view('pdf.base64', ['base64' => $doc["base64"], 'name' => $type]);
            } else {
                abort(404, 'Page not found');
            }
        } else {
            abort(404, 'Page not found');
        }
    }
}
