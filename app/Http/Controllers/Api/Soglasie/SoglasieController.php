<?php

namespace App\Http\Controllers\Api\Soglasie;

use App\Http\Controllers\Controller;

class SoglasieController extends Controller
{
    protected $login;
    protected $pass;
    protected $pay;
    protected $subuser;
    protected $subuser_pass;
    protected $env_type;

    public function __construct()
    {
        $this->env_type = config('app.env');
        $this->login = config('app.login_soglasie');
        $this->pass = config('app.pass_soglasie');
        $this->subuser = config('app.subuser_soglasie');
        $this->subuser_pass = config('app.subuser_pass_soglasie');
    }

    public function calc($arFilds)
    {
        $kbm = ["success" => true, "kbm" => 1];
        if (!empty($arFilds["Drivers"]["Driver"]))
            $kbm = $this->dkbm($arFilds);
        if ($kbm["success"]) {
            $arFilds["kbm_req"] = $kbm["kbm"];
            $scoring = $this->scoring($arFilds);
            if ($scoring["success"]) {
                $arFilds["scoringID"] = $scoring["scoringid"];
                $calc = $this->CalcProduct($arFilds);
                if (!empty($arFilds["type_calc"]) && $arFilds["type_calc"] == 'min') {
                    return $calc;
                } else if ($calc["success"]) {
                    $policy = $this->Policy($arFilds);
                    if ($policy["success"]) {
                        $this->PolicyStatus($policy["policyId"]);
                        if (!empty($this->pay)) {
                            $this->pay["sum"] = $calc["info"]["sum"];
                            return $this->pay;
                        } else {
                            return ['success' => false, 'error' => 'Сервис временно не доступен', 'method' => 'Status'];
                        }
                    } else {
                        return $policy;
                    }
                } else {
                    return $calc;
                }
            } else {
                return $scoring;
            }
        } else {
            return $kbm;
        }
    }

    public function dkbm($arFilds)
    {
        $LicensePlate = $arFilds["LicensePlate"];

        $VIN = $arFilds["VIN"];
        $number_bodywork = $arFilds["number_bodywork"];
        $number_chassis = $arFilds["number_chassis"];
        $persons = '';
        if (!empty($arFilds["Drivers"]["Driver"])) {
            foreach ($arFilds["Drivers"]["Driver"] as $drive) {
                $persons .= '<PhysicalPerson>
                                <DriverDocument>
                                    <Serial>' . $drive["DriverDocument"]["Seria"] . '</Serial>
                                    <Number>' . $drive["DriverDocument"]["Number"] . '</Number>
                                </DriverDocument>
                                <PersonNameBirthHash>### ' . $drive["LastName"] . '|' . $drive["FirstName"] . '|' . $drive["MiddleName"] . '|' . date("d-m-Y", strtotime($drive["BirthDate"])) . '</PersonNameBirthHash>
                            </PhysicalPerson>';
            }
            $multi = true;
        } else {
            $LastName = $arFilds["Owner"]["LastName"] ? $arFilds["Owner"]["LastName"] : $arFilds["Owner_calc"]["LastName"];
            $FirstName = $arFilds["Owner"]["FirstName"] ? $arFilds["Owner"]["FirstName"] : $arFilds["Owner_calc"]["FirstName"];
            $MiddleName = $arFilds["Owner"]["MiddleName"] ? $arFilds["Owner"]["MiddleName"] : $arFilds["Owner_calc"]["MiddleName"];
            $BirthDate = $arFilds["Owner"]["BirthDate"] ? $arFilds["Owner"]["BirthDate"] : $arFilds["Owner_calc"]["BirthDate"];
            $NumberPass = $arFilds["Owner"]["Number"] ? $arFilds["Owner"]["Number"] : $arFilds["Owner_calc"]["Number"];
            $SeriaPass = $arFilds["Owner"]["Seria"] ? $arFilds["Owner"]["Seria"] : $arFilds["Owner_calc"]["Seria"];
            $persons = '<PhysicalPerson>
                                <PersonDocument>
                                    <DocPerson>6</DocPerson>
                                    <Serial>' . $SeriaPass . '</Serial>
                                    <Number>' . $NumberPass . '</Number>
                                </PersonDocument>
                                <PersonNameBirthHash>### ' . $LastName . '|' . $FirstName . '|' . $MiddleName . '|' . date("d-m-Y", strtotime($BirthDate)) . '</PersonNameBirthHash>
                         </PhysicalPerson>';
            $multi = false;
        }
        if (!empty($persons)) {
            $personsSoap = new \SoapVar($persons, XSD_ANYXML);
            $wsdl = "https://b2b.soglasie.ru/RSAProxyEx/dkbm?wsdl";
            $login = $this->login;
            $password = $this->pass;
            $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0, "login" => $login, "password" => $password));
            $client->getKbm(array('request' => array(
                    'CalcRequestValue' => [
                        'InsurerID' => '',
                        'CalcKBMRequest' => [
                            'CarIdent' => [
                                'LicensePlate' => $LicensePlate,
                                'VIN' => $VIN,
                                'BodyNumber' => $number_bodywork,
                                'ChassisNumber' => $number_chassis,
                            ],
                            'DriversRestriction' => $multi,
                            'DateKBM' => date("Y-m-d\Th:m:s"),
                            'PhysicalPersons' => [
                                'PhysicalPerson' => $personsSoap
                            ],
                        ]
                    ]
                ))
            );
            $resp = $client->__last_response;
            $req = $client->__last_request;
            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logGetKbm_response.xml', print_r($resp, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logGetKbm_request.xml', print_r($req, true));
        }
        if (!empty($responseArray["SBody"]["ns2getKbmResponse"]["response"]["CalcResponseValue"])) {
            return ["success" => true, "kbm" => $responseArray["SBody"]["ns2getKbmResponse"]["response"]["CalcResponseValue"]["CalculatedKBMValue"]];
        } else {
            return ["success" => false, 'error' => 'Сервис временно не доступен', 'method' => 'kbm'];
        }
    }

    public function scoring($arFilds)
    {
        $city = $arFilds["CityOfUse"];
        $LastName = $arFilds["Owner"]["LastName"] ? $arFilds["Owner"]["LastName"] : $arFilds["Owner_calc"]["LastName"];
        $FirstName = $arFilds["Owner"]["FirstName"] ? $arFilds["Owner"]["FirstName"] : $arFilds["Owner_calc"]["FirstName"];
        $MiddleName = $arFilds["Owner"]["MiddleName"] ? $arFilds["Owner"]["MiddleName"] : $arFilds["Owner_calc"]["MiddleName"];
        $BirthDate = $arFilds["Owner"]["BirthDate"] ? $arFilds["Owner"]["BirthDate"] : $arFilds["Owner_calc"]["BirthDate"];
        $NumberPass = $arFilds["Owner"]["Number"] ? $arFilds["Owner"]["Number"] : $arFilds["Owner_calc"]["Number"];
        $SeriaPass = $arFilds["Owner"]["Seria"] ? $arFilds["Owner"]["Seria"] : $arFilds["Owner_calc"]["Seria"];
        $datePass = date("Y-m-d", strtotime($arFilds["Owner"]["Date_pass"]));
        $typePass = 6;

        $requestSoap = [];
        if ($arFilds["type_calc"] == 'min') {
            if (!empty($arFilds["Drivers"])) {
                $driveFirst = $arFilds["Drivers"]["Driver"][0];
                $LastName = $driveFirst["LastName"];
                $FirstName = $driveFirst["FirstName"];
                $MiddleName = $driveFirst["MiddleName"];
                $BirthDate = $driveFirst["BirthDate"];
                $NumberPass = $driveFirst["DriverDocument"]["Number"];
                $SeriaPass = $driveFirst["DriverDocument"]["Seria"];
                $datePass = date("Y-m-d", strtotime($driveFirst["DriverDocument"]["DateIssue"]));
                $typePass = 15;
            }
            $requestSoap = new \SoapVar('
               <request partial="true">
                <private>
                  <lastname>' . $LastName . '</lastname>
                  <firstname>' . $FirstName . '</firstname>
                  <middlename>' . $MiddleName . '</middlename>
                  <birthday>' . $BirthDate . '</birthday>
                </private>
              </request>', XSD_ANYXML);
        } else {
            $cityReg = '';
            if (!empty($arFilds["Owner"]["address"]["city"])) {
                $cityReg = $arFilds["Owner"]["address"]["city"];
            }
            $streetReg = '';
            if (!empty($arFilds["Owner"]["address"]["street"])) {
                $streetReg = $arFilds["Owner"]["address"]["street"];
            }
            $buildingReg = '';
            if (!empty($arFilds["Owner"]["address"]["home"])) {
                $buildingReg = $arFilds["Owner"]["address"]["home"];
            }
            $flatReg = '';
            if (!empty($arFilds["Owner"]["address"]["apartament"])) {
                $flatReg = $arFilds["Owner"]["address"]["apartament"];
            }
            $requestSoap = new \SoapVar('
               <request>
                <private>
                   <lastname>' . $LastName . '</lastname>
                   <firstname>' . $FirstName . '</firstname>
                   <middlename>' . $MiddleName . '</middlename>
                   <birthday>' . $BirthDate . '</birthday>
                   <birthplace/>
                   <documents>
                      <document>
                         <doctype>' . $typePass . '</doctype>
                         <docseria>' . $SeriaPass . '</docseria>
                         <docnumber>' . $NumberPass . '</docnumber>
                         <docdatebegin>' . $datePass . '</docdatebegin>
                         <docplace/>
                      </document>
                   </documents>
                   <addresses>
                      <address>
                         <type>Registered</type>
                         <index/>
                         <address/>
                         <city>' . $cityReg . '</city>
                         <street>' . $streetReg . '</street>
                         <building>' . $buildingReg . '</building>
                         <flat>' . $flatReg . '</flat>
                      </address>
                   </addresses>
                </private>
             </request>', XSD_ANYXML);
            /*$private = [
                'lastname' => $LastName,
                'firstname' => $FirstName,
                'middlename' => $MiddleName,
                'birthday' => $BirthDate,
                'birthplace' => false,
                'documents' => [
                    'document' => [
                        'doctype' => $typePass,
                        'docseria' => $SeriaPass,
                        'docnumber' => $NumberPass,
                        'docdatebegin' => $datePass,
                        'docplace' => false,
                    ],
                ],
                'addresses' => [
                    'address' => [
                        'type' => 'Registered',
                        'address' => false,
                        'city' => !empty($arFilds["Owner"]["address"]["city"]) ? $arFilds["Owner"]["address"]["city"] : '',
                        'street' => !empty($arFilds["Owner"]["address"]["street"]) ? $arFilds["Owner"]["address"]["street"] : '',
                        'building' => !empty($arFilds["Owner"]["address"]["home"]) ? $arFilds["Owner"]["address"]["home"] : '',
                        'flat' => !empty($arFilds["Owner"]["address"]["apartament"]) ? $arFilds["Owner"]["address"]["apartament"] : '',
                    ],
                ],
            ];*/
        }
        if (!empty($requestSoap)) {
            $wsdl = "https://b2b.soglasie.ru/scoring/scoring?wsdl";
            $login = $this->login;
            $password = $this->pass;
            //$requestSoap = (array)$requestSoap;
            $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 1, "login" => $login, "password" => $password));
            $client->getScoringId(["request" => $requestSoap]);
            $resp = $client->__last_response;
            $req = $client->__last_request;
            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logGetScoringId_response.xml', print_r($resp, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logGetScoringId_request.xml', print_r($req, true));
        }
        if (!empty($responseArray["SBody"]["ns2getScoringIdResponse"]["response"]["scoringid"])) {
            return array('success' => true, 'scoringid' => $responseArray["SBody"]["ns2getScoringIdResponse"]["response"]["scoringid"]);
        } else {
            return array('success' => false);
        }
    }

    public function getCars($car = false)
    {
        $wsdl = "https://b2b.soglasie.ru/CCM/CCMPort.wsdl";
        $login = $this->login;
        $password = $this->pass;
        $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0, "login" => $login, "password" => $password));
        $result = $client->getCatalog(array('catalog' => 1436)
        );
        if (!empty($result->catalog->values->data) && !empty($car)) {
            $model = '';
            foreach ($result->catalog->values->data as $item) {
                if (!strpos('----' . strtolower($item->str), strtolower($car["model"])) === false && !strpos('----' . strtolower($item->str), strtolower($car["mark"])) === false) {
                    $model = $item->val;
                }
            }
            if (!empty($model)) {
                return ["success" => true, "model" => $model];
            } else {
                return ["success" => false, 'error' => 'Сервис временно не доступен', 'method' => 'getCars'];
            }
        } else {
            echo '<pre>';
            print_r($result);
            echo '</pre>';
        }
    }

    public function gender($name, $calc = false)
    {
        if ($calc) {
            $male = 'М';
            $female = 'Ж';
        } else {
            $male = 'male';
            $female = 'female';
        }
        $nameEnds = (mb_substr($name, -1, 1));
        if (mb_strtolower($name) == 'саша' || mb_strtolower($name) == 'женя' || mb_strtolower($name) == 'слава') {
            return $male;
        } elseif ($nameEnds == 'а' || $nameEnds == 'я') {
            return $female;
        } else {
            return $male;
        }
    }

    public function CalcProduct($arFilds)
    {
        function getAge($datePerson, $date)
        {
            if ($datePerson[1] > $date[1] || $datePerson[1] == $date[1] && $datePerson[2] > $date[0])
                return ($date[2] - $datePerson[0] - 1);
            else
                return ($date[2] - $datePerson[0]);
        }

        $InsuranceMonth = 12;
        $InsuranceID = 8;
        $car = $this->getCars(["model" => $arFilds["Model"], "mark" => $arFilds["Mark"]]);
        $Category = $arFilds["Category"];
        $Type = 2; // Легковые автомобили
        $kladrCity = $arFilds["kladr_city"];
        $phone = $arFilds["phone"];
        $Power = $arFilds["Power"];
        $Purpose = 1;
        $LicensePlate = $arFilds["LicensePlate"];

        $VIN = $arFilds["VIN"];
        $number_bodywork = $arFilds["number_bodywork"];
        $number_chassis = $arFilds["number_chassis"];

        $WithUseTrailer = 0;
        $YearIssue = $arFilds["YearIssue"];

        $seriaNuberTs = [];
        //$yearsUser = '';
        $multi = 1;
        $driversFio = [];
        $experienceDrive = '';
        $yearsDrivers = '';
        if (!empty($arFilds["Drivers"]["Driver"])) {
            $yearsDrivers = '<param>
                  <brief>Возраст</brief>';
            $experienceDrive = '<param>
                  <brief>Стаж</brief>';
            $yearsUser = '';
            $experience = '';
            foreach ($arFilds["Drivers"]["Driver"] as $drive) {
                $seriaNuberTs[] = $drive["DriverDocument"]["Seria"] . $drive["DriverDocument"]["Number"];
                $yearsUser = getAge(explode('-', $drive["BirthDate"]), explode('.', $arFilds["PolicyBeginDate"])) + 1;
                $yearsDrivers .= '<val>' . $yearsUser . '</val>';
                $experience = getAge(explode('-', $drive["ExperienceDate"]), explode('.', $arFilds["PolicyBeginDate"])) + 1;
                $experienceDrive .= '<val>' . $experience . '</val>';
                $driversFio[] = $drive["LastName"] . ' ' . $drive["FirstName"] . ' ' . $drive["MiddleName"];
            }
            $experienceDrive .= '</param>';
            $yearsDrivers .= '</param>';
            $multi = 0;
        } /*else {
            $yearsUser = getAge(explode('-', $arFilds["Owner_calc"]["BirthDate"]), explode('.', $arFilds["PolicyBeginDate"])) + 1;
            $yearsDrivers .= '<val>' . $yearsUser . '</val>';
        }*/

        $SeriaPass = '';
        if ($arFilds["type_calc"] == 'min') {
            if (!empty($arFilds["Drivers"])) {
                $driverFirst = $arFilds["Drivers"]["Driver"][0];
                $LastName = $driverFirst["LastName"];
                $FirstName = $driverFirst["FirstName"];
                $MiddleName = $driverFirst["MiddleName"];
                $BirthDate = $driverFirst["BirthDate"];
                $SeriaPass = $driverFirst["DriverDocument"]["Seria"];
            } else {
                $LastName = $arFilds["Owner_calc"]["LastName"];
                $FirstName = $arFilds["Owner_calc"]["FirstName"];
                $MiddleName = $arFilds["Owner_calc"]["MiddleName"];
                $BirthDate = $arFilds["Owner_calc"]["BirthDate"];
                $SeriaPass = $arFilds["Owner_calc"]["Seria"];
            }
        } else {
            $LastName = $arFilds["Owner"]["LastName"];
            $FirstName = $arFilds["Owner"]["FirstName"];
            $MiddleName = $arFilds["Owner"]["MiddleName"];
            $BirthDate = $arFilds["Owner"]["BirthDate"];
            $SeriaPass = $arFilds["Owner"]["Seria"];
        }

        $wsdl = "https://b2b.soglasie.ru/CCM/CCMPort.wsdl";
        $login = $this->login;
        $subuser = $this->subuser;
        $password = $this->pass;
        $dateStart = date("Y-m-d\Th:i:s", strtotime($arFilds["PolicyBeginDate"]));
        $years = strtotime($arFilds["PolicyBeginDate"] . ' + 12 month - 1 day');
        $dateEnd = gmdate("Y-m-d\Th:i:s", $years);

        $fioReplaceDriver = '';
        if (!empty($driversFio)) {
            $fioReplaceDriver = implode(';', $driversFio);
        }
        $scoringID = '';
        if (!empty($arFilds["scoringID"])) {
            $scoringID = $arFilds["scoringID"];
        }
        $model = '';
        if ($car["success"]) {
            $model = $car["model"];
        }
        $kbm = '';
        if (!empty($arFilds["kbm_req"])) {
            $kbm = $arFilds["kbm_req"];
        }
        $seriaNuberTsReplace = '';
        if (!empty($seriaNuberTs)) {
            $seriaNuberTsReplace = implode(';', $seriaNuberTs);
        }

        $numberAuto = '';
        if (!empty($VIN)) {
            $numberAuto = '
                <param>
                  <brief>VIN</brief>
                  <val>' . $VIN . '</val>
               </param>';
        } elseif (!empty($number_bodywork)) {
            $numberAuto = '
                <param>
                  <brief>BodyNumber</brief>
                  <val>' . $number_bodywork . '</val>
               </param>';
        } elseif (!empty($number_chassis)) {
            $numberAuto = '
                <param>
                  <brief>ChassisNumber</brief>
                  <val>' . $number_chassis . '</val>
               </param>';
        }

        $contractSoap = new \SoapVar('
        <contract>
               <datecalc>' . date("Y-m-d\Th:i:s") . '</datecalc>
               <datebeg>' . $dateStart . '</datebeg>
               <dateend>' . $dateEnd . '</dateend>
               <param>
                  <brief>ВУ</brief>
                  <val>' . $seriaNuberTsReplace . '</val>
               </param>
               ' . $yearsDrivers . '
               <param>
                  <brief>ГодПостройки</brief>
                  <val>' . $YearIssue . '</val>
               </param>
               <param>
                  <brief>ГосРегТС</brief>
                  <val>' . $LicensePlate . '</val>
               </param>
               <param>
                  <brief>ДопускБезОграничений</brief>
                  <val>' . $multi . '</val>
               </param>
               <param>
                  <brief>ИДЗапросаСкоринг</brief>
                  <val>' . $scoringID . '</val>
               </param>
               ' . $numberAuto . '
               <param>
                  <brief>МодельТС</brief>
                  <val>' . $model . '</val>
               </param>
               <param>
                  <brief>Мощность</brief>
                  <val>' . $Power . '</val>
               </param>
               <param>
                  <brief>НаличиеГрубыхНарушений</brief>
                  <val>0</val>
               </param>
               <param>
                  <brief>НаличиеДС</brief>
                  <val>0</val>
               </param>
               <param>
                  <brief>ПериодИсп</brief>
                  <val>' . $InsuranceMonth . '</val>
               </param>
               <param>
                  <brief>ПризнСтрахПрицеп</brief>
                  <val>' . $WithUseTrailer . '</val>
               </param>
               <param>
                  <brief>Пролонгация</brief>
                  <val>0</val>
               </param>
               <param>
                  <brief>СерияПаспСтрах</brief>
                  <val>' . $SeriaPass . '</val>
               </param>
               <param>
                  <brief>СрокСтрах</brief>
                  <val>' . $InsuranceID . '</val>
               </param>
               ' . $experienceDrive . '
               <param>
                  <brief>СтрахованиеПрицепаОСАГО</brief>
                  <val>0</val>
               </param>
               <param>
                  <brief>СтраховательФЛЮЛ</brief>
                  <val>1001</val>
               </param>
               <param>
                  <brief>СтраховательДатаРождения</brief>
                  <val>' . $BirthDate . '</val>
               </param>
               <param>
                  <brief>СтраховательИмя</brief>
                  <val>' . $FirstName . '</val>
               </param>
               <param>
                  <brief>СтраховательФамилия</brief>
                  <val>' . $LastName . '</val>
               </param>
               <param>
                  <brief>СтраховательОтчество</brief>
                  <val>' . $MiddleName . '</val>
               </param>
               <param>
                  <brief>СобственникИмя</brief>
                  <val>' . $FirstName . '</val>
               </param>
               <param>
                  <brief>СобственникФамилия</brief>
                  <val>' . $LastName . '</val>
               </param>
               <param>
                  <brief>СобственникОтчество</brief>
                  <val>' . $MiddleName . '</val>
               </param>
               <param>
                  <brief>СтрокаФИОВодителей</brief>
                  <val>' . $fioReplaceDriver . '</val>
               </param>
               <param>
                  <brief>ТаксиПрокатТестДрайв</brief>
                  <val>' . $Purpose . '</val>
               </param>
               <param>
                  <brief>ТелМоб</brief>
                  <val>' . $phone . '</val>
               </param>
               <param>
                  <brief>ТерриторияИспользования</brief>
                  <val>' . $kladrCity . '</val>
               </param>
               <param>
                  <brief>ТипСобственникаТС</brief>
                  <val>1001</val>
               </param>
               <param>
                  <brief>ТипТСОСАГО</brief>
                  <val>' . $Type . '</val>
               </param>
               <param>
                  <brief>ТранзитныйНомер</brief>
                  <val>0</val>
               </param>
               <param>
                  <brief>ТСИностранное</brief>
                  <val>0</val>
               </param>
               <param>
                  <brief>ПолСобственника</brief>
                  <val>' . $this->gender($FirstName, true) . '</val>
               </param>
               <param>
                  <brief>ПолСтрахователя</brief>
                  <val>' . $this->gender($FirstName, true) . '</val>
               </param>
               <coeff>
                  <brief>Кбм</brief>
                  <val>' . $kbm . '</val>
               </coeff>
            </contract>', XSD_ANYXML);

        $responseArray = [];
        if (!empty($contractSoap)) {
            $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0, "login" => $login, "password" => $password));
            $client->CalcProduct(array('data' => array(
                    'subuser' => $subuser,
                    'debug' => false,
                    'product' => [
                        'brief' => 'ОСАГО'
                    ],
                    'contract' => $contractSoap,
                ))
            );
            $resp = $client->__last_response;
            $req = $client->__last_request;
            $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $resp);
            $xml = simplexml_load_string($xml);
            $json = json_encode($xml);
            $responseArray = json_decode($json, true);
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logCalcProduct_response.xml', print_r($resp, true));
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logCalcProduct_request.xml', print_r($req, true));
        }
        if (!empty($responseArray["SBody"]["ns2calcProductResponse"]["data"]["contract"]["coeff"][0]["result"])) {
            $coeff = $responseArray["SBody"]["ns2calcProductResponse"]["data"]["contract"]["coeff"];
            $coefficient = [
                "Tb" => $coeff[0]["result"],
                "Kt" => $coeff[9]["result"],
                "Kbm" => $coeff[1]["result"],
                "Ko" => $coeff[5]["result"],
                "Kn" => $coeff[4]["result"],
                "Kp" => $coeff[8]["result"],
                "Ks" => $coeff[6]["result"],
                "Km" => $coeff[3]["result"],
                "Kvs" => $coeff[2]["result"],
                "Kpr" => $coeff[7]["result"],
            ];
            if (!empty($responseArray["SBody"]["ns2calcProductResponse"]["data"]["contract"]["result"])) {
                return [
                    'success' => true,
                    'info' => [
                        "sum" => $responseArray["SBody"]["ns2calcProductResponse"]["data"]["contract"]["result"],
                        "Coefficients" => $coefficient
                    ]
                ];
            } else {
                return ["success" => false, 'error' => 'Сервис временно не доступен', 'method' => 'CalcProduct'];
            }
        } else {
            return ["success" => false, 'error' => 'Сервис временно не доступен', 'method' => 'CalcProduct'];
        }
    }

    public function Policy($arFilds)
    {
        if($this->env_type == 'local'){
            $url = "https://b2b.soglasie.ru/daily/online/api/eosago"; // тестовый
        }else{
            $url = "https://b2b.soglasie.ru/online/api/eosago";
        }
        $login = $this->login;
        $subuser = $this->subuser;
        $password = $this->subuser_pass;
        $Mark = $arFilds["Mark"];
        $Model = $arFilds["Model"];
        $car = $this->getCars(["model" => $Model, "mark" => $Mark]);
        $RegistrationRussia = true;
        $Power = $arFilds["Power"];
        $Purpose = 'Personal';
        $LicensePlate = $arFilds["LicensePlate"];

        $VIN = $arFilds["VIN"];
        $number_bodywork = $arFilds["number_bodywork"];
        $number_chassis = $arFilds["number_chassis"];
        $carSerial = $arFilds["DocCarSerial"];
        $carNumber = $arFilds["DocCarNumber"];
        $carDocDate = date('Y-m-d', strtotime($arFilds["DocumentCarDate"]));
        $YearIssue = $arFilds["YearIssue"];

        $LastName = $arFilds["Owner"]["LastName"];
        $FirstName = $arFilds["Owner"]["FirstName"];
        $MiddleName = $arFilds["Owner"]["MiddleName"];
        $BirthDate = $arFilds["Owner"]["BirthDate"];
        $NumberPass = $arFilds["Owner"]["Number"];
        $SeriaPass = $arFilds["Owner"]["Seria"];
        $street = $arFilds["Owner"]["address"]["street"];
        $home = $arFilds["Owner"]["address"]["home"];
        $apartament = '';
        if (!empty($arFilds["Owner"]["address"]["apartament"]))
            $apartament = $arFilds["Owner"]["address"]["apartament"];
        $mail = $arFilds["email"];
        $phone = $arFilds["phone"];
        $kladrAddress = $arFilds["kladr_address"];

        $dateStart = date("Y-m-d\TH:i:s", strtotime($arFilds["PolicyBeginDate"]));
        $dateEnd = date("Y-m-d\TH:i:s", strtotime($arFilds["PolicyBeginDate"] . ' + 1 year - 1 day'));
        $dateStart2 = date("Y-m-d", strtotime($arFilds["PolicyBeginDate"]));
        $dateEnd2 = date("Y-m-d", strtotime($arFilds["PolicyBeginDate"] . ' + 1 year - 1 day'));
        $docTs = '';
        switch ($arFilds["type_doc_ts"]) {
            case 'sts':
                $docTs = 31;
                break;
            case 'pts':
                $docTs = 30;
                break;
        }
        $model = '';
        if ($car["success"]) {
            $model = $car["model"];
        }
        $dataArray = [
            "CodeInsurant" => 000,
            "BeginDate" => $dateStart,
            "EndDate" => $dateEnd,
            "Period1Begin" => $dateStart2,
            "Period1End" => $dateEnd2,
            "IsTransCar" => false,
            "CarInfo" => [
                "VIN" => $VIN,
                "BodyNumber" => $number_bodywork,
                "ChassisNumber" => $number_chassis,
                "LicensePlate" => $LicensePlate,
                "MarkModelCarCode" => $model,
                "MarkPTS" => $Mark,
                "ModelPTS" => $Model,
                "YearIssue" => $YearIssue,
                "DocumentCar" => [
                    "TypeRSA" => $docTs,
                    "Serial" => $carSerial,
                    "Number" => $carNumber,
                    "Date" => $carDocDate,
                    "IsPrimary" => true
                ],
                "EngCap" => $Power,
                "GoalUse" => $Purpose,
                "Rented" => false
            ],
            "Insurer" => [
                "Phisical" => [
                    "Resident" => $RegistrationRussia,
                    "Surname" => $LastName,
                    "Name" => $FirstName,
                    "Patronymic" => $MiddleName,
                    "BirthDate" => $BirthDate,
                    "Sex" => $this->gender($FirstName),
                    "Documents" => [
                        "Document" => [
                            [
                                "TypeRSA" => 12,
                                "Serial" => $SeriaPass,
                                "Number" => $NumberPass,
                                "IsPrimary" => true
                            ]]
                    ],
                    "Addresses" => [
                        "Address" => [[
                            "Type" => "Registered",
                            "Country" => 643,
                            "AddressCode" => $kladrAddress,
                            "Street" => $street,
                            "Hous" => $home,
                            "Flat" => $apartament
                        ]]
                    ],
                    "Email" => $mail,
                    "PhoneMobile" => $phone
                ]
            ],
            "CarOwner" => [
                "Phisical" => [
                    "Resident" => $RegistrationRussia,
                    "Surname" => $LastName,
                    "Name" => $FirstName,
                    "Patronymic" => $MiddleName,
                    "BirthDate" => $BirthDate,
                    "Sex" => $this->gender($FirstName),
                    "Documents" => [
                        "Document" => [[
                            "TypeRSA" => 12,
                            "Serial" => $SeriaPass,
                            "Number" => $NumberPass,
                            "IsPrimary" => true
                        ]]
                    ],
                    "Addresses" => [
                        "Address" => [[
                            "Type" => "Registered",
                            "Country" => 643,
                            "AddressCode" => $kladrAddress,
                            "Street" => $street,
                            "Hous" => $home,
                            "Flat" => $apartament
                        ]]
                    ],
                ]
            ],
            "CashPaymentOption" => false,
            "IKP1" => " "//243395,
        ];
        if (!empty($arFilds["Drivers"])) {
            foreach ($arFilds["Drivers"]["Driver"] as $driver) {
                $dataArray["Drivers"]["Driver"][] = [
                    "Face" => [
                        "Resident" => $RegistrationRussia,
                        "Surname" => $driver["LastName"],
                        "Name" => $driver["FirstName"],
                        "Patronymic" => $driver["MiddleName"],
                        "BirthDate" => $driver["BirthDate"],
                        "Sex" => $this->gender($driver["FirstName"]),
                        "Documents" => [
                            "Document" => [
                                [
                                    "TypeRSA" => 20,
                                    "Serial" => $driver["DriverDocument"]["Seria"],
                                    "Number" => $driver["DriverDocument"]["Number"],
                                ]
                            ],
                        ],
                    ],
                    "DrivingExpDate" => $driver["ExperienceDate"]
                ];
            }
        }
        if (!empty($arFilds["cart_diag"])) {
            $yearMinus = '';
            if ((date('Y') - (int)$YearIssue) >= 7) {
                $yearMinus = 1;
            } else if ((date('Y') - (int)$YearIssue) < 7) {
                $yearMinus = 2;
            }
            if ($yearMinus) {
                $dataArray["CarInfo"]["TicketCar"] = [
                    "Type" => 14,
                    "Number" => $arFilds["cart_diag"],
                    "Date" => date("Y-m-d", strtotime($arFilds["date_cart_diag"] . ' - ' . $yearMinus . ' year')),
                ];
                $dataArray["CarInfo"]["TicketCarYear"] = explode('-', $arFilds["date_cart_diag"])[0];
                $dataArray["CarInfo"]["TicketCarMonth"] = explode('-', $arFilds["date_cart_diag"])[1];
                $dataArray["CarInfo"]["TicketDiagnosticDate"] = date("Y-m-d", strtotime($arFilds["date_cart_diag"] . ' - ' . $yearMinus . ' year'));
            }
        }
        $data_string = json_encode($dataArray);
        $header = array(
            'Accept-Encoding: gzip,deflate',
            'Content-Length: ' . strlen($data_string),
            'Content-Type: application/json',
            'Accept: application/json',
            'Host: b2b.soglasie.ru',
            'Connection: Keep-Alive',
            'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)',
            'Authorization: Basic ' . base64_encode($login . ':' . $subuser . ':' . $password)
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $resp = json_decode($response, true);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logHeaderPolicy.xml', print_r($header, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logPolicy.json', $data_string);

        if (!empty($resp["policyId"])) {
            return ["success" => true, "policyId" => $resp["policyId"]];
        } else {
            return ["success" => false, 'error' => 'Сервис временно не доступен', 'method' => 'Policy', 'dop' => !empty($resp["error"])?$resp["error"]:$err, "stack"=>!empty($resp["stack"])?$resp["stack"]:''];
        }
    }

    public function PolicyStatus($policyId)
    {
        if($this->env_type == 'local'){
            $url = 'https://b2b.soglasie.ru/daily/online/api/eosago/' . $policyId . '/status'; // тестовый
        }else{
            $url = 'https://b2b.soglasie.ru/online/api/eosago/' . $policyId . '/status';
        }
        $login = $this->login;
        $subuser = $this->subuser;
        $password = $this->pass;
        $header = array(
            'Authorization: Basic ' . base64_encode($login . ':' . $password)
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $resp = json_decode($response, true);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logPolicyStatus_response.xml', print_r($resp, true), FILE_APPEND);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logPolicyStatus_header.xml', $url . ':' . print_r($header, true), FILE_APPEND);

        if (!empty($resp["status"])) {
            switch ($resp["status"]) {
                case 'WORKING':
                    sleep(15);
                    $this->PolicyStatus($policyId);
                    break;
                case 'COMPLETE':
                    switch ($resp["policy"]["status"]) {
                        case 'RSA_CHECK_OK':
                            $this->pay = $this->PayUrl($resp["policyId"]);
                            return true;
                            break;
                        case 'RSA_CHECK_FAIL':
                        case 'RSA_SIGN_FAIL':
                        case 'SK_CHECK_FAIL':
                            $this->UnsetPolicy($policyId);
                            return ["success" => false, "policyId" => $policyId, 'error' => 'Сервис временно не доступен', 'method' => 'Status'];
                            break;
                        default:
                            sleep(15);
                            $this->PolicyStatus($policyId);
                            break;
                    }
                    break;
                case 'ERROR':
                    $this->UnsetPolicy($policyId);
                    return ["success" => false, "policyId" => $policyId, 'error' => 'Сервис временно не доступен', 'method' => 'Status'];
                    break;
            }
        } else {
            return ["success" => false, "policyId" => $policyId, 'error' => 'Сервис временно не доступен', 'method' => 'Status'];
        }
    }

    public function UnsetPolicy($policyId)
    {
        if($this->env_type == 'local'){
            $url = 'https://b2b.soglasie.ru/daily/online/api/eosago/' . $policyId . '/suspend';
        }else{
            $url = 'https://b2b.soglasie.ru/online/api/eosago/' . $policyId . '/suspend';
        }
        $login = $this->login;
        $password = $this->subuser_pass;
        $header = array(
            'Authorization: Basic ' . base64_encode($login . ':' . $password)
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return ["success" => false, 'error' => 'Сервис временно не доступен'];
    }

    public function PayUrl($policyId)
    {
        if($this->env_type == 'local'){
            $url = 'https://b2b.soglasie.ru/daily/online/api/eosago/' . $policyId . '/paylink';
        }else{
            $url = 'https://b2b.soglasie.ru/online/api/eosago/' . $policyId . '/paylink';
        }
        $login = $this->login;
        $password = $this->pass;
        $header = array(
            'Authorization: Basic ' . base64_encode($login . ':' . $password),
            'Content-Type: application/json',
            'Accept: application/json'
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $resp = json_decode($response, true);

        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logPayUrl_response.xml', print_r($resp, true));
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/logPayUrl_header.xml', $url . ':' . print_r($header, true));

        if (!empty($resp["PayLink"])) {
            return ["success" => true, "url" => $resp["PayLink"], "policyId" => $policyId];
        } else {
            return ["success" => false, 'error' => 'Сервис временно не доступен', 'method' => 'PayUrl', 'dop' => $err];
        }
    }

    public function PdfPolicyDownload($policyId)
    {
        if($this->env_type == 'local'){
            $url = 'https://b2b.soglasie.ru/daily/online/api/eosago/' . $policyId . '/policy';
        }else{
            $url = 'https://b2b.soglasie.ru/online/api/eosago/' . $policyId . '/policy';
        }

        $login = $this->login;
        $password = $this->pass;
        $header = array(
            'Authorization: Basic ' . base64_encode($login . ':' . $password)
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $resp = json_decode($response, true);

        //file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/PdfPolicyDownload_response.xml', print_r($resp, true));
        //file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/logs/soglasie/PdfPolicyDownload_header.xml', $url . ':' . print_r($header, true));

        if(empty($resp["error"])){
            return ["success"=>true, "doc"=>$response];
        }else{
            return ["success"=>false];
        }
    }

    public function PdfNoticeDownload($policyId)
    {
        if($this->env_type == 'local'){
            $url = 'https://b2b.soglasie.ru/daily/online/api/eosago/' . $policyId . '/notice';
        }else{
            $url = 'https://b2b.soglasie.ru/online/api/eosago/' . $policyId . '/notice';
        }

        $login = $this->login;
        $password = $this->pass;
        $header = array(
            'Authorization: Basic ' . base64_encode($login . ':' . $password)
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $resp = json_decode($response, true);
        echo '<pre>';
        print_r($resp);
        echo '</pre>';
    }
}
