<?php

namespace App\Http\Controllers;

use App\AutoUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AutoUserController extends Controller
{
    public function addCars(Request $request)
    {
        $user_id = Auth::User()->id;
        $user = User::find($user_id);
        $user->AutoUsers()->create([
            'vin' => '3000000000'
        ]);
    }
    public function updateCar(Request $request)
    {
        $id_auto = $request->input('id_auto');
        $user_id = Auth::User()->id;
        $user = User::find($user_id);
        $user->AutoUsers()->where('id',$id_auto)->update(['name'=>'value']);
    }
    public function getListCarUser()
    {
        $user = User::find(Auth::User()->id);
        $cars = $user->AutoUsers();
        $arrCarsParse = array();
        if($cars){
            foreach($cars as $item){
                $auto = AutoUser::find($item->id);
                if(!empty($auto->Marks))
                $mark = $auto->Marks[0];
                if(!empty($auto->Models))
                $model = $auto->Models[0];
                if(!empty($auto->TypeCar))
                $type_car = $auto->TypeCar[0];
                if(!empty($auto->CategoryCar))
                $type_car = $auto->CategoryCar[0];

                $arrCarsParse[]=array("mark"=>$mark, "model"=>$model, "type"=>$type_car, "item"=>$item);
            }
        }
        return $arrCarsParse;
    }
}
