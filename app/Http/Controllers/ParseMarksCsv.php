<?php


namespace App\Http\Controllers;

use App\BasebuyCarGeneration;
use App\BasebuyCarMark;
use App\BasebuyCarModel;
use App\BasebuyCarModification;
use App\BasebuyCarSeries;
use ParseCsv;

class ParseMarksCsv extends Controller
{
    protected $root;

    public function __construct()
    {
        if(config('app.env') == 'local'){
            $this->root = '/var/www/laravel/data/www/pitstop.dev.letsrock.pro/public';
        }else{
            $this->root = '/var/www/u0821920/data/www/pitstop-lar/public';
        }
    }
    public function pr($arr){
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }
    public function markParse()
    {
        $originalFile = $this->root . '/storage/csv-auto/car_mark.csv';
        $csv = new ParseCsv\Csv($originalFile);
        $headers = $csv->titles;
        /*
        [0] => 'id_car_mark'
        [1] => 'name'
        [2] => 'name_rus'
        [3] => 'date_create'
        [4] => 'date_update'
        [5] => 'id_car_type'
         */
        $marks = [];
        foreach ($csv->data as $datum) {
            $id_car_mark = (int)str_replace("'", "", $datum[$headers[0]]);
            $name = str_replace("'", "", $datum[$headers[1]]);
            $name_rus = str_replace("'", "", $datum[$headers[2]]);
            $id_car_type = (int)str_replace("'", "", $datum[$headers[5]]);
            $mark_on = BasebuyCarMark::where('id_car_mark', $id_car_mark)->where('name', $name)->first();
            if(empty($mark_on->id_car_mark)){
                $marks[$id_car_mark] = BasebuyCarMark::create([
                    'id_car_mark'=>$id_car_mark,
                    'name'=>$name,
                    'name_rus'=>$name_rus,
                    'id_car_type'=>$id_car_type
                ]);
            }else{
                $marks[$id_car_mark] = $mark_on->update([
                    'id_car_mark'=>$id_car_mark,
                    'name'=>$name,
                    'name_rus'=>$name_rus,
                    'id_car_type'=>$id_car_type
                ]);
            }
        }
        if(!empty($marks))
        $this->modelParse($marks);
    }
    public function modelParse($marks){
        $originalFile = $this->root . '/storage/csv-auto/car_model.csv';
        $csv = new ParseCsv\Csv($originalFile);
        $headers = $csv->titles;
        /*
        [0] => 'id_car_model'
        [1] => 'id_car_mark'
        [2] => 'name'
        [3] => 'name_rus'
        [4] => 'date_create'
        [5] => 'date_update'
        [6] => 'id_car_type'
         */
        $models = [];
        foreach ($csv->data as $datum) {
            $id_car_model = (int)str_replace("'", "", $datum[$headers[0]]);
            $id_car_mark = (int)str_replace("'", "", $datum[$headers[1]]);
            $name = str_replace("'", "", $datum[$headers[2]]);
            $name_rus = str_replace("'", "", $datum[$headers[3]]);
            $id_car_type = (int)str_replace("'", "", $datum[$headers[6]]);
            if(!empty($marks[$id_car_mark])){
                $model_on = BasebuyCarModel::where('id_car_model', $id_car_model)->where('name', $name)->where('id_car_mark', $id_car_mark)->first();
                if(empty($model_on->id_car_model)){
                    $models[$id_car_model] = BasebuyCarModel::create([
                        'id_car_model'=>$id_car_model,
                        'id_car_mark'=>$id_car_mark,
                        'name'=>$name,
                        'name_rus'=>$name_rus,
                        'id_car_type'=>$id_car_type
                    ]);
                }else{
                    $models[$id_car_model] = $model_on->update([
                        'id_car_model'=>$id_car_model,
                        'id_car_mark'=>$id_car_mark,
                        'name'=>$name,
                        'name_rus'=>$name_rus,
                        'id_car_type'=>$id_car_type
                    ]);
                }
            }
        }
        if(!empty($marks))
        $this->generationParse($models);
    }
    public function generationParse($models){
        $originalFile = $this->root . '/storage/csv-auto/car_generation.csv';
        $csv = new ParseCsv\Csv($originalFile);
        $headers = $csv->titles;
        /*
        [0] => 'id_car_generation'
        [1] => 'id_car_model'
        [2] => 'name'
        [3] => 'year_begin'
        [4] => 'year_end'
        [5] => 'date_create'
        [6] => 'date_update'
        [7] => 'id_car_type'
         */
        $generations = [];
        foreach ($csv->data as $datum) {
            $id_car_generation = (int)str_replace("'", "", $datum[$headers[0]]);
            $id_car_model = (int)str_replace("'", "", $datum[$headers[1]]);
            $name = str_replace("'", "", $datum[$headers[2]]);
            $year_begin = (int)str_replace("'", "", $datum[$headers[3]]);
            $year_end = (int)str_replace("'", "", $datum[$headers[4]]);
            $id_car_type = (int)str_replace("'", "", $datum[$headers[7]]);
            if(!empty($models[$id_car_model])){
                $generation_on = BasebuyCarGeneration::where('id_car_generation', $id_car_generation)->where('id_car_model', $id_car_model)->where('name', $name)->first();
                if(empty($generation_on->id_car_generation)){
                    $generations[$id_car_generation] = BasebuyCarGeneration::create([
                        'id_car_generation'=>$id_car_generation,
                        'id_car_model'=>$id_car_model,
                        'name'=>$name,
                        'year_begin'=>$year_begin,
                        'year_end'=>$year_end,
                        'id_car_type'=>$id_car_type
                    ]);
                }else{
                    $generations[$id_car_generation] = $generation_on->update([
                        'id_car_generation'=>$id_car_generation,
                        'id_car_model'=>$id_car_model,
                        'name'=>$name,
                        'year_begin'=>$year_begin,
                        'year_end'=>$year_end,
                        'id_car_type'=>$id_car_type
                    ]);
                }
            }
        }
        if(!empty($generations))
        $this->serieParse($generations, $models);
    }
    public function serieParse($generations, $models){
        $originalFile = $this->root . '/storage/csv-auto/car_serie.csv';
        $csv = new ParseCsv\Csv($originalFile);
        $headers = $csv->titles;
        /*
        [0] => 'id_car_serie'
        [1] => 'id_car_model'
        [2] => 'id_car_generation'
        [3] => 'name'
        [4] => 'date_create'
        [5] => 'date_update'
        [6] => 'id_car_type'
         */
        $serises = [];
        foreach ($csv->data as $datum) {
            $id_car_serie = (int)str_replace("'", "", $datum[$headers[0]]);
            $id_car_model = (int)str_replace("'", "", $datum[$headers[1]]);
            $id_car_generation = (int)str_replace("'", "", $datum[$headers[2]]);
            $name = str_replace("'", "", $datum[$headers[3]]);
            $id_car_type = (int)str_replace("'", "", $datum[$headers[6]]);
            if(!empty($models[$id_car_model]) && !empty($generations[$id_car_generation])){
                $series_on = BasebuyCarSeries::where('id_car_serie', $id_car_serie)->where('id_car_model', $id_car_model)->where('id_car_generation', $id_car_generation)->where('name', $name)->first();
                if(empty($series_on->id_car_serie)){
                    $serises[$id_car_serie] = BasebuyCarSeries::create([
                        'id_car_serie'=>$id_car_serie,
                        'id_car_model'=>$id_car_model,
                        'id_car_generation'=>$id_car_generation,
                        'name'=>$name,
                        'id_car_type'=>$id_car_type
                    ]);
                }else{
                    $serises[$id_car_serie] = $series_on->update([
                        'id_car_serie'=>$id_car_serie,
                        'id_car_model'=>$id_car_model,
                        'id_car_generation'=>$id_car_generation,
                        'name'=>$name,
                        'id_car_type'=>$id_car_type
                    ]);
                }
            }
        }
        if(!empty($serises))
            $this->modificationParse($serises, $models);
    }
    public function modificationParse($serises, $models){
        $originalFile = $this->root . '/storage/csv-auto/car_modification.csv';
        $csv = new ParseCsv\Csv($originalFile);
        $headers = $csv->titles;
        /*
        [0] => 'id_car_modification'
        [1] => 'id_car_serie'
        [2] => 'id_car_model'
        [3] => 'name'
        [4] => 'start_production_year'
        [5] => 'end_production_year'
        [6] => 'date_create'
        [7] => 'date_update'
        [8] => 'id_car_type'
         */
        $modifications = [];
        foreach ($csv->data as $datum) {
            $id_car_modification = (int)str_replace("'", "", $datum[$headers[0]]);
            $id_car_serie = (int)str_replace("'", "", $datum[$headers[1]]);
            $id_car_model = (int)str_replace("'", "", $datum[$headers[2]]);
            $name = str_replace("'", "", $datum[$headers[3]]);
            $start_production_year = (int)str_replace("'", "", $datum[$headers[4]]);
            $end_production_year = (int)str_replace("'", "", $datum[$headers[5]]);
            $id_car_type = (int)str_replace("'", "", $datum[$headers[8]]);
            if(!empty($models[$id_car_model]) && !empty($serises[$id_car_serie])){
                $modification_on = BasebuyCarModification::where('id_car_modification', $id_car_modification)->where('id_car_serie', $id_car_serie)->where('id_car_model', $id_car_model)->where('name', $name)->first();
                if(empty($modification_on->id_car_modification)){
                    $modifications[$id_car_modification] = BasebuyCarModification::create([
                        'id_car_modification'=>$id_car_modification,
                        'id_car_serie'=>$id_car_serie,
                        'id_car_model'=>$id_car_model,
                        'name'=>$name,
                        'start_production_year'=>$start_production_year,
                        'end_production_year'=>$end_production_year,
                        'id_car_type'=>$id_car_type
                    ]);
                }else{
                    $modifications[$id_car_modification] = $modification_on->update([
                        'id_car_modification'=>$id_car_modification,
                        'id_car_serie'=>$id_car_serie,
                        'id_car_model'=>$id_car_model,
                        'name'=>$name,
                        'start_production_year'=>$start_production_year,
                        'end_production_year'=>$end_production_year,
                        'id_car_type'=>$id_car_type
                    ]);
                }
            }
        }
        if(!empty($modifications)){
            $this->pr($modifications);
        }
    }
}
