<?php

namespace App\Widgets;

use App\City;
use Arrilot\Widgets\AbstractWidget;

class CitiesList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $list = City::getList(false, true);

        return view('widgets.cities_list', [
            'config' => $this->config,
            'list' => $list
        ]);
    }
}
