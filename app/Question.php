<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Question extends Model
{
    protected $fillable = ['id_category', 'phone', 'email', 'question'];
    public function category()
    {
        return $this->belongsTo('App\Category', 'id_category', 'id');
    }
    public static function getListQuestion($category_slug = false)
    {
        global $category;
        $category = $category_slug;
        if(!empty($category)){
            return static::where('active', 1)->orderBy('created_at', 'desc')->whereHas('category', function ($query) {
               global $category;
                    $query->where('slug', $category);
            })->get();
        }else{
            return static::where('active', 1)->orderBy('created_at', 'desc')->get();
        }
    }
}
