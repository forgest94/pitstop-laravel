<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Buyer extends Model
{
    protected $fillable = [
        "yur_fiz",
        "PASSPORT_PATH_ADDRESS",
        "ADDITIONAL_PASSPORT_PATH_ADDRESS",
        "LAST_NAME",
        "FIRST_NAME",
        "MIDDLE_NAME",
        "DATE_BIRTH",
        "PLACE_BIRTH",
        "PASSPORT_SERIAL",
        "PASSPORT_NUMBER",
        "PASSPORT_DATE",
        "PASSPORT_WHERE",
        "PASSPORT_PATH_CITY",
        "PASSPORT_PATH_STREET",
        "PASSPORT_PATH_NUMBER",
        "NATIONALITY",
        "SEX",
        "INN",
        "LEGAL_NAME",
        "LEGAL_DATE",
        "LEGAL_INN",
        "LEGAL_ADDRESS",
        "LEGAL_TEL",
        "ADDITIONAL_LAST_NAME",
        "ADDITIONAL_FIRST_NAME", 
        "ADDITIONAL_MIDDLE_NAME", 
        "ADDITIONAL_DATE_BIRTH", 
        "ADDITIONAL_PLACE_BIRTH", 
        "ADDITIONAL_PASSPORT_SERIAL", 
        "ADDITIONAL_PASSPORT_NUMBER", 
        "ADDITIONAL_PASSPORT_DATE", 
        "ADDITIONAL_PASSPORT_WHERE", 
        "ADDITIONAL_PASSPORT_PATH_CITY", 
        "ADDITIONAL_PASSPORT_PATH_STREET", 
        "ADDITIONAL_PASSPORT_PATH_NUMBER", 
        "ADDITIONAL_NATIONALITY", 
        "ADDITIONAL_SEX",
        "ADDITIONAL_INN"
    ];
}
