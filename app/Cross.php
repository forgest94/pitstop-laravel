<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cross extends Model
{
    protected $fillable = [
        'name',
        'dop_name',
        'id_allinsurances',
        'number_cross',
        'price'
    ];

    public function StrahCross()
    {
        return $this->hasMany('App\Allinsurance', 'id', 'id_allinsurances');
    }
}
