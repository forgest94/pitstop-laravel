<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AutoOrder extends Model
{
    protected $fillable = [
        'mark', 
        'model', 
        'type_car', 
        'category', 
        'gos_reg', 
        'vin', 
        'number_bodywork', 
        'year', 
        'number_chassis',
        'power',
        'seria_pts', 
        'number_pts', 
        'date_pts', 
        'seria_sts', 
        'number_sts', 
        'date_sts'
    ];

    public static function isVin($vin){
        return static::where('vin', $vin)->orderBy('id', 'desc')->get();
    }
}
