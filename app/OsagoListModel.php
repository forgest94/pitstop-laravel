<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsagoListModel extends Model
{
    protected $fillable = ['id_mark', 'id_model', 'value', 'category', 'type'];
    public static function getListMoldel($mark)
    {
        return static::where('id_mark', $mark)->orderBy('value', 'asc')->get();
    }
    public static function getByID($id)
    {
        return static::where('id_model', $id)->first();
    }
}
