<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{

    public static function getList($count=false, $favorite=false){
        if($favorite){
            if ($count) {
                return static::where('check_favorite', $favorite)->orderBy('id', 'desc')->paginate($count);
            } else {
                return static::where('check_favorite', $favorite)->orderBy('id', 'desc')->get();
            }
        }else{
            if ($count) {
                return static::orderBy('id', 'desc')->paginate($count);
            } else {
                return static::orderBy('id', 'desc')->get();
            }
        }
    }

    public static function isCity($slug){
        return static::where('slug', $slug)->orderBy('id', 'desc')->first();
    }

    public function pages()
    {
        return $this->belongsToMany('App\Page', 'cities_pages');
    }
}
