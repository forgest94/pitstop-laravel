<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OsagoListTransportPurpose extends Model
{
    protected $fillable = ['id_purpose', 'name_purpose'];
    public static function getListTP($purposeId = false)
    {
        if($purposeId){
            return static::where('id_purpose', $purposeId)->orderBy('id_purpose', 'asc')->get();
        }else{
            return static::orderBy('id_purpose', 'asc')->get();
        }
    }
}
