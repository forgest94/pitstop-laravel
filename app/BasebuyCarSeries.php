<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BasebuyCarSeries extends Model
{
    protected $fillable =
        [
            'id_car_serie',
            'id_car_model',
            'id_car_generation',
            'name',
            'id_car_type'
        ];
    public static function getListSeries($model, $generation)
    {
        return static::where('id_car_model', $model)->where('id_car_generation', $generation)->orderBy('name', 'asc')->get();
    }
    public static function isGeneration($model, $generation){
        return static::where('id_car_model', $model)->where('id_car_generation', $generation)->first();
    }
    public static function getByID($id)
    {
        return static::where('id_car_serie', $id)->first();
    }
}
