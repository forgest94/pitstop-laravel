<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PeronsOrder extends Model
{
    protected $fillable = [
        'LastName', 'FirstName', 'MiddleName', 'BirthDate', 'ExperienceDate', 'docser_numb'
    ];
}
