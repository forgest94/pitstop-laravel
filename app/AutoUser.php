<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AutoUser extends Model
{
    protected $fillable = [
    'mark', 
    'model', 
    'type_car', 
    'category', 
    'gos_reg', 
    'vin', 
    'number_bodywork', 
    'year', 
    'number_chassis',
    'power',
    'seria_pts', 
    'number_pts', 
    'date_pts', 
    'seria_sts', 
    'number_sts', 
    'date_sts'
    ];

    public static function isAuto($fildsAuto){
        return static::where('vin', $fildsAuto["vin"])
                ->orWhere('number_bodywork', $fildsAuto["number_bodywork"])
            ->orWhere('number_chassis', $fildsAuto["number_chassis"])->first();
    }
    /*public function Marks()
    {
        return $this->hasMany('App\OsagoListMark', 'id_mark', 'mark_id');
    }
    public function Models()
    {
        return $this->hasMany('App\OsagoListModel', 'id_model', 'model_id');
    }
    public function TypeCar()
    {
        return $this->hasMany('App\OsagoListTransportType', 'id_type', 'type_car_id');
    }
    public function CategoryCar()
    {
        return $this->hasMany('App\OsagoListTransportCategorye', 'id_category', 'category_id');
    }*/
}
