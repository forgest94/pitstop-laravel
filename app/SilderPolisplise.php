<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;

class SilderPolisplise extends Model
{
    use Resizable;

    public static function getListSlider()
    {
        return static::where('active', 1)->where('image', '<>', '')->orderBy('sort','asc')->get();
    }
}
