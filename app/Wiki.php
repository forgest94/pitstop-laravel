<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Wiki extends Model
{
    public static function getAllWiki()
    {
        return static::where('active', 1)->orderBy('sort', 'asc')->get();
    }
}
