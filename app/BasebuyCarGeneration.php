<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BasebuyCarGeneration extends Model
{
    protected $fillable =
        [
            'id_car_generation',
            'id_car_model',
            'name',
            'year_begin',
            'year_end',
            'id_car_type'
        ];
    public static function getListGeneration($model)
    {
        return static::where('id_car_model', $model)->orderBy('year_begin', 'desc')->get();
    }
    public static function isModel($model){
        return static::where('id_car_model', $model)->first();
    }
    public static function getByID($id)
    {
        return static::where('id_car_generation', $id)->first();
    }
}
