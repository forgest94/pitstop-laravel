<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Payout extends Model
{
    protected $fillable = [
        "date_start",
        "user_id",
        "status",
        "date_end"
    ];

    public static function getList($count = false, $where = false)
    {
        if (!empty($where["date"])) {
            if ($count) {
                if(!empty($where["user_id"])){
                    return static::where('user_id', $where["user_id"])
                        ->whereIn('date_start', [$where["date"]["start"], $where["date"]["end"]])
                        ->orWhereIn('date_end', [$where["date"]["start"], $where["date"]["end"]])
                        ->orderBy('id', 'desc')
                        ->paginate($count);
                }else{
                    return static::whereIn('date_start', [$where["date"]["start"], $where["date"]["end"]])
                        ->orWhereIn('date_end', [$where["date"]["start"], $where["date"]["end"]])
                        ->orderBy('id', 'desc')
                        ->paginate($count);
                }
            } else {
                if(!empty($where["user_id"])){
                    return static::where('user_id', $where["user_id"])
                        ->whereIn('date_start', [$where["date"]["start"], $where["date"]["end"]])
                        ->orWhereIn('date_end', [$where["date"]["start"], $where["date"]["end"]])
                        ->orderBy('id', 'desc')
                        ->get();
                }else{
                    return static::whereIn('date_start', [$where["date"]["start"], $where["date"]["end"]])
                        ->orWhereIn('date_end', [$where["date"]["start"], $where["date"]["end"]])
                        ->orderBy('id', 'desc')
                        ->get();
                }
            }
        } else{
            if ($count) {
                return static::orderBy('id', 'desc')->paginate($count);
            } else {
                return static::orderBy('id', 'desc')->get();
            }
        }
    }

    public function OrdersPayouts()
    {
        return $this->belongsToMany('App\Order', 'orders_payouts');
    }

    public function AgentPayout()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}
