<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BasebuyCarModel extends Model
{
    protected $fillable =
        [
            'id_car_model',
            'id_car_mark',
            'name',
            'name_rus',
            'id_car_type'
        ];
    public static function getListModel($mark)
    {
        return static::where('id_car_mark', $mark)->orderBy('name', 'asc')->get();
    }
    public static function getByID($id)
    {
        return static::where('id_car_model', $id)->first();
    }
}
