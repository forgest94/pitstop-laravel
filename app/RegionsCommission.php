<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RegionsCommission extends Model
{
    protected $fillable = [
        "region_id",
        "commission_region",
        "type_commission_region",
        "commission_id"
    ];


    public static function isAttach($region, $commission)
    {
        return static::where('region_id', $region)->where('commission_id', $commission)->orderBy('id', 'desc')->first();
    }

}
