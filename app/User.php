<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'city',
        'number_card',
        'role_id',
        'bank',
        'name_recipient',
        'name_curator',
        'phone_curator',
        'mail_curator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getListUsersRole($id, $count = false, $where = false)
    {
        if (!empty($where["filter"])) {
            if ($count) {
                return static::where('email', 'like', '%'.$where["filter"].'%')
                    ->orWhere('name', 'like', '%'.$where["filter"].'%')
                    ->orWhere('phone', 'like', '%'.$where["filter"].'%')
                    ->where('role_id', $id)
                    ->orderBy('id', 'desc')
                    ->paginate($count);
            } else {
                return static::where('email', 'like', '%'.$where["filter"].'%')
                    ->orWhere('name', 'like', '%'.$where["filter"].'%')
                    ->orWhere('phone', 'like', '%'.$where["filter"].'%')
                    ->where('role_id', $id)
                    ->orderBy('id', 'desc')
                    ->get();
            }
        } else {
            if ($count) {
                return static::where('role_id', $id)->orderBy('id', 'desc')->paginate($count);
            } else {
                return static::where('role_id', $id)->orderBy('id', 'desc')->get();
            }
        }
    }

    public static function getByID($id)
    {
        return static::where('id', $id)->orderBy('id', 'desc')->firstOrFail();
    }

    public static function ordersAgents($count = false, $where = false)
    {
        if (!empty($where["date"])) {
            if ($count) {
                return static::where('role_id', 4)
                    ->leftJoin('orders', 'users.id', '=', 'orders.manager_id')
                    ->where('pay', 1)
                    ->whereIn('orders.created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orderBy('id', 'desc')
                    ->paginate($count);
            } else {
                return static::where('role_id', 4)
                    ->leftJoin('orders', 'users.id', '=', 'orders.manager_id')
                    ->where('pay', 1)
                    ->whereIn('orders.created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orderBy('id', 'desc')
                    ->get();
            }
        } else {
            if ($count) {
                return static::where('role_id', 4)
                    ->leftJoin('orders', 'users.id', '=', 'orders.manager_id')
                    ->where('pay', 1)
                    ->orderBy('id', 'desc')
                    ->paginate($count);
            } else {
                return static::where('role_id', 4)
                    ->leftJoin('orders', 'users.id', '=', 'orders.manager_id')
                    ->where('pay', 1)
                    ->orderBy('id', 'desc')
                    ->get();
            }
        }
    }

    public function AutoUsers()
    {
        return $this->belongsToMany('App\AutoUser', 'list_auto_users')->orderBy('id', 'desc');
    }

    public function PeronsUser()
    {
        return $this->belongsToMany('App\PeronsUser', 'list_persons_users')->orderBy('id', 'desc');
    }

    public function Order()
    {
        return $this->belongsToMany('App\Order', 'list_orders_users')->orderBy('id', 'desc');
    }

    public function OrderAgents()
    {
        return $this->hasMany('App\Order', 'manager_id', 'id')->where('pay', 1);
    }

    public function OrdersAgentsInsurance($insurance_id)
    {
        return $this->hasMany('App\Order', 'manager_id', 'id')->where('pay', 1)->where('id_allinsuranses', $insurance_id);
    }

    public function CommissionsCompany()
    {
        /*return $this->belongsToMany('App\Allinsurance', 'regions_insurances')->withPivot([
            'commission_region',
            'type_commission_region',
            'region_id'
        ])->orderBy('id', 'desc');*/
        return $this->hasMany('App\Commission', 'user_id', 'id')->where('commissions.delete', '=', 0)->orderBy('id', 'desc');
    }
}
