<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsagoListMark extends Model
{
    protected $fillable = ['id_mark', 'value'];
    public static function getAllMark()
    {
        return static::orderBy('value', 'asc')->get();
    }
    public static function getByID($id)
    {
        return static::where('id_mark', $id)->first();
    }
}
