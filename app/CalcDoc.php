<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CalcDoc extends Model
{
    protected $fillable = [
        "id_buyer",
        "id_seller",
        "pay",
        "doc_url",
        "AUTO_MAKE",
        "FIRST_MODEL",
        "AUTO_TYPE",
        "AUTO_NUMBER",
        "AUTO_VIN",
        "AUTO_CREATOR_COUNTRY",
        "AUTO_CATEGORY",
        "AUTO_YEAR",
        "AUTO_CHASSIS_NUMBER",
        "AUTO_BODY_NUMBER",
        "AUTO_COLOR",
        "AUTO_POWER",
        "AUTO_ECO_CLASS",
        "AUTO_DOCUMENT",
        "AUTO_STS_SERIAL",
        "AUTO_STS_NUMBER",
        "AUTO_STS_DATE",
        "DOC_ADDRESS",
        "DOC_DATE",
        "DOC_USER_MAIL",
        "DOC_USER_PHONE"
    ];
}
