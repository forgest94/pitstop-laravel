<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Review extends Model
{
    protected $fillable = ['name', 'city', 'rating', 'body', 'mail'];
    public static function getListReview($count=false, $sort=false, $where=false)
    {
        if($count){
            if($where){
                if($sort["date"]){
                    return static::where('active', 1)->where($where[0], $where[1])->orderBy('created_at', $sort["date"])->paginate($count);
                }else if($sort["rating"]){
                    return static::where('active', 1)->where($where[0], $where[1])->orderBy('rating',$sort["rating"])->paginate($count);
                }else{
                    return static::where('active', 1)->where($where[0], $where[1])->orderBy('created_at','desc')->paginate($count);
                }
            }else{
                if($sort["date"]){
                    return static::where('active', 1)->orderBy('created_at', $sort["date"])->paginate($count);
                }else if($sort["rating"]){
                    return static::where('active', 1)->orderBy('rating',$sort["rating"])->paginate($count);
                }else{
                    return static::where('active', 1)->orderBy('created_at','desc')->paginate($count);
                }
            } 
        }else{
            if($where){
                if($sort["date"]){
                    return static::where('active', 1)->where($where[0], $where[1])->orderBy('created_at', $sort["date"])->get();
                }else if($sort["rating"]){
                    return static::where('active', 1)->where($where[0], $where[1])->orderBy('rating',$sort["rating"])->get();
                }else{
                    return static::where('active', 1)->where($where[0], $where[1])->orderBy('created_at','desc')->get();
                }
            }else{
                if($sort["date"]){
                    return static::where('active', 1)->orderBy('created_at', $sort["date"])->get();
                }else if($sort["rating"]){
                    return static::where('active', 1)->orderBy('rating',$sort["rating"])->get();
                }else{
                    return static::where('active', 1)->orderBy('created_at','desc')->get();
                }
            }   
        }
    }
}
