<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RegionsInsurance extends Model
{
    protected $fillable = [
        "region_id",
        "commission_region",
        "type_commission_region",
        "user_id"
    ];


    public static function isAttach($region, $insurance, $user)
    {
        return static::where('region_id', $region)->where('allinsurance_id', $insurance)->where('user_id', $user)->orderBy('id', 'desc')->first();
    }

}
