<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Region extends Model
{
    public static function getList($count = false, $where = false)
    {
        if($count){
            return static::orderBy('id', 'desc')->paginate($count);
        }else{
            return static::orderBy('id', 'desc')->get();
        }
    }
}
