<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Advantage extends Model
{
    public static function getList(Type $var = null)
    {
        return static::where('active', 1)->orderBy('sort','asc')->get();
    }
}
