<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Allinsurance extends Model
{
    public static function getByID($id)
    {
        return static::where('id', $id)->orderBy('id', 'desc')->first();
    }
    public static function getAll($count)
    {
        return static::orderBy('sort', 'asc')->paginate($count);
    }
    public static function getAllInsurance($slug=false)
    {
        if($slug){
            return static::where('active', 1)->where('slug', $slug)->orderBy('sort', 'asc')->first();
        }else{
            return static::where('active', 1)->orderBy('sort', 'asc')->get();
        }
    }

    public function RegionsInsurance()
    {
        return $this->belongsToMany('App\Region', 'regions_insurances')->withPivot([
            'commission_region',
            'type_commission_region',
        ]);
    }
}
