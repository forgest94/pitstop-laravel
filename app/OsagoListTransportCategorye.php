<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OsagoListTransportCategorye extends Model
{
    protected $fillable = ['id_category', 'name_category'];
    public static function getListTC($category = false, $where = false, $img = false)
    {
        if($category){
            return static::where('id_category', $category)->where('active', 1)->orderBy('id_category', 'asc')->get();
        }elseif($where){
            return static::whereRaw($where)->orderBy('id_category', 'asc')->get();
        }elseif($img){
            return static::orderBy('id_category', 'asc')->where('active', 1)->where('img', '!=', '')->get();
        }else{
            return static::orderBy('id_category', 'asc')->where('active', 1)->get();
        }
    }
}
