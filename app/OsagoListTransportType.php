<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OsagoListTransportType extends Model
{
    protected $fillable = ['id_category', 'id_type', 'name_type'];
    public static function getListLTT($categoryId = false, $where = false)
    {
        if($categoryId){
            return static::where('id_category', $categoryId)->orderBy('name_type', 'asc')->get();
        }elseif($where){
            return static::where($where[0], $where[1])->orderBy('name_type', 'asc')->get();
        }else{
            return static::orderBy('name_type', 'asc')->get();
        }
    }
}
