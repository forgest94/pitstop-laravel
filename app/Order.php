<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $fillable = [
        'id_allinsuranses',
        'id_auto',
        'id_persons',
        'id_user',
        'pay',
        'RegionOfUse',
        'CityOfUse',
        'DriversRestriction',
        'LastName_Owner',
        'FirstName_Owner',
        'MiddleName_Owner',
        'BirthDate_Owner',
        'Number_pass',
        'Seria_pass',
        'LastName_Insurer',
        'FirstName_Insurer',
        'MiddleName_Insurer',
        'BirthDate_Insurer',
        'Number_pas_Insurer',
        'Seria_pas_Insurer',
        'Date_pass_Insurer',
        'Kod_doc_pass_Insurer',
        'For_doc_pass_Insurer',
        'Address_doc_pass_Insurer',
        'Apartment_doc_pass_Insurer',
        'date_start',
        'polis_number',
        'region_insurer',
        'city_insurer',
        'street_insurer',
        'home_insurer',
        'building_insurer',
        'price',
        'phone',
        'mail',
        'kv',
        'manager_id',
        'commission_id',
        'price_commission'
    ];

    public static function findOrderPolice($id_police)
    {
        return static::where('polis_number', $id_police)->orderBy('created_at', 'desc')->firstOrFail();
    }

    public static function getListBuy($count = false, $where = false)
    {
        if (!empty($where["date"])) {

            if ($count) {
                return static::where('pay', 1)
                    ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                    ->orderBy('id', 'desc')->paginate($count);
            } else {
                return static::where('pay', 1)
                    ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                    ->orderBy('id', 'desc')->get();
            }
        } else {
            if ($count) {
                return static::where('pay', 1)->orderBy('id', 'desc')->paginate($count);
            } else {
                return static::where('pay', 1)->orderBy('id', 'desc')->get();
            }
        }
    }

    public static function sumOrderAgent($id, $where = false)
    {
        if (!empty($where["date"])) {
            return static::where('pay', 1)
                ->where('manager_id', $id)
                ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                ->orderBy('id', 'desc')->sum('price_commission');
        } else {
            return static::where('pay', 1)->where('manager_id', $id)->orderBy('id', 'desc')->sum('price_commission');
        }
    }

    public static function getListBuyAgent($count = false, $where = false, $agent_id)
    {
        if (!empty($where["date"])) {
            if ($count) {
                return static::where('pay', 1)
                    ->where('manager_id', $agent_id)
                    ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                    ->orderBy('id', 'desc')->paginate($count);
            } else {
                return static::where('pay', 1)
                    ->where('manager_id', $agent_id)
                    ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                    ->orderBy('id', 'desc')->get();
            }
        } else {
            if ($count) {
                return static::where('pay', 1)->where('manager_id', $agent_id)->orderBy('id', 'desc')->paginate($count);
            } else {
                return static::where('pay', 1)->where('manager_id', $agent_id)->orderBy('id', 'desc')->get();
            }
        }
    }

    public static function getListAll($count = false, $where = false)
    {
        if ($count) {
            return static::orderBy('id', 'desc')->paginate($count);
        } else {
            return static::orderBy('id', 'desc')->get();
        }
    }

    public static function sumPriceOrders($where = false)
    {
        if (!empty($where["date"])) {
            return static::where('pay', 1)
                ->where('manager_id', 1)
                ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                ->orderBy('created_at', 'desc')->sum('price_commission');
        } else {
            return static::where('pay', 1)->where('manager_id', 1)->orderBy('created_at', 'desc')->sum('price_commission');
        }
    }

    public static function sumPriceOrdersAgents($where = false)
    {
        if (!empty($where["date"])) {
            return static::where('pay', 1)
                ->where('manager_id', '!=', 1)
                ->where('manager_id', '!=', false)
                ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                ->orderBy('created_at', 'desc')->sum('price_commission');
        } else {
            return static::where('pay', 1)->where('manager_id', '!=', 1)->where('manager_id', '!=', false)->orderBy('created_at', 'desc')->sum('price_commission');
        }
    }

    public static function sumIncomeOrdersAgent($where = false, $agent_id)
    {
        if (!empty($where["date"])) {
            return static::where('pay', 1)
                ->where('manager_id', $agent_id)
                ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                ->orderBy('created_at', 'desc')->sum('price_commission');
        } else {
            return static::where('pay', 1)->where('manager_id', $agent_id)->orderBy('created_at', 'desc')->sum('price_commission');
        }
    }

    public static function sumPriceOrdersAgent($where = false, $agent_id)
    {
        if (!empty($where["date"])) {
            return static::where('pay', 1)
                ->where('manager_id', $agent_id)
                ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                ->orderBy('created_at', 'desc')->sum('price');
        } else {
            return static::where('pay', 1)->where('manager_id', $agent_id)->orderBy('created_at', 'desc')->sum('price');
        }
    }

    public static function sumPayOrders($where = false)
    {
        if (!empty($where["date"])) {
            return static::where('pay', 1)
                ->whereBetween('created_at', [$where["date"]["start"], $where["date"]["end"]])
                    ->orWhereDate('created_at' , $where["date"]["start"])
                    ->orWhereDate('created_at' , $where["date"]["end"])
                ->orderBy('created_at', 'desc')->count();
        } else {
            return static::where('pay', 1)->orderBy('created_at', 'desc')->count();
        }
    }

    /*public function Agents()
    {
        return $this->hasMany('App\User', 'id', 'manager_id');
    }*/

    public function Agent()
    {
        return $this->hasMany('App\User', 'id', 'manager_id');
    }

    public function PeronsOrder()
    {
        return $this->belongsToMany('App\PeronsOrder', 'list_persons_orders');
    }

    public function AutoOrder()
    {
        return $this->hasMany('App\AutoOrder', 'id', 'id_auto');
    }

    public function Allinsurance()
    {
        return $this->hasMany('App\Allinsurance', 'id', 'id_allinsuranses');
    }


    public function CrossOrder()
    {
        return $this->belongsToMany('App\Cross', 'orders_cross');
    }

    public function UserOrder()
    {
        return $this->belongsToMany('App\User', 'list_orders_users')->orderBy('id', 'desc');
    }
}
