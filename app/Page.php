<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public static function ViewBySlug($slug)
    {
      return static::where('slug',$slug)->where('status', 1)->firstOrFail();
    }

    public function city()
    {
        return $this->belongsToMany('App\City', 'cities_pages');
    }
}
