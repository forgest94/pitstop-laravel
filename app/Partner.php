<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Partner extends Model
{
    public static function getListPartner($count=false)
    {
        if($count){
            return static::where('active', 1)->where('img', '<>', '')->orderBy('sort','asc')->paginate($count);
        }else{
            return static::where('active', 1)->where('img', '<>', '')->orderBy('sort','asc')->get();
        }
    }
}
