<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*
Route::group(['prefix' => 'calculation'], function () {
    Route::post('/calcInStrah', 'CalcController@sectionStrah');
    Route::post('/sendCodeCalc', 'CalcController@sendSmsCode');
    Route::post('/onOrderCalc', 'CalcController@onOrder');
    Route::post('/calc', 'CalcController@AllUpPrice');
    Route::post('/calcUnPay', 'CalcController@sendCalc');
    Route::post('/todoc', 'CalcController@calc_doc');
    Route::post('/isCodePhone', 'CalcController@isCodePhone');
    Route::post('/checkCross', 'CalcController@CrossOsagoFunctions');
});*/
