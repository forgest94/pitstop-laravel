<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
/opt/php73/bin/php /var/www/u0821920/data/php-bin/composer install
*/

use Illuminate\Support\Facades\Cache;

Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Cache::flush();

    return "Кэш очищен.";
});


Route::get('/osago', 'EosagoController@headerSoap');

Route::get('/csv-parse', 'ParseMarksCsv@markParse'); // /opt/php73/bin/php artisan route:call /csv-parse


Route::group(['prefix' => 'alfa'], function () {
    Route::get('/getUPID', 'Api\Alfastrah\PartnersInteraction@getUPID');
    Route::get('/listmark/{type}', 'Api\Alfastrah\OSAGOlists@listMark');
    Route::get('/listModel/{type}/{markId?}', 'Api\Alfastrah\OSAGOlists@listModel');
    Route::get('/listTransportCategory/{type}/{category?}', 'Api\Alfastrah\OSAGOlists@listTransportCategory');
    Route::get('/listTransportPurpose/{type}/{purposeId?}', 'Api\Alfastrah\OSAGOlists@listTransportPurpose');
    Route::get('/listTransportType/{type}/{categoryId?}', 'Api\Alfastrah\OSAGOlists@listTransportType');
    Route::post('/loadDriver', 'Api\Alfastrah\EOsagoRelaunch@loadDriver');
    Route::post('/loadInsurerOwner', 'Api\Alfastrah\EOsagoRelaunch@loadInsurerOwner');
    Route::post('/loadVehicle', 'Api\Alfastrah\EOsagoRelaunch@loadVehicle');
    Route::get('/CalcEOsago', 'Api\Alfastrah\OsagoCalcRelaunch@CalcEOsago');
    Route::post('/userPartner', 'Api\Alfastrah\EosagoController@userPartner');
    //Route::get('/infoPay', 'Api\Alfastrah\EOsagoRelaunch@loadContractEosago');

    Route::get('/pay', 'Api\Alfastrah\MerchantServices@pay');
    Route::get('/infoPay/{slug}/{contract}', 'Api\Alfastrah\MerchantServices@infoPayPage');

    /*
    • Если партнер использует платежную систему АС, то вызвать сервис оплаты MerchantServices
    • Если партнер использует свою платежную систему, то после оплаты клиентом вызвать
    PartnersCallbackService
    ---------------------------------------------------------------------------------------
    • Метод GetContractSigned сервиса GetContractSigned при необходимости выгрузить полис
    клиента по переданному UPID
    • После успешной оплаты ЕОСАГО, если партнер использует допродажи КАСКО 5/10, то
    вызвать сервис additionalKasko для получения идентификаторов Каско
    • Для получения киентом образца печатных форм Каско вызвать метод GetContractSigned
    сервиса GetContractSigned по UPID и идентификатору Каско
    • Для оплаты КАСКО 5/10 вызвать сервис оплаты MerchantServices
    */
}
);

Route::group(['prefix' => 'docs'], function () {
    Route::get('/order/{id}', 'DocsController@orderDocs');
    Route::get('/all_orders', 'DocsController@allOrdersDocs');
});

Route::group(['prefix' => 'ingos'], function () {
    Route::get('/login', 'Api\Ingostrah\IngostrahAllMethod@Login');
    Route::get('/dicti', 'Api\Ingostrah\IngostrahAllMethod@GetDicti');
    Route::get('/tariff', 'Api\Ingostrah\IngostrahAllMethod@GetTariffMin');
}
);

Route::group(['prefix' => 'makc'], function () {
    Route::get('/guides', 'Api\Makc\MakcController@guides');
}
);

Route::group(['prefix' => 'avocod'], function () {
    Route::post('/grz', 'Api\Avtocod\AvtocodController@getInfoGRZ');
    Route::post('/grz/vin', 'Api\Avtocod\AvtocodController@getInfoVIN');
    Route::get('/balance', 'Api\Avtocod\AvtocodController@getBalance');
}
);

Route::group(['prefix' => 'renins'], function () {
    Route::get('/marks', 'Api\Renins\ReninsController@marksInfo');
    Route::get('/infoPay', 'Api\Renins\ReninsController@infoPayPage');
    Route::get('/file', 'Api\Renins\ReninsController@docPdf');
}
);
Route::group(['prefix' => 'soglasie'], function () {
    Route::get('/kbm', 'Soglasie\SoglasieController@CalcProduct');
}
);
Route::group(['prefix' => 'auto'], function () {
    Route::get('/select_mark/{id_mark}', 'CarsController@getModels');
    Route::get('/select_model/{id_model}', 'CarsController@getGenerations');
    Route::get('/select_year/{id_model}/{id_generation}', 'CarsController@getModifications');
});

Route::group(['prefix' => 'ajax'], function () {
    Route::get('/select_mark', 'Api\Alfastrah\OSAGOlists@listModel');
    Route::post('/updateUser', 'HomeController@updateUser');
    Route::get('/addCars', 'AutoUserController@addCars');
    Route::post('/add_about', 'Controller@about_form');
    Route::post('/phone_form', 'Controller@phone_form');
    Route::post('/addReview', 'ReviewController@addReview');
    Route::post('/addQuestion', 'QuestionController@addQuestion');
    Route::post('/tokbm', 'Controller@calc_kbm');
    Route::post('/todiag', 'Controller@calc_diag');
    Route::get('/select_cat', 'Api\Alfastrah\OSAGOlists@listTransportType');
    Route::post('/editMoreHome', 'HomeController@editMoreHome');
    Route::post('/deleteMoreHome', 'HomeController@deleteMoreHome');
    Route::post('/addMoreHome', 'HomeController@addMoreHome');
    Route::get('/kladr', 'Controller@kladr');
}
);
Route::group(['prefix' => 'calculation'], function () {
    Route::post('/calcInStrah', 'CalcController@sectionStrah');
    Route::post('/sendCodeCalc', 'CalcController@sendSmsCode');
    Route::post('/onOrderCalc', 'CalcController@onOrder');
    Route::post('/calc', 'CalcController@AllUpPrice');
    Route::post('/calcUnPay', 'CalcController@sendCalc');
    Route::post('/todoc', 'CalcController@calc_doc');
    Route::post('/isCodePhone', 'CalcController@isCodePhone');
    Route::post('/checkCross', 'CalcController@CrossOsagoFunctions');
});
Route::group(['prefix' => 'registration'], function () {
    Route::post('/resetPassword', 'Auth\ResetPasswordController@sendReset');
    Route::post('/add', 'Auth\RegisterController@addUser');
    Route::post('/firstSend', 'Auth\RegisterController@registerFirstSend');
}
);
Route::group(['prefix' => 'reset'], function () {
    Route::post('/isPhone', 'Auth\ResetPasswordController@isPhone');
    Route::post('/isCode', 'Auth\ResetPasswordController@isCode');
    Route::post('/updatePassword', 'Auth\ResetPasswordController@sendReset');
}
);
Route::post('/verifild', 'VerifildsController@VirifildApp');
Route::get('/status_orders', 'UpdateStatusOrder@updateStatus'); // /opt/php73/bin/php artisan route:call /status_orders

Route::get('/police-please', 'PageController@policePlease');
Route::get('/police-please/{slug}', 'PageController@policePlease');
Route::get('/unDocPolice', 'DocsController@insurableDocs');
Route::get('/domPDF', 'Controller@domPDF');
Route::get('/mailSend', 'Controller@testMail');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/reconstruction', function () {
    return 'Сайт находится в разработке';
})->name('reconstruction');


/* Page */
Route::group(['prefix' => 'panel'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['prefix' => 'company'], function () {
        Route::get('/', 'HomeController@PageCompany')->name('panel.page.company');
        Route::post('/editActive', 'HomeController@editCompanyActive')->name('panel.company.editActive');
        Route::post('/commission_regions', 'HomeController@commissionCompany')->name('panel.company.commission');
    });
    Route::group(['prefix' => 'agents'], function () {
        Route::get('/', 'HomeController@PageAgents')->name('panel.page.agents');
        Route::get('/add', 'HomeController@PageAgentsAdd')->name('panel.agents.add');
        Route::post('/edit', 'HomeController@AgentsEdit')->name('panel.agents.edit');
        Route::post('/add', 'HomeController@PageAgentsAdd');
    });
    Route::group(['prefix' => 'payouts'], function () {
        Route::get('/', 'HomeController@PagePayouts')->name('panel.page.payouts');
        Route::get('/add', 'HomeController@PagePayoutsAdd')->name('panel.payouts.add');
        Route::post('/addPayouts', 'HomeController@PagePayoutsAdd')->name('panel.payouts.addForm');
        Route::post('/searchAgent', 'HomeController@PayoutsAgentSearch')->name('panel.payouts.searchAgent');
        Route::post('/editStatus', 'HomeController@PayoutsEditStatus')->name('panel.payouts.editStatus');
    });
});
Route::get('/test', 'PageController@test');
Route::get('/{slug}/{city?}', 'PageController@show');
Route::get('/', 'PageController@index');

