function createXMLHttp() {
    if (typeof XMLHttpRequest != "undefined") { // для браузеров аля Mozilla
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) { // для Internet Explorer (all versions) 
        var aVersions = [
            "MSXML2.XMLHttp.5.0",
            "MSXML2.XMLHttp.4.0",
            "MSXML2.XMLHttp.3.0",
            "MSXML2.XMLHttp",
            "Microsoft.XMLHttp"
        ];
        for (var i = 0; i < aVersions.length; i++) {
            try {
                var oXmlHttp = new ActiveXObject(aVersions[i]);
                return oXmlHttp;
            } catch (oError) {}
        }
        throw new Error("Невозможно создать объект XMLHttp.");
    }
}
    // фукнция Автоматической упаковки формы любой сложности
function getRequestBody(oForm) {
    var aParams = new Array();
    for (var i = 0; i < oForm.elements.length; i++) {
        var sParam = encodeURIComponent(oForm.elements[i].name);
        sParam += "=";
        sParam += encodeURIComponent(oForm.elements[i].value);
        aParams.push(sParam);
    }
    return aParams.join("&");
}
function postAjax(url, oForm, callback, field=false) { 
    // создаем Объект
    var oXmlHttp = createXMLHttp();
    // получение данных с формы
      var sBody = getRequestBody(oForm);
    // подготовка, объявление заголовков
    oXmlHttp.open("POST", url, true);
    oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    // описание функции, которая будет вызвана, когда придет ответ от сервера
    oXmlHttp.onreadystatechange = function() {
        if (oXmlHttp.readyState == 4) {
            if (oXmlHttp.status == 200) {
                callback(oXmlHttp.responseText);
            } else {
                callback('error' + oXmlHttp.statusText);
            }
        }
    };
    // отправка запроса, sBody - строка данных с формы
    oXmlHttp.send(sBody);
  }
function personalUpdate(d) {
    if(d){
        JSON.parse(d, function(k, v) {
            if(k == 'error'){
                document.querySelectorAll('input[name="'+v+'"]')[0].classList.add("error");
                document.getElementById('mess_good').innerHTML = '';
            }else if(k == 'good'){
                document.getElementById('password_confirmation').value = '';
                document.getElementById('password').value = '';
                document.getElementById('mess_good').innerHTML = 'Данные успешно изменены.';
                var elems_inp = document.querySelectorAll('input');
                    elems_inp.forEach(element => {
                        element.classList.remove("error");
                    });
            }
        });
    }
}
function about_form(d) {
    if(d){
        JSON.parse(d, function(k, v) {
            if(v == true){
                var elems_inp = document.querySelectorAll('input.inp_ab');
                    elems_inp.forEach(element => {
                        if(element.getAttribute('name') != 'nda')
                        element.value = '';
                    });
                    document.getElementById('comment').innerHTML = '';
                    document.getElementById('mess_good').innerHTML = 'Сообщение отправленно.';
            }
        });
    }
}

var click = document.querySelector('[data-click]');
var select = document.querySelector('[data-select]');
// Нужно поймать событие "e"
click.addEventListener('click', function(e) {
    var fun = this.getAttribute('data-click'), $ths = this;
    switch(fun){
        case 'personal_update':
            var elems_inp = $ths.closest("form").querySelectorAll('input'), errors = '';
            elems_inp.forEach(element => {
                if(element.classList.contains('req') && element.value == ''){
                    element.classList.add("error");
                    errors = 'Y';
                }else if(element.getAttribute('name') == 'current-password'){
                    var confirm = document.getElementById('password_confirmation');
                    var new_pass = document.getElementById('password');
                    if(element.value != ''){
                        if((confirm.value == '' || confirm.value != new_pass.value || new_pass.value.length < 6) && new_pass.value != ''){
                            new_pass.classList.add("error");
                            confirm.classList.add("error");
                            errors = 'Y';
                        }else{
                            new_pass.classList.remove("error");
                            confirm.classList.remove("error");
                        }
                    }else{
                        new_pass.classList.remove("error");
                        confirm.classList.remove("error");
                    }
                }else if(element.getAttribute('name') != 'password_confirmation' && element.getAttribute('name') != 'password'){
                    element.classList.remove("error");
                }
            });
            if(!errors){
                postAjax('/updateUser', $ths.closest("form"), personalUpdate);
            }else{
                document.getElementById('mess_good').innerHTML = '';
            }
        break;
        case 'form_about':
            var elems_inp = $ths.closest("form").querySelectorAll('input'), errors = '';
            elems_inp.forEach(element => {
                if (element.classList.contains('req') && (element.value == '' || (element.getAttribute('name') == 'nda' && !element.checked)) ){
                    if(element.getAttribute('name') == 'nda'){
                        element.closest('div').querySelectorAll('label.form-block__nda')[0].classList.add("error");
                    }else{
                        element.classList.add("error");
                    }
                    errors = 'Y';
                }else{
                    if(element.getAttribute('name') == 'nda'){
                        element.closest('div').querySelectorAll('label.form-block__nda')[0].classList.remove("error");
                    }else{
                        element.classList.remove("error");
                    }
                }
            });
            if(!errors){
                postAjax('/add_about', $ths.closest("form"), about_form)
            }else{
                document.getElementById('mess_good').innerHTML = '';
            }
        break;
    }
});