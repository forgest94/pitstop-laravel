function getRequestBody(oForm) {
    var aParams = new Array();
    for (var i = 0; i < oForm.elements.length; i++) {
        var sParam = encodeURIComponent(oForm.elements[i].name);
        sParam += "=";
        sParam += encodeURIComponent(oForm.elements[i].value);
        aParams.push(sParam);
    }
    return aParams.join("&");
}
function personalUpdate(d) {
    if(d.error){
        document.querySelectorAll('input[name="'+d.error+'"]')[0].classList.add("error");
        document.getElementById('mess_good').innerHTML = '';
    }else if(d.good){
        document.getElementById('password_confirmation').value = '';
        document.getElementById('password').value = '';
        document.getElementById('mess_good').innerHTML = 'Данные успешно изменены.';
        var elems_inp = document.querySelectorAll('input');
            elems_inp.forEach(function(element, i, arr) {
                element.classList.remove("error");
            });
    }
}
function about_form(d) {
        if(d.success == true){
            var elems_inp = document.querySelectorAll('input.inp_ab');
                elems_inp.forEach(function(element, i, arr) {
                    if(element.getAttribute('name') != 'nda')
                    element.value = '';
                });
                document.getElementById('comment').innerHTML = '';
                document.getElementById('mess_good').innerHTML = 'Сообщение отправленно.';
        }
}

function status(response) {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
    } else {
        return Promise.reject(new Error(response.statusText))
    }
}

function json(response) {
    return response.json()
}
function findOption(select) {
    const option = select.querySelector(`option[value="${select.value}"]`)
    return option.value;
 }
new Vue({
    el: '#allPage',
    data: '',
    // определяйте методы в объекте `methods`
    methods: {  
        /*cars_add: function (event) {
            var url = '/ingos/login';
            fetch(url, {
                method: 'get',
                headers: {
                  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                }/*,
                body: 'foo=bar&lorem=ipsum'
              })
              .then(json)
              .then(function (data) {
                console.log(data);
              })
              .catch(function (error) {
                console.log('Request failed', error);
              });
        },*/
        personal_update: function (event) {
            var $ths = event.path[0];
            var form = event.path[1];
            var elems_inp = form.querySelectorAll('input'), errors = '';
            elems_inp.forEach(function(element, i, arr) {
                if(element.classList.contains('req') && element.value == ''){
                    element.classList.add("error");
                    errors = 'Y';
                }else if(element.getAttribute('name') == 'current-password'){
                    var confirm = document.getElementById('password_confirmation');
                    var new_pass = document.getElementById('password');
                    if(element.value != ''){
                        if((confirm.value == '' || confirm.value != new_pass.value || new_pass.value.length < 6) && new_pass.value != ''){
                            new_pass.classList.add("error");
                            confirm.classList.add("error");
                            errors = 'Y';
                        }else{
                            new_pass.classList.remove("error");
                            confirm.classList.remove("error");
                        }
                    }else{
                        new_pass.classList.remove("error");
                        confirm.classList.remove("error");
                    }
                }else if(element.getAttribute('name') != 'password_confirmation' && element.getAttribute('name') != 'password'){
                    element.classList.remove("error");
                }
            });
            if(!errors){
                var sBody = getRequestBody(form);
                /*postAjax('/updateUser', $ths.closest("form"), personalUpdate);*/
                var url = '/updateUser';
                fetch(url, {
                    method: 'post',
                    headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: sBody
                })
                .then(json)
                .then(function (data) {
                    personalUpdate(data);
                })
                .catch(function (error) {
                    console.log('Request failed', error);
                });
            }else{
                document.getElementById('mess_good').innerHTML = '';
            }
        },
        form_about: function (event) {
            var $ths = event.path[0];
            var form = event.path[1];
            var elems_inp = form.querySelectorAll('input'), errors = '';
            elems_inp.forEach(function(element, i, arr) {
                if (element.classList.contains('req') && (element.value == '' || (element.getAttribute('name') == 'nda' && !element.checked)) ){
                    if(element.getAttribute('name') == 'nda'){
                        element.closest('div').querySelectorAll('label.form-block__nda')[0].classList.add("error");
                    }else{
                        element.classList.add("error");
                    }
                    errors = 'Y';
                }else{
                    if(element.getAttribute('name') == 'nda'){
                        element.closest('div').querySelectorAll('label.form-block__nda')[0].classList.remove("error");
                    }else{
                        element.classList.remove("error");
                    }
                }
            });
            if(!errors){
                var sBody = getRequestBody(form);
                var url = '/add_about';
                fetch(url, {
                    method: 'post',
                    headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: sBody
                })
                .then(json)
                .then(function (data) {
                    about_form(data);
                })
                .catch(function (error) {
                    console.log('Request failed', error);
                });
                /*postAjax('/add_about', $ths.closest("form"), about_form)*/
            }else{
                document.getElementById('mess_good').innerHTML = '';
            }
        },
        next_cars_save: function(event){
            var block = event.path[2], error = '', buttom = event.path[0];
            block.querySelectorAll('input.req').forEach(element => {
                if(element.value == ''){
                    error = 'Y';
                    element.classList.add('error');
                }else{
                    element.classList.remove('error');
                }
            });
            block.querySelectorAll('select.req').forEach(element => {
                if(findOption(element) == 0){
                    error = 'Y';
                    element.classList.add('error');
                }else{
                    element.classList.remove('error');
                }
            });
            if(error == 'Y'){
                event.preventDefault();
                buttom.setAttribute('data-type-screen', '');
                buttom.setAttribute('data-id-screen', '');
            }else{
                buttom.setAttribute('data-type-screen', buttom.getAttribute('data-ts'));
                buttom.setAttribute('data-id-screen', buttom.getAttribute('data-is'));
            }
        },
        addrev: function(event){
            event.preventDefault();
            console.log('s');
            /*var $ths = event.path[0];
            var form = event.path[1];
            
            var elems_inp = form.querySelectorAll('input'), errors = '';
            elems_inp.forEach(function(element, i, arr) {
                if (element.classList.contains('req') && (element.value == '' || (element.getAttribute('name') == 'nda' && !element.checked)) ){
                    if(element.getAttribute('name') == 'nda'){
                        element.closest('div').querySelectorAll('label.form-block__nda')[0].classList.add("error");
                    }else{
                        element.classList.add("error");
                    }
                    errors = 'Y';
                }else{
                    if(element.getAttribute('name') == 'nda'){
                        element.closest('div').querySelectorAll('label.form-block__nda')[0].classList.remove("error");
                    }else{
                        element.classList.remove("error");
                    }
                }
            });
            if(!errors){
                var sBody = getRequestBody(form);
                var url = '/addReview';
                fetch(url, {
                    method: 'post',
                    headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: sBody
                })
                .then(json)
                .then(function (data) {
                    about_form(data);
                })
                .catch(function (error) {
                    console.log('Request failed', error);
                });
                /*postAjax('/add_about', $ths.closest("form"), about_form)
            }else{
                document.getElementById('mess_good').innerHTML = '';
            }*/
        },
        save_cars: function(){

        },
    }
});