<?php
class AlfaSoapClient extends \SoapClient
{
    private $username;
    private $pass;
    public function __setAuth($username, $pass)
    {
        $this->username = $username;
        $this->password = $pass;
    }
    public function __soapCall($function_name, $arguments, $options=null, $input_headers=null, &$output_headers=null)
    {
        return parent::__soapCall($function_name, $arguments, $options, $this->generateWSSecurityHeader());
    }
    private function generatePasswordDigest()
    {
        $this->nonce = mt_rand();
        $this->timestamp = gmdate('Y-m-d\TH:i:s\Z');
        $packedNonce = pack('H*', $this->nonce);
        $packedTimestamp = pack('a*', $this->timestamp);
        $packedPassword = pack('a*', $this->password);
        $hash = sha1($packedNonce . $packedTimestamp . $packedPassword);
        $packedHash = pack('H*', $hash);
        return base64_encode($packedHash);
    }
    private function generateWSSecurityHeader()
    {
        $passDigest = $this->generatePasswordDigest();
        $xml = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
            <wsse:UsernameToken wsu:Id="UsernameToken-14">
                <wsse:Username>' . $this->username . '</wsse:Username>
                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">' . $passDigest . '</wsse:Password>
                <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">' . base64_encode(pack('H*', $this->nonce)) . '</wsse:Nonce>
                <wsu:Created>' . $this->timestamp . '</wsu:Created>
            </wsse:UsernameToken>
        </wsse:Security>';
        return new \SoapHeader('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
            'Security',
            new \SoapVar($xml, XSD_ANYXML), 
            true
        );
    }
}

$username = 'E_PARTNER';
$pass = 'ALFAE313';
$result = new AlfaSoapClient('https://b2b-test2.alfastrah.ru/cxf/partner/PartnersInteraction?wsdl', array('trace' => true,'exceptions' => 1,'cache_wsdl'=>WSDL_CACHE_NONE));
$result->__setAuth($username, $pass);
$result->__soapCall("getUPID", array('UPIDRequest'=> array('callerCode'=>'Дружба')));
//echo $result->__last_response;

$xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result->__last_response);
$xml = simplexml_load_string($xml);
$json = json_encode($xml);
$responseArray = json_decode($json,true);
print_r($responseArray);