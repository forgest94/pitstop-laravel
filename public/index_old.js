import {BasePage} from "../base-page";
import HeaderComponent from "../../components/header/header.js";
import PartnersComponent from "../../components/partners/partners.js";
import CalcComponent from "../../components/calc/calc-osago.js";
import AnswerComponent from "../../components/answer/answer.js";
import Tabs from "../partial/tabs.js";

import $ from "jquery/dist/jquery";
import "../vendor/izimodal";
import "select2/dist/js/select2.full.js";
import "../vendor/autosize.js";


class Index extends BasePage {
    init() {
        this.$element.find('.js-header').each((i, el) => {
            new HeaderComponent(el);
        });

        this.$element.find('.js-partners').each((i, el) => {
            new PartnersComponent(el);
        });

        this.$element.find('.js-calc').each((i, el) => {
            new CalcComponent(el);
        });

        this.$element.find('.js-answer').each((i, el) => {
            new AnswerComponent(el);
        });

        new Tabs();

        this.$element.find('.js-screen-link').on('click', (e) => {
            const $el = $(e.target);
            const typeScreen = $el.data('type-screen');
            const idScreen = $el.data('id-screen');
            if(typeScreen && idScreen){
                const $points = this.$element.find('.js-progressbar-point').filter('[data-type-screen=' + typeScreen + ']');
                const $items = this.$element.find('.js-progressbar-item').filter('[data-type-screen=' + typeScreen + ']');
                const $screens = this.$element.find('.js-screen').filter('[data-type-screen=' + typeScreen + ']');
                $screens.hide();
                $screens.filter('[data-id-screen=' + idScreen + ']').show();

                $points.removeClass('calc__progressbar-point_active');
                $points.filter('[data-id-screen=' + idScreen + ']').addClass('calc__progressbar-point_active');

                $items.removeClass('calc__progressbar-item_active');
                $items.filter('[data-id-screen=' + idScreen + ']').addClass('calc__progressbar-item_active');
            }
        });

        this.$element.find('.js-select').select2({
            minimumResultsForSearch: 10, //скрыть поиск по селекту
            theme: 'pit'
        });

        this.$element.find('.js-select').on("select2:select", function(e) { 
            var id_select = $(this).attr('id'), 
            ajax = $(this).data('ajaxs'),
            option = $(this).find('option:selected')[0],
            type = $(this).data('type'),
            id_elem = $(this).data('idelem'),
            type_i = $(this).data('typei');
            if(ajax == 'Y' && $(option).val() != ''){ 
                $.ajax({
                    type: "GET",
                    url: "/ajax/"+id_select,
                    data: {
                        'value': $(option).val(),
                        'type': type
                    },
                    dataType: 'json',
                    success: function(data){
                        if(id_elem && data && type_i){
                            switch(type_i){
                                case 'select':
                                    var options = '<option value="0">Выбрать</option>';
                                    data.forEach(element => {
                                        options+='<option value="'+element.id_model+'">'+element.value+'</option>';
                                    });
                                    $(document).find('#'+id_elem).html(options);
                                break;
                                case 'text':
                                    $(document).find('#'+id_elem).text(data);
                                break; 
                            }
                        }
                    }
                });
            }
        });
    }
}

const page = new Index();